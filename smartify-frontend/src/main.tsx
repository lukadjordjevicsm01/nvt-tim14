import ReactDOM from 'react-dom/client'
import './index.css'
import Navbar from "./components/navbar.tsx";
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Login from "./pages/auth/login.tsx";
import Registration from "./pages/auth/registration.tsx";
import AddDevice from "./pages/device/add-device.tsx"
import {createTheme, ThemeProvider} from "@mui/material";
import { CssBaseline } from '@mui/material/';
import {Toaster} from "react-hot-toast";
import Welcome from "./pages/welcome.tsx";
import RealEstateRegistrationForm from "./components/realestate/real-estate-registration-form.tsx";
import HomePage from "./pages/home-page.tsx";
import AmbientSensor from "./pages/device/home/ambient-sensor.tsx";
import Lamp from "./pages/device/outside/lamp.tsx";
import Gate from "./pages/device/outside/gate.tsx";
import SolarPanelSystem from "./pages/device/electroenergetic/solar-panel-system.tsx";
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend, BarElement,
} from 'chart.js';
import AirConditioning from "./pages/device/home/air-conditioning.tsx";
import HomeBattery from "./pages/device/electroenergetic/home_battery.tsx";
import VehicleCharger from "./pages/device/electroenergetic/vehicle-charger.tsx";
import Sprinkler from "./pages/device/outside/sprinkler.tsx";
import WashingMachine from "./pages/device/home/washing-machine.tsx";
import ElectricityUsage from "./pages/admin/electricity-usage.tsx";
import Shared from "./pages/shared.tsx";
import Unauthenticated from "./pages/auth/unauthenticated.tsx";
import Authenticated from "./pages/auth/authenticated.tsx";
import AuthenticateAdmin from "./pages/auth/authenticate-admin.tsx";
import AuthenticateUser from "./pages/auth/authenticate-user.tsx";

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    BarElement,
    Title,
    Tooltip,
    Legend,
    BarElement
);

const theme = createTheme({
    palette: {
        primary: {
            main: "#092147",
            light: "#174384",
            contrastText: "#F5F5F5"
        },
        secondary: {
            main: "#F5F5F5",
            contrastText: '#092147'
        },
        error: {
            main: "#DD3636"
        },
        success: {
            main: "#66FF77"
        },
        background: {
            default: "#F5F5F5",
        }
    },
    typography: {
        allVariants: {
            color: "#092147",
            fontFamily: "Poppins"
        },

    },
});

const router = createBrowserRouter([
    {
        path:"/",
        element: <Navbar/>,
        children: [
            {path:"/", element: <Welcome/>},
            {
                path:"/home",
                element:
                    <Authenticated>
                        <HomePage/>
                    </Authenticated>
            },
            {
                path:"/shared",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <Shared/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/addDevice/:realEstateId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <AddDevice/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/registerRealEstate",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <RealEstateRegistrationForm/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/ambientSensor/:ambientSensorId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <AmbientSensor/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/airConditioning/:airConditioningId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <AirConditioning/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/washingMachine/:washingMachineId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <WashingMachine/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/lamp/:lampId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <Lamp/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/gate/:gateId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <Gate/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/solarPanelSystem/:solarPanelSystemId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <SolarPanelSystem/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/homeBattery/:homeBatteryId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <HomeBattery/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/vehicleCharger/:vehicleChargerId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <VehicleCharger/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/sprinkler/:sprinklerId",
                element:
                    <Authenticated>
                        <AuthenticateUser>
                            <Sprinkler/>
                        </AuthenticateUser>
                    </Authenticated>
            },
            {
                path:"/electricityUsage",
                element:
                    <Authenticated>
                        <AuthenticateAdmin>
                            <ElectricityUsage/>
                        </AuthenticateAdmin>
                    </Authenticated>
            }
        ],
    },
    {
        path:"/login", element:
            <Unauthenticated>
                <Login/>
            </Unauthenticated>
    },
    {
        path:"/registration", element:
            <Unauthenticated>
                <Registration/>
            </Unauthenticated>
    },
])

ReactDOM.createRoot(document.getElementById('root')!).render(
    <ThemeProvider theme={theme} >
        <LocalizationProvider dateAdapter={AdapterDateFns}>

            <CssBaseline/>
            <RouterProvider router={router}/>
            <Toaster position="bottom-center"
                 toastOptions={{
                     success: {
                         style: {
                             background: theme.palette.success.main,
                         },
                     },
                     error: {
                         style: {
                             background: theme.palette.error.main,
                             color: "#F5F5F5"
                         },
                     },
                 }}
            />
        </LocalizationProvider>
    </ThemeProvider>
)
