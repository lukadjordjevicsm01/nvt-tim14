import {useNavigate} from "react-router-dom";
import {useState} from "react";
import * as yup from "yup";
import {useFormik} from "formik";
import {Button, IconButton, InputAdornment, TextField} from "@mui/material";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import "./login-registration-form.css";
import defaultPicture from '../../assets/logo-icon-dark.svg';
import api from "../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {getRole} from "../../util/token-utils.tsx";

export default function RegistrationForm() {
    const navigate = useNavigate();

    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);
    const handleClickShowPassword = () => setShowPassword(!showPassword);
    const handleClickShowConfirmPassword = () => setShowConfirmPassword(!showConfirmPassword);

    const [profilePicturePreview, setProfilePicturePreview] =
        useState(defaultPicture);

    const role = getRole();


    const handleLogin = () => {
        navigate('/login')
    }

    const validationSchema = yup.object({
        name: yup
            .string()
            .required('Name is required'),
        surname: yup
            .string()
            .required('Surname is required'),
        email: yup
            .string()
            .email('Enter a valid email')
            .required('Email is required'),
        password: yup
            .string()
            .min(8, 'Password should be of minimum 8 characters length')
            .matches(
                /^(?=.*[A-Za-z])(?=.*\d)/,
                'Password must contain numbers and letters'
            )
            .required('Password is required'),
        confirmPassword: yup
            .string()
            .oneOf([yup.ref('password'), undefined], 'Passwords must match')
            .required('Confirm Password is required'),
        profilePicture: yup
            .mixed()
            .test('fileRequired', 'Profile picture is required', (value) => {
                return value instanceof File || (typeof value === 'string' && value.length > 0);
            }),
    });

    const formik = useFormik({
        initialValues: {
            name: '',
            surname: '',
            email: '',
            password: '',
            confirmPassword: '',
            profilePicture: undefined
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const formData = new FormData();
            formData.append('name', values.name);
            formData.append('surname', values.surname);
            formData.append('email', values.email);
            formData.append('password', values.password);
            formData.append('confirmPassword', values.confirmPassword);
            // @ts-ignore
            formData.append('profilePicture', values.profilePicture);

            if (role !== null && role.includes("ROLE_SUPERADMIN"))
                api.post('user/register-admin', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                }).then(res => {
                    if (res.status === 200) {
                        toast.success("Admin successfully added.")
                    }
                }).catch((error) => {
                    toast.error(error.response.data.message);
                });
            else
                api.post('user/register', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                }).then(res => {
                    if (res.status === 200) {
                        toast.success("A verification email has been sent. Check your inbox.")
                    }
                }).catch((error) => {
                    toast.error(error.response.data.message);
                });
        }
    });

    return (
        <div id={"form-container"} >
            {role == null &&
                <div className={"headings"}>
                    <h2>Take the first step</h2>
                    <h1>Sign up</h1>
                </div>
            }

            {role != null && role.includes("ROLE_SUPERADMIN") &&
                <div className={"headings"}>
                    <h1>Admin registration</h1>
                </div>
            }

            <form onSubmit={formik.handleSubmit}>
                <TextField
                    fullWidth
                    id="name"
                    name="name"
                    label="Name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                />
                <TextField
                    fullWidth
                    id="surname"
                    name="surname"
                    label="Surname"
                    value={formik.values.surname}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.surname && Boolean(formik.errors.surname)}
                    helperText={formik.touched.surname && formik.errors.surname}
                />
                <TextField
                    fullWidth
                    id="email"
                    name="email"
                    label="Email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                />
                <TextField
                    fullWidth
                    id="password"
                    name="password"
                    label="Password"
                    type={showPassword ? 'text' : 'password'}
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    helperText={formik.touched.password && formik.errors.password}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                >
                                    {showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                        )
                    }}/>
                <TextField
                    fullWidth
                    id="confirmPassword"
                    name="confirmPassword"
                    label="Confirm password"
                    type={showConfirmPassword ? 'text' : 'password'}
                    value={formik.values.confirmPassword}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                    helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle confirm password visibility"
                                    onClick={handleClickShowConfirmPassword}
                                >
                                    {showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                        )
                    }}
                />

                <div id={"image-upload"}>
                    <img alt={'Not uploaded'} src={profilePicturePreview || formik.values.profilePicture} />

                    <Button variant='contained'
                            sx={{
                                fontSize: "16px",
                                textTransform: "capitalize",
                                maxWidth: '180px', maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                            }}
                            component='label' >
                        Upload picture
                        <input
                            name='profilePicture'
                            accept='image/*'
                            id='contained-button-file'
                            type='file'
                            hidden
                            onChange={(e) => {
                                // @ts-ignore
                                const selectedFile = e.target.files[0];
                                formik.setFieldValue('profilePicture', selectedFile);
                                setProfilePicturePreview(URL.createObjectURL(selectedFile));
                            }}
                        />
                    </Button>
                    {formik.touched.profilePicture && formik.errors.profilePicture && (
                        <p className="error-message">{formik.errors.profilePicture.toString()}</p>
                    )}
                </div>

                <Button
                    sx={{
                        fontSize: "20px",
                        textTransform: "capitalize"
                    }}
                    variant="contained" fullWidth type="submit">
                    Register
                </Button>
            </form>
            {role == null &&
                <div>
                    <p>Already have an account? <span onClick={handleLogin}>Login</span> </p>
                </div>
            }
        </div>
    )
}