import {useFormik} from "formik";
import {Autocomplete, Box, Button, FormControlLabel, Radio, RadioGroup, TextField} from "@mui/material";
import * as yup from "yup";
import "../auth/login-registration-form.css"
import api from "../../config/axios-config.tsx";
import {City, Country} from "../../model/real-estate-model.tsx";
import {useEffect, useState} from "react";
import {MapContainer, Marker, TileLayer, useMapEvents} from "react-leaflet";
import {LatLng, LatLngExpression} from "leaflet";
import axios from "axios";
import defaultPicture from '../../assets/logo-icon-dark.svg';
import toast from "react-hot-toast";
import {getUserId} from "../../util/token-utils.tsx";
import {useNavigate} from "react-router-dom";

export default function RealEstateRegistrationForm (){

    const navigate = useNavigate();

    const validationSchema = yup.object({
        name: yup
            .string()
            .required('Name is required'),
        floors: yup
            .number()
            .min(0, "Cannot have less then 0 floors")
            .required('Floors are required'),
        street: yup
            .string()
            .required('Street is required'),
        country: yup
            .object()
            .required('Country is required'),
        city: yup
            .object()
            .required('City is required'),
        realEstateType: yup
            .string()
            .required("Real estate type is required"),
        picture: yup
            .mixed()
            .test('fileRequired', 'Real estate picture is required', (value) => {
                return value instanceof File || (typeof value === 'string' && value.length > 0);
            }),
    });

    const formik = useFormik({
        initialValues: {
            name: '',
            floors: 0,
            street: '',
            country: null,
            city: null,
            realEstateType: 'APARTMENT',
            picture: undefined
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const formData = new FormData();

            formData.append('realEstateType', values.realEstateType);
            formData.append('realEstatePicture', values.picture); // Assuming values.picture is a File object
            formData.append('address.cityId', values.city.id);
            formData.append('address.longitude', position.lng.toString());
            formData.append('address.latitude', position.lat.toString());
            formData.append('address.streetName', values.street);
            formData.append('floors', values.floors.toString());
            formData.append('userId', getUserId().toString());
            formData.append('name', values.name);

            api.post<string>('realEstate/register', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data', // Important for sending FormData
                },
            })
                .then(res => {
                    if (res.status == 200){
                        toast.success("Successfully registered your real estate!");
                        navigate('/home');
                    }

                })
                .catch(error => {
                    toast.error(error.response.data.message);
                });
            console.log(values, position.lat, position.lng);
        },
    });

    const [picturePreview, setPicturePreview] =
        useState(defaultPicture);

    const [countries, setCountries] = useState<Country[]>([]);

    const [cities, setCities] = useState<City[]>([]);

    const handleCountryChange = (country: Country | null) => {
        setCities([]);
        formik.setFieldValue('city', null);
        formik.setFieldValue('country', country);
        if (country == null) return;
        api.get<City[]>(`realEstate/cities/${country.countryCode}`)
            .then(res => {
                if (res.status == 200)
                {
                    formik.setFieldValue('city', res.data[0]);
                    setCities(res.data);
                }
            })
            .catch(error => toast.error(error.response.data.message));
    }
    useEffect(() => {
        api.get<Country[]>('realEstate/countries')
            .then(res => {
                if (res.status == 200){
                    formik.setFieldValue('country', res.data[0]);
                    setCountries(res.data);
                }
            })
            .catch(error => toast.error(error.response.data.message))
    }, []);

    const [position, setPosition] =
        useState<LatLng>(new LatLng(51.505,-0.09));

    const handlePositionChange = (newPosition: LatLng) => {
        if (formik.values.city == null || formik.values.country == null){
            toast.error("Please select country and a city.");
            return;
        }
        reverseGeocode(newPosition);
    }

    const reverseGeocode = (newPosition: LatLng) : void => {
        axios
            .get(
                `https://nominatim.openstreetmap.org/reverse?format=json&lat=${newPosition.lat}&lon=${newPosition.lng}`
            )
            .then((response) => {
                const address = response.data.address;

                if (formik.values.country.countryCode.toLowerCase() !== address.country_code.toLowerCase()){
                    toast.error("Please click on a country you chose.")
                    return;
                }

                // if (address.city == undefined || (formik.values.country.countryCode.toLowerCase() + ";" + formik.values.city.name.toLowerCase()
                //     !== address.country_code.toLowerCase() + ";" + address.city.toLowerCase())){
                //     toast.error("Please click on a city you chose.")
                //     return;
                // }

                formik.setFieldValue('street', `${address.road} ${address.house_number}`)
                setPosition(newPosition);
            })
            .catch((error) => {
                console.error('Error fetching reverse geocoding data:', error);
            });
    };


    return(
        <>
            <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center", height: "100%"}}>
                <div id={"form-container"} style={{width: "50%", padding: "5%", height:"100%"}}>
                    <div className={"headings"}>
                        <h1>Register new real estate</h1>
                    </div>

                    <form onSubmit={formik.handleSubmit}>
                        <TextField
                            fullWidth
                            id="name"
                            name="name"
                            label="Name"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.name && Boolean(formik.errors.name)}
                            helperText={formik.touched.name && formik.errors.name}/>

                        <div style={{display: "flex", gap: "10px", justifyContent:"space-between"}} >
                            <RadioGroup
                                row
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                aria-labelledby="demo-controlled-radio-buttons-group"
                                name="realEstateType"
                                value={formik.values.realEstateType}
                            >
                                <FormControlLabel value="APARTMENT" control={<Radio />} label="Apartment" />
                                <FormControlLabel value="HOUSE" control={<Radio />} label="House" />
                            </RadioGroup>

                            <div id={"image-upload"}>
                                <img alt={'Not uploaded'} src={picturePreview || formik.values.picture} />

                                <Button variant='contained'
                                        sx={{
                                            fontSize: "16px",
                                            textTransform: "capitalize",
                                            maxWidth: '180px', maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                                        }}
                                        component='label' >
                                    Upload picture
                                    <input
                                        name='picture'
                                        accept='image/*'
                                        id='contained-button-file'
                                        type='file'
                                        hidden
                                        onChange={(e) => {
                                            const selectedFile = e.target.files[0];
                                            formik.setFieldValue('picture', selectedFile);
                                            setPicturePreview(URL.createObjectURL(selectedFile));
                                        }}
                                    />
                                </Button>
                                {formik.touched.picture && formik.errors.picture && (
                                    <p className="error-message">{formik.errors.picture.toString()}</p>
                                )}
                            </div>
                        </div>

                        <div style={{display: "flex", gap: "10px"}}>
                            <TextField
                                sx={{width: "75%"}}
                                disabled={true}
                                fullWidth
                                id="street"
                                name="street"
                                label="Street"
                                value={formik.values.street}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.street && Boolean(formik.errors.street)}
                                helperText={formik.touched.street && formik.errors.street}/>

                            <TextField
                                sx={{width: "25%"}}
                                fullWidth
                                id="floors"
                                name="floors"
                                label="Floors"
                                value={formik.values.floors}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.floors && Boolean(formik.errors.floors)}
                                helperText={formik.touched.floors && formik.errors.floors}/>
                        </div>

                        <Box
                            sx={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                gap: '10px'
                            }}
                        >
                            <Autocomplete
                                sx={{
                                    width: '50%'
                                }}
                                onBlur={formik.handleBlur}
                                id="country-select"
                                options={countries}
                                onChange={(__event, newCountry) => handleCountryChange(newCountry)}
                                autoHighlight
                                getOptionLabel={(option) => option.name}
                                renderOption={(props, option) => (
                                    <Box component="li" sx={{'& > img': {mr: 2, flexShrink: 0}}} {...props}>
                                        <img
                                            loading="lazy"
                                            width="20"
                                            srcSet={`https://flagcdn.com/w40/${option.countryCode.toLowerCase()}.png 2x`}
                                            src={`https://flagcdn.com/w20/${option.countryCode.toLowerCase()}.png`}
                                            alt=""/>
                                        {option.name} ({option.countryCode})
                                    </Box>
                                )}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        name="country"
                                        label="Choose a country"
                                        error={formik.touched.country && Boolean(formik.errors.country)}

                                        inputProps={{
                                            ...params.inputProps,
                                            autoComplete: 'new-password', // disable autocomplete and autofill
                                        }}/>
                                )}/>

                            <Autocomplete
                                value={formik.values.city}
                                onBlur={formik.handleBlur}
                                onChange={(__event, newCity) => {
                                    formik.setFieldValue('city', newCity);
                                    formik.setFieldValue('street', '');
                                }}
                                getOptionLabel={(option) => option.name} // Use the city name as the label
                                sx={{
                                    width: '50%'
                                }}
                                disablePortal
                                id="city-select"
                                options={cities}
                                renderInput={(params) => <TextField
                                    error={formik.touched.city && Boolean(formik.errors.city)}
                                    name="city"
                                    {...params} label="City"/>}/>
                        </Box>
                        <Button
                            sx={{
                                fontSize: "20px",
                                textTransform: "capitalize"
                            }}
                            variant="contained" fullWidth type="submit">
                            Register
                        </Button>
                    </form>
                </div>

                <MapContainer
                    center={[45.2454, 19.83191]}
                    zoom={13}
                    style={{ height: '100%', width: '50%' }}

                >
                    <TileLayer
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    />

                    <LocationMarker position={position} handlePositionChange={handlePositionChange}></LocationMarker>
                </MapContainer>
            </div>

        </>
    )

}

function LocationMarker({ position , handlePositionChange } : LocationMarkerProps) {

    useMapEvents({
        click(e) {
            handlePositionChange(e.latlng)
        },
    })

    return position === null ? null : (
        <Marker position={position}>
        </Marker>
    )
}

interface LocationMarkerProps {
    position: LatLngExpression;
    handlePositionChange: (newPosition: LatLng) => void;
}
