import {City} from "../../model/real-estate-model.tsx";
import {DataGrid, GridColDef, GridRowId} from "@mui/x-data-grid";
import React from "react";
import toast from "react-hot-toast";

export default function CitiesTable({cities, handleSelectedCity}: CitiesTableProps) {

    const columns: GridColDef[] = [
        {
            field: 'name',
            headerName: 'Name',
            width: 200,
            type: 'string',
            headerAlign: 'left',
            align: 'left',
        },
    ];

    const [selectionModel, setSelectionModel] = React.useState<GridRowId[]>([]);

    const handleSelectionChange = (newSelection: GridRowId[]) => {
        setSelectionModel(newSelection);
        if (newSelection.length == 1) {
            const selectedCity = cities.find(city => city.id === newSelection[0]);
            if (selectedCity) {
                handleSelectedCity(selectedCity);
            }
        } else {
            toast.error("Please select only one row.")
        }
    };

    return(
        <div style={{
            width: '100%',
            maxHeight: '90%',
            display: 'flex',
            flexDirection: 'column',
            gap: '5px'
        }}>
            <DataGrid
                getRowId={(row) => row.id}
                rows={cities}
                columns={columns}
                onRowSelectionModelChange={handleSelectionChange}
                rowSelectionModel={selectionModel}
                initialState={{
                    pagination: {
                        paginationModel: {page: 0, pageSize: 10},
                    },
                    sorting: {
                        sortModel: [{field: 'name', sort: 'asc'}]
                    }
                }}
                pageSizeOptions={[5, 10, 25]}
            />
        </div>
    )

}

interface CitiesTableProps {
    cities: City[],
    handleSelectedCity: (city: City) => void
}