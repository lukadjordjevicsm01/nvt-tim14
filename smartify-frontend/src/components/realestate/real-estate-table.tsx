import {RealEstate} from "../../model/real-estate-model.tsx";
import {DataGrid, GridColDef, GridRowId} from "@mui/x-data-grid";
import React from "react";
import toast from "react-hot-toast";

export default function RealEstateTable({realEstates, handleSelectedRealEstate}: RealEstateTableProps) {

    const columns: GridColDef[] = [
        {
            field: 'streetName',
            headerName: 'Address',
            width: 250,
            type: 'string',
            headerAlign: 'left',
            align: 'left',
            valueGetter: params => {
                return params.row.address.streetName;
            }
        },
        {
            field: 'name',
            headerName: 'Name',
            width: 200,
            type: 'string',
            headerAlign: 'left',
            align: 'left',
        },
        {
            field: 'email',
            headerName: 'Owner',
            width: 250,
            type: 'string',
            headerAlign: 'left',
            align: 'left',
            valueGetter: params => {
                return params.row.userDTO.email;
            }
        },
    ];

    const [selectionModel, setSelectionModel] = React.useState<GridRowId[]>([]);

    const handleSelectionChange = (newSelection: GridRowId[]) => {
        setSelectionModel(newSelection);
        if (newSelection.length == 1) {
            const selectedRealEstate = realEstates.find(realEstate => realEstate.id === newSelection[0]);
            if (selectedRealEstate) {
                handleSelectedRealEstate(selectedRealEstate);
            }
        } else {
            toast.error("Please select only one row.")
        }
    };

    return(
        <div style={{
            width: '100%',
            maxHeight: '90%',
            display: 'flex',
            flexDirection: 'column',
            gap: '5px'
        }}>
            <DataGrid
                getRowId={(row) => row.id}
                rows={realEstates}
                columns={columns}
                onRowSelectionModelChange={handleSelectionChange}
                rowSelectionModel={selectionModel}
                initialState={{
                    pagination: {
                        paginationModel: {page: 0, pageSize: 10},
                    },
                    sorting: {
                        sortModel: [{field: 'streetName', sort: 'asc'}]
                    }
                }}
                pageSizeOptions={[5, 10, 25]}
            />
        </div>
    )

}

interface RealEstateTableProps {
    realEstates: RealEstate[]
    handleSelectedRealEstate: (realEstate: RealEstate) => void
}