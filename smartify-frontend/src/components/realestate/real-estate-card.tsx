import {RealEstate, StatusChange} from "../../model/real-estate-model.tsx";
import {Box, Button, capitalize, Dialog, DialogContent, Switch, TextareaAutosize, Typography} from "@mui/material";
import React, {useEffect, useState} from "react";
import toast from "react-hot-toast";
import {useNavigate} from "react-router-dom";
import "./real-estate-overview.css";
import api from "../../config/axios-config.tsx";
import {imagesUrl} from "../../util/environment.ts";
import {Device} from "../../model/device-model.tsx";
import {getUserId, getUserMail} from "../../util/token-utils.tsx";
import Permissions from "../util/permissions.tsx";


export default function RealEstateCard ({realEstate, admin, handleStatusChange, shared} : RealEstateCardProps){

    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [permissionDialogOpen, setPermissionDialogOpen] = useState<boolean>(false);
    const [reason, setReason] = useState<string>("");

    const navigate = useNavigate();

    const [devices, setDevices] = useState([]);

    useEffect(() => {
        if (admin) return

        let url: string =
            shared ?
            `/device/getSharedDevicesByRealEstate/${realEstate.id}/${getUserId()}` :
            `/device/${realEstate.id}`;
        api
          .get<Device[]>(url)
          .then((res) => {
              let fetched_devices: Device[] = [];
              if (res.status === 200) {
                  fetched_devices = res.data.filter(device => device.deviceType !== "SolarPanel");
              }
              setDevices(fetched_devices)
          })
          .catch(() => {
              toast.error(`Error fetching devices for real estate ${realEstate.id}`);
              return { realEstateId: realEstate.id, devices: [] };
          })
    }, []);

    const handleReasonFieldOpen = () => {
        setDialogOpen(true);
        setReason("");

    };

    const handleReasonFieldClose = () => {
        setDialogOpen(false);
    };

    const handleStatusClick = (accept: boolean) => {
        if (!accept && reason.length < 5){
            toast.error("You must provide a reason with more than 5 characters");
            return;
        }
        setDialogOpen(false);
        const statusChange: StatusChange = {
            realEstateId: realEstate.id,
            reason: reason,
            status: accept ? "ACCEPTED" : "REJECTED"
        }
        handleStatusChange(statusChange);
    }


    const handleSwitch = (deviceId: number) => {
        api.post(`device/switch/${deviceId}`, {}, {
            params: {userEmail: getUserMail()}
        })
            .then(res => {
                if (res.status === 200) {
                    const updatedDevices = devices.map(device => {
                        if (device.id === deviceId) {
                            if (!device.on)
                                toast.success("Device turned on.");
                            else
                                toast.success("Device turned off.");
                            return { ...device, on: !device.on };
                        }
                        return device;
                    });
                    setDevices(updatedDevices);
                }
            })
            .catch(() => toast.error("Error. Device activity switch failed."));
    };

    return (
        <Box className={"real-estate-container"} sx={{
            backgroundColor: 'primary.light',
            color: 'background.default'
        }}>
            <Box sx={{
                display: 'flex',
                justifyContent: 'space-between'
            }}>
                <h2>{realEstate.name}</h2>
                {admin &&
                    <Box
                        sx={{
                            display: 'flex',
                            gap: '10px',
                        }}
                    >
                        <Button variant="contained" fullWidth sx={{textTransform: "capitalize"}}
                                onClick={() => handleStatusClick(true)}>Accept</Button>
                        <Button variant="contained" fullWidth sx={{textTransform: "capitalize"}}
                                onClick={handleReasonFieldOpen}>Reject</Button>

                        <Dialog open={dialogOpen} onClose={handleReasonFieldClose} fullWidth>
                            <DialogContent
                                sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    gap: '10px'
                                }}
                            >
                                <h3>{realEstate.name}</h3>
                                <TextareaAutosize
                                    style={{ width: '100%', fontFamily: 'inherit', fontSize: 'large' }}
                                    minRows={3}
                                    placeholder="Enter your reason for rejecting this real estate"
                                    value={reason}
                                    onChange={(e) => setReason(e.target.value)}
                                />
                                <Button
                                    variant="contained"
                                    onClick={() => handleStatusClick(false)}
                                    sx={{textTransform: "capitalize"}}
                                >
                                    apply</Button>
                            </DialogContent>
                        </Dialog>
                    </Box>
                }

                {!admin &&
                    <Box
                        sx={{
                            display: 'flex',
                            gap: '10px'
                        }}
                    >
                        {realEstate.status === 'ACCEPTED' &&
                            <Box
                                sx={{
                                    display: "flex",
                                    gap: "10px",
                                    width: "500px"
                                }}
                            >
                                {realEstate.userDTO.id === getUserId() &&
                                    <Button
                                        variant="contained"
                                        fullWidth
                                        onClick={() => navigate(`/addDevice/${realEstate.id}`)}
                                        sx={{textTransform: "capitalize"}}
                                    >
                                        Add new device
                                    </Button>
                                }
                                {realEstate.userDTO.id === getUserId() &&
                                    <Button
                                        variant="contained"
                                        fullWidth
                                        onClick={() => setPermissionDialogOpen(true)}
                                        sx={{textTransform: "capitalize"}}
                                    >
                                        Permissions
                                    </Button>
                                }
                                <Dialog open={permissionDialogOpen} onClose={() => setPermissionDialogOpen(false)} fullWidth>
                                    <DialogContent
                                        sx={{
                                            display: "flex",
                                            flexDirection: "column",
                                            gap: "10px"
                                        }}
                                    >
                                        <Permissions type="realEstate" entityId={realEstate.id}/>
                                    </DialogContent>
                                </Dialog>
                            </Box>
                        }
                    </Box>
                }
            </Box>

            <Typography sx={{color: "primary.contrastText"}}>Status: {capitalize(realEstate.status.toLowerCase())}</Typography>
            {admin || realEstate.userDTO.id != getUserId() &&
                <Typography sx={{color: "primary.contrastText"}}>Owner: { capitalize(realEstate.userDTO.name)} { capitalize(realEstate.userDTO.surname)}</Typography>
            }
            <Typography sx={{color: "primary.contrastText"}}>Address: {realEstate.address.streetName}</Typography>
            <Box component="li" sx={{'& > img': {mr: 2, flexShrink: 0}}}>
                <img
                    loading="lazy"
                    width="20"
                    srcSet={`https://flagcdn.com/w40/${realEstate.address.countryDTO.countryCode.toLowerCase()}.png 2x`}
                    src={`https://flagcdn.com/w20/${realEstate.address.countryDTO.countryCode.toLowerCase()}.png`}
                    alt=""/>
                {realEstate.address.countryDTO.name}, {realEstate.address.cityDTO.name}
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    gap: '10px',
                    flexWrap: 'wrap',
                    marginTop: '10px',
                    justifyContent: 'space-between'
                }}>
                {devices.map(device =>
                    <Box className={"device-card"}
                         key={device.id}
                         onClick={() => navigate(`/${device.deviceType}/${device.id}`)}
                        sx={{
                            color: 'primary.main'
                        }}>
                        <img className={"device-img"} src={`${imagesUrl}/device/${device.imagePath}`} alt={device.name}/>
                        <Box
                            sx={{
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}>
                            <h3>{device.name}</h3>
                            <Switch
                                checked={device.on}
                                onChange={() => handleSwitch(device.id)}
                                onClick={(e) => e.stopPropagation()}
                                inputProps={{ 'aria-label': 'controlled' }}/>
                        </Box>
                        <Typography >Consumption: {device.consumption} kWh</Typography>
                        {device.online &&
                            <Typography
                                sx={{
                                    color: 'success.main'
                                }}>
                                • Online
                            </Typography>}
                        {!device.online &&
                            <Typography
                                sx={{
                                    color: 'error.main'
                                }}>
                                • Offline
                            </Typography>}
                    </Box>
                )}
            </Box>
        </Box>
    )
}


interface RealEstateCardProps {
    realEstate: RealEstate,
    admin: boolean,
    handleStatusChange: (statusChange: StatusChange) => void,
    shared: boolean;
}