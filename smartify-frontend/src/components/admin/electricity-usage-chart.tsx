import {Bar} from "react-chartjs-2";

export default function ElectricityUsageChart({electricityUsage}) {

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top' as const,
            },
        },
        barPercentage: 0.4,
    };
    const labels = ['Electricity Usage (kWh)']
    const data = {
        labels,
        datasets: [
            {
                label: 'Consumed',
                data: [electricityUsage.consumed],
                backgroundColor: '#DD3636',
            },
            {
                label: 'Produced',
                data: [electricityUsage.produced],
                backgroundColor: '#174384',
            },
        ],
    };

    return (
        <Bar options={options} data={data} />
    )
}