import React, {useEffect, useState} from "react";
import {VehicleChargerRecord} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import DateTimeRangePicker from "../../util/date-time-range-picker.tsx";
import {DataGrid, GridColDef} from "@mui/x-data-grid";

export default function VehicleChargerTable({vehicleChargerId} : VehicleChargerTableProps) {
    const[vehicleChargerRecords, setVehicleChargerRecords]
        = useState<VehicleChargerRecord[]>([])

    useEffect(() => {
        getVehicleChargerData()
    }, []);

    const getVehicleChargerData = (from: string = "-1h", to: string = "now()") => {
        api.get<VehicleChargerRecord[]>
        (`/device/vehicleCharger/getVehicleChargerRecords/${vehicleChargerId}`,
            {
                params: {from: from, to: to}
            })
            .then(res => {
                if (res.status === 200) {
                    setVehicleChargerRecords(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const dateOptions: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false
    };

    const columns: GridColDef[] = [
        { field: 'message', headerName: 'Message', width: 300, sortable: false },
        { field: 'subject', headerName: 'Subject', width: 200, sortable: false },
        { field: 'vehicleChargerSocketName', headerName: 'Socket Name', width: 250, sortable: false },
        { field: 'chargingDuration', headerName: 'Charging Duration', width: 200, sortable: false },
        {
            field: 'timestamp',
            headerName: 'Time',
            type: 'Date',
            valueFormatter: params => new Date(params?.value).toLocaleString("ru-RU",
                dateOptions),
            width: 250
        },
    ];

    const getMeasurement = (from: string = "-1h", to: string = "now()") => {
        getVehicleChargerData(from, to)
    }

    return(
        <div style={{
            width: '80%',
            height: 'calc(100vh - 200px)',
            display: 'flex',
            flexDirection: 'column',
            gap: '5px'
        }}>
            <DateTimeRangePicker getMeasurement={getMeasurement} flag={"none"}/>
            <DataGrid
                getRowId={(row) => row.timestamp}
                rows={vehicleChargerRecords}
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: {page: 0, pageSize: 10},
                    },
                    sorting: {
                        sortModel: [{field: 'timestamp', sort: 'desc'}]
                    }
                }}
                pageSizeOptions={[5, 10, 25]}
            />
        </div>
    )



}

interface VehicleChargerTableProps {
    vehicleChargerId: number
}