import {Box, FormControlLabel, Radio, RadioGroup} from "@mui/material";
import DateTimeRangePicker from "../util/date-time-range-picker.tsx";
import {Bar} from "react-chartjs-2";
import React, {useEffect, useState} from "react";
import {AirConditioningRecord, DeviceAvailabilityRecord} from "../../model/device-model.tsx";
import api from "../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {getHours, getMinutes} from "date-fns";
import {number} from "yup";

export default function DeviceAvailabilityGraph ({deviceId}) {
    
    const [deviceAvailabilityRecords, setDeviceAvailabilityRecords] = useState<DeviceAvailabilityRecord[]>([]);
    
    const [measurement, setMeasurement] = useState<string>("time");
    
    const [fromTo, setFromTo]
        = useState<FromTo>({from: "-6h", to: "now()", aggregationWindow: "1h"})
    
    const getDeviceAvailabilityRecords = () => {
        api.get<DeviceAvailabilityRecord[]>(`/device/availabilityRecords/${deviceId}`,
                {
                    params: {from: fromTo.from, to: fromTo.to, aggregationWindow: fromTo.aggregationWindow}
                })
            .then(res => {
                if (res.status === 200) {
                    setDeviceAvailabilityRecords(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }
    
    // useEffect(() => {
    //     getDeviceAvailabilityRecords();
    // }, []);
    
    const handleFromToChange = (from: string = "-6h", to: string = "now()", aggregationWindow: string = "1h") => {
        setFromTo({from: from, to: to, aggregationWindow: aggregationWindow});
    }
    useEffect(() => {
        getDeviceAvailabilityRecords();
    }, [fromTo]);
    
    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top' as const,
            },
        },
        ticks: {
            autoSkip: true,
            maxTicksLimit: 15
        },
    };
    const [labels, setLabels] = useState<string[]>([])
    const [data , setData] = useState<{
        labels: string[],
        datasets: {
            label: string,
            data: number[],
            backgroundColor: string }[]
    }>({
        datasets: [],
        labels: []
    });
    
    const dateOptions: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
    };
    useEffect(() => {
        //update data and labels
        setLabels(deviceAvailabilityRecords.map(deviceAvailabilityRecord => {
            if (deviceAvailabilityRecord.unit === "minutes"){
                let start = getHours(new Date(deviceAvailabilityRecord.intervalStart)).toString();
                let end = getHours(new Date(deviceAvailabilityRecord.intervalEnd)).toString();
                start = start.length === 1 ? "0" + start : start;
                end = end.length === 1 ? "0" + end : end;

                let startMinutes = getMinutes(new Date(deviceAvailabilityRecord.intervalStart)).toString();
                let endMinutes = getMinutes(new Date(deviceAvailabilityRecord.intervalEnd)).toString();
                startMinutes = startMinutes.length === 1 ? "0" + startMinutes : startMinutes;
                endMinutes = endMinutes.length === 1 ? "0" + endMinutes : endMinutes;
                
                return `${start}:${startMinutes}-${end}:${endMinutes}`
            }
            
            return new Date(deviceAvailabilityRecord.intervalStart).toLocaleString("ru-RU", dateOptions);
        }));
        
    }, [deviceAvailabilityRecords]);
    
    useEffect(() => {
        if (measurement === "time"){
            createTimeData();
            return;
        }
        createPercentageData();
    }, [labels]);
    
    useEffect(() => {
        getDeviceAvailabilityRecords();
    }, [measurement]);
    
    const createTimeData = () => {
        const displayValuesOnline =  deviceAvailabilityRecords.map(record => +record.onlineTime.toFixed(2));
        const displayValuesOffline =  deviceAvailabilityRecords.map(record => +record.offlineTime.toFixed(2));
        const unit = deviceAvailabilityRecords[0] ? deviceAvailabilityRecords[0].unit : "";
        const newData = {
            labels: labels,
            datasets: [
                {
                    label: `Online (${unit})` ,
                    data: displayValuesOnline,
                    backgroundColor: "#66FF77"
                },
                {
                    label: `Offline (${unit})`,
                    data: displayValuesOffline,
                    backgroundColor: "#DD3636"
                }
            ]
        }
        setData(newData);
    }
    
    const createPercentageData = () => {
        const displayValuesOnline =  deviceAvailabilityRecords.map(record => +record.onlinePercentage.toFixed(2));
        const displayValuesOffline =  deviceAvailabilityRecords.map(record => +record.offlinePercentage.toFixed(2));
        const newData = {
            labels: labels,
            datasets: [
                {
                    label: `Online (%)` ,
                    data: displayValuesOnline,
                    backgroundColor: "#66FF77"
                },
                {
                    label: `Offline (%)`,
                    data: displayValuesOffline,
                    backgroundColor: "#DD3636"
                }
            ]
        }
        setData(newData);
    }
    
    return(
        <Box
            sx={{
                display: "flex",
                flexDirection: "column",
                height: "calc(100vh - 300px)",
                width: "100%"
            }}
        >
            <Box
                sx={{
                    display: "flex",
                    gap: "20px"
                }}
            >
                <DateTimeRangePicker getMeasurement={handleFromToChange} flag={"deviceAvailability"}/>
                <RadioGroup
                    name="controlled-radio-buttons-group"
                    value={measurement}
                    onChange={(event, value) =>
                        setMeasurement(value)}
                    row
                >
                    <FormControlLabel value="time" control={<Radio/>} label="Time"/>
                    <FormControlLabel value="percentage" control={<Radio/>} label="Percentage"/>
                </RadioGroup>
            </Box>
            <Bar data={data} options={options}/>
        </Box>
    )
}

interface FromTo{
    from: string,
    to: string,
    aggregationWindow: string
}