import {Box} from "@mui/material";
import DateTimeRangePicker from "../../util/date-time-range-picker.tsx";
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import React, {useEffect, useState} from "react";
import {AirConditioningRecord, WashingMachineRecord} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";

export default function WashingMachineTable({washingMachineId}: {washingMachineId: string}) {

    const [washingMachineRecords, setWashingMachineRecords] =
        useState<WashingMachineRecord[]>([])

    const getWashingMachineRecords = (from: string = "-1h", to: string = "now()") => {
        api.get<WashingMachineRecord[]>(`/device/washingMachine/getRecords/${washingMachineId}`,
            {
                params: {from: from, to: to}
            })
            .then(res => {
                if (res.status === 200) {
                    console.log(res.data);
                    setWashingMachineRecords(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    useEffect(() => {
        getWashingMachineRecords();
    }, [])

    const dateOptions: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false
    };

    const columns: GridColDef[] = [
        { field: 'action', headerName: 'Action', width: 250, sortable: false },
        { field: 'subject', headerName: 'Subject', width: 250, sortable: false },
        {
            field: 'timestamp',
            headerName: 'Time',
            type: 'Date',
            valueFormatter: params => new Date(params?.value)
                .toLocaleString("ru-RU", dateOptions),
            width: 250
        },
    ];

    return (
        <Box sx={{
            width: '100%',
            height: 'calc(100vh - 200px)',
            display: 'flex',
            flexDirection: 'column',
            gap: '5px'
        }}>
            <DateTimeRangePicker getMeasurement={getWashingMachineRecords} flag={"none"}/>
            <DataGrid
                getRowId={(row) => row.timestamp}
                rows={washingMachineRecords}
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: {page: 0, pageSize: 10},
                    },
                    sorting: {
                        sortModel: [{field: 'timestamp', sort: 'desc'}]
                    }
                }}
                pageSizeOptions={[5, 10, 25]}
            />
        </Box>
    )
}