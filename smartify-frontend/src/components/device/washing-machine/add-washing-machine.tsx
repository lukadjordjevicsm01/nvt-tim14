import "../add-device-component.css"
import {useFormik} from "formik";
import * as yup from "yup";
import {Autocomplete, Box, Button, TextField} from "@mui/material";
import {useState} from "react";
import api from "../../../config/axios-config.tsx";
import {UserTokenState} from "../../../model/user-model.tsx";
import toast from "react-hot-toast";
import {WashingMachine, WashingMachineMode} from "../../../model/device-model.tsx";
import {useParams} from "react-router-dom";
import defaultPicture from "../../../assets/logo-icon-dark.svg";


export default function AddWashingMachine() {
    const { realEstateId } = useParams<{ realEstateId: string }>();

    const [powerSupplyTypes] = useState<string[]>(["Autonomously", "House Battery", "Mains"]);
    const [selectedPowerSupplyType, setSelectedPowerSupplyType] = useState<string>("Autonomously");

    const [deviceImagePreview, setDeviceImagePreview] =
        useState(defaultPicture);

    const handlePowerSupplyTypeChange = (category: string | null) => {
        setSelectedPowerSupplyType(category || "Autonomously");
    };


    const [modes, setModes] = useState<WashingMachineMode[]>([]);

    const handleAddMode = () => {
        const modeName = document.getElementById('mode-name') as HTMLInputElement;
        const modeDuration = document.getElementById('mode-duration') as HTMLInputElement;

        if (modeName.value == '' || modeDuration.value == '') return;

        if (modeName && modeDuration) {
            const newMode: WashingMachineMode = {
                name: modeName.value,
                duration: parseInt(modeDuration.value, 10),
            };

            setModes([...modes, newMode]);
            modeName.value = '';
            modeDuration.value = '';
            modeFormik.setFieldValue('modeName', '');
            modeFormik.setFieldValue('modeDuration', '');
        }
    };

    const handleRemoveMode = (indexToRemove: number) => {
        const updatedModes = modes.filter((_mode, index) => index !== indexToRemove);
        setModes(updatedModes);
    };

    const validationSchema = yup.object({
        name: yup
            .string()
            .required('Device Name is required'),
        consumption: yup
            .number()
            .typeError('Consumption must be a number')
            .positive("Consumption must be greater than 0")
            .required('Consumption is required'),
    });

    const formik = useFormik({
        initialValues: {
            name: '',
            consumption: '',
            deviceImage: undefined
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            if (modes.length == 0) {
                toast.error("You need to add washing machine mode");
                return
            }
            if (values.deviceImage == undefined) {
                toast.error("Please upload device image.")
                return
            }

            const washingMachine : WashingMachine = {
                online: true,
                on: true,
                name: values.name,
                powerSupplyType: selectedPowerSupplyType.toUpperCase().replace(/\s+/g, '_'),
                consumption: Number(values.consumption),
                currentMode: null,
                supportedModes: modes,
                settingsActive: null,
                customSettings: null,
                id: -1,
                image: null,
                imagePath: "",
                deviceType: "washingMachine"
            }
            console.log(washingMachine)
            api.post<WashingMachine>(`device/washingMachine/create/${realEstateId}`, washingMachine).then(res => {
                if (res.status === 200) {
                    const formData = new FormData();
                    formData.append('id', res.data.id.toString());
                    formData.append('image', values.deviceImage)
                    api.post('device/addImage', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        },
                    }).then().catch(() => {
                        toast.error("Error. Image upload failed.");
                    });

                    toast.success("Device added successfully.")
                    formik.resetForm()
                    modeFormik.resetForm()
                    setModes([])
                    formik.setFieldValue('deviceImage', undefined);
                    setDeviceImagePreview(defaultPicture);
                }
            }).catch(() => {
                toast.error("Error. Device creation failed.");
            });
        },
    });

    const modeValidationSchema = yup.object({
        modeName: yup
            .string(),
        modeDuration: yup
            .number()
            .typeError('Duration must be a number')
            .positive("Duration must be greater than 0")
    });

    const modeFormik = useFormik({
        initialValues: {
            modeName: '',
            modeDuration: '',
        },
        validationSchema: modeValidationSchema,
        onSubmit:() => {
            handleAddMode()
        }
    })


    return (
        <form
            onSubmit={formik.handleSubmit}
            className={"form-container"}>
            <Box id={"device-info"}>
                <TextField
                    id="name"
                    name="name"
                    label="Device Name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                />
                <Autocomplete
                    options={powerSupplyTypes}
                    renderInput={(params) => (
                        <TextField {...params} label="Power Supply Type" variant="outlined"/>
                    )}
                    onChange={(_event, value) => handlePowerSupplyTypeChange(value)}
                    value={selectedPowerSupplyType}
                    disableClearable
                />
                <TextField
                    id="consumption"
                    name="consumption"
                    label="Consumption (kWh)"
                    value={formik.values.consumption}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.consumption && Boolean(formik.errors.consumption)}
                    helperText={formik.touched.consumption && formik.errors.consumption}
                />
            </Box>
            <h3>Supported Modes</h3>
            <form id={"mode-add-container"}
                  onSubmit={modeFormik.handleSubmit}>
                <TextField
                    id="mode-name"
                    name="modeName"
                    label="Mode Name"
                    value={modeFormik.values.modeName}
                    onChange={modeFormik.handleChange}
                    onBlur={modeFormik.handleBlur}
                    error={modeFormik.touched.modeName && Boolean(modeFormik.errors.modeName)}
                    helperText={modeFormik.touched.modeName && modeFormik.errors.modeName}
                />
                <TextField
                    id="mode-duration"
                    name="modeDuration"
                    label="Mode Duration (min)"
                    value={modeFormik.values.modeDuration}
                    onChange={modeFormik.handleChange}
                    onBlur={modeFormik.handleBlur}
                    error={modeFormik.touched.modeDuration && Boolean(modeFormik.errors.modeDuration)}
                    helperText={modeFormik.touched.modeDuration && modeFormik.errors.modeDuration}
                />
                <Button
                    onClick={handleAddMode}
                    sx={{
                        fontSize: "18px",
                        textTransform: "capitalize",
                        maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                    }}
                    variant="contained">
                    Add Mode
                </Button>
            </form>
            <Box id={"added-modes-container"}>
                {modes.map((mode, index) => (
                    <div key={index} className="added-mode-item">
                        <div className="added-mode-item-info">
                            <span>{`Name: ${mode.name}`}</span>
                            <span>{`Duration: ${mode.duration} min`}</span>
                        </div>
                        <Button
                            onClick={() => handleRemoveMode(index)}
                            variant="outlined"
                            sx={{textTransform: 'capitalize', maxHeight: '40px'}}
                        >
                            Remove
                        </Button>
                    </div>
                ))}
            </Box>
            <div id={"image-upload"}>
                <img alt={'Not uploaded'} src={deviceImagePreview || formik.values.deviceImage}/>

                <Button variant='contained'
                        sx={{
                            fontSize: "16px",
                            textTransform: "capitalize",
                            maxWidth: '180px', maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                        }}
                        component='label'>
                    Upload image
                    <input
                        name='deviceImage'
                        accept='image/*'
                        id='contained-button-file'
                        type='file'
                        hidden
                        onChange={(e) => {
                            const selectedFile = e.target.files[0];
                            formik.setFieldValue('deviceImage', selectedFile);
                            setDeviceImagePreview(URL.createObjectURL(selectedFile));
                        }}
                    />
                </Button>
                {formik.touched.deviceImage && formik.errors.deviceImage && (
                    <p className="error-message">{formik.errors.deviceImage.toString()}</p>
                )}
            </div>
            <Button
                sx={{
                    fontSize: "20px",
                    textTransform: "capitalize"
                }}
                variant="contained"
                type="submit">
                Add
            </Button>
        </form>
    )
}