import {Box, Button, Checkbox, Dialog, DialogContent, FormControlLabel, Typography, useTheme} from "@mui/material";
import {TimePicker} from "@mui/x-date-pickers";
import React, {useEffect, useState} from "react";
import {addHours} from "date-fns";
import {SprinklerSetting} from "../../../model/device-model.tsx";
import toast from "react-hot-toast";
import api from "../../../config/axios-config.tsx";
import {getUserMail} from "../../../util/token-utils.tsx";

export default function SprinklerSettingPopup ({sprinklerId, sprinklerSetting, updateParentSetting} : SprinklerSettingPopupProps) {
    
    const [repeatingDays, setRepeatingDays] = useState({
        MONDAY: false,
        TUESDAY: false,
        WEDNESDAY: false,
        THURSDAY: false,
        FRIDAY: false,
        SATURDAY: false,
        SUNDAY: false
    });
    
    const theme = useTheme();
    
    const [startTime, setStartTime] = useState<Date>(new Date());
    const [endTime, setEndTime] = useState<Date>(addHours(new Date(), 1));
    
    const [confirmationDialogOpen, setConfirmationDialogOpen] = useState<boolean>(false);
    
    useEffect(() => {
        const repatingDaysCopy = {...repeatingDays}
        for (let day of sprinklerSetting.repeatDays){
            console.log(sprinklerSetting);
            repatingDaysCopy[day] = true;
        }
        setRepeatingDays(repatingDaysCopy);
        setStartTime(new Date(sprinklerSetting.startTime));
        setEndTime(new Date(sprinklerSetting.endTime));
        
    }, []);
    
    const handleSaveSetting = () => {
        const noDaySelected = Object.values(repeatingDays).every(value => value === false);
        if (noDaySelected){
            toast.error("Please select at least one day  to repeat this setting.");
            return;
        }
        
        const repeatingDaysValues = Object.entries(repeatingDays)
            .filter(([day, value]) => value === true)
            .map(([day]) => day);
        const updatedSprinklerSetting: SprinklerSetting = {
            id: sprinklerSetting.id,
            endTime: endTime.toISOString(),
            startTime: startTime.toISOString(),
            repeatDays: repeatingDaysValues
        }
        
        api.post(`/device/sprinkler/updateSetting/${sprinklerSetting.id}/${sprinklerId}`, {
                addedBy: getUserMail(),
                endTime: endTime.getTime(),
                startTime: startTime.getTime(),
                repeatDays: repeatingDaysValues
            })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                    updateParentSetting(updatedSprinklerSetting, true);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
        
    }
    
    const handleDeleteSetting = () => {
        api.delete(`/device/sprinkler/deleteSetting/${sprinklerSetting.id}/${sprinklerId}`, {
            params: {email: getUserMail()}
            })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                    updateParentSetting(sprinklerSetting, false);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }
    const handleCheckboxChange = (event) => {
        setRepeatingDays({ ...repeatingDays, [event.target.name]: event.target.checked });
    };
    
    return (
        <Box
            sx={{
                width: "100%",
                padding: "20px",
                display: "flex",
                flexDirection: "column"
            }}
        >
            <Box
                sx={{
                    display: "flex",
                }}
            >
                <FormControlLabel
                    control={<Checkbox checked={repeatingDays.MONDAY} onChange={handleCheckboxChange}
                                       name="MONDAY"/>}
                    label="Mon"
                />
                <FormControlLabel
                    control={<Checkbox checked={repeatingDays.TUESDAY} onChange={handleCheckboxChange}
                                       name="TUESDAY"/>}
                    label="Tue"
                />
                <FormControlLabel
                    control={<Checkbox checked={repeatingDays.WEDNESDAY} onChange={handleCheckboxChange}
                                       name="WEDNESDAY"/>}
                    label="Wed"
                />
                <FormControlLabel
                    control={<Checkbox checked={repeatingDays.THURSDAY} onChange={handleCheckboxChange}
                                       name="THURSDAY"/>}
                    label="Thu"
                />
                <FormControlLabel
                    control={<Checkbox checked={repeatingDays.FRIDAY} onChange={handleCheckboxChange}
                                       name="FRIDAY"/>}
                    label="Fri"
                />
                <FormControlLabel
                    control={<Checkbox checked={repeatingDays.SATURDAY} onChange={handleCheckboxChange}
                                       name="SATURDAY"/>}
                    label="Sat"
                />
                <FormControlLabel
                    control={<Checkbox checked={repeatingDays.SUNDAY} onChange={handleCheckboxChange}
                                       name="SUNDAY"/>}
                    label="Sun"
                />
            </Box>
            <Box
                sx={{
                    display: "flex",
                    alignItems: "center",
                    gap: "10px"
                }}
            >
                <TimePicker
                    label="Start"
                    ampm={false}
                    value={startTime}
                    onChange={(newValue) => setStartTime(newValue)}
                    sx={{
                        width: "120px"
                    }}
                />
                <p>-</p>
                <TimePicker
                    label="End"
                    ampm={false}
                    value={endTime}
                    onChange={(newValue) => setEndTime(newValue)}
                    sx={{
                        width: "120px"
                    }}
                />
                
                <Button
                    variant="contained"
                    onClick={handleSaveSetting}
                    sx={{
                        maxWidth: "100px",
                        minWidth: "60px",
                        minHeight: "40px",
                        maxHeight: "40px",
                        backgroundColor: "primary.main",
                        color: "secondary.main",
                    }}>
                    Save
                </Button>
                
                <Button
                    variant="contained"
                    onClick={() => setConfirmationDialogOpen(true)}
                    sx={{
                        maxWidth: "100px",
                        minWidth: "60px",
                        minHeight: "40px",
                        maxHeight: "40px",
                        backgroundColor: "error.main",
                        color: "secondary.main",
                    }}>
                    Remove
                </Button>
                
                <Dialog open={confirmationDialogOpen} onClose={() => setConfirmationDialogOpen(false)} fullWidth
                        maxWidth={"sm"}>
                    <DialogContent>
                        <Box
                            sx={{
                                width: "100%",
                                padding: "20px",
                                display: "flex",
                                flexDirection: "column"
                            }}
                        >
                            <h3>Removing a setting</h3>
                            <Typography>Are you sure you want to remove this setting?</Typography>
                            <Box
                                sx={{
                                    display: "flex",
                                    gap: "20px"
                                }}
                            >
                                <Button
                                    variant="contained"
                                    onClick={handleDeleteSetting}
                                    sx={{
                                        maxWidth: "100px",
                                        minWidth: "60px",
                                        minHeight: "40px",
                                        maxHeight: "40px",
                                        backgroundColor: "primary.main",
                                        color: "secondary.main",
                                    }}>
                                    Yes
                                </Button>
                                
                                <Button
                                    variant="contained"
                                    onClick={() => setConfirmationDialogOpen(false)}
                                    sx={{
                                        maxWidth: "80px",
                                        minWidth: "80px",
                                        minHeight: "40px",
                                        maxHeight: "40px",
                                        backgroundColor: "primary.main",
                                        color: "secondary.main",
                                    }}>
                                    No
                                </Button>
                            </Box>
                        </Box>
                    </DialogContent>
                </Dialog>
                
            </Box>
        </Box>
    )
}

interface SprinklerSettingPopupProps {
    sprinklerId: number,
    sprinklerSetting: SprinklerSetting,
    updateParentSetting: (updatedSetting: SprinklerSetting, update: boolean) => void
}