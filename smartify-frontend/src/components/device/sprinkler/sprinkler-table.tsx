import DateTimeRangePicker from "../../util/date-time-range-picker.tsx";
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import React, {useEffect, useRef, useState} from "react";
import {GateEventRecord, SprinklerEventRecord} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";

export default function SprinklerTable({sprinklerId} : SprinklerTableProps ) {
    
    const [sprinklerEventRecords, setSprinklerEventRecords]
        = useState<SprinklerEventRecord[]>([]);
    
    const [fromTo, setFromTo]
        = useState<FromTo>({from: "-1h", to: "now()"})
    
    const fromToRef = useRef(fromTo);
    fromToRef.current = fromTo;
    
    useEffect(() => {
        getSprinklerEventRecords();
    }, []);
    
    useEffect(() => {
        getSprinklerEventRecords(fromTo.from, fromTo.to);
    }, [fromTo]);
    
    const getFromTo = (from: string = "-1h", to: string = "now()") => {
        setFromTo({from: from, to: to});
    }
    
    const getSprinklerEventRecords = (from: string = "-1h", to: string = "now()") => {
        api.get<SprinklerEventRecord[]>(`/device/sprinkler/getSprinklerEvents/${sprinklerId}`,
                {
                    params: {from: from, to: to}
                })
            .then(res => {
                if (res.status === 200) {
                    setSprinklerEventRecords(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }
    
    const dateOptions: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false
    };
    
    const columns: GridColDef[] = [
        { field: 'action', headerName: 'Action', width: 200, sortable: false },
        { field: 'subject', headerName: 'Subject', width: 150, sortable: false },
        { field: 'setting', headerName: 'Setting', width: 300, sortable: false },
        {
            field: 'timestamp',
            headerName: 'Time',
            type: 'Date',
            valueFormatter: params => new Date(params?.value).toLocaleString("ru-RU",
                dateOptions),
            width: 250
        },
    ];
    
    return (
        <div style={{
            width: '100%',
            height: 'calc(100vh - 200px)',
            display: 'flex',
            flexDirection: 'column',
            gap: '5px'
        }}>
            <DateTimeRangePicker getMeasurement={getFromTo} flag={"none"}/>
            <DataGrid
                getRowId={(row) => row.timestamp}
                rows={sprinklerEventRecords}
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: {page: 0, pageSize: 10},
                    },
                    sorting: {
                        sortModel: [{field: 'timestamp', sort: 'desc'}]
                    }
                }}
                pageSizeOptions={[5, 10, 25]}
            />
        </div>
    )
}

interface SprinklerTableProps {
    sprinklerId: number
}

interface FromTo{
    from: string,
    to: string
}