import {Device} from "../../model/device-model.tsx";
import React, {useEffect, useState} from "react";
import {imagesUrl} from "../../util/environment.ts";
import {Box, Button, capitalize, Dialog, DialogContent, FormControlLabel, Switch, Typography} from "@mui/material";
import api from "../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {getUserId, getUserMail} from "../../util/token-utils.tsx";
import AddPermission from "../util/permissions.tsx";
import Permissions from "../util/permissions.tsx";
import {RealEstate} from "../../model/real-estate-model.tsx";
import {User} from "../../model/user-model.tsx";

export default function DeviceData ({ device }: DeviceProps) {

    const [deviceState, setDeviceState] = useState<Device>(device);
    const [activity, setActivity] = useState<string>("On");
    const [status, setStatus] = useState<string>("Online");
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [shared, setShared] = useState<boolean>(true);

    const getDeviceOwner = () => {
        api
            .get<User>(`/device/getDeviceOwner/${device.id}`)
            .then((res) => {
                if (res.status === 200) {
                    console.log(res.data.id);
                    console.log(getUserId());
                    setShared(res.data.id !== getUserId());
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    useEffect(() => {
        getDeviceOwner();
    }, [])

    useEffect(() => {
        if (deviceState.on) setActivity("On")
        else setActivity("Off")

        if (deviceState.online) setStatus("Online")
        else setStatus("Offline")
    }, [deviceState]);

    const handleOnSwitch = (deviceId: number) => {
        api.post(`device/switch/${deviceId}`, {},{
            params: {userEmail: getUserMail()}
        })
            .then(res => {
                if (res.status === 200) {
                    const updatedDevice = { ...deviceState, on: !deviceState.on };
                    setDeviceState(updatedDevice);
                    if (!deviceState.on)
                        toast.success("Device turned on.");
                    else
                        toast.success("Device turned off.");
                }
            })
            .catch(() => toast.error("Error. Device activity switch failed."));
    };
    
    return (
        <Box sx={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            gap: "5px"
        }}>
            <img src={`${imagesUrl}/device/${deviceState.imagePath}`} alt={deviceState.name} style={{
                width: "100%",
                height: "300px",
                objectFit: "cover",
                marginBottom: "10px",
                borderRadius: "5px"
            }}/>
            <h3>{deviceState.name}</h3>
            <Typography> Consumption: {deviceState.consumption} kWh </Typography>
            <Typography> Power supply type: {capitalize(deviceState.powerSupplyType.toLowerCase().replace("_", " "))} </Typography>
            <Box sx={{
                display: "flex",
                alignItems: 'center',
                gap: "40px",
            }}>
                {device.online &&
                    <Typography
                        sx={{
                            color: 'success.main'
                        }}>
                        • Online
                    </Typography>}
                {!device.online &&
                    <Typography
                        sx={{
                            color: 'error.main'
                        }}>
                        • Offline
                    </Typography>}
                <FormControlLabel control={
                    <Switch
                        checked={deviceState.on}
                        onChange={() => handleOnSwitch(deviceState.id)}
                        inputProps={{ 'aria-label': 'controlled' }}/>
                } label={activity}/>
            </Box>
            {!shared &&
                <Button
                    variant="contained"
                    sx={{
                        backgroundColor: "primary.main",
                        color: "secondary.main",
                    }}
                    onClick={() => setDialogOpen(true)}
                >
                    Permissions
                </Button>
            }
            <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)} fullWidth>
                <DialogContent
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "10px"
                    }}
                >
                    <Permissions type="device" entityId={device.id}/>
                </DialogContent>
            </Dialog>
        </Box>
    )
}

interface DeviceProps {
    device: Device;
}