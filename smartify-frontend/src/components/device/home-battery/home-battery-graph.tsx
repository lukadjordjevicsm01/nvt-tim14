import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {MeasurementRecord} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {Box, Typography} from "@mui/material";
import DateTimeRangePicker from "../../util/date-time-range-picker.tsx";
import CustomGraph from "../../util/custom-graph.tsx";
import WebSocketService from "../../../util/web-socket-service.ts";
import {subHours} from "date-fns";

export default function HomeBatteryGraph({realTime}: {realTime: boolean}) {
    const { homeBatteryId } = useParams();

    const [consumptions, setConsumptions] = useState<MeasurementRecord[]>([]);
    const [lastConsumption, setLastConsumption] = useState<number>();

    useEffect(() => {
        if (consumptions.length === 0) {
            getHomeBatteryData();
        }
        const subscription = WebSocketService.subscribeToTopic(`/home-battery/${homeBatteryId}`,
            payload => {
                console.log('Received message:', payload);
                if (payload.command === 'value') {
                    setConsumptions(prevConsumption => {
                        const newConsumption = prevConsumption.filter((record) => {
                            const recordTimestamp = new Date(record.timestamp);
                            if (recordTimestamp >= subHours(new Date(), 1))
                                return record;
                        });
                        newConsumption.push(payload.payload);
                        setLastConsumption(payload.payload.measurement);
                        return newConsumption;
                    });
                }
            });
        return () => {
            if (subscription)
                subscription.unsubscribe();
        };
    }, []);

    const getHomeBatteryData = (from: string = "-1h", to: string = "now()") => {
        api.get<MeasurementRecord[]>
        (`/device/homeBattery/getConsumptionRecords/${homeBatteryId}`,
            {
                params: {from: from, to: to}
            })
            .then(res => {
                if (res.status === 200) {
                    setConsumptions(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const getMeasurement = (from: string = "-1h", to: string = "now()") => {
        getHomeBatteryData(from, to)
    }

    return (
        <Box sx={{
            display: "flex",
            flexDirection: "column",
            gap: "10px",
            height: "100%"
        }}>
            <Box sx={{
                display: "flex",
                gap: "20px"
            }}>
                {!realTime && <DateTimeRangePicker getMeasurement={getMeasurement} flag={"none"}/>}
            </Box>
            <Box sx={{
                display: "flex",
                gap: "20px",
                margin: "auto"
            }}>
                {realTime && lastConsumption &&
                    <Typography sx={{color: "primary.light"}}>Last Minute Consumption: {lastConsumption.toFixed(3)} kWh</Typography>
                }
            </Box>
            <Box sx={{
                height: "100%",
            }}>
                {consumptions.length !== 0 &&
                    <CustomGraph title={"Consumption"}
                                 measurements={consumptions}
                                 color={'#174384'} aggregationWindow={"none"}/>
                }
            </Box>
        </Box>
    );

}