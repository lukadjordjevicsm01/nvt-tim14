import {useEffect, useState} from "react";
import React from 'react';

import { Line } from 'react-chartjs-2';
import {
    Box, Button,
    Dialog, DialogContent,
    FormControlLabel,
    MenuItem,
    Radio,
    RadioGroup,
    Select,
    SelectChangeEvent,
    Typography
} from "@mui/material";
import {AmbientSensorRecord} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {useParams} from "react-router-dom";
import {DateTimePicker} from "@mui/x-date-pickers";
import {addDays, addHours, subDays, subHours} from "date-fns";
import DateTimeRangePicker from "../../util/date-time-range-picker.tsx";
import CustomGraph from "../../util/custom-graph.tsx";
import WebSocketService from "../../../util/web-socket-service.ts";


export default function AmbientSensorGraph({realTime}: {realTime: boolean}) {
    const { ambientSensorId } = useParams();

    const [measurement, setMeasurement] = useState<string>("temperature")
    const [timespanFrom, setTimespan] = useState<string>("-1h")

    const [temperatures, setTemperatures] = useState<AmbientSensorRecord[]>([]);
    const [humidities , setHumidities] = useState<AmbientSensorRecord[]>([]);

    const [lastTemperature, setLastTemperature] = useState<number>();
    const [lastHumidity, setLastHumidity] = useState<number>();

    const [selectedAggregationWindow, setSelectedAggregationWindow] = useState<string>("none")

    const [webSocket, setWebSocket] = useState<any>();

    const subscribeToWebSocket = (measurements: AmbientSensorRecord[], setMeasurements, setLastMeasurement) => {
        return WebSocketService.subscribeToTopic(`/ambient-sensor/${measurement}/${ambientSensorId}`,
            payload => {
                console.log('Received message:', payload);
                if (payload.command === 'value') {
                    setMeasurements(prevMeasurements => {
                        let newMeasurements = prevMeasurements.filter((record) => {
                            const recordTimestamp = new Date(record.timestamp);
                            if (recordTimestamp >= subHours(new Date(), 1))
                                return record;
                        });
                        newMeasurements.push(payload.payload);
                        setLastMeasurement(payload.payload.measurement);
                        return newMeasurements;
                    });
            }
        });
    }

    const getTemperatures = (from: string = "-1h", to: string = "now()", aggregationWindow: string = "none") => {
        api.get<AmbientSensorRecord[]>(`/device/ambientSensor/get-ambient-sensor-records/${ambientSensorId}`,
            {
                params: {
                    from: from,
                    to: to,
                    measurement: "temperature",
                    aggregationWindow: aggregationWindow}})
            .then(res => {
                if (res.status === 200) {
                    setTemperatures(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const getHumidities = (from: string = "-1h", to: string = "now()", aggregationWindow: string = "none") => {
        api.get<AmbientSensorRecord[]>(`/device/ambientSensor/get-ambient-sensor-records/${ambientSensorId}`,
            {
                params: {
                    from: from,
                    to: to,
                    measurement: "humidity",
                    aggregationWindow: aggregationWindow}})
            .then(res => {
                if (res.status === 200) {
                    setHumidities(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const getMeasurement = (from: string = "-1h", to: string = "now()", aggregationWindow: string = "none") => {
        setSelectedAggregationWindow(aggregationWindow);
        if (measurement === "temperature")
            getTemperatures(from, to, aggregationWindow)
        else
            getHumidities(from, to, aggregationWindow)
        setTimespan(from);
    }

    useEffect(() => {
        getTemperatures();
        return () => {
            if (webSocket)
                webSocket.unsubscribe();
        };
    }, []);

    useEffect(() => {
        if (webSocket)
            webSocket.unsubscribe();
        if (measurement == "temperature") {
            if (temperatures.length === 0)
                getTemperatures(timespanFrom);
            if (realTime) setWebSocket(subscribeToWebSocket(temperatures, setTemperatures, setLastTemperature));
        } else {
            if (humidities.length === 0)
                getHumidities(timespanFrom);
            if (realTime) setWebSocket(subscribeToWebSocket(humidities, setHumidities, setLastHumidity));
        }
    }, [measurement])

    return (
        <Box sx={{
            display: "flex",
            flexDirection: "column",
            gap: "10px",
            height: "100%"
        }}>
            <Box sx={{
                display: "flex",
                gap: "20px"
            }}>
                <RadioGroup
                    sx={{
                        display: "flex",
                        flexDirection: "row"
                    }}
                    defaultValue="temperature"
                    name="radio-buttons-group"
                    onChange={(event) => setMeasurement(event.target.value)}>
                    <FormControlLabel value="temperature"
                                      control={<Radio />} label="Temperature (°C)" />
                    <FormControlLabel value="humidity"
                                      control={<Radio />} label="Humidity (%)" />
                </RadioGroup>

                {!realTime && <DateTimeRangePicker getMeasurement={getMeasurement} flag={"aggregate"}/>}
            </Box>
            <Box sx={{
                display: "flex",
                gap: "20px",
                margin: "auto"
            }}>
                {measurement == "temperature" && realTime && lastTemperature &&
                    <Typography sx={{color: "error.main"}}>Last Temperature: {lastTemperature}°C</Typography>
                }
                {measurement == "humidity" && realTime && lastHumidity &&
                    <Typography sx={{color: "primary.light"}}>Last Humidity: {lastHumidity}%</Typography>
                }
            </Box>
            <Box sx={{
                height: "100%",
            }}>
                {measurement === "temperature" && temperatures.length !== 0 &&
                    <CustomGraph title={"Temperatures"}
                                 measurements={temperatures}
                                 color={'#DD3636'} aggregationWindow={selectedAggregationWindow}/>
                }
                {measurement === "humidity" && humidities.length !== 0 &&
                    <CustomGraph title={"Humidities"}
                                 measurements={humidities}
                                 color={'#174384'} aggregationWindow={selectedAggregationWindow}/>
                }

            </Box>
        </Box>
    );
}