import "../add-device-component.css"
import {useFormik} from "formik";
import * as yup from "yup";
import {Autocomplete, Box, Button, TextField} from "@mui/material";
import {useState} from "react";
import api from "../../../config/axios-config.tsx";
import {UserTokenState} from "../../../model/user-model.tsx";
import toast from "react-hot-toast";
import {AmbientSensor} from "../../../model/device-model.tsx";
import {useParams} from "react-router-dom";
import defaultPicture from "../../../assets/logo-icon-dark.svg";


export default function AddAmbientSensor() {
    const { realEstateId } = useParams<{ realEstateId: string }>();

    const [powerSupplyTypes] = useState<string[]>(["Autonomously", "House Battery", "Mains"]);
    const [selectedPowerSupplyType, setSelectedPowerSupplyType] = useState<string>("Autonomously");

    const [deviceImagePreview, setDeviceImagePreview] =
        useState(defaultPicture);

    const handlePowerSupplyTypeChange = (category: string | null) => {
        setSelectedPowerSupplyType(category || "Autonomously");
    };



    const validationSchema = yup.object({
        name: yup
            .string()
            .required('Device Name is required'),
        consumption: yup
            .number()
            .typeError('Consumption must be a number')
            .positive("Consumption must be greater than 0")
            .required('Consumption is required'),
    });

    const formik = useFormik({
        initialValues: {
            name: '',
            consumption: '',
            deviceImage: undefined
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            if (values.deviceImage == undefined) {
                toast.error("Please upload device image.")
                return
            }
            const ambientSensor : AmbientSensor = {
                online: true,
                on: true,
                name: values.name,
                powerSupplyType: selectedPowerSupplyType.toUpperCase().replace(/\s+/g, '_'),
                consumption: Number(values.consumption),
                temperature: null,
                humidity: null,
                id: -1,
                image: null,
                imagePath: "",
                deviceType: "sprinkler"
            }
            api.post<AmbientSensor>(`device/ambientSensor/create/${realEstateId}`, ambientSensor).then(res => {
                if (res.status === 200) {
                    const formData = new FormData();
                    formData.append('id', res.data.id.toString());
                    formData.append('image', values.deviceImage)
                    api.post('device/addImage', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        },
                    }).then().catch(() => {
                        toast.error("Error. Image upload failed.");
                    });

                    toast.success("Device added successfully.")
                    formik.resetForm()
                    formik.setFieldValue('deviceImage', undefined);
                    setDeviceImagePreview(defaultPicture);
                }
            }).catch(() => {
                toast.error("Error. Device creation failed.");
            });
        },
    });

    return (
        <form
            onSubmit={formik.handleSubmit}
            className={"form-container"}>
            <Box id={"device-info"}>
                <TextField
                    id="name"
                    name="name"
                    label="Device Name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                />
                <Autocomplete
                    options={powerSupplyTypes}
                    renderInput={(params) => (
                        <TextField {...params} label="Power Supply Type" variant="outlined"/>
                    )}
                    onChange={(_event, value) => handlePowerSupplyTypeChange(value)}
                    value={selectedPowerSupplyType}
                    disableClearable
                />
                <TextField
                    id="consumption"
                    name="consumption"
                    label="Consumption (kWh)"
                    value={formik.values.consumption}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.consumption && Boolean(formik.errors.consumption)}
                    helperText={formik.touched.consumption && formik.errors.consumption}
                />
            </Box>
            <div id={"image-upload"}>
                <img alt={'Not uploaded'} src={deviceImagePreview || formik.values.deviceImage}/>

                <Button variant='contained'
                        sx={{
                            fontSize: "16px",
                            textTransform: "capitalize",
                            maxWidth: '180px', maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                        }}
                        component='label'>
                    Upload image
                    <input
                        name='deviceImage'
                        accept='image/*'
                        id='contained-button-file'
                        type='file'
                        hidden
                        onChange={(e) => {
                            const selectedFile = e.target.files[0];
                            formik.setFieldValue('deviceImage', selectedFile);
                            setDeviceImagePreview(URL.createObjectURL(selectedFile));
                        }}
                    />
                </Button>
                {formik.touched.deviceImage && formik.errors.deviceImage && (
                    <p className="error-message">{formik.errors.deviceImage.toString()}</p>
                )}
            </div>
            <Button
                sx={{
                    fontSize: "20px",
                    textTransform: "capitalize"
                }}
                variant="contained"
                type="submit">
                Add
            </Button>
        </form>
    )
}