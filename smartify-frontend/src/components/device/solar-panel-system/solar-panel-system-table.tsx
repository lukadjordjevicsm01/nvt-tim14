import React, {useEffect, useState} from "react";
import {SolarPanelSystemEventRecord} from "../../../model/device-model.tsx";
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import DateTimeRangePicker from "../../util/date-time-range-picker.tsx";

export default function SolarPanelSystemTable({solarPanelSystemId} : SolarPanelSystemTableProps) {

    const [solarPanelSystemEventRecords, setSolarPanelSystemEventRecords]
        = useState<SolarPanelSystemEventRecord[]>([])

    useEffect(() => {
        getSolarPanelSystemData()
    }, []);

    const getSolarPanelSystemData = (from: string = "-1h", to: string = "now()") => {
        api.get<SolarPanelSystemEventRecord[]>
        (`/device/solarPanelSystem/getSolarPanelSystemEvents/${solarPanelSystemId}`,
            {
                params: {from: from, to: to}
            })
            .then(res => {
                if (res.status === 200) {
                    setSolarPanelSystemEventRecords(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const dateOptions: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false
    };

    const columns: GridColDef[] = [
        { field: 'action', headerName: 'Action', width: 250, sortable: false },
        { field: 'subject', headerName: 'Subject', width: 250, sortable: false },
        {
            field: 'timestamp',
            headerName: 'Time',
            type: 'Date',
            valueFormatter: params => new Date(params?.value).toLocaleString("ru-RU",
                dateOptions),
            width: 250
        },
        {
            field: 'value',
            headerName: 'Value (kWh)',
            width: 250,
            type: 'number',
            headerAlign: 'left',
            align: 'left',
            sortable: false
        },

    ];

    const getMeasurement = (from: string = "-1h", to: string = "now()") => {
        getSolarPanelSystemData(from, to)
    }

    return(
        <div style={{
            width: '100%',
            height: 'calc(100vh - 200px)',
            display: 'flex',
            flexDirection: 'column',
            gap: '5px'
        }}>
            <DateTimeRangePicker getMeasurement={getMeasurement} flag={"none"}/>
            <DataGrid
                getRowId={(row) => row.timestamp}
                rows={solarPanelSystemEventRecords}
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: {page: 0, pageSize: 10},
                    },
                    sorting: {
                        sortModel: [{field: 'timestamp', sort: 'desc'}]
                    }
                }}
                pageSizeOptions={[5, 10, 25]}
            />
        </div>
    )

}

interface SolarPanelSystemTableProps {
    solarPanelSystemId: number
}