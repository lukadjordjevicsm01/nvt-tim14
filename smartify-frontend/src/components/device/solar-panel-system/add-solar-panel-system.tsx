import "../add-device-component.css"
import {useFormik} from "formik";
import * as yup from "yup";
import {Autocomplete, Box, Button, TextField} from "@mui/material";
import {useState} from "react";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {SolarPanelSystem, SolarPanel} from "../../../model/device-model.tsx";
import {useParams} from "react-router-dom";
import defaultPicture from "../../../assets/logo-icon-dark.svg";


export default function AddSolarPanelSystem() {
    const { realEstateId } = useParams<{ realEstateId: string }>();

    const [powerSupplyTypes] = useState<string[]>(["Autonomously", "House Battery", "Mains"]);
    const [selectedPowerSupplyType, setSelectedPowerSupplyType] = useState<string>("Autonomously");

    const [panelPowerSupplyTypes] = useState<string[]>(["Autonomously", "House Battery", "Mains"]);
    const [selectedPanelPowerSupplyType, setSelectedPanelPowerSupplyType] = useState<string>("Autonomously");

    const [solarPanels, setSolarPanels] = useState<SolarPanel[]>([])

    const [deviceImagePreview, setDeviceImagePreview] =
        useState(defaultPicture);

    const handlePowerSupplyTypeChange = (category: string | null) => {
        setSelectedPowerSupplyType(category || "Autonomously");
    };

    const handlePanelPowerSupplyTypeChange = (category: string | null) => {
        setSelectedPanelPowerSupplyType(category || "Autonomously");
    };


    const handleAddSolarPanel = () => {
        const name = document.getElementById('solar-panel-name') as HTMLInputElement;
        const powerSupplyType = document.getElementById('panel-power-supply-type') as HTMLInputElement;
        const consumption = document.getElementById('consumption') as HTMLInputElement;
        const area = document.getElementById('area') as HTMLInputElement;
        const efficiency = document.getElementById('efficiency') as HTMLInputElement;

        if (name.value == '' || powerSupplyType.value == '' || consumption.value == '' || area.value == ''
            || efficiency.value == '') return;

        if (name && powerSupplyType && consumption && area && efficiency) {
            const solarPanel : SolarPanel = {
                online: true,
                on: true,
                name: name.value,
                powerSupplyType: selectedPanelPowerSupplyType.toUpperCase().replace(/\s+/g, '_'),
                consumption: Number(consumption.value),
                area: Number(area.value),
                efficiency: Number(efficiency.value),
                id: -1,
                image: null,
                imagePath: "",
                deviceType: "solarPanel"
            }

            setSolarPanels([...solarPanels, solarPanel]);
            panelFormik.resetForm()
        }
    };

    const handleRemoveSolarPanel = (indexToRemove: number) => {
        const updatedSolarPanels = solarPanels.filter((_solarPanel, index) => index !== indexToRemove);
        setSolarPanels(updatedSolarPanels);
    };


    const validationSchema = yup.object({
        name: yup
            .string()
            .required('Device Name is required'),

    });

    const formik = useFormik({
        initialValues: {
            name: '',
            deviceImage: undefined
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {

            if (solarPanels.length == 0) {
                toast.error("You need to add solar panel");
                return
            }

            if (values.deviceImage == undefined) {
                toast.error("Please upload device image.")
                return
            }

            const solarPanelSystem : SolarPanelSystem = {
                online: true,
                on: true,
                name: values.name,
                powerSupplyType: selectedPowerSupplyType.toUpperCase().replace(/\s+/g, '_'),
                consumption: 0,
                solarPanels: solarPanels,
                id: -1,
                image: null,
                imagePath: "",
                deviceType: "solarPanelSystem"
            }
            api.post<SolarPanelSystem>(`device/solarPanelSystem/create/${realEstateId}`, solarPanelSystem).then(res => {
                if (res.status === 200) {
                    const formData = new FormData();
                    formData.append('id', res.data.id.toString());
                    formData.append('image', values.deviceImage)
                    api.post('device/addImage', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        },
                    }).then().catch(() => {
                        toast.error("Error. Image upload failed.");
                    });

                    toast.success("Device added successfully.")
                    formik.resetForm()
                    setSolarPanels([])
                    formik.setFieldValue('deviceImage', undefined);
                    setDeviceImagePreview(defaultPicture);
                }
            }).catch(() => {
                toast.error("Error. Device creation failed.");
            });
        },
    });

    const panelValidationSchema = yup.object({
        name: yup
            .string()
            .required('Solar Panel Name is required'),
        consumption: yup
            .number()
            .typeError('Consumption must be a number')
            .positive("Consumption must be greater than 0")
            .required('Consumption is required'),
        area: yup
            .number()
            .typeError('Area must be a number')
            .positive("Area must be greater than 0")
            .required('Area is required'),
        efficiency: yup
            .number()
            .typeError('Efficiency must be a number')
            .positive("Efficiency must be greater than 0")
            .max(100, "Efficiency can't be over 100%")
            .required('Efficiency is required'),
    });

    const panelFormik = useFormik({
        initialValues: {
            name: '',
            consumption: '',
            area: '',
            efficiency: '',
        },
        validationSchema: panelValidationSchema,
        onSubmit: (values) => {
            const solarPanel : SolarPanel = {
                online: true,
                on: true,
                name: values.name,
                powerSupplyType: selectedPowerSupplyType.toUpperCase().replace(/\s+/g, '_'),
                consumption: Number(values.consumption),
                area: Number(values.area),
                efficiency: Number(values.efficiency),
                id: -1,
                image: null,
                imagePath: "",
                deviceType: "sprinkler"
            }
            setSolarPanels([...solarPanels, solarPanel]);
        },
    });

    return (
        <form
            onSubmit={formik.handleSubmit}
            className={"form-container"}>
            <Box id={"device-info-solar-panel-system"}>
                <TextField
                    id="name"
                    name="name"
                    label="Device Name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                />
                <Autocomplete
                    options={powerSupplyTypes}
                    id={"power-supply-type"}
                    renderInput={(params) => (
                        <TextField {...params} label="Power Supply Type" variant="outlined"/>
                    )}
                    onChange={(_event, value) => handlePowerSupplyTypeChange(value)}
                    value={selectedPowerSupplyType}
                    disableClearable
                />
            </Box>
            <Box id={"add-solar-panel-container"}>
                <h3>Solar Panels</h3>
                <form
                    onSubmit={panelFormik.handleSubmit}
                    className={"form-container"}>
                    <Box id={"device-info"}>
                        <TextField
                            id="solar-panel-name"
                            name="name"
                            label="Solar Panel Name"
                            value={panelFormik.values.name}
                            onChange={panelFormik.handleChange}
                            onBlur={panelFormik.handleBlur}
                            error={panelFormik.touched.name && Boolean(panelFormik.errors.name)}
                            helperText={panelFormik.touched.name && panelFormik.errors.name}
                        />
                        <Autocomplete
                            options={panelPowerSupplyTypes}
                            id={"panel-power-supply-type"}
                            renderInput={(params) => (
                                <TextField {...params} label="Power Supply Type" variant="outlined"/>
                            )}
                            onChange={(_event, value) => handlePanelPowerSupplyTypeChange(value)}
                            value={selectedPanelPowerSupplyType}
                            disableClearable
                        />
                        <TextField
                            id="consumption"
                            name="consumption"
                            label="Consumption (kWh)"
                            value={panelFormik.values.consumption}
                            onChange={panelFormik.handleChange}
                            onBlur={panelFormik.handleBlur}
                            error={panelFormik.touched.consumption && Boolean(panelFormik.errors.consumption)}
                            helperText={panelFormik.touched.consumption && panelFormik.errors.consumption}
                        />
                    </Box>
                    <Box id={"device-info-solar-panel"}>
                        <TextField
                            id="area"
                            name="area"
                            label="Area (m²)"
                            value={panelFormik.values.area}
                            onChange={panelFormik.handleChange}
                            onBlur={panelFormik.handleBlur}
                            error={panelFormik.touched.area && Boolean(panelFormik.errors.area)}
                            helperText={panelFormik.touched.area && panelFormik.errors.area}
                        />
                        <TextField
                            id="efficiency"
                            name="efficiency"
                            label="Efficiency (%)"
                            value={panelFormik.values.efficiency}
                            onChange={panelFormik.handleChange}
                            onBlur={panelFormik.handleBlur}
                            error={panelFormik.touched.efficiency && Boolean(panelFormik.errors.efficiency)}
                            helperText={panelFormik.touched.efficiency && panelFormik.errors.efficiency}
                        />
                    </Box>
                    <Box id={"added-panels-container"}>
                        {solarPanels.map((panel, index) => (
                            <div key={index} className="added-panel-item">
                                <div className="added-panel-item-info">
                                    <span>{`Name: ${panel.name}`}</span>
                                    <span>{`Power Supply Type: ${panel.powerSupplyType}`}</span>
                                    <span>{`Consumption: ${panel.consumption} kWh`}</span>
                                    <span>{`Area: ${panel.area} m²`}</span>
                                    <span>{`Efficiency: ${panel.efficiency}%`}</span>
                                </div>
                                <Button
                                    onClick={() => handleRemoveSolarPanel(index)}
                                    variant="outlined"
                                    sx={{textTransform: 'capitalize', maxHeight: '40px'}}
                                >
                                    Remove
                                </Button>
                            </div>
                        ))}
                    </Box>
                    <Button
                        onClick={handleAddSolarPanel}
                        sx={{
                            fontSize: "18px",
                            textTransform: "capitalize",
                            width: "25%",
                            maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                        }}
                        variant="contained">
                        Add Solar Panel
                    </Button>
                </form>
            </Box>
            <div id={"image-upload"}>
                <img alt={'Not uploaded'} src={deviceImagePreview || formik.values.deviceImage}/>

                <Button variant='contained'
                        sx={{
                            fontSize: "16px",
                            textTransform: "capitalize",
                            maxWidth: '180px', maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                        }}
                        component='label'>
                    Upload image
                    <input
                        name='deviceImage'
                        accept='image/*'
                        id='contained-button-file'
                        type='file'
                        hidden
                        onChange={(e) => {
                            const selectedFile = e.target.files[0];
                            formik.setFieldValue('deviceImage', selectedFile);
                            setDeviceImagePreview(URL.createObjectURL(selectedFile));
                        }}
                    />
                </Button>
                {formik.touched.deviceImage && formik.errors.deviceImage && (
                    <p className="error-message">{formik.errors.deviceImage.toString()}</p>
                )}
            </div>
            <Button
                sx={{
                    fontSize: "20px",
                    textTransform: "capitalize"
                }}
                variant="contained"
                type="submit">
                Add
            </Button>
        </form>
    )
}