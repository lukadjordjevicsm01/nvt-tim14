import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {MeasurementRecord} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {Box} from "@mui/material";
import DateTimeRangePicker from "../../util/date-time-range-picker.tsx";
import CustomGraph from "../../util/custom-graph.tsx";

export default function LampGraph(){
    const { lampId } = useParams();

    const [brightnessRecords, setBrightnessRecords]
        = useState<MeasurementRecord[]>([]);

    const [selectedAggregationWindow, setSelectedAggregationWindow] = useState<string>("none")

    const getBrightnessRecords = (from: string = "-1h", to: string = "now()", aggregationWindow: string = "none") => {
        setSelectedAggregationWindow(aggregationWindow)
        api.get<MeasurementRecord[]>(`/device/lamp/get-lamp-records/${lampId}`,
            {
                params: {
                    from: from,
                    to: to,
                    measurement: "Brightness",
                    aggregationWindow: aggregationWindow}})
            .then(res => {
                if (res.status === 200) {
                    setBrightnessRecords(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    useEffect(() => {
        getBrightnessRecords();
        return () => {};
    }, []);


    return(
        <Box sx={{
            display: "flex",
            flexDirection: "column",
            gap: "10px",
            height: "100%"
        }}>
            <Box sx={{
                display: "flex",
                gap: "20px"
            }}>
                <DateTimeRangePicker getMeasurement={getBrightnessRecords} flag={"aggregate"}/>
            </Box>

            <Box sx={{
                height: "100%",
            }}>
                {brightnessRecords.length !== 0 && <CustomGraph title={"Brightness(lux)"}
                             measurements={brightnessRecords}
                             color={'#DD3636'} aggregationWindow={selectedAggregationWindow}/>}
            </Box>
        </Box>
    )
}