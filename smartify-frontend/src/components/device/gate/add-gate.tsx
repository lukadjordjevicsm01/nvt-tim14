import "../add-device-component.css"
import {useFormik} from "formik";
import * as yup from "yup";
import {Autocomplete, Box, Button, TextField} from "@mui/material";
import {useState} from "react";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {Gate} from "../../../model/device-model.tsx";
import {useParams} from "react-router-dom";
import defaultPicture from "../../../assets/logo-icon-dark.svg";


export default function AddGate() {
    const { realEstateId } = useParams<{ realEstateId: string }>();

    const [powerSupplyTypes] = useState<string[]>(["Autonomously", "House Battery", "Mains"]);
    const [selectedPowerSupplyType, setSelectedPowerSupplyType] = useState<string>("Autonomously");

    const [deviceImagePreview, setDeviceImagePreview] =
        useState(defaultPicture);

    const handlePowerSupplyTypeChange = (category: string | null) => {
        setSelectedPowerSupplyType(category || "Autonomously");
    };

    const [vehicles, setVehicles] = useState<string[]>([]);

    const handleAddVehicle = () => {
        const licencePlate = document.getElementById('licence-plate') as HTMLInputElement;

        if (licencePlate.value == '') return;

        if (licencePlate) {


            setVehicles([...vehicles, licencePlate.value]);
            licencePlate.value = '';
            vehicleFormik.setFieldValue('licencePlate', '');
        }
    };

    const handleRemoveVehicles = (indexToRemove: number) => {
        const updatedVehicles = vehicles.filter((_vehicle, index) => index !== indexToRemove);
        setVehicles(updatedVehicles);
    };


    const validationSchema = yup.object({
        name: yup
            .string()
            .required('Device Name is required'),
        consumption: yup
            .number()
            .typeError('Consumption must be a number')
            .positive("Consumption must be greater than 0")
            .required('Consumption is required'),
    });

    const formik = useFormik({
        initialValues: {
            name: '',
            consumption: '',
            deviceImage: undefined
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            if (values.deviceImage == undefined) {
                toast.error("Please upload device image.")
                return
            }
            const gate : Gate = {
                online: true,
                on: true,
                name: values.name,
                powerSupplyType: selectedPowerSupplyType.toUpperCase().replace(/\s+/g, '_'),
                consumption: Number(values.consumption),
                open: null,
                mode: null,
                allowedVehicles: vehicles,
                id: -1,
                image: null,
                imagePath: "",
                deviceType: "sprinkler"

            }
            api.post<Gate>(`device/gate/create/${realEstateId}`, gate).then(res => {
                if (res.status === 200) {
                    const formData = new FormData();
                    formData.append('id', res.data.id.toString());
                    formData.append('image', values.deviceImage)
                    api.post('device/addImage', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        },
                    }).then().catch(() => {
                        toast.error("Error. Image upload failed.");
                    });

                    toast.success("Device added successfully.")
                    formik.resetForm()
                    setVehicles([])
                    formik.setFieldValue('deviceImage', undefined);
                    setDeviceImagePreview(defaultPicture);
                }
            }).catch(() => {
                toast.error("Error. Device creation failed.");
            });
        },
    });

    const vehicleValidationSchema = yup.object({
        licencePlate: yup
            .string()
            .min(7, "Licence plate must contain 7 characters at least")
            .max(10, "Licence plate must contain 10 characters at most"),

    });

    const vehicleFormik = useFormik({
        initialValues: {
            licencePlate: '',
        },
        validationSchema: vehicleValidationSchema,
        onSubmit:() => {
            handleAddVehicle()
        }
    })

    return (
        <form
            onSubmit={formik.handleSubmit}
            className={"form-container"}>
            <Box id={"device-info"}>
                <TextField
                    id="name"
                    name="name"
                    label="Device Name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                />
                <Autocomplete
                    options={powerSupplyTypes}
                    renderInput={(params) => (
                        <TextField {...params} label="Power Supply Type" variant="outlined"/>
                    )}
                    onChange={(_event, value) => handlePowerSupplyTypeChange(value)}
                    value={selectedPowerSupplyType}
                    disableClearable
                />
                <TextField
                    id="consumption"
                    name="consumption"
                    label="Consumption (kWh)"
                    value={formik.values.consumption}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.consumption && Boolean(formik.errors.consumption)}
                    helperText={formik.touched.consumption && formik.errors.consumption}
                />
            </Box>
            <h3>Allowed Vehicles</h3>
            <form id={"vehicle-add-container"}
                  onSubmit={vehicleFormik.handleSubmit}>
                <TextField
                    sx={{
                        width: "75%",
                    }}
                    id="licence-plate"
                    name="licencePlate"
                    label="Licence Plate"
                    value={vehicleFormik.values.licencePlate}
                    onChange={vehicleFormik.handleChange}
                    onBlur={vehicleFormik.handleBlur}
                    error={vehicleFormik.touched.licencePlate && Boolean(vehicleFormik.errors.licencePlate)}
                    helperText={vehicleFormik.touched.licencePlate && vehicleFormik.errors.licencePlate}
                />
                <Button
                    onClick={handleAddVehicle}
                    sx={{
                        fontSize: "18px",
                        textTransform: "capitalize",
                        width: "25%",
                        maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                    }}
                    variant="contained">
                    Add Vehicle
                </Button>
            </form>
            <Box id={"added-vehicles-container"}>
                {vehicles.map((vehicle, index) => (
                    <div key={index} className="added-vehicle-item">
                        <div className="added-mode-vehicle-info">
                            <span>{`Vehicle: ${vehicle}`}</span>
                        </div>
                        <Button
                            onClick={() => handleRemoveVehicles(index)}
                            variant="outlined"
                            sx={{textTransform: 'capitalize', maxHeight: '40px'}}
                        >
                            Remove
                        </Button>
                    </div>
                ))}
            </Box>
            <div id={"image-upload"}>
                <img alt={'Not uploaded'} src={deviceImagePreview || formik.values.deviceImage}/>

                <Button variant='contained'
                        sx={{
                            fontSize: "16px",
                            textTransform: "capitalize",
                            maxWidth: '180px', maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                        }}
                        component='label'>
                    Upload image
                    <input
                        name='deviceImage'
                        accept='image/*'
                        id='contained-button-file'
                        type='file'
                        hidden
                        onChange={(e) => {
                            const selectedFile = e.target.files[0];
                            formik.setFieldValue('deviceImage', selectedFile);
                            setDeviceImagePreview(URL.createObjectURL(selectedFile));
                        }}
                    />
                </Button>
                {formik.touched.deviceImage && formik.errors.deviceImage && (
                    <p className="error-message">{formik.errors.deviceImage.toString()}</p>
                )}
            </div>
            <Button
                sx={{
                    fontSize: "20px",
                    textTransform: "capitalize"
                }}
                variant="contained"
                type="submit">
                Add
            </Button>
        </form>
    )
}