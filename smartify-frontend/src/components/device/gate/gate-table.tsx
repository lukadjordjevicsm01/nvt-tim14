import React, {useEffect, useRef, useState} from "react";
import {GateEventRecord} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import DateTimeRangePicker from "../../util/date-time-range-picker.tsx";
import WebSocketService from "../../../util/web-socket-service.ts";

export default function GateTable({gateId} : GateTableProps){

    const [gateEventRecords, setGateEventRecords]
        = useState<GateEventRecord[]>([])

    const [fromTo, setFromTo]
        = useState<FromTo>({from: "-1h", to: "now()"})

    const fromToRef = useRef(fromTo);
    fromToRef.current = fromTo;

    useEffect(() => {
        getGateEventRecords()
        const subscription = WebSocketService.subscribeToTopic(`/gate/${gateId}`, payload => {
            console.log('Received message:', payload);
            if (payload.command === 'event'){
                const currentFromTo = fromToRef.current;
                if (currentFromTo.to === "now()")
                    setGateEventRecords(prevGateEventRecords => {
                        return [...prevGateEventRecords, payload.payload];
                    });
            }
        });

        return () => {
            if (subscription)
                subscription.unsubscribe();
        };
    }, []);

    const getGateEventRecords = (from: string = "-1h", to: string = "now()") => {
        api.get<GateEventRecord[]>(`/device/gate/getGateEvents/${gateId}`,
          {
              params: {from: from, to: to}
          })
          .then(res => {
              if (res.status === 200) {
                  setGateEventRecords(res.data);
              }
          })
          .catch((error) => toast.error(error.response.data.message));
    }

    const getFromTo = (from: string = "-1h", to: string = "now()") => {
      setFromTo({from: from, to: to});
    }

    useEffect(() => {
        getGateEventRecords(fromTo.from, fromTo.to)
    }, [fromTo]);

    const dateOptions: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false
    };

    const columns: GridColDef[] = [
        { field: 'action', headerName: 'Action', width: 350, sortable: false },
        { field: 'subject', headerName: 'Subject', width: 250, sortable: false },
        {
            field: 'timestamp',
            headerName: 'Time',
            type: 'Date',
            valueFormatter: params => new Date(params?.value).toLocaleString("ru-RU",
                dateOptions),
            width: 250
        },
    ];


    return(
      <div style={{
          width: '100%',
          height: 'calc(100vh - 200px)',
          display: 'flex',
          flexDirection: 'column',
          gap: '5px'
      }}>
          <DateTimeRangePicker getMeasurement={getFromTo} flag={"none"}/>
          <DataGrid
            getRowId={(row) => row.timestamp}
            rows={gateEventRecords}
            columns={columns}
            initialState={{
                pagination: {
                    paginationModel: {page: 0, pageSize: 10},
                },
                sorting: {
                    sortModel: [{field: 'timestamp', sort: 'desc'}]
                }
            }}
            pageSizeOptions={[5, 10, 25]}
          />
      </div>
    )
}


interface GateTableProps {
    gateId: number
}

interface FromTo{
    from: string,
    to: string
}