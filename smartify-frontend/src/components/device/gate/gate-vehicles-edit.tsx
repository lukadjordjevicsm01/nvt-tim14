import {Box, Button, TextField} from "@mui/material";
import * as yup from "yup";
import {useFormik} from "formik";
import {useState} from "react";


export default function GateVehiclesEdit ({changeAllowedVehicles, currentlyAllowedVehicles}){

    const [vehicles, setVehicles] = useState<string[]>(currentlyAllowedVehicles);
    const [changed, setChanged] = useState<boolean>(false);

    const vehicleValidationSchema = yup.object({
        licencePlate: yup
            .string()
            .min(7, "Licence plate must contain 7 characters at least")
            .max(10, "Licence plate must contain 10 characters at most"),

    });

    const vehicleFormik = useFormik({
        initialValues: {
            licencePlate: '',
        },
        validationSchema: vehicleValidationSchema,
        onSubmit:() => {
            handleAddVehicle()
        }
    })

    const handleAddVehicle = () => {
        const licencePlate = document.getElementById('licence-plate') as HTMLInputElement;

        if (licencePlate.value == '') return;

        if (licencePlate) {

            if (arraysEqualIgnoreOrder([...vehicles, licencePlate.value], currentlyAllowedVehicles))
                setChanged(false);
            else
                setChanged(true);
            setVehicles([...vehicles, licencePlate.value]);
            licencePlate.value = '';
            vehicleFormik.setFieldValue('licencePlate', '');
        }
    };

    const handleRemoveVehicles = (indexToRemove: number) => {
        const updatedVehicles = vehicles.filter((_vehicle, index) => index !== indexToRemove);
        if (arraysEqualIgnoreOrder(updatedVehicles, currentlyAllowedVehicles))
            setChanged(false);
        else
            setChanged(true);
        setVehicles(updatedVehicles);
    };

    function arraysEqualIgnoreOrder(arr1: string[], arr2: string[]): boolean {
        if (arr1.length !== arr2.length) {
            return false;
        }

        const set1 = new Set(arr1);
        const set2 = new Set(arr2);

        for (const element of set1) {
            if (!set2.has(element)) {
                return false;
            }
        }

        return true;
    }

    return(
        <div style={{
            width: "100%",
            height: 'calc(100vh - 200px)',
            display: 'flex',
            flexDirection: 'column',
            gap: '30px'
        }}>
            <h2>Allowed Vehicles</h2>
            <form style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "flex-start",
                width: "100%"
            }}
                  onSubmit={vehicleFormik.handleSubmit}>
                <TextField
                    sx={{
                        width: "75%",
                    }}
                    id="licence-plate"
                    name="licencePlate"
                    label="Licence Plate"
                    value={vehicleFormik.values.licencePlate}
                    onChange={vehicleFormik.handleChange}
                    onBlur={vehicleFormik.handleBlur}
                    error={vehicleFormik.touched.licencePlate && Boolean(vehicleFormik.errors.licencePlate)}
                    helperText={vehicleFormik.touched.licencePlate && vehicleFormik.errors.licencePlate}
                />
                <Button
                    onClick={handleAddVehicle}
                    sx={{
                        fontSize: "18px",
                        textTransform: "capitalize",
                        width: "25%",
                        maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                    }}
                    variant="contained">
                    Add Vehicle
                </Button>
            </form>
            <Box sx={{
                display: "flex",
                gap: "10px",
                flexWrap: "wrap"
            }}>
                {vehicles.map((vehicle, index) => (
                    <div key={index} className="added-vehicle-item">
                        <div className="added-mode-vehicle-info">
                            <span>{`Vehicle: ${vehicle}`}</span>
                        </div>
                        <Button
                            onClick={() => handleRemoveVehicles(index)}
                            variant="outlined"
                            sx={{textTransform: 'capitalize', maxHeight: '40px'}}
                        >
                            Remove
                        </Button>
                    </div>
                ))}
            </Box>
            <Button
                variant="contained"
                sx={{textTransform: 'capitalize',
                    height: '60px',
                    width: "50%",
                }}
                disabled={!changed}
                onClick={() => changeAllowedVehicles(vehicles)}
            >
                Apply changes
            </Button>
        </div>
    )
}