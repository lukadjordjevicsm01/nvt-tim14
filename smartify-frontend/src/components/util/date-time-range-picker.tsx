import {Box, Button, Dialog, DialogContent, MenuItem, Select} from "@mui/material";
import React, {useEffect, useState} from "react";
import {addDays, addHours, subDays, differenceInHours} from "date-fns";
import {DateTimePicker} from "@mui/x-date-pickers";

export default function DateTimeRangePicker({getMeasurement, flag})  {

    const [timespanFrom, setTimespan] = useState<string>(
        flag === "deviceAvailability" ? "-6h" : "-1h")
    const [openDateTimeRange, setOpenDateTimeRange] = useState<boolean>(false)

    const [dateTo, setDateTo] = useState<Date>(new Date(Date.now()))
    const [dateFrom, setDateFrom] = useState<Date>(subDays(new Date(Date.now()), 1))
    
    const aggregationWindowMap: Map<string, string> = new Map();
    aggregationWindowMap.set("-1h", "none");
    aggregationWindowMap.set("-6h", "1h");
    aggregationWindowMap.set("-12h", "1h");
    aggregationWindowMap.set("-24h", "1h");
    aggregationWindowMap.set("-168h", "1d");
    aggregationWindowMap.set("-720h", "1d");
    
    const handleDateRangeButtonClick = () => {
        const from = dateFrom.toISOString();
        const to = dateTo.toISOString();
        if (flag === "aggregate" || flag === "deviceAvailability"){
            getMeasurement(from.toString(), to.toString(), calculateAggregationWindow());
            return;
        }
        getMeasurement(from.toString(), to.toString())
        setOpenDateTimeRange(false);
    }

    useEffect(() => {
        if (timespanFrom !== "custom") {
            if (flag === "aggregate" || flag === "deviceAvailability"){
                getMeasurement(timespanFrom, "now()", calculateAggregationWindow());
                return;
            }
            getMeasurement(timespanFrom)
        } else {
            setOpenDateTimeRange(true)
        }

    }, [timespanFrom])
    
    
    const calculateAggregationWindow = (): string => {
        if (timespanFrom !== "custom"){
            console.log("Aggr window: ", aggregationWindowMap.get(timespanFrom), "Ts from: ", timespanFrom );
            return aggregationWindowMap.get(timespanFrom);
        }
        
        if (differenceInHours(dateTo, dateFrom) > 48)
            return "1d"
        
        return "1h"
        
    }

    return (
        <>
            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                label="Timespan"
                value={timespanFrom}
                onChange={(e) => setTimespan(e.target.value as string)}
                sx={{
                    width: "20%"
                }}>
                {flag !== "deviceAvailability" &&<MenuItem value="-1h">🕒 Past 1h</MenuItem>}
                <MenuItem value="-6h">🕒 Past 6h</MenuItem>
                <MenuItem value="-12h">🕒 Past 12h</MenuItem>
                <MenuItem value="-24h">🕒 Past 24h</MenuItem>
                <MenuItem value="-168h">🕒 Past week</MenuItem>
                <MenuItem value="-720h">🕒 Past month</MenuItem>
                <MenuItem value="custom">🕒 Custom timespan </MenuItem>
            </Select>

            <Dialog
                open={openDateTimeRange}
                onClose={() => setOpenDateTimeRange(false)}
                fullWidth>
                <DialogContent>
                    <h2>Pick your desired timespan</h2>
                    <Box sx={{
                        display: "flex",
                        gap: "20px",
                        marginTop: "20px",
                        marginBottom: "20px"
                    }}>
                        <DateTimePicker
                            label={"Select start date"}
                            disableFuture
                            value={dateFrom}
                            ampm={false}
                            format={"dd.MM.yyyy, HH:mm"}
                            onChange={(newValue) => setDateFrom(newValue)}
                        />
                        <p> - </p>
                        <DateTimePicker
                            label={"Select end date"}
                            disableFuture
                            value={dateTo}
                            ampm={false}
                            format={"dd.MM.yyyy, HH:mm"}
                            minDateTime={addHours(dateFrom, 1)}
                            maxDateTime={addDays(dateFrom, 30)}
                            onChange={(newValue) => setDateTo(newValue)}
                        />
                    </Box>
                    <Button
                        sx={{
                            fontSize: "20px",
                            textTransform: "capitalize"
                        }}
                        variant="contained" fullWidth type="submit"
                        onClick={handleDateRangeButtonClick}
                    >
                        Confirm
                    </Button>
                </DialogContent>
            </Dialog>
        </>
    )
}