import {Box, Button, Dialog, DialogContent, TextField, Typography} from "@mui/material";
import React, {useEffect, useState} from "react";
import * as yup from "yup";
import {useFormik} from "formik";
import {User} from "../../model/user-model.tsx";
import api from "../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {imagesUrl} from "../../util/environment.ts";

export default function Permissions({entityId, type}) {

    const [sharedWith, setSharedWith] = useState<User[]>([]);
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [selectedUser, setSelectedUser] = useState<User>()

    const getSharedWith = () => {
        api.get<User[]>(`/${type}/getSharedWith/${entityId}`)
            .then(res => {
                if (res.status === 200) {
                    console.log(res.data)
                    setSharedWith(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const handleXClick = (user: User) => {
        api.post(`/${type}/revokePermission/${entityId}`, {}, {
            params: {
                userEmail: user.email
            }
        })
            .then(res => {
                if (res.status === 200) {
                    console.log(res.data);
                    setSharedWith(current =>
                        current.filter(u =>
                            u.email != res.data.email
                        )
                    );
                    setSelectedUser(null);
                    setDialogOpen(false);
                    toast.success("Permission revoked.")
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    useEffect(() => {
        getSharedWith();
    }, [])

    useEffect(() => {
        if (selectedUser)
            setDialogOpen(true)
    }, [selectedUser])

    const validationSchema = yup.object({
        email: yup
            .string()
            .test('is-email', 'Enter a valid email', (value) => {
                if (value === 'admin') {
                    return true;
                }

                try {
                    yup.string().email().validateSync(value, { abortEarly: true });
                    return true;
                } catch (error) {
                    return false;
                }
            })
            .required('Email is required'),
    });

    const formik = useFormik({
        initialValues: {
            email: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            api.post(`/${type}/grantPermission/${entityId}`, {}, {
                params: {
                    userEmail: values.email
                }
            })
                .then(res => {
                    if (res.status === 200) {
                        console.log(res.data);
                        setSharedWith(current => [...current, res.data]);
                        toast.success("Permission granted.")
                    }
                })
                .catch((error) => toast.error(error.response.data.message));
        },
    });

    return (
        <Box
            sx={{
                display: "flex",
                flexDirection: "column",
                gap: "10px"
            }}
        >
            <h2>Permissions</h2>
            <form
                onSubmit={formik.handleSubmit}
                style={{
                    display: "flex",
                    flexDirection: "row",
                    gap: "10px",
                    width: "100%"
                }}
            >
                <TextField
                    id="email"
                    name="email"
                    label="Email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                    sx={{
                        width: "100%",
                        input: {
                            height: "27px"
                        }
                    }}
                />
                <Button
                    sx={{
                        maxWidth: "60px",
                        minWidth: "60px",
                        maxHeight: "60px",
                        minHeight: "60px"
                    }}
                    variant="contained"
                    type="submit">
                    Add
                </Button>
            </form>

            <h3>Granted permissions</h3>
            <Box
                sx={{
                    overflow: "auto",
                    height: "300px"
                }}
            >
                { sharedWith.map((user) =>
                    <Box
                        key={user.id}
                        sx={{
                            display: "flex",
                            flexDirection: "row",
                            backgroundColor: "#739FDF",
                            width: "fit-content",
                            borderRadius: "10px",
                            padding: "10px",
                            alignItems: "center",
                            gap: "10px"
                        }}
                    >
                        <img
                            style={{
                                width: "50px",
                                height: "50px",
                                borderRadius: "50%"
                            }}
                            src={`${imagesUrl}/user/${user.profilePicturePath}`}
                            alt="Picture not found"
                        />
                        <Box
                            sx={{
                                display: "flex",
                                flexDirection: "column"
                            }}
                        >
                            <Typography>{user.name} {user.surname}</Typography>
                            <Typography>{user.email}</Typography>
                        </Box>
                        <Button
                            sx={{
                                maxWidth: "20px",
                                minWidth: "20px",
                                maxHeight: "20px",
                                minHeight: "20px"
                            }}
                            variant="contained"
                            onClick={() => setSelectedUser(user)}
                        >
                            X
                        </Button>
                    </Box>
                )}

                <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
                    <DialogContent
                        sx={{
                            display: "flex",
                            flexDirection: "column",
                            gap: "10px"
                        }}
                    >
                        <h2>Warning</h2>
                        <Typography>Are you sure you want to revoke this permission?</Typography>
                        <Box
                            sx={{
                                display: "flex",
                                gap: "10px"
                            }}
                        >
                            <Button
                                variant="contained"
                                sx={{
                                    backgroundColor: "primary.main",
                                    color: "secondary.main",
                                }}
                                onClick={() => handleXClick(selectedUser)}
                            >
                                Yes
                            </Button>
                            <Button
                                variant="contained"
                                sx={{
                                    backgroundColor: "primary.main",
                                    color: "secondary.main",
                                }}
                                onClick={() => {
                                    setSelectedUser(null);
                                    setDialogOpen(false);
                                }}
                            >
                                No
                            </Button>
                        </Box>
                    </DialogContent>
                </Dialog>

            </Box>
        </Box>
    )
}