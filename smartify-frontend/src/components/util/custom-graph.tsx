import {Line} from "react-chartjs-2";
import React, {useEffect, useState} from "react";
import {AmbientSensorRecord, MeasurementRecord} from "../../model/device-model.tsx";

export default function CustomGraph({title, measurements, color, aggregationWindow}: CustomGraphProps) {
    const [data , setData] = useState<{
        labels: string[],
        datasets: {
            label: string,
            data: number[],
            borderColor: string,
            backgroundColor: string }[]
    }>({
        datasets: [],
        labels: []
    });
    const [labels, setLabels] = useState<string[]>([]);

    const dateOptionsHour: Intl.DateTimeFormatOptions = {
        hour: '2-digit',
        minute: '2-digit',
        hour12: false
    };

    const dateOptionsDay: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: '2-digit',
        year: '2-digit'
    };

    useEffect(() => {
        const dateOptions: Intl.DateTimeFormatOptions = aggregationWindow == "1d" ? dateOptionsDay : dateOptionsHour;
        // const { min, max } = measurements.reduce(
        //     (result, current) => {
        //         if (current.timestamp < result.min.timestamp) {
        //             result.min = current;
        //         }
        //         if (current.timestamp > result.max.timestamp) {
        //             result.max = current;
        //         }
        //         return result;
        //     },
        //     {
        //         min: measurements[0],
        //         max: measurements[0],
        //     }
        // );
        // if (max.timestamp - min.timestamp > 24 * 60 * 60 * 1000)
        //     dateOptions = dateOptionsDay
        setLabels(measurements.map(record => {
            return new Date(record.timestamp).toLocaleString("ru-RU", dateOptions);
        }));
    }, [measurements])

    useEffect(() => {
        createChartData()
    }, [labels])

    const options = {
        maintainAspectRatio: false,
        ticks: {
            autoSkip: true,
            maxTicksLimit: 10
        },
        plugins: {
            legend: {
                position: 'top' as const,
            },
            title: {
                display: false,
                text: 'Real time graph',
            },
        },
    };

    const createChartData = () => {
        const displayValues =  measurements.map(record => record.measurement);
        const newData = {
            labels: labels,
            datasets: [
                {
                    label: title,
                    data: displayValues,
                    borderColor: color, //'#DD3636',
                    backgroundColor: color // '#DD3636',
                }]
        }
        setData(newData);
    }

    return (
        <>
            <Line data={data} options={options}/>
        </>
    )
}

interface CustomGraphProps {
    title: string,
    measurements: AmbientSensorRecord[] | MeasurementRecord[],
    color: string,
    aggregationWindow: string
}