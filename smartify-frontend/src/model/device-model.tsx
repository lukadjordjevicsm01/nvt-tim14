export interface DeviceCategory {
    name: string;
    devices: string[];
}

export interface Device {
    id: number,
    online: boolean,
    on: boolean,
    name: string,
    powerSupplyType: string,
    consumption: number,
    image: File | null,
    imagePath: string,
    deviceType: string
}

// =============================================== HOME DEVICES ========================================================

export interface AmbientSensor extends Device{
    temperature: number | null,
    humidity: number | null
}

export interface AmbientSensorRecord {
    measurement: number,
    timestamp: number
}

export interface MeasurementRecord {
    measurement: number,
    timestamp: number
}

export interface AirConditioning extends Device{
    currentTemperature: number | null,
    minTemperature: number,
    maxTemperature: number,
    currentMode: string | null,
    supportedModes: string[],
    settingsActive: boolean | null,
    customSettings: AirConditioningSettings[] | null
}

export interface AirConditioningSettings {
    id: number,
    startTime: string,
    endTime: string,
    mode: string,
    temperature: number
}

export interface AirConditioningRecord {
    id: number,
    subject: string,
    currentTemperature: number,
    action: string,
    timestamp: number
}

export interface WashingMachine extends Device{
    currentMode: WashingMachineMode | null,
    supportedModes: WashingMachineMode[],
    settingsActive: boolean | null,
    customSettings: WashingMachineSetting[] | null

}

export interface WashingMachineMode{
    name: string,
    duration: number
}

export interface WashingMachineSetting {
    mode: WashingMachineMode,
    startTime: string
}

export interface WashingMachineRecord {
    id: number,
    subject: string,
    action: string,
    timestamp: number
}

// =============================================== OUTSIDE DEVICES =====================================================

export interface Lamp extends Device{
    currentBrightness: number | null,
    automatic: boolean | null,
    lightOn: boolean | null
}

export interface Gate extends Device{
    open: boolean | null,
    mode: string | null,
    allowedVehicles: string[]
}

export interface GateEventRecord{
    action: string,
    subject: string,
    timestamp: number
}

export interface Sprinkler extends Device{
    sprinklerSettings: SprinklerSetting[],
    sprinklerOn: boolean
}

export interface SprinklerSetting {
    id: number | null
    startTime: string,
    endTime: string,
    repeatDays: string[]
}

export interface SprinklerEventRecord {
    action: string,
    subject: string,
    setting: string
    timestamp: number
}
// ========================================== ELECTRO ENERGETIC DEVICES ================================================

export interface SolarPanelSystem extends Device{
    solarPanels: SolarPanel[]
}

export interface SolarPanelSystemEventRecord{
    action: string,
    subject: string,
    timestamp: number
}

export interface SolarPanel extends Device{
    area: number,
    efficiency: number
}

export interface HomeBattery extends Device{
    capacity: number,
    currentEnergy: number | null
}


export interface VehicleCharger extends Device {
    chargingPower: number,
    socketNumber: number,
    vehicleChargerSockets: VehicleChargerSocket[] | null
}

export interface VehicleChargerRecord extends Device {
    vehicleChargerId: number,
    vehicleChargerSocketName: string,
    message: string,
    subject: string,
    chargingDuration: string,
    finished: boolean
}

export interface VehicleChargerSocket {
    name: string,
    active: boolean | null,
    chargingLimit: number
}

export interface CurrentBatteryPercentage {
    vehicleChargerId: number,
    chargeSocketName: string,
    currentBatteryPercentage: number
}

export interface ElectricityUsage {
    consumed: number,
    produced: number
}

export interface DeviceAvailabilityRecord {
    onlineTime: number,
    offlineTime: number,
    onlinePercentage: number,
    offlinePercentage: number,
    intervalStart: number,
    intervalEnd: number,
    unit: string
}