import {User} from "./user-model.tsx";
import {Device} from "./device-model.tsx";

export interface Country {
    countryCode: string;
    name: string;
}

export interface City {
    id: number,
    name: string
}


export interface StatusChange {
    realEstateId: number,
    status: string,
    reason: string
}

export interface RealEstate {
    id: number,
    realEstateType: string,
    status: string,
    floors: number,
    name: string,
    address: Address,
    userDTO: User
}

export interface Address {
    latitude: number,
    longitude: number,
    streetName: string,
    countryDTO: Country,
    cityDTO: City
}

export interface RealEstateDevices {
    realEstateId: number,
    devices: Device[]
}
