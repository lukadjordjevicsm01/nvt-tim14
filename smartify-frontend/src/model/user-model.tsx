export interface UserTokenState{
    accessToken: string,
    expiresIn: number
}

export interface User {
    id: number,
    name: string,
    surname: string,
    email: string,
    profilePicturePath: string
}

export interface PasswordChange {
    email: string,
    newPassword: string
}