import {jwtDecode} from "jwt-decode";

export function getUserId(): number {
    if (isLoggedIn()) {
        const userString: any = localStorage.getItem('user');
        const user = JSON.parse(userString);
        // @ts-ignore
        return jwtDecode(user.accessToken).id;
    }
    return -1;
}

export function isLoggedIn(): boolean {
    return localStorage.getItem('user') != null;
}

export function getUserMail(): string {
    if (isLoggedIn()) {
        const userString: any = localStorage.getItem('user');
        const user = JSON.parse(userString);
        // @ts-ignore
        return jwtDecode(user.accessToken).sub;
    }
    return "";
}

export function getRole(): any {
    if (isLoggedIn()) {
        const userString: any = localStorage.getItem('user');
        const user = JSON.parse(userString);
        // @ts-ignore
        const roles = jwtDecode(user.accessToken).role;

        return roles.map((obj: { name: any; }) => obj.name);
    }
    return null;
}

export function getUserPasswordResetDate(): number {
    if (isLoggedIn()) {
        const userString: any = localStorage.getItem('user');
        const user = JSON.parse(userString);
        // @ts-ignore
        return jwtDecode(user.accessToken).prdate;
    }
    return -1;
}

export function logout() {
    localStorage.removeItem('user');
}