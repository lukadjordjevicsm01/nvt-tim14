import * as Stomp from 'stompjs';

import {socketUrl} from "./environment.ts";
import {Client, Message} from "stompjs";
import SockJS from "sockjs-client";

class WebSocketService {

  private stompClient: Client;
  private isLoaded: boolean = false;

  constructor() {
    const ws = new SockJS(socketUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;

    this.stompClient.connect({}, function () {
      that.isLoaded = true;
    });

  }
  subscribeToTopic(topic: string, callback: (payload: any) => void) {
    if (this.isLoaded){
      return this.stompClient.subscribe(topic, (message: Message) => {
        const payload = JSON.parse(message.body);
        callback(payload);
      })
    }
  }

  // Add more methods as needed for your application
}

export default new WebSocketService();