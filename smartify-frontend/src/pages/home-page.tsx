import {useEffect, useState} from "react";
import {getRole, getUserId, getUserMail, getUserPasswordResetDate, logout} from "../util/token-utils.tsx";
import {RealEstate, RealEstateDevices, StatusChange} from "../model/real-estate-model.tsx";
import api from "../config/axios-config.tsx";
import {Button, Dialog, DialogContent, IconButton, InputAdornment, TextField} from "@mui/material";
import {useNavigate} from "react-router-dom";
import "./home-page.css";
import RealEstateCard from "../components/realestate/real-estate-card.tsx";
import toast from "react-hot-toast";
import * as yup from "yup";
import {useFormik} from "formik";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import {PasswordChange} from "../model/user-model.tsx";
import {Device} from "../model/device-model.tsx";

export default function HomePage (){

    const [role, setRole] = useState<any>(null);
    const [openChangePassword, setOpenChangePassword] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);

    const [realEstates, setRealEstates]
        = useState<RealEstate[]>([])

    const navigate = useNavigate();

    useEffect(() => {
        setRole(getRole());
    }, [])

    useEffect(() => {
        if (role === null) return;
        let url = `realEstate/${getUserId()}`;
        if (role.includes('ROLE_ADMIN')) {
            url = "realEstate/pending";
            if (getUserPasswordResetDate() == 0) handleOpenChangePassword();
        }
        fetchRealEstates(url);

    }, [role]);

    const fetchRealEstates = (url: string) => {
        api.get<RealEstate[]>(url)
            .then(res => {
                if (res.status == 200){
                    setRealEstates(res.data);
                }
            })
            .catch(error => toast.error(error.response.data.message))
    }

    const handleStatusChange = (statusChange: StatusChange) => {
        api.post(`realEstate/changeStatus`, statusChange)
            .then(res => {
                    if (res.status == 200){
                        setRealEstates((prevRealEstates) => {
                            let updatedRealEstates = prevRealEstates.map((estate) =>
                                estate.id === statusChange.realEstateId ? { ...estate, status: statusChange.status } : estate
                            );
                            if (role !== null && role.includes('ROLE_ADMIN')){
                                updatedRealEstates = prevRealEstates.filter((estate) =>
                                    estate.id !== statusChange.realEstateId
                                );
                            }
                            return updatedRealEstates;
                        });
                        toast.success("Status changed successfully");
                    }
                }
            )
            .catch(error => toast.error(error.response.data.message))
    }

    const handleOpenChangePassword = () => {
        setOpenChangePassword(true);
    };

    const handleCloseChangePassword = () => {
        setOpenChangePassword(false);
        logout();
        navigate("/login");
    };

    const handleClickShowPassword = () => setShowPassword(!showPassword);

    const handleClickShowConfirmPassword = () => setShowConfirmPassword(!showConfirmPassword);

    const validationSchema = yup.object({
        password: yup
            .string()
            .min(8, 'Password should be of minimum 8 characters length')
            .matches(
                /^(?=.*[A-Za-z])(?=.*\d)/,
                'Password must contain numbers and letters'
            )
            .required('Password is required'),
        confirmPassword: yup
            .string()
            .oneOf([yup.ref('password'), undefined], 'Passwords must match')
            .required('Confirm Password is required')
    })

    const formik = useFormik({
        initialValues: {
            password: '',
            confirmPassword: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const passwordChange: PasswordChange = {
                email: getUserMail(),
                newPassword: values.password
            }

            api.post("user/change-password", passwordChange)
                .then(res => {
                    if (res.status === 200) {
                        toast.success("Password successfully changed.");
                        handleCloseChangePassword();
                }})
                .catch((error) => {
                    toast.error(error.response.data.message);
                });
        }
    })

    return(
        <>
            {role != null && role.includes('ROLE_USER') &&
                <div id="home-page-main-container">
                    <div id="home-page-heading-container">
                        <h1>My Real estates</h1>
                        <Button variant="contained"
                                onClick={() => navigate('/registerRealEstate')}
                                sx={{
                                    textTransform: "capitalize",
                                    maxWidth: '180px', maxHeight: '50px', minWidth: '180px', minHeight: '50px'
                                    }}>
                            Add new
                        </Button>
                    </div>
                    <div id="home-page-real-estate-container">
                        {realEstates.map(realEstate =>
                            <RealEstateCard
                                key={realEstate.id} realEstate={realEstate}
                                admin={false}
                                handleStatusChange={handleStatusChange}
                                shared={false}
                            />)}
                    </div>
                    {realEstates.length === 0 &&
                        <h2 style={{textAlign:'center', width:'100%'}}>You do not have any real estates currently</h2>}

                </div>}
            {role != null && role.includes('ROLE_ADMIN') &&
                <div id="home-page-main-container">
                    <div id="home-page-heading-container">
                        <h1>Real estate requests</h1>
                    </div>
                    <div id="home-page-real-estate-container">
                        {realEstates.map(realEstate =>
                            <RealEstateCard
                                key={realEstate.id}
                                realEstate={realEstate}
                                admin={true}
                                handleStatusChange={handleStatusChange}
                                shared={false}
                            />)}
                    </div>
                    {realEstates.length === 0 &&
                        <h2 style={{textAlign:'center', width:'100%'}}>You do not have any pending real estate requests</h2>}

                </div>
            }

            <Dialog
                open={openChangePassword}
                fullWidth>
                <DialogContent>
                    <h2 style={{marginBottom: "15px"}}>Change the default password</h2>
                    <form onSubmit={formik.handleSubmit}>
                        <TextField
                            fullWidth
                            id="password"
                            name="password"
                            label="Password"
                            type={showPassword ? 'text' : 'password'}
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.password && Boolean(formik.errors.password)}
                            helperText={formik.touched.password && formik.errors.password}
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}/>
                        <TextField
                            fullWidth
                            id="confirmPassword"
                            name="confirmPassword"
                            label="Confirm password"
                            type={showConfirmPassword ? 'text' : 'password'}
                            value={formik.values.confirmPassword}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                            helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle confirm password visibility"
                                            onClick={handleClickShowConfirmPassword}
                                        >
                                            {showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}
                        />

                        <Button
                            sx={{
                                fontSize: "20px",
                                textTransform: "capitalize"
                            }}
                            variant="contained" fullWidth type="submit">
                            Change password
                        </Button>
                    </form>
                </DialogContent>
            </Dialog>
        </>

    )
}