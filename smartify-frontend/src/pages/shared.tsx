import {Box, ToggleButton, ToggleButtonGroup} from "@mui/material";
import React, {useEffect, useState} from "react";
import RealEstateCard from "../components/realestate/real-estate-card.tsx";
import api from "../config/axios-config.tsx";
import toast from "react-hot-toast";
import {RealEstate} from "../model/real-estate-model.tsx";
import {getUserId} from "../util/token-utils.tsx";

export default function Shared() {
    const [display, setDisplay] = useState('realEstates');
    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
        if (newDisplay === "devices")
            getSharedDeviceRealEstates();
    };

    const [realEstates, setRealEstates] = useState<RealEstate[]>([]);
    const [deviceRealEstates, setDeviceRealEstates] = useState<RealEstate[]>([]);

    const getSharedDeviceRealEstates = () => {
        api
            .get<RealEstate[]>(`realEstate/getSharedDeviceRealEstates/${getUserId()}`)
            .then((res) => {
                if (res.status === 200) {
                    console.log(res.data);
                    setDeviceRealEstates(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const getSharedRealEstates = () => {
        api
            .get<RealEstate[]>(`realEstate/getSharedRealEstates/${getUserId()}`)
            .then((res) => {
                if (res.status === 200) {
                    console.log(res.data);
                    setRealEstates(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    useEffect(() => {
        getSharedRealEstates();
    }, []);

    return (
        <Box id="home-page-main-container">
            <ToggleButtonGroup
                color="primary"
                exclusive
                value={display}
                onChange={handleToggleChange}
                aria-label="Platform"
                sx={{
                    padding: "15px"
                }}
            >
                <ToggleButton value="realEstates">Real estates</ToggleButton>
                <ToggleButton value="devices">Devices</ToggleButton>
            </ToggleButtonGroup>


            {display == "realEstates" && realEstates.length === 0 &&
                <h2
                    style={{
                        textAlign:'center',
                        width:'100%'
                }}
                >
                    You do not have any shared real estates.
                </h2>
            }
            {display == "realEstates" && realEstates.length !== 0 &&
                <Box
                    sx={{
                        borderRadius: "5px",
                        padding: "15px",
                        width: "100%",
                        display: "flex",
                        flexDirection: "column",
                        gap: "5px",
                        boxShadow: "3px 3px 6px black",
                    }}
                >
                    {realEstates.map(realEstate =>
                            <RealEstateCard
                                realEstate={realEstate}
                                admin={false}
                                handleStatusChange={() => {}}
                                shared={false}
                            />
                        )
                    }
                </Box>
            }

            {display == "devices" && deviceRealEstates.length === 0 &&
                <h2
                    style={{
                        textAlign:'center',
                        width:'100%'
                    }}
                >
                    You do not have any shared devices.
                </h2>
            }
            {display == "devices" && deviceRealEstates.length !== 0 &&
                <Box
                    sx={{
                        borderRadius: "5px",
                        padding: "15px",
                        width: "100%",
                        display: "flex",
                        flexDirection: "column",
                        gap: "5px",
                    }}
                >
                    {deviceRealEstates.map(realEstate =>
                            <RealEstateCard
                                realEstate={realEstate}
                                admin={false}
                                handleStatusChange={() => {}}
                                shared={true}
                            />
                        )
                    }
                </Box>
            }

        </Box>
    )
}