import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {Lamp} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import DeviceData from "../../../components/device/device-data.tsx";
import WebSocketService from "../../../util/web-socket-service.ts";

import {
    Box,
    FormControl,
    FormControlLabel,
    FormLabel,
    Radio,
    RadioGroup,
    styled,
    Switch,
    ToggleButton,
    ToggleButtonGroup
} from "@mui/material";
import LampGraph from "../../../components/device/lamp/lamp-graph.tsx";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function Lamp() {
    const { lampId } = useParams();
    const [display, setDisplay] = useState('brightness');

    const [lamp, setLamp] = useState<Lamp>();

    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };


    useEffect(() => {
        api.get<Lamp>(`/device/lamp/getLamp/${lampId}`)
            .then(res => {
                if (res.status === 200) {
                    setLamp(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));

        const subscription = WebSocketService.subscribeToTopic(`/lamp/${lampId}`, payload => {
            console.log('Received message:', payload);
            if (payload.command === 'switchLight'){
                setLamp(prevLamp => {
                    return {...prevLamp, lightOn: payload.payload.lightOn};
                });
            }
        });

        return () => {
            if (subscription)
                subscription.unsubscribe();
        };
    }, []);

    function handleLightOnSwitch() {
        const on: boolean = !lamp.lightOn;
        const updatedLamp = { ...lamp, lightOn: !lamp.lightOn };
        setLamp(updatedLamp);
        api.post<any>(`/device/lamp/switchLight/${lampId}`, {}, {
            params: {on: on}
        })
          .then(res => {
              if (res.status === 200) {
                  toast.success(res.data.message);
              }
          })
          .catch((error) => toast.error(error.response.data.message));
    }

    function handleLampModeChange(mode: string) {
        const automatic: boolean = mode == "automatic";
        const updatedLamp = { ...lamp, automatic: automatic };
        setLamp(updatedLamp);

        api.post<any>(`/device/lamp/switchMode/${lampId}`, {}, {
            params: {automatic: automatic}
        })
          .then(res => {
              if (res.status === 200) {
                  toast.success(res.data.message);
              }
          })
          .catch((error) => toast.error(error.response.data.message));
    }

    return(
        <Box sx={{
            display: "flex",
            height: "calc(100vh - 100px)"
        }}>
                {lamp &&
                  <Box sx={{
                      display: "flex",
                      flexDirection: "column",
                      gap: "10px",
                      width: "25%",
                      height: "calc(100vh - 100px)",
                      overflow: "auto",
                      padding: "20px",
                  }}>
                      <DeviceData device={lamp}/>
                      <Box sx={{
                          padding: "0px 20px 20px 20px",
                          display: "flex",
                          alignItems: "start"
                      }}>
                          <FormControl>
                              <FormLabel id="demo-controlled-radio-buttons-group">Lamp mode</FormLabel>
                              <RadioGroup
                                aria-labelledby="demo-controlled-radio-buttons-group"
                                name="controlled-radio-buttons-group"
                                value={lamp.automatic ? "automatic" : "manual"}
                                onChange={(event, value) =>
                                  handleLampModeChange(value)}
                              >
                                  <FormControlLabel value="manual" control={<Radio/>} label="Manual"/>
                                  <FormControlLabel value="automatic" control={<Radio/>} label="Automatic"/>
                              </RadioGroup>
                          </FormControl>
                          {!lamp.automatic &&
                            <FormControlLabel control={<LightToggleSwitch
                              checked={lamp.lightOn}
                              onChange={() => handleLightOnSwitch()}
                              inputProps={{'aria-label': 'controlled'}}/>} label={lamp.lightOn ? 'On' : 'Off'}
                            />
                          }
                          <img src={lamp.lightOn ? "/img/light-bulb-on.png" :
                            "/img/light-bulb-off.png"} style={{
                              width: "70px",
                              height: "100px",
                              objectFit: "contain"
                          }}/>
                      </Box>
                  </Box>
                }
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                padding: "20px",
                width: "75%",
                height: "calc(100vh - 100px)",
                gap: "10px"
            }}>
                <ToggleButtonGroup
                  color="primary"
                  exclusive
                  value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    <ToggleButton value="brightness">Brightness graph</ToggleButton>
                    <ToggleButton value="availability">Availability</ToggleButton>
                </ToggleButtonGroup>

                {display === "brightness" && lamp && <LampGraph/>}

                {display === "availability" && lamp && <DeviceAvailabilityGraph deviceId={lamp.id}/>}

            </Box>
        </Box>
    )
}

const LightToggleSwitch = styled(Switch)(({ theme }) => ({
    width: 90,
    height: 40,
    padding: 7,
    marginRight: 5,
    display: "flex",
    alignItems: "center",
    '& .MuiSwitch-switchBase': {
        margin: 1,
        padding: 0,
        transform: 'translateX(6px)',
        '&.Mui-checked': {
            color: '#fff',
            transform: 'translateX(50px)',
            '& .MuiSwitch-thumb:before': {
                backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="30" width="30" viewBox="0 0 25 20"><path fill="${encodeURIComponent(
                  '#fff',
                )}" d="M9 21c0 .5.4 1 1 1h4c.6 0 1-.5 1-1v-1H9v1zm3-19C8.1 2 5 5.1 5 9c0 2.4 1.2 4.5 3 5.7V17c0 .5.4 1 1 1h6c.6 0 1-.5 1-1v-2.3c1.8-1.3 3-3.4 3-5.7 0-3.9-3.1-7-7-7z"/></svg>')`,
            },
            '& + .MuiSwitch-track': {
                opacity: 1,
            },
        },
    },
    '& .MuiSwitch-thumb': {
        width: 35,
        height: 35,
        borderRadius: "10px",
        marginTop: "2px",
        '&:before': {
            content: "''",
            position: 'absolute',
            width: '100%',
            height: '100%',
            left: 0,
            top: 0,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="30" width="30" viewBox="0 0 25 20"><path fill="${encodeURIComponent(
              '#fff',
            )}" d="M9,21c0,0.55,0.45,1,1,1h4c0.55,0,1-0.45,1-1v-1H9V21z M12,2C8.14,2,5,5.14,5,9c0,2.38,1.19,4.47,3,5.74V17 c0,0.55,0.45,1,1,1h6c0.55,0,1-0.45,1-1v-2.26c1.81-1.27,3-3.36,3-5.74C19,5.14,15.86,2,12,2z M14,13.7V16h-4v-2.3 C8.48,12.63,7,11.53,7,9c0-2.76,2.24-5,5-5s5,2.24,5,5C17,11.49,15.49,12.65,14,13.7z"/></svg>')`,
        },
    },
    '& .MuiSwitch-track': {
        borderRadius: 20 / 2,
    },
}));