import {
    Box,
    Button,
    capitalize,
    Checkbox,
    Dialog, DialogContent,
    FormControlLabel,
    styled,
    Switch, ToggleButton,
    ToggleButtonGroup,
    Typography
} from "@mui/material";
import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {Sprinkler, SprinklerSetting} from "../../../model/device-model.tsx";
import DeviceData from "../../../components/device/device-data.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {TimePicker} from "@mui/x-date-pickers";
import {addHours} from "date-fns";
import {DataGrid, GridColDef, GridValueGetterParams} from "@mui/x-data-grid";
import {getUserMail} from "../../../util/token-utils.tsx";
import WebSocketService from "../../../util/web-socket-service.ts";
import GateTable from "../../../components/device/gate/gate-table.tsx";
import GateVehiclesEdit from "../../../components/device/gate/gate-vehicles-edit.tsx";
import SprinklerTable from "../../../components/device/sprinkler/sprinkler-table.tsx";
import SprinklerSettingPopup from "../../../components/device/sprinkler/sprinkler-setting-popup.tsx";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function Sprinkler () {
    
    const { sprinklerId } = useParams();
    
    const [display, setDisplay] = useState('actions');
    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };
    
    const [sprinkler, setSprinkler] = useState<Sprinkler>();
    
    const [startTime, setStartTime] = useState<Date>(new Date());
    const [endTime, setEndTime] = useState<Date>(addHours(new Date(), 1));
    
    const [repeatingDays, setRepeatingDays] = useState({
        MONDAY: false,
        TUESDAY: false,
        WEDNESDAY: false,
        THURSDAY: false,
        FRIDAY: false,
        SATURDAY: false,
        SUNDAY: false
    });
    const handleCheckboxChange = (event) => {
        setRepeatingDays({ ...repeatingDays, [event.target.name]: event.target.checked });
    };
    const handleAddSetting = () => {
        const noDaySelected = Object.values(repeatingDays).every(value => value === false);
        if (noDaySelected){
            toast.error("Please select at least one day  to repeat this setting.");
            return;
        }
        
        const repeatingDaysValues = Object.entries(repeatingDays)
            .filter(([day, value]) => value === true)
            .map(([day]) => day);
        
        const sprinklerSetting: SprinklerSetting = {
            id: null,
            endTime: endTime.toISOString(),
            startTime: startTime.toISOString(),
            repeatDays: repeatingDaysValues
        }
        console.log(sprinklerSetting);
        
        api.post<SprinklerSetting>(`/device/sprinkler/addSetting/${sprinklerId}`, {
                addedBy: getUserMail(),
                endTime: endTime.getTime(),
                startTime: startTime.getTime(),
                repeatDays: repeatingDaysValues
            })
            .then(res => {
                if (res.status === 200) {
                    toast.success("Setting added.");
                    setSprinkler(prev => {
                        let settings = [...prev.sprinklerSettings]; // Convert to array
                        settings.push(res.data);
                        return {...prev, sprinklerSettings: settings};
                    });
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }
    
    const [selectedSetting, setSelectedSetting] = useState<SprinklerSetting | null>(null);
    const handleUpdateDeleteSetting = (updatedSetting: SprinklerSetting, update: boolean) => {
        setSelectedSetting(null);
        if (update){
            setSprinkler(prev => {
                let settings = [...prev.sprinklerSettings]; // Convert to array
                settings = settings.map(item => {
                    if (item.id === updatedSetting.id) return updatedSetting
                    
                    return item;
                })
                return {...prev, sprinklerSettings: settings};
            });
        } else {
            setSprinkler(prev => {
                let settings = [...prev.sprinklerSettings]; // Convert to array
                settings = settings.filter(setting => setting.id != updatedSetting.id);
                return {...prev, sprinklerSettings: settings};
            });
        }
        
        
    }
    
    const handleSprinklerSwitch = () => {
        const sprinklerOn: boolean = !sprinkler.sprinklerOn;
        
        api.post<any>(`/device/sprinkler/switchSprinkler/${sprinklerId}`, {}, {
                params: {sprinklerOn: sprinklerOn, email: getUserMail()}
            })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                    const updatedSprinkler = { ...sprinkler, sprinklerOn: sprinklerOn };
                    setSprinkler(updatedSprinkler);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }
    
    const dateOptions: Intl.DateTimeFormatOptions = {
        hour: '2-digit',
        minute: '2-digit',
        hour12: false
    };
    const columns: GridColDef[] = [
        {
            field: 'repeatDays',
            headerName: 'Days',
            width: 250,
            sortable: false,
            valueGetter: (params: GridValueGetterParams) =>{
                if (params.row.repeatDays.length === 7) return "Every day";
                const abbreviatedDays = params.row.repeatDays
                    .map(day => capitalize(day.substring(0, 3).toLowerCase()))
                    .sort((a, b) => {
                        const daysOfWeek = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
                        return daysOfWeek.indexOf(a) - daysOfWeek.indexOf(b);}
                    );
                return abbreviatedDays.join('  ');
            }
        },
        
        {
            field: 'startTime',
            headerName: 'Start',
            type: 'Date',
            valueFormatter: params => new Date(params?.value)
                .toLocaleString("ru-RU", dateOptions),
            width: 90
        },
        {
            field: 'endTime',
            headerName: 'End',
            type: 'Date',
            valueFormatter: params => new Date(params?.value)
                .toLocaleString("ru-RU", dateOptions),
            width: 90
        }
    ];
    
    useEffect(() => {
        api.get<Sprinkler>(`/device/sprinkler/get-sprinkler/${sprinklerId}`)
            .then(res => {
                if (res.status === 200) {
                    setSprinkler(res.data);
                    console.log("Sprinkler fetched -> ", res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
        
        const subscription = WebSocketService.subscribeToTopic(`/sprinkler/${sprinklerId}`, payload => {
            console.log('Received message:', payload);
            if (payload.command === 'switchSprinkler'){
                console.log("Socket sprinkler", payload.payload);
                setSprinkler(prevSprinkler => {
                    return {...prevSprinkler, sprinklerOn: payload.payload.sprinklerOn};
                });
            }
        });

        return () => {
            if (subscription)
                subscription.unsubscribe();
        };
    }, []);
    
    return (
        <Box
            sx={{
                display: "flex",
                paddingBottom: "20px",
                height: "100%"
            }}
        >
            {sprinkler &&
                <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "10px",
                        minWidth: "35%",
                        height: "calc(100vh - 100px)",
                        overflow: "auto",
                        padding: "20px",
                    }}
                >
                    <DeviceData device={sprinkler}/>

                    <Box sx={{
                        display: "flex",
                        gap: "10px",
                        alignItems: "center"
                    }}>
                        <Typography>Sprinkler is {sprinkler.sprinklerOn ? "" : "not"} sprinkling</Typography>
                        <SprinklerToggleSwitch
                            checked={sprinkler.sprinklerOn}
                            onChange={() => handleSprinklerSwitch()}
                            inputProps={{'aria-label': 'controlled'}}/>
                    </Box>

                    <h3>Settings</h3>
                    <Box
                        sx={{
                            width: '100%',
                            height: 'calc(100vh - 200px)',
                            display: 'flex',
                            flexDirection: 'column',
                            gap: '10px'
                        }}
                    >
                        <Box
                            sx={{
                                display: "flex",
                            }}
                        >
                            <FormControlLabel
                                control={<Checkbox checked={repeatingDays.MONDAY} onChange={handleCheckboxChange}
                                                   name="MONDAY"/>}
                                label="Mon"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={repeatingDays.TUESDAY} onChange={handleCheckboxChange}
                                                   name="TUESDAY"/>}
                                label="Tue"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={repeatingDays.WEDNESDAY} onChange={handleCheckboxChange}
                                                   name="WEDNESDAY"/>}
                                label="Wed"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={repeatingDays.THURSDAY} onChange={handleCheckboxChange}
                                                   name="THURSDAY"/>}
                                label="Thu"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={repeatingDays.FRIDAY} onChange={handleCheckboxChange}
                                                   name="FRIDAY"/>}
                                label="Fri"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={repeatingDays.SATURDAY} onChange={handleCheckboxChange}
                                                   name="SATURDAY"/>}
                                label="Sat"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={repeatingDays.SUNDAY} onChange={handleCheckboxChange}
                                                   name="SUNDAY"/>}
                                label="Sun"
                            />
                        </Box>
                        <Box
                            sx={{
                                display: "flex",
                                alignItems: "center",
                                gap: "10px"
                            }}
                        >
                            <TimePicker
                                label="Start"
                                ampm={false}
                                value={startTime}
                                onChange={(newValue) => setStartTime(newValue)}
                                sx={{
                                    width: "120px"
                                }}
                            />
                            <p>-</p>
                            <TimePicker
                                label="End"
                                ampm={false}
                                value={endTime}
                                onChange={(newValue) => setEndTime(newValue)}
                                sx={{
                                    width: "120px"
                                }}
                            />

                            <Button
                                variant="contained"
                                onClick={handleAddSetting}
                                sx={{
                                    maxWidth: "60px",
                                    minWidth: "60px",
                                    minHeight: "40px",
                                    maxHeight: "40px",
                                    backgroundColor: "primary.main",
                                    color: "secondary.main",
                                }}>
                                Add
                            </Button>
                        </Box>

                        <DataGrid
                            getRowId={(row) => row.startTime}
                            rows={sprinkler.sprinklerSettings}
                            columns={columns}
                            onRowClick={(params) => setSelectedSetting(params.row)}
                            initialState={{
                                // pagination: {
                                //     paginationModel: {page: 0, pageSize: 10},
                                // },
                                sorting: {
                                    sortModel: [{field: 'startTime', sort: 'asc'}]
                                }
                            }}
                            pageSizeOptions={[5, 10, 25]}
                        />
                    </Box>
                </Box>
            }
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                gap: "20px",
                padding: "20px",
                flexGrow: "1",
                height: "100%"
            }}>
                <ToggleButtonGroup
                    color="primary"
                    exclusive
                    value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    <ToggleButton value="actions">Actions</ToggleButton>
                    <ToggleButton value="availability">Availability</ToggleButton>
                </ToggleButtonGroup>
                
                {display === "actions" && sprinkler && <SprinklerTable sprinklerId={sprinkler.id}></SprinklerTable>}

                {display === "availability" && sprinkler && <DeviceAvailabilityGraph deviceId={sprinkler.id}/>}
            
            </Box>
            
            {sprinkler &&
                <Dialog open={selectedSetting !== null} onClose={() => setSelectedSetting(null)} fullWidth
                        maxWidth={"sm"}>
                    <DialogContent
                        sx={{
                            padding: "0",
                            width: "100%",
                        }}
                    >
                        <SprinklerSettingPopup
                            sprinklerId={sprinkler.id}
                            sprinklerSetting={selectedSetting}
                            updateParentSetting={handleUpdateDeleteSetting}/>
                    </DialogContent>
                </Dialog>
            }
        </Box>
    
    
    )
}



const SprinklerToggleSwitch = styled(Switch)(({ theme }) => ({
    width: 90,
    height: 40,
    padding: 7,
    marginRight: 5,
    display: "flex",
    alignItems: "center",
    '& .MuiSwitch-switchBase': {
        margin: 1,
        padding: 0,
        transform: 'translateX(6px)',
        '&.Mui-checked': {
            color: '#fff',
            transform: 'translateX(50px)',
            '& .MuiSwitch-thumb:before': {
                backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 28 44"><path fill="${encodeURIComponent(
                    '#fff',
                )}" d="M37 32.2254L25.8413 44L19.381 36.5634L22.106 33.6879L25.8413 37.6293L34.2749 28.7301L37 32.2254ZM27.2978 21.6901C24.2203 12.0225 14.0952 0 14.0952 0C14.0952 0 0 16.7324 0 26.6479C0 34.853 6.31937 41.5211 14.0952 41.5211H14.894C14.3771 39.9346 14.0952 38.2986 14.0952 36.5634C14.0952 28.6806 19.9683 22.2355 27.2978 21.6901Z"/></svg>')`,
            },
            '& + .MuiSwitch-track': {
                opacity: 1,
            },
        },
    },
    '& .MuiSwitch-thumb': {
        width: 35,
        height: 35,
        borderRadius: "10px",
        marginTop: "2px",
        '&:before': {
            content: "''",
            position: 'absolute',
            width: '100%',
            height: '100%',
            left: 0,
            top: 0,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 28 44"><path fill="${encodeURIComponent(
                '#fff',
            )}" d="M37 36.1194V41.3731H18.5V36.1194H37ZM4.625 28.2388C4.625 23.5893 9.25 14.9994 13.875 8.24836C17.3438 13.3445 20.8125 19.4913 22.3619 24.2197C23.7494 23.5367 25.2756 23.1427 26.8713 22.9851C23.8419 12.7403 13.875 0 13.875 0C13.875 0 0 17.7313 0 28.2388C0 36.9337 6.22063 44 13.875 44C14.1525 44 14.4069 44 14.6613 44C14.1525 42.3188 13.875 40.5851 13.875 38.7463C8.76437 38.7463 4.625 34.0442 4.625 28.2388Z"/></svg>')`,
        },
    },
    '& .MuiSwitch-track': {
        borderRadius: 20 / 2,
    },
}));