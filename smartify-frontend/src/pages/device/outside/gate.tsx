import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {Gate, GateEventRecord} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {
    Box,
    Button, FormControl,
    FormControlLabel,
    FormLabel,
    Radio,
    RadioGroup, styled, Switch,
    ToggleButton,
    ToggleButtonGroup
} from "@mui/material";
import DeviceData from "../../../components/device/device-data.tsx";
import GateTable from "../../../components/device/gate/gate-table.tsx";
import GateVehiclesEdit from "../../../components/device/gate/gate-vehicles-edit.tsx";
import {getUserMail} from "../../../util/token-utils.tsx";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function Gate(){
    const { gateId } = useParams();

    const [display, setDisplay] = useState('actions');

    const [gate, setGate] = useState<Gate>();

    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };

    const handleVehicleChange = (vehicles: string[]) => {
        updateAllowedVehicles(vehicles);
        setGate(prevGate => {
            return {...prevGate, allowedVehicles: vehicles}
        });
        setDisplay("actions");
    }

    const updateAllowedVehicles = (vehicles: string[]) => {
        api.post<any>(`/device/gate/updateVehicles/${gateId}`, vehicles)
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const updateGateMode = (mode: string) => {
        api.post<any>(`/device/gate/updateMode/${gateId}`, {}, {
            params: {gateMode: mode, email: getUserMail()}
        })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const handleGateModeChange = (mode: string) => {
        updateGateMode(mode);
        setGate(prevGate => {
            return {...prevGate, mode: mode, open: false}
        });
    }

    const handleGateManualSwitch = () => {
        const open: boolean = !gate.open;
        const updatedGate = { ...gate, open: open};
        setGate(updatedGate);
        api.post<any>(`/device/gate/changeGateState/${gateId}`, {}, {
            params: {open: open, email: getUserMail()}
        })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    useEffect(() => {
        api.get<Gate>(`/device/gate/getGate/${gateId}`)
            .then(res => {
                if (res.status === 200) {
                    setGate(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }, []);


    return(
        <Box sx={{
            display: "flex",
            paddingBottom: "20px",
            height: "100%"
        }}>
            {gate &&
                <Box sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "10px",
                    minWidth: "25%",
                    height: "calc(100vh - 100px)",
                    overflow: "auto",
                    padding: "20px",
                }}>
                    <DeviceData device={gate}/>
                    <Box sx={{
                        padding: "0px 20px 20px 20px",
                        display: "flex",
                        alignItems: "start",
                        justifyContent: "space-between"
                    }}>
                        <FormControl>
                            <FormLabel>Gate mode</FormLabel>
                            <RadioGroup
                                name="controlled-radio-buttons-group"
                                value={gate.mode}
                                onChange={(event, value) =>
                                    handleGateModeChange(value)}
                            >
                                <FormControlLabel value="MANUAL" control={<Radio/>} label="Manual"/>
                                <FormControlLabel value="PUBLIC" control={<Radio/>} label="Public"/>
                                <FormControlLabel value="PRIVATE" control={<Radio/>} label="Private"/>
                            </RadioGroup>
                        </FormControl>

                        {gate.mode === "MANUAL" &&
                            <FormControlLabel control={<GateToggleSwitch
                                checked={gate.open}
                                onChange={() => handleGateManualSwitch()}
                                inputProps={{'aria-label': 'controlled'}}/>} label={gate.open ? 'Open' : 'Closed'}
                            />
                        }
                    </Box>
                </Box>
            }
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                gap: "20px",
                padding: "20px",
                flexGrow: "1",
                height: "100%"
            }}>
                <ToggleButtonGroup
                    color="primary"
                    exclusive
                    value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    <ToggleButton value="actions" sx={{width: 250}}>Actions</ToggleButton>
                    <ToggleButton value="edit"  sx={{width: 250}}>Edit</ToggleButton>
                    <ToggleButton value="availability"  sx={{width: 250}}>Availability</ToggleButton>
                </ToggleButtonGroup>

                {display === "actions" && gate && <GateTable gateId={gate.id}></GateTable>}
                {display === "edit" && gate &&
                    <GateVehiclesEdit currentlyAllowedVehicles={gate.allowedVehicles}
                                      changeAllowedVehicles={handleVehicleChange}/>
                }
                {display === "availability" && gate && <DeviceAvailabilityGraph deviceId={gate.id}/>}

            </Box>
        </Box>
    )
}


const GateToggleSwitch = styled(Switch)(({ theme }) => ({
    width: 90,
    height: 40,
    padding: 7,
    marginRight: 5,
    display: "flex",
    alignItems: "center",
    '& .MuiSwitch-switchBase': {
        margin: 1,
        padding: 0,
        transform: 'translateX(6px)',
        '&.Mui-checked': {
            color: '#fff',
            transform: 'translateX(50px)',
            '& .MuiSwitch-thumb:before': {
                backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="30" width="30" viewBox="0 0 25 20"><path fill="${encodeURIComponent(
                    '#fff',
                )}" d="M14 6v15H3v-2h2V3h9v1h5v15h2v2h-4V6h-3zm-4 5v2h2v-2h-2z"/></svg>')`,
            },
            '& + .MuiSwitch-track': {
                opacity: 1,
            },
        },
    },
    '& .MuiSwitch-thumb': {
        width: 35,
        height: 35,
        borderRadius: "10px",
        marginTop: "2px",
        '&:before': {
            content: "''",
            position: 'absolute',
            width: '100%',
            height: '100%',
            left: 0,
            top: 0,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="30" width="30" viewBox="0 0 25 20"><path fill="${encodeURIComponent(
                '#fff',
            )}" d="M19,19V5c0-1.1-0.9-2-2-2H7C5.9,3,5,3.9,5,5v14H3v2h18v-2H19z M15,13h-2v-2h2V13z"/></svg>')`,
        },
    },
    '& .MuiSwitch-track': {
        borderRadius: 20 / 2,
    },
}));