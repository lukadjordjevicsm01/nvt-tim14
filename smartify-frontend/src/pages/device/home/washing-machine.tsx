import React, {useEffect, useState} from "react";
import {
    WashingMachine,
    WashingMachineMode,
    WashingMachineSetting
} from "../../../model/device-model.tsx";
import {
    Box,
    Button,
    capitalize, Dialog,
    DialogContent,
    MenuItem,
    Select,
    ToggleButton,
    ToggleButtonGroup,
    Typography
} from "@mui/material";
import DeviceData from "../../../components/device/device-data.tsx";
import api from "../../../config/axios-config.tsx";
import {useParams} from "react-router-dom";
import toast from "react-hot-toast";
import {getUserMail} from "../../../util/token-utils.tsx";
import WashingMachineTable from "../../../components/device/washing-machine/washing-machine-table.tsx";
import {DateTimePicker} from "@mui/x-date-pickers";
import {addHours} from "date-fns";
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import WebSocketService from "../../../util/web-socket-service.ts";
import RegistrationForm from "../../../components/auth/registration-form.tsx";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function WashingMachine() {
    const { washingMachineId } = useParams();

    const [display, setDisplay] = useState('table');
    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };


    const [washingMachine, setWashingMachine] = useState<WashingMachine>();
    const [selectedMode, setSelectedMode] = useState<WashingMachineMode>(
            {
                name: "Idle",
                duration: 0
            }
    );
    const [selectedSettingModeMode, setSelectedSettingModeMode] = useState<WashingMachineMode>(
        {
            name: "Idle",
            duration: 0
        }
    );

    const [startTime, setStartTime] = useState<Date>(addHours(new Date(), 1));

    const [currentMode, setCurrentMode] = useState<string>("Idle");

    const [dialogOpen, setDialogOpen] = useState<boolean>(false);

    const getWashingMachine = () => {
        api.get<WashingMachine>(`/device/washingMachine/getWashingMachine/${washingMachineId}`)
            .then(res => {
                if (res.status === 200) {
                    console.log(res.data)
                    setWashingMachine(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    useEffect(() => {
        const webSocket = WebSocketService.subscribeToTopic(
            `/washing-machine/${washingMachineId}`,
            payload => {
                console.log(payload)
                if (payload.command === 'value') setCurrentMode(payload.payload);
            }
        )
        getWashingMachine();
        return () => {
            if (webSocket)
                webSocket.unsubscribe();
        };
    }, [])

    useEffect(() => {
        if (washingMachine !== undefined) {
            setSelectedMode(washingMachine.supportedModes[0]);
            setSelectedSettingModeMode(washingMachine.supportedModes[0]);
            setCurrentMode(washingMachine.currentMode !== null ? washingMachine.currentMode.name : "Idle")
        }
    }, [washingMachine])

    const handleChangeSelectedMode = (modeName: string) => {
        for (let mode of washingMachine.supportedModes) {
            if (mode.name === modeName) setSelectedMode(mode);
        }
    }

    const handleChangeSettingSelectedMode = (modeName: string) => {
        for (let mode of washingMachine.supportedModes) {
            if (mode.name === modeName) setSelectedSettingModeMode(mode);
        }
    }

    const dateOptions: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false
    };

    const columns: GridColDef[] = [
        {
            field: 'name',
            headerName: 'Mode',
            width: 110,
            sortable: false,
            valueGetter: params => {
                return params.row.mode.name;
            }
        },
        {
            field: 'startTime',
            headerName: 'Start',
            type: 'Date',
            valueFormatter: params => new Date(params?.value)
                .toLocaleString("ru-RU", dateOptions),
            width: 150
        },
        {
            field: 'duration',
            headerName: 'Duration (min)',
            width: 120,
            valueGetter: params => {
                return params.row.mode.duration;
            }
        }
    ];

    const handleSetAction = () => {
        api.post(`/device/washingMachine/setAction/${washingMachineId}`,
            {
                id: washingMachine.id,
                washingMachineModeName: selectedMode.name,
                duration: selectedMode.duration,
                email: getUserMail()
            })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                    setDialogOpen(false);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const handleAddSetting = () => {
        const washingMachineSetting: WashingMachineSetting = {
            mode: selectedSettingModeMode,
            startTime: startTime.toISOString(),
        }
        api.post(`/device/washingMachine/addSetting/${washingMachineId}`,
                {
                    mode: selectedSettingModeMode,
                    startTime: startTime.getTime(),
                })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                    setWashingMachine(prev => {
                        let settings = [...prev.customSettings]; // Convert to array
                        settings.push(washingMachineSetting);
                        return {...prev, customSettings: settings};
                    });
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    return (
        <Box
            sx={{
                display: "flex"
            }}
        >
            {washingMachine &&
                <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "10px",
                        width: "35%",
                        height: "calc(100vh - 100px)",
                        overflow: "auto",
                        padding: "20px",
                    }}
                >
                    <DeviceData device={washingMachine}/>
                    <Typography>Current mode: {capitalize(currentMode)}</Typography>

                    <h3>Choose action</h3>
                    <Box
                        sx={{
                            display: "flex",
                            gap: "10px",
                            alignItems: "center",
                        }}
                    >
                        <Select
                            labelId="washing-machine-select"
                            label="Select mode"
                            value={selectedMode.name}
                            onChange={(e) => handleChangeSelectedMode(e.target.value as string)}
                            sx={{
                                width: "40%"
                            }}>
                            {washingMachine.supportedModes.map((supportedMode) =>
                                <MenuItem
                                    key={supportedMode.name}
                                    value={supportedMode.name}
                                >
                                    {capitalize(supportedMode.name)} ({supportedMode.duration} min)
                                </MenuItem>
                            )}
                        </Select>

                        <Button
                            variant="contained"
                            onClick={currentMode === "Idle" ? handleSetAction : () => setDialogOpen(true)}
                            sx={{
                                maxWidth: "60px",
                                minWidth: "60px",
                                minHeight: "40px",
                                maxHeight: "40px",
                                backgroundColor: "primary.main",
                                color: "secondary.main",
                            }}>
                            Set
                        </Button>
                    </Box>

                    <h3>Settings</h3>
                    <Box
                        sx={{
                            width: '100%',
                            height: 'calc(100vh - 200px)',
                            display: 'flex',
                            flexDirection: 'column',
                            gap: '10px'
                        }}
                    >
                        <Box
                            sx={{
                                display: "flex",
                                flexDirection: "column",
                                //alignItems: "center",
                                gap: "10px"
                            }}
                        >
                            <Box
                                sx={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    gap: "10px"
                                }}
                            >
                                <Select
                                    labelId="washing-machine-select"
                                    label="Select mode"
                                    value={selectedSettingModeMode.name}
                                    onChange={(e) =>
                                        handleChangeSettingSelectedMode(e.target.value as string)}
                                    sx={{
                                        width: "40%"
                                    }}>
                                    {washingMachine.supportedModes.map((supportedMode) =>
                                        <MenuItem
                                            key={supportedMode.name}
                                            value={supportedMode.name}
                                        >
                                            {capitalize(supportedMode.name)} ({supportedMode.duration} min)
                                        </MenuItem>
                                    )}
                                </Select>

                                <DateTimePicker
                                    label="Start time"
                                    value={startTime}
                                    onChange={(newValue) => setStartTime(newValue)}
                                    disablePast
                                    ampm={false}
                                    format={"dd.MM.yyyy, HH:mm"}
                                    sx={{
                                        width: "210px"
                                    }}
                                />

                                <Button
                                    variant="contained"
                                    onClick={handleAddSetting}
                                    sx={{
                                        maxWidth: "60px",
                                        minWidth: "60px",
                                        minHeight: "40px",
                                        maxHeight: "40px",
                                        backgroundColor: "primary.main",
                                        color: "secondary.main",
                                    }}
                                >
                                    Add
                                </Button>
                            </Box>


                        </Box>

                        <DataGrid
                            getRowId={(row) => row.startTime}
                            rows={washingMachine.customSettings}
                            columns={columns}
                            initialState={{
                                sorting: {
                                    sortModel: [{field: 'startTime', sort: 'asc'}]
                                }
                            }}
                            pageSizeOptions={[5, 10, 25]}
                        />
                    </Box>
                </Box>
            }

            <Box sx={{
                display: "flex",
                flexDirection: "column",
                gap: "20px",
                padding: "20px",
                flexGrow: "1",
                height: "100%"
            }}>
                <ToggleButtonGroup
                    color="primary"
                    exclusive
                    value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    <ToggleButton value="table">History</ToggleButton>
                    <ToggleButton value="availability">Availability</ToggleButton>
                </ToggleButtonGroup>

                {display === "table" && washingMachineId && <WashingMachineTable washingMachineId={washingMachineId}/>}

                {display === "availability" && washingMachineId && <DeviceAvailabilityGraph deviceId={washingMachineId}/>}
            </Box>

            <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
                <DialogContent
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "10px"
                    }}
                >
                    <h2>Warning</h2>
                    <Typography>Washing machine is currently washing. Are you sure you want to override it?</Typography>
                    <Box
                        sx={{
                            display: "flex",
                            gap: "10px"
                        }}
                    >
                        <Button
                            variant="contained"
                            sx={{
                                backgroundColor: "primary.main",
                                color: "secondary.main",
                            }}
                            onClick={handleSetAction}
                        >
                            Yes
                        </Button>
                        <Button
                            variant="contained"
                            sx={{
                                backgroundColor: "primary.main",
                                color: "secondary.main",
                            }}
                            onClick={() => setDialogOpen(false)}
                        >
                            No
                        </Button>
                    </Box>
                </DialogContent>
            </Dialog>
        </Box>
    )
}
