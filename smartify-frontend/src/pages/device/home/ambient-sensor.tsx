import DeviceData from "../../../components/device/device-data.tsx";
import React, {useEffect, useState} from "react";
import {AmbientSensor, Device} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {useParams} from "react-router-dom";
import {Box, ToggleButton, ToggleButtonGroup} from "@mui/material";
import AmbientSensorGraph from "../../../components/device/ambient-sensor/ambient-sensor-graph.tsx";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function AmbientSensor() {
    const { ambientSensorId } = useParams();
    const [display, setDisplay] = useState('realTime');

    const [device, setDevice] = useState<Device>();

    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };

    useEffect(() => {
        api.get<AmbientSensor>(`/device/ambientSensor/getAmbientSensor/${ambientSensorId}`)
            .then(res => {
                if (res.status === 200) {
                    setDevice(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }, []);

    return (
        <Box sx={{
            display: "flex"
        }}>
            {device &&
                <Box sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "10px",
                    width: "25%",
                    height: "calc(100vh - 100px)",
                    overflow: "auto",
                    padding: "20px",
                }}>
                    <DeviceData device={device}/>
                </Box>

            }
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                padding: "20px",
                width: "75%",
                height: "calc(100vh - 100px)",
            }}>
                <ToggleButtonGroup
                    color="primary"
                    exclusive
                    value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    <ToggleButton value="realTime">Real time</ToggleButton>
                    <ToggleButton value="history">History</ToggleButton>
                    <ToggleButton value="availability">Availability</ToggleButton>
                </ToggleButtonGroup>

                {display == "realTime" && <AmbientSensorGraph realTime={true}/>}
                {display == "history" && <AmbientSensorGraph realTime={false}/>}
                {display === "availability" && ambientSensorId && <DeviceAvailabilityGraph deviceId={ambientSensorId}/>}
            </Box>
        </Box>
    )
}