import {useParams} from "react-router-dom";
import {AirConditioning, AirConditioningSettings} from "../../../model/device-model.tsx";
import React, {useEffect, useState} from "react";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {Box, Button, capitalize, MenuItem, Select, ToggleButton, ToggleButtonGroup, Typography} from "@mui/material";
import DeviceData from "../../../components/device/device-data.tsx";
import AirConditioningTable from "../../../components/device/air-conditioning/air-conditioning-table.tsx";
import {getUserMail} from "../../../util/token-utils.tsx";
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import {TimePicker} from "@mui/x-date-pickers";
import {addHours} from "date-fns";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function AirConditioning() {
    const { airConditioningId } = useParams();

    const [airConditioning, setAirConditioning] = useState<AirConditioning>();

    const [display, setDisplay] = useState('table');
    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };

    const [desiredTemperature, setDesiredTemperature] = useState<number>(18);
    const handleIncrementDesiredTemperature = () => {
        if (desiredTemperature + 1 > airConditioning.maxTemperature) {
            toast("Max temperature reached.")
            return;
        }

        setDesiredTemperature((prevState) => prevState + 1);
    }
    const handleDecrementDesiredTemperature = () => {
        if (desiredTemperature - 1 < airConditioning.minTemperature) {
            toast("Min temperature reached.")
            return;
        }

        setDesiredTemperature((prevState) => prevState - 1);
    }
    const [selectedMode, setSelectedMode] = useState<string>("cooling");
    const [selectedSettingModeMode, setSelectedSettingModeMode] = useState<string>("cooling");

    const [startTime, setStartTime] = useState<Date>(new Date());
    const [endTime, setEndTime] = useState<Date>(addHours(new Date(), 1));

    const [settingTemperature, setSettingTemperature] = useState<number>(18)
    const handleIncrementSettingTemperature = () => {
        if (settingTemperature + 1 > airConditioning.maxTemperature) {
            toast("Max temperature reached.")
            return;
        }

        setSettingTemperature((prevState) => prevState + 1);
    }
    const handleDecrementSettingTemperature = () => {
        if (settingTemperature - 1 < airConditioning.minTemperature) {
            toast("Min temperature reached.")
            return;
        }

        setSettingTemperature((prevState) => prevState - 1);
    }

    const handleSetAction = () => {
        api.post(`/device/airConditioning/setAction/${airConditioningId}`,
            {
                id: airConditioning.id,
                airConditioningMode: selectedMode.toUpperCase(),
                currentTemperature: desiredTemperature,
                email: getUserMail()
            })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                    setAirConditioning(prev => {
                        return {...prev, currentTemperature: desiredTemperature, currentMode: selectedMode};
                    });
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const handleAddSetting = () => {
        const airConditioningSetting: AirConditioningSettings = {
            id: null,
            mode: selectedSettingModeMode.toUpperCase(),
            startTime: startTime.toISOString(),
            endTime: endTime.toISOString(),
            temperature: settingTemperature
        }
        console.log(airConditioningSetting)
        api.post(`/device/airConditioning/addSetting/${airConditioningId}`,
            {
                id: null,
                mode: selectedSettingModeMode.toUpperCase(),
                startTime: startTime.getTime(),
                endTime: endTime.getTime(),
                temperature: settingTemperature
            })
            .then(res => {
                if (res.status === 200) {
                    toast.success(res.data.message);
                    setAirConditioning(prev => {
                        let settings = [...prev.customSettings]; // Convert to array
                        settings.push(airConditioningSetting);
                        return {...prev, customSettings: settings};
                    });
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    const getAirConditioning = () => {
        api.get<AirConditioning>(`/device/airConditioning/getAirConditioning/${airConditioningId}`)
            .then(res => {
                if (res.status === 200) {
                    console.log(res.data)
                    setAirConditioning(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }

    useEffect(() => {
        getAirConditioning();
    }, [])

    const dateOptions: Intl.DateTimeFormatOptions = {
        hour: '2-digit',
        minute: '2-digit',
        hour12: false
    };

    const columns: GridColDef[] = [
        { field: 'mode', headerName: 'Mode', width: 110, sortable: false },
        { field: 'temperature', headerName: 'Temp °C', width: 90, sortable: false },
        {
            field: 'startTime',
            headerName: 'Start',
            type: 'Date',
            valueFormatter: params => new Date(params?.value)
                .toLocaleString("ru-RU", dateOptions),
            width: 90
        },
        {
            field: 'endTime',
            headerName: 'End',
            type: 'Date',
            valueFormatter: params => new Date(params?.value)
                .toLocaleString("ru-RU", dateOptions),
            width: 90
        }
    ];

    return (
        <Box sx={{
            display: "flex"
        }}>
            {airConditioning &&
                <Box sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "10px",
                    width: "25%",
                    height: "calc(100vh - 100px)",
                    overflow: "auto",
                    padding: "20px",
                }}>
                    <DeviceData device={airConditioning}/>

                    <Box sx={{
                        display: "flex",
                        gap: "20px"
                    }}>
                        <Typography>Mode: {capitalize(airConditioning.currentMode.toLowerCase())}</Typography>
                        <Typography>Temperature: {airConditioning.currentTemperature}°C</Typography>
                    </Box>

                    <h3>Choose action</h3>
                    <Box sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                    }}>
                        <Select
                            labelId="demo-simple-select-label"
                            label="Select mode"
                            value={selectedMode}
                            onChange={(e) => setSelectedMode(e.target.value as string)}
                            sx={{
                                width: "40%"
                            }}>
                            {airConditioning.supportedModes.map((supportedMode) =>
                                <MenuItem key={supportedMode}
                                    value={supportedMode.toLowerCase()}
                                >
                                    {capitalize(supportedMode.toLowerCase())}
                                </MenuItem>
                            )}
                        </Select>
                        <Box sx={{
                            display: "flex",
                            alignItems: "center",
                            gap: "10px"
                        }}>
                            <Button
                                variant="contained"
                                sx={{
                                    borderRadius: "50%",
                                    backgroundColor: "primary.main",
                                    maxWidth: "30px",
                                    minWidth: "30px",
                                    minHeight: "30px",
                                    maxHeight: "30px",
                                    color: "secondary.main",
                                    fontSize: "20px"
                                }}
                                onClick={handleDecrementDesiredTemperature}>-</Button>
                            <Typography>{desiredTemperature}°C</Typography>
                            <Button
                                variant="contained"
                                sx={{
                                    borderRadius: "50%",
                                    backgroundColor: "primary.main",
                                    maxWidth: "30px",
                                    minWidth: "30px",
                                    minHeight: "30px",
                                    maxHeight: "30px",
                                    color: "secondary.main",
                                    fontSize: "20px"
                                }}
                                onClick={handleIncrementDesiredTemperature}>+</Button>
                        </Box>

                        <Button
                            variant="contained"
                            onClick={handleSetAction}
                            sx={{
                                maxWidth: "60px",
                                minWidth: "60px",
                                minHeight: "40px",
                                maxHeight: "40px",
                                backgroundColor: "primary.main",
                                color: "secondary.main",
                        }}>
                            Set
                        </Button>
                    </Box>

                    <h3>Settings</h3>
                    <Box sx={{
                        width: '100%',
                        height: 'calc(100vh - 200px)',
                        display: 'flex',
                        flexDirection: 'column',
                        gap: '10px'
                    }}>
                        <Box sx={{
                            display: "flex",
                            alignItems: "center",
                            gap: "20px"
                        }}>
                            <Select
                                labelId="demo-simple-select-label"
                                label="Select mode"
                                value={selectedSettingModeMode}
                                onChange={(e) => setSelectedSettingModeMode(e.target.value as string)}
                                sx={{
                                    width: "50%"
                                }}>
                                {airConditioning.supportedModes.map((supportedMode) =>
                                    <MenuItem key={supportedMode}
                                              value={supportedMode.toLowerCase()}
                                    >
                                        {capitalize(supportedMode.toLowerCase())}
                                    </MenuItem>
                                )}
                            </Select>

                            <Box sx={{
                                display: "flex",
                                alignItems: "center",
                                gap: "10px"
                            }}>
                                <Button
                                    variant="contained"
                                    sx={{
                                        borderRadius: "50%",
                                        backgroundColor: "primary.main",
                                        maxWidth: "30px",
                                        minWidth: "30px",
                                        minHeight: "30px",
                                        maxHeight: "30px",
                                        color: "secondary.main",
                                        fontSize: "20px"
                                    }}
                                    onClick={handleDecrementSettingTemperature}>-</Button>
                                <Typography>{settingTemperature}°C</Typography>
                                <Button
                                    variant="contained"
                                    sx={{
                                        borderRadius: "50%",
                                        backgroundColor: "primary.main",
                                        maxWidth: "30px",
                                        minWidth: "30px",
                                        minHeight: "30px",
                                        maxHeight: "30px",
                                        color: "secondary.main",
                                        fontSize: "20px"
                                    }}
                                    onClick={handleIncrementSettingTemperature}>+</Button>
                            </Box>

                        </Box>

                        <Box sx={{
                            display: "flex",
                            alignItems: "center",
                            gap: "10px"
                        }}>
                            <TimePicker
                                label="Start"
                                ampm={false}
                                value={startTime}
                                onChange={(newValue) => setStartTime(newValue)}
                                sx={{
                                    width: "120px"
                                }}
                            />
                            <p>-</p>
                            <TimePicker
                                label="End"
                                ampm={false}
                                value={endTime}
                                onChange={(newValue) => setEndTime(newValue)}
                                sx={{
                                    width: "120px"
                                }}
                            />

                            <Button
                                variant="contained"
                                onClick={handleAddSetting}
                                sx={{
                                    maxWidth: "60px",
                                    minWidth: "60px",
                                    minHeight: "40px",
                                    maxHeight: "40px",
                                    backgroundColor: "primary.main",
                                    color: "secondary.main",
                                }}>
                                Add
                            </Button>

                        </Box>

                        <DataGrid
                            getRowId={(row) => row.startTime}
                            rows={airConditioning.customSettings}
                            columns={columns}
                            initialState={{
                                // pagination: {
                                //     paginationModel: {page: 0, pageSize: 10},
                                // },
                                sorting: {
                                    sortModel: [{field: 'startTime', sort: 'asc'}]
                                }
                            }}
                            pageSizeOptions={[5, 10, 25]}
                        />
                    </Box>

                </Box>
            }

            <Box sx={{
                display: "flex",
                flexDirection: "column",
                gap: "20px",
                padding: "20px",
                flexGrow: "1",
                height: "100%"
            }}>
                <ToggleButtonGroup
                    color="primary"
                    exclusive
                    value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    <ToggleButton value="table">History</ToggleButton>
                    <ToggleButton value="availability">Availability</ToggleButton>
                </ToggleButtonGroup>

                {display === "table" && airConditioning && <AirConditioningTable airConditioningId={airConditioningId}/>}

                {display === "availability" && airConditioning && <DeviceAvailabilityGraph deviceId={airConditioning.id}/>}
            </Box>

        </Box>
    )
}