import "./add-device.css"
import {Autocomplete, Box, TextField} from "@mui/material";
import React, {useEffect, useState} from "react";
import {DeviceCategory} from "../../model/device-model.tsx";
import AddAmbientSensor from "../../components/device/ambient-sensor/add-ambient-sensor.tsx";
import AddAirConditioning from "../../components/device/air-conditioning/add-air-conditioning.tsx";
import AddWashingMachine from "../../components/device/washing-machine/add-washing-machine.tsx";
import AddLamp from "../../components/device/lamp/add-lamp.tsx";
import AddGate from "../../components/device/gate/add-gate.tsx";
import AddSprinkler from "../../components/device/sprinkler/add-sprinkler.tsx";
import AddSolarPanelSystem from "../../components/device/solar-panel-system/add-solar-panel-system.tsx";
import AddHomeBattery from "../../components/device/home-battery/add-home-battery.tsx";
import AddVehicleCharger from "../../components/device/vehicle-charger/add-vehicle-charger.tsx";

interface ComponentMap {
    [key: string]: React.FC<any>;
}

const componentMapping: ComponentMap = {
    'Ambient Sensor': AddAmbientSensor,
    'Air Conditioning': AddAirConditioning,
    'Washing Machine': AddWashingMachine,
    'Lamp': AddLamp,
    'Gate': AddGate,
    'Sprinkler': AddSprinkler,
    'Solar Panel System': AddSolarPanelSystem,
    'Home Battery': AddHomeBattery,
    'Vehicle Charger': AddVehicleCharger
};

export default function AddDevice() {

    const initialCategories: DeviceCategory[] = [
        {
            name: "House",
            devices: ["Ambient Sensor", "Air Conditioning", "Washing Machine"],
        },
        {
            name: "Outside",
            devices: ["Lamp", "Gate", "Sprinkler"],
        },
        {
            name: "Electro Energetic",
            devices: ["Solar Panel System", "Home Battery", "Vehicle Charger"],
        },
    ];

    const [deviceCategories] = useState<DeviceCategory[]>(initialCategories);
    const [selectedCategory, setSelectedCategory] = useState<string>("House");
    const [selectedDevice, setSelectedDevice] = useState<string | null>(null);


    useEffect(() => {
        const defaultDevice =
            deviceCategories.find((category) =>
                category.name === selectedCategory)?.devices[0] ?? null;
        setSelectedDevice(defaultDevice);
    }, [selectedCategory, deviceCategories]);

    const handleCategoryChange = (category: string | null) => {
        setSelectedCategory(category || "House");
    };

    const SelectedComponent = componentMapping[selectedDevice || ''];
    
    return (
        <div id={"add-device-container"}>
            <h1>Add Device</h1>
            <Box
                sx={{
                    width: '60vw',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    gap: '10px',
                }}
            >
                <Autocomplete
                    sx={{ width: '50%' }}
                    options={deviceCategories.map((category) => category.name)}
                    renderInput={(params) => (
                        <TextField {...params} label="Category" variant="outlined" />
                    )}
                    onChange={(_event, value) => handleCategoryChange(value)}
                    value={selectedCategory}
                    disableClearable
                />

                <Autocomplete
                    sx={{ width: '50%' }}
                    options={
                        selectedCategory
                            ? deviceCategories.find((category) =>
                            category.name === selectedCategory)?.devices ?? [] : []
                    }
                    renderInput={(params) => (
                        <TextField {...params} label="Device" variant="outlined" />
                    )}
                    value={selectedDevice}
                    onChange={(_event, value) => setSelectedDevice(value)}
                    clearOnEscape={false}
                    clearOnBlur={false}
                    disableClearable
                />
            </Box>
            <Box className={"selected-component"}>
                {SelectedComponent && <SelectedComponent/>}
            </Box>
        </div>
    )
}