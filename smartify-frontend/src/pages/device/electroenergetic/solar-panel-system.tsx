import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {SolarPanelSystem} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {Box, ToggleButton, ToggleButtonGroup} from "@mui/material";
import DeviceData from "../../../components/device/device-data.tsx";
import {imagesUrl} from "../../../util/environment.ts";
import SolarPanelSystemTable from "../../../components/device/solar-panel-system/solar-panel-system-table.tsx";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function SolarPanelSystem(){
    const {solarPanelSystemId} = useParams();
    const [display, setDisplay] = useState('table');
    const [solarPanelSystem, setSolarPanelSystem] = useState<SolarPanelSystem>();

    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };

    useEffect(() => {
        api.get<SolarPanelSystem>(`/device/solarPanelSystem/getSolarPanelSystem/${solarPanelSystemId}`)
            .then(res => {
                if (res.status === 200) {
                    setSolarPanelSystem(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));
    }, []);

    return(
        <Box sx={{
            display: "flex",
            paddingBottom: "20px",
            height: "100%"
        }}>
            {solarPanelSystem &&
                <Box sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "10px",
                    width: "25%",
                    height: "calc(100vh - 100px)",
                    overflow: "auto",
                    padding: "20px",
                }}>
                    <DeviceData device={solarPanelSystem}/>
                    <h3>Solar Panels</h3>
                    {solarPanelSystem.solarPanels
                        .sort((a, b) => a.name.localeCompare(b.name))
                        .map(solarPanel =>
                        <Box key={solarPanel.id} sx={{
                            width: "100%",
                            paddingTop: "10px",
                            borderBottom: "2px solid #092147"
                        }}>
                            <img src={`${imagesUrl}/device/${solarPanel.imagePath}`} alt={solarPanel.name} style={{
                                width: "100%",
                                aspectRatio: "16/9",
                                objectFit: "cover",
                                marginBottom: "10px",
                                borderRadius: "5px"
                            }}/>
                            <h4>{solarPanel.name}</h4>
                            <p>Consumption: {solarPanel.consumption} kWh</p>
                            <p>Area: {solarPanel.area}m²</p>
                            <p>Efficiency: {solarPanel.efficiency}%</p>
                        </Box>
                        )}
                </Box>
            }
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                gap: "20px",
                padding: "20px",
                flexGrow: "1",
                height: "100%"
            }}>
                <ToggleButtonGroup
                    color="primary"
                    exclusive
                    value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    {/*<ToggleButton value="graph">Graph</ToggleButton>*/}
                    <ToggleButton value="table">History</ToggleButton>
                    <ToggleButton value="availability">Availability</ToggleButton>
                </ToggleButtonGroup>

                {display === "table" &&solarPanelSystem && <SolarPanelSystemTable solarPanelSystemId={solarPanelSystem.id}></SolarPanelSystemTable>}
                {display === "availability" && solarPanelSystem && <DeviceAvailabilityGraph deviceId={solarPanelSystem.id}/>}

            </Box>
        </Box>
    )

}