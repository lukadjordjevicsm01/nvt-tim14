import {
    Box,
    Button,
    ToggleButton,
    ToggleButtonGroup,
    Typography,
    Dialog,
    DialogTitle,
    DialogContent,
    TextField
} from "@mui/material";
import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {CurrentBatteryPercentage, VehicleCharger} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import DeviceData from "../../../components/device/device-data.tsx";
import {useFormik} from "formik";
import {UserTokenState} from "../../../model/user-model.tsx";
import * as yup from "yup";
import {getUserMail} from "../../../util/token-utils.tsx";
import VehicleChargerTable from "../../../components/device/vehicle-charger/vehicle-charger-table.tsx";
import WebSocketService from "../../../util/web-socket-service.ts";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function VehicleCharger() {
    const {vehicleChargerId} = useParams();
    const [display, setDisplay] = useState('table');
    const [vehicleCharger, setVehicleCharger] = useState<VehicleCharger>();
    const [openDialog, setOpenDialog] = useState(false);
    const [chargeSocketName, setChargeSocketName] = useState("");
    const [currentBatteryPercentages, setCurrentBatteryPercentages] = useState<CurrentBatteryPercentage[]>([])

    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };

    const handleStartChargingClick = (name) => {
        setChargeSocketName(name);
        setOpenDialog(true);
    };

    const handleBatteryPercentage = (socketName: string) : string => {
        const batteryPercentage = currentBatteryPercentages
            .find((item) =>
                item.chargeSocketName == socketName)
        if (batteryPercentage)
            return batteryPercentage.currentBatteryPercentage.toFixed(1)
        return "0"
    };

    const handleStopChargingClick = (name) => {
        setChargeSocketName(name);
        const chargingData = {
            vehicleChargerId: vehicleChargerId,
            chargeSocketName: chargeSocketName,
            batteryCapacity: null,
            currentBatteryPercentage: null,
            chargePercentageLimit: null,
            userEmail: getUserMail()
        }
        api.post<UserTokenState>(`device/vehicleCharger/finishCharging`, chargingData).then(res => {
            if (res.status == 200) {
                formik.resetForm()
                setOpenDialog(false)
                setVehicleCharger(prev => {
                    let chargerSockets=  [...prev.vehicleChargerSockets];
                    chargerSockets = chargerSockets.map(item => {
                        if (item.name.includes(chargeSocketName))
                            return {...item, active: false}
                        return item
                    })
                    return {...prev, vehicleChargerSockets: chargerSockets}
                })
            }
        }).catch(() => {
            toast.error("Error. Vehicle charging did not stop.")
        })

    }

    useEffect(() => {
        api.get<VehicleCharger>(`/device/vehicleCharger/getVehicleCharger/${vehicleChargerId}`)
            .then(res => {
                if (res.status === 200) {
                    setVehicleCharger(res.data);
                    console.log(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));

        const subscription = WebSocketService.subscribeToTopic(`/vehicle-charger/${vehicleChargerId}`,
            payload => {
                console.log('Received message:', payload);
                if (payload.command === 'charging-finished') {
                    setVehicleCharger(prev => {
                        let chargerSockets=  [...prev.vehicleChargerSockets];
                        chargerSockets = chargerSockets.map(item => {
                            if (item.name.includes(payload.payload.vehicleChargerSocketName))
                                return {...item, active: false}
                            return item
                        })
                        toast.success("Charging finished at " + payload.payload.vehicleChargerSocketName)
                        return {...prev, vehicleChargerSockets: chargerSockets}
                    })
                }
                if (payload.command === 'current-battery-percentage') {
                    console.log(payload.payload)
                    setCurrentBatteryPercentages(prev => {
                        const previous = [...prev];
                        for (const i in previous) {
                            if (previous[i].chargeSocketName == payload.payload.chargeSocketName) {
                                previous[i] = payload.payload;
                                return previous
                            }
                        }
                        previous.push(payload.payload)
                        return previous
                    })
                }
            });
        return () => {
            if (subscription)
                subscription.unsubscribe();
        };

    }, []);

    const validationSchema = yup.object({
        batteryCapacity: yup
            .number()
            .typeError('Battery capacity must be a number')
            .required('Vehicle battery capacity is required'),
        currentBatteryPercentage: yup
            .number()
            .typeError('Battery percentage must be a number')
            .min(0, 'Battery percentage must be between 0 and 100')
            .max(100, 'Battery percentage must be between 0 and 100')
            .required('Current battery percentage is required'),
        chargePercentageLimit: yup
            .number()
            .typeError('Charge limit must be a number')
            .min(0, 'Charge limit must be between 0 and 100')
            .max(100, 'Charge limit must be between 0 and 100')
            .required('Charge limit is required'),
    });

    const formik = useFormik({
        initialValues: {
            batteryCapacity: '',
            currentBatteryPercentage: '',
            chargePercentageLimit: 100
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            const chargingData = {
                vehicleChargerId: vehicleChargerId,
                chargeSocketName: chargeSocketName,
                batteryCapacity: values.batteryCapacity,
                currentBatteryPercentage: values.currentBatteryPercentage,
                chargePercentageLimit: values.chargePercentageLimit,
                userEmail: getUserMail()
            }
            api.post<UserTokenState>(`device/vehicleCharger/startCharging`, chargingData).then(res => {
                if (res.status == 200) {
                    toast.success("Vehicle charging started.")
                    formik.resetForm()
                    setOpenDialog(false)
                    setVehicleCharger(prev => {
                        let chargerSockets=  [...prev.vehicleChargerSockets];
                        chargerSockets = chargerSockets.map(item => {
                            if (item.name.includes(chargeSocketName))
                                return {...item, active: true}
                            return item
                        })
                        return {...prev, vehicleChargerSockets: chargerSockets}
                    })
                }
            }).catch(() => {
                toast.error("Error. Vehicle charging did not start.")
            })

        },
    });

    return(
        <Box sx={{
            display: "flex",
            paddingBottom: "20px",
            height: "100%"
        }}>
            {vehicleCharger &&
                <Box sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "10px",
                    minWidth: "400px",
                    width: "25%",
                    height: "calc(100vh - 100px)",
                    overflow: "auto",
                    padding: "20px",
                }}>
                    <DeviceData device={vehicleCharger}/>
                    <h3>Charging Sockets</h3>
                    {vehicleCharger.vehicleChargerSockets
                        .sort((a, b) => a.name.localeCompare(b.name))
                        .map(chargingSocket =>
                            <Box key={chargingSocket.name} sx={{
                                width: "100%",
                                padding: "15px 0",
                                borderBottom: "2px solid #092147",
                                display: "flex",
                                flexDirection: "column",
                                gap: "10px"
                            }}>
                                <h4>{chargingSocket.name}</h4>
                                {chargingSocket.active &&
                                    <Box>
                                        <Typography
                                            sx={{
                                                color: 'success.main'
                                            }}>
                                            Active
                                        </Typography>
                                        {currentBatteryPercentages && <Typography>
                                            Current Percentage: {handleBatteryPercentage(chargingSocket.name)}%
                                        </Typography>}
                                    </Box>}
                                {!chargingSocket.active &&
                                    <Typography
                                        sx={{
                                            color: 'error.main'
                                        }}>
                                        Not Active
                                    </Typography>}
                                {chargingSocket.active &&
                                    <Button variant='contained'
                                            sx={{
                                                fontSize: "16px",
                                                textTransform: "capitalize",
                                                width: '100%',
                                                maxHeight: '50px', minHeight: '50px'
                                            }}
                                            onClick={() => handleStopChargingClick(chargingSocket.name)}>
                                        Stop charging
                                    </Button>
                                }
                                {!chargingSocket.active &&
                                    <Box sx={{
                                        display: "flex",
                                        flexDirection: "column",
                                        gap: '10px'
                                    }}>
                                        <Button variant='contained'
                                                sx={{
                                                    fontSize: "16px",
                                                    textTransform: "capitalize",
                                                    width: '100%',
                                                    maxHeight: '50px', minHeight: '50px'
                                                }}
                                                onClick={() => handleStartChargingClick(chargingSocket.name)}>
                                            Start charging
                                        </Button>
                                    </Box>
                                }
                            </Box>
                        )}
                    <Dialog open={openDialog} onClose={() => setOpenDialog(false)}>
                        <DialogTitle>Start charging on {chargeSocketName}</DialogTitle>
                        <DialogContent>
                            <form
                                onSubmit={formik.handleSubmit}>
                                <Box sx={{
                                    display: "flex",
                                    flexDirection: "column",
                                    gap: '20px'
                                }}>
                                    <TextField
                                        sx={{marginTop: "10px"}}
                                        id="batteryCapacity"
                                        name="batteryCapacity"
                                        label="Vehicle Battery Capacity (kWh)"
                                        value={formik.values.batteryCapacity}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.batteryCapacity && Boolean(formik.errors.batteryCapacity)}
                                        helperText={formik.touched.batteryCapacity && formik.errors.batteryCapacity}
                                    />
                                    <TextField
                                        id="currentBatteryPercentage"
                                        name="currentBatteryPercentage"
                                        label="Current Battery (%)"
                                        value={formik.values.currentBatteryPercentage}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.currentBatteryPercentage && Boolean(formik.errors.currentBatteryPercentage)}
                                        helperText={formik.touched.currentBatteryPercentage && formik.errors.currentBatteryPercentage}
                                    />
                                    <TextField
                                        id="chargePercentageLimit"
                                        name="chargePercentageLimit"
                                        label="Charge Limit (%)"
                                        value={formik.values.chargePercentageLimit}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.chargePercentageLimit && Boolean(formik.errors.chargePercentageLimit)}
                                        helperText={formik.touched.chargePercentageLimit && formik.errors.chargePercentageLimit}
                                    />
                                    <Button variant='contained'
                                            sx={{
                                                fontSize: "16px",
                                                textTransform: "capitalize",
                                                width: '100%',
                                                maxHeight: '50px', minHeight: '50px'
                                            }}
                                            type="submit">
                                        Start charging
                                    </Button>
                                </Box>
                            </form>
                        </DialogContent>
                    </Dialog>
                </Box>
            }
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                gap: "20px",
                padding: "20px",
                flexGrow: "1",
                height: "100%"
            }}>
                <ToggleButtonGroup
                    color="primary"
                    exclusive
                    value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    {/*<ToggleButton value="graph">Graph</ToggleButton>*/}
                    <ToggleButton value="table">History</ToggleButton>
                    <ToggleButton value="availability">Availability</ToggleButton>
                </ToggleButtonGroup>

                {display === "table" && vehicleCharger && <VehicleChargerTable vehicleChargerId={vehicleCharger.id}></VehicleChargerTable>}
                {display === "availability" && vehicleCharger && <DeviceAvailabilityGraph deviceId={vehicleCharger.id}/>}

            </Box>
        </Box>
    )
}