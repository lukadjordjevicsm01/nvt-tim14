import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {HomeBattery} from "../../../model/device-model.tsx";
import api from "../../../config/axios-config.tsx";
import toast from "react-hot-toast";
import {Box, ToggleButton, ToggleButtonGroup} from "@mui/material";
import DeviceData from "../../../components/device/device-data.tsx";
import HomeBatteryConsumptionTable from "../../../components/device/home-battery/home-battery-consumption-table.tsx";
import HomeBatteryGraph from "../../../components/device/home-battery/home-battery-graph.tsx";
import WebSocketService from "../../../util/web-socket-service.ts";
import DeviceAvailabilityGraph from "../../../components/device/device-availability-graph.tsx";

export default function HomeBattery() {
    const {homeBatteryId} = useParams();
    const [display, setDisplay] = useState('history');
    const [homeBattery, setHomeBattery] = useState<HomeBattery>();

    const handleToggleChange = (
        event: React.MouseEvent<HTMLElement>,
        newDisplay: string,
    ) => {
        setDisplay(newDisplay);
    };

    useEffect(() => {
        api.get<HomeBattery>(`/device/homeBattery/getHomeBattery/${homeBatteryId}`)
            .then(res => {
                if (res.status === 200) {
                    setHomeBattery(res.data);
                }
            })
            .catch((error) => toast.error(error.response.data.message));

        const subscription = WebSocketService.subscribeToTopic(`/home-battery/${homeBatteryId}`,
            payload => {
                console.log('Received message:', payload);
                if (payload.command === 'currentEnergy') {
                    setHomeBattery(prevHomeBattery => {
                       return {...prevHomeBattery, currentEnergy: payload.payload.currentEnergy}
                    });
                }
            });
        return () => {
            if (subscription)
                subscription.unsubscribe();
        };
    }, []);

    return(
        <Box sx={{
            display: "flex",
            paddingBottom: "20px",
            height: "100%"
        }}>
            {homeBattery &&
                <Box sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: "25%",
                    height: "calc(100vh - 100px)",
                    overflow: "auto",
                    padding: "20px",
                }}>
                    <DeviceData device={homeBattery}/>
                    <Box sx={{
                        width: "100%",
                    }}>
                        <p>Capacity: {homeBattery.capacity} kWh</p>
                        <p>Current energy: {homeBattery.currentEnergy.toFixed(3)} kWh</p>
                    </Box>

                </Box>
            }
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                gap: "20px",
                padding: "20px",
                width: "75%",
                flexGrow: "1",
                height: "calc(100vh - 120px)"
            }}>
                <ToggleButtonGroup
                    color="primary"
                    exclusive
                    value={display}
                    onChange={handleToggleChange}
                    aria-label="Platform">
                    <ToggleButton value="history">History</ToggleButton>
                    <ToggleButton value="graph">Graph</ToggleButton>
                    <ToggleButton value="availability">Availability</ToggleButton>
                </ToggleButtonGroup>

                {display == "graph" &&
                    <HomeBatteryGraph realTime = {true}/>}
                {display == "history" && homeBattery &&
                    <HomeBatteryConsumptionTable homeBatteryId={homeBattery.id}></HomeBatteryConsumptionTable>}
                {display === "availability" && homeBattery && <DeviceAvailabilityGraph deviceId={homeBattery.id}/>}


            </Box>
        </Box>
    )
}