import {getRole} from "../../util/token-utils.tsx";
import {Box, Button, ToggleButton, ToggleButtonGroup} from "@mui/material";
import React, {useEffect, useState} from "react";
import {City, RealEstate} from "../../model/real-estate-model.tsx";
import api from "../../config/axios-config.tsx";
import toast from "react-hot-toast";
import RealEstateTable from "../../components/realestate/real-estate-table.tsx";
import CitiesTable from "../../components/realestate/cities-table.tsx";
import {ElectricityUsage} from "../../model/device-model.tsx";
import ElectricityUsageChart from "../../components/admin/electricity-usage-chart.tsx";
import DateTimeRangePicker from "../../components/util/date-time-range-picker.tsx";

export default function ElectricityUsage() {
    const role = getRole();

    const [subjectCriteria, setSubjectCriteria] = useState('realEstates');
    const [realEstates, setRealEstates]
        = useState<RealEstate[]>([])
    const [cities, setCities]
        = useState<City[]>([])
    const [selectedRealEstate, setSelectedRealEstate]
        = useState<RealEstate>(undefined)
    const [selectedCity, setSelectedCity]
        = useState<City>(undefined)
    const [buttonDisabled, setButtonDisabled] = useState(true)
    const [electricityUsage, setElectricityUsage] = useState<ElectricityUsage>(undefined)
    const [fromTo, setFromTo]
        = useState<FromTo>({from: "-1h", to: "now()"})


    useEffect(() => {
        loadRealEstates();
        loadCities();
    }, []);

    const loadRealEstates = () => {
        api.get<RealEstate[]>(`realEstate/accepted`)
            .then(res => {
                if (res.status == 200){
                    setRealEstates(res.data);
                    console.log(res.data);
                }
            })
            .catch(error => toast.error(error.response.data.message))
    }

    const loadCities = () => {
        api.get<RealEstate[]>(`realEstate/cities`)
            .then(res => {
                if (res.status == 200){
                    setCities(res.data);
                    console.log(res.data);
                }
            })
            .catch(error => toast.error(error.response.data.message))
    }

    const handleToggleSubjectChange = (
        event: React.MouseEvent<HTMLElement>,
        newSubjectCriteria: string,
    ) => {
        setSubjectCriteria(newSubjectCriteria);
        setButtonDisabled(true)
    };

    const handleSelectedRealEstate = (realEstate: RealEstate) => {
        setSelectedRealEstate(realEstate);
        setSelectedCity(undefined)
        setButtonDisabled(false)
    }

    const handleSelectedCity = (city: City) => {
        setSelectedCity(city)
        setSelectedRealEstate(undefined)
        setButtonDisabled(false)
    }

    const getElectricityUsage = () => {
        if (selectedRealEstate) {
            api.get<ElectricityUsage>
            (`/device/homeBattery/getElectricityUsageByRealEstate/${selectedRealEstate.id}`,
                {
                    params: {from: fromTo.from, to: fromTo.to}
                })
                .then(res => {
                    if (res.status === 200) {
                        console.log(res.data);
                        setElectricityUsage(res.data)
                    }
                })
                .catch((error) => toast.error(error.response.data.message));
        }
        if (selectedCity) {
            api.get<ElectricityUsage>
            (`/device/homeBattery/getElectricityUsageByCity/${selectedCity.id}`,
                {
                    params: {from: fromTo.from, to: fromTo.to}
                })
                .then(res => {
                    if (res.status === 200) {
                        console.log(res.data);
                        setElectricityUsage(res.data)
                    }
                })
                .catch((error) => toast.error(error.response.data.message));
        }

    }

    const getMeasurement = (from: string = "-1h", to: string = "now()") => {
        setFromTo({from: from, to: to});
    }

    useEffect(() => {
        getElectricityUsage();
    }, [fromTo])

    return (
        <>
            {!role.includes("ROLE_ADMIN") &&
                <Box sx={{
                    padding: "20px"
                }}>
                    <h1>You are not authorized for this page!</h1>
                </Box>
            }
            {role.includes("ROLE_ADMIN") &&
                <Box sx={{
                    height: "calc(100vh - 200px)",
                    padding: "20px",
                    display: "flex",
                    gap: "20px"
                }}>
                    <Box sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "10px",
                        width: "50%",
                    }}>
                        <Box sx={{
                            display: "flex",
                            gap: "10px",
                        }}>
                            <ToggleButtonGroup
                                color="primary"
                                exclusive
                                value={subjectCriteria}
                                onChange={handleToggleSubjectChange}
                                aria-label="Platform">
                                <ToggleButton value="realEstates">Real Estates</ToggleButton>
                                <ToggleButton value="cities">Cities</ToggleButton>
                            </ToggleButtonGroup>
                        </Box>
                        {subjectCriteria == 'realEstates' &&
                            <RealEstateTable realEstates={realEstates}
                                             handleSelectedRealEstate={handleSelectedRealEstate}></RealEstateTable>}
                        {subjectCriteria == 'cities' &&
                            <CitiesTable cities={cities}
                                         handleSelectedCity={handleSelectedCity}></CitiesTable>}
                        <Button
                            disabled={buttonDisabled}
                            onClick={getElectricityUsage}
                            variant="contained"
                            sx={{
                                textTransform: "capitalize",
                                backgroundColor: "primary.main",
                                color: "secondary.main",
                            }}>
                            See Electricity Usage
                        </Button>
                    </Box>
                    <Box sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "10px",
                        flexGrow: "1"
                    }}>
                        <DateTimeRangePicker getMeasurement={getMeasurement} flag={"none"}/>
                        {electricityUsage && <ElectricityUsageChart electricityUsage={electricityUsage}/>}
                    </Box>
                </Box>
            }
        </>
    )
}

interface FromTo{
    from: string,
    to: string
}