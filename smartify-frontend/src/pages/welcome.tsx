import {Button, SvgIcon} from "@mui/material";
import LogoDark from "../assets/logo-dark.svg?react"
import {useNavigate} from "react-router-dom";
import "./auth/login-registration.css"
import {useEffect, useState} from "react";


export default function Welcome () {
    const navigate = useNavigate();
    const [topic, setTopic] = useState('general');
    const [receivedMessages, setReceivedMessages] = useState([]);


    const handleRegistration = () => {
        navigate('/registration')
    }

    return (
        <div id={"main-container"}>
            <div style={{width: "50%", height: "100%"}}>
                <img src={"img/smarthouse-welcome.png"} alt={"Welcome"}
                     style={{
                         width: "100%",
                         height: "100%",
                         objectFit: "cover"
                     }}/>
            </div>

            <div style={{
                width: "50%",
                display: "flex",
                flexDirection: "column",
                gap: "30px",
                textAlign: "left",
                padding: "3%"
            }}>
                <div>
                    <SvgIcon id={"logo"} inheritViewBox component={LogoDark}/>
                    <h1 id={"slogan"}>Your Home, Your Way</h1>
                </div>
                <div style={{
                    fontSize: "22px",
                }}>
                    Welcome to Smartify – where your home becomes a personalized haven with cutting-edge technology.
                    "Your home, your way" is our commitment, giving you control over ambiance, security,
                    and efficiency. Smartify integrates innovation and comfort,
                    ensuring your living space adapts to you. Welcome to the future of smart living.
                </div>
                <Button style={{
                    fontSize: "16px",
                    textTransform: "capitalize",
                    background: "#092147",
                    color: "#F5F5F5",
                    maxWidth: '160px', maxHeight: '40px', minWidth: '160px', minHeight: '40px'
                }} onClick={handleRegistration}>Get started</Button>
            </div>
        </div>
    )
}