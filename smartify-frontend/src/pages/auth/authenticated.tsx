import {isLoggedIn} from "../../util/token-utils.tsx";
import {Navigate} from "react-router-dom";

export default function Authenticated({ children } : any) {
    if (!isLoggedIn())
        return <Navigate to="/login" />;

    return children
}
