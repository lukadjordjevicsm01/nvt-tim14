import RegistrationForm from "../../components/auth/registration-form.tsx";
import {SvgIcon} from "@mui/material";
import LogoDark from "../../assets/logo-dark.svg?react"
import "./login-registration.css"


export default function Registration(){
    return (
        <div id={"main-container"}>
            <div id={"logo-slogan"}>
                <SvgIcon id={"logo"} inheritViewBox component={LogoDark}/>
                <h1 id={"slogan"}>Your Home, Your Way</h1>
            </div>
            <div style={{width: "50%", padding: "10%"}}>
                <RegistrationForm/>
            </div>

        </div>
    )
}