import {isLoggedIn} from "../../util/token-utils.tsx";
import {Navigate, useNavigate} from "react-router-dom";

export default function Unauthenticated({ children } : any) {

    if (isLoggedIn())
        return <Navigate to="/home" />;

    return children
}