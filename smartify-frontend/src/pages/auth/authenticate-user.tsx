import {getRole} from "../../util/token-utils.tsx";
import {Navigate} from "react-router-dom";

export default function AuthenticateUser({ children } : any) {
    if (getRole() != "ROLE_USER")
        return <Navigate to={"/home"}/>

    return children;
}