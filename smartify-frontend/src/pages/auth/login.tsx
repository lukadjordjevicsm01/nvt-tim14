/// <reference types="vite-plugin-svgr/client" />
import LoginForm from "../../components/auth/login-form.tsx";
import {SvgIcon} from "@mui/material";
import LogoDark from "../../assets/logo-dark.svg?react"
import "./login-registration.css"

export default function Login() {
    return (
        <div id={"main-container"}>
            <div id={"logo-slogan"}>
                <SvgIcon id={"logo"} inheritViewBox component={LogoDark}/>
                <h1 id={"slogan"}>Your Home, Your Way</h1>
            </div>
            <LoginForm/>
        </div>
    )
}