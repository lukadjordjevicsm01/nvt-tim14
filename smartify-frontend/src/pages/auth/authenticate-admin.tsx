import {getRole, isLoggedIn} from "../../util/token-utils.tsx";
import {Navigate} from "react-router-dom";

export default function AuthenticateAdmin({ children } : any) {
    if (!getRole().includes("ROLE_ADMIN") && !getRole().includes("ROLE_SUPERADMIN"))
        return <Navigate to={"/home"}/>

    return children;
}
