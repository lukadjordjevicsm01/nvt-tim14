INSERT INTO users (name, surname, email, password, is_verified, profile_picture_path, last_password_reset_date)
VALUES (
           'Milos',
           'Cuturic',
           'milos@mail.com',
           '$2a$12$7TU1dI3DgBLR4gEGXqn1GOZALBEw8cDraCBbCcbtp71/n13KR53h2',
           true,
           '',
           '2023-11-30 00:00:00.0'
       );
INSERT INTO users (name, surname, email, password, is_verified, profile_picture_path, last_password_reset_date)
VALUES (
           'Luka',
           'Djordjevic',
           'luka@mail.com',
           '$2a$12$.SfCRXyPumZjPiUVDBd3L.tDv2Uq3iCynP3hUZWeLrLCepTg67Qwi',
           true,
           '',
           '2023-11-30 00:00:00.0'
       );
INSERT INTO users (name, surname, email, password, is_verified, profile_picture_path, last_password_reset_date)
VALUES (
           'Marko',
           'Janosevic',
           'marko@mail.com',
           '$2a$12$u5AYPWbQ2HLWuxiNXFsI/Od5fQuS.kubEgV.he2c1.iH6Et93Q0We',
           true,
           '',
           '2023-11-30 00:00:00.0'
       );
INSERT INTO users (name, surname, email, password, is_verified, profile_picture_path, last_password_reset_date)
VALUES (
           'Pera',
           'Peric',
           'pera@mail.com',
           '$2a$12$u5AYPWbQ2HLWuxiNXFsI/Od5fQuS.kubEgV.he2c1.iH6Et93Q0We',
           true,
           'admin.svg',
           '2023-11-30 00:00:00.0'
       );


INSERT INTO ROLE (name) VALUES ('ROLE_USER');
INSERT INTO ROLE (name) VALUES ('ROLE_ADMIN');
INSERT INTO ROLE (name) VALUES ('ROLE_SUPERADMIN');

INSERT INTO USER_ROLE (user_id, role_id) VALUES (1, 2);
INSERT INTO USER_ROLE (user_id, role_id) VALUES (2, 2);
INSERT INTO USER_ROLE (user_id, role_id) VALUES (3, 1);
INSERT INTO USER_ROLE (user_id, role_id) VALUES (4, 1);

INSERT INTO countries (name, country_code) VALUES ('Serbia', 'RS');
INSERT INTO countries (name, country_code) VALUES ('Bosnia and Herzegovina', 'BA');
INSERT INTO countries (name, country_code) VALUES ('Montenegro', 'ME');

-- Serbia
INSERT INTO cities (name, country_id, zip_code) VALUES ('Belgrade', 1, '11');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Novi Sad', 1, '21');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Niš', 1, '18');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Kragujevac', 1, '34');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Sremska Mitrovica', 1, '22');

-- Bosnia and Herzegovina
INSERT INTO cities (name, country_id, zip_code) VALUES ('Sarajevo', 2, '71');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Banja Luka', 2, '78');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Tuzla', 2, '75');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Mostar', 2, '88');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Zenica', 2, '72');

-- Montenegro
INSERT INTO cities (name, country_id, zip_code) VALUES ('Podgorica', 3, '81000');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Nikšić', 3, '81400');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Herceg Novi', 3, '85340');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Budva', 3, '85310');
INSERT INTO cities (name, country_id, zip_code) VALUES ('Bar', 3, '85000');


INSERT INTO addresses (city_id, latitude, longitude, street_name)
VALUES (1, -78.9012, 45.6789, 'Example Street 1');

INSERT INTO addresses (city_id, latitude, longitude, street_name)
VALUES (2, -78.9012, 45.6789, 'Example Street 2');

INSERT INTO real_estate (real_estate_type, address_id, picture_path, status, floors, real_estate_name, owner_id)
VALUES (1, 1, '', 1, 2, 'Example Apartment 1', 3);

INSERT INTO real_estate (real_estate_type, address_id, picture_path, status, floors, real_estate_name, owner_id)
VALUES (1, 2, '', 1, 2, 'Example Apartment 2', 3);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Ambient sensor 1', 0, 0.001, 1, 'ambientSensor.jpg');
INSERT INTO ambient_sensors (temperature, humidity, id) VALUES (10.0, 10.0, 1);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Lamp 1', 1, 0.01, 1, 'lamp.jpg');
INSERT INTO lamps (is_automatic, current_brightness, is_light_on, id) VALUES (false, 1000.0, false, 2);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Gate 1', 1, 0.4, 1, 'gate.jpg');
INSERT INTO gates (is_open, mode, id) VALUES (false, 2, 3);
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (3, 'SM034RN');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (3, 'RU088RN');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (3, 'SM123AB');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (3, 'SM099ES');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (3, 'NS055RB');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (3, 'SM064RA');


INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Solar Panel System 1', 1, 0, 1, 'solarPanelSystem.jpg');
INSERT INTO solar_panel_systems (id) VALUES (4);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Solar Panel 1', 1, 0, 1, 'solarPanel.jpg');
INSERT INTO solar_panels (area, efficiency, id, solar_panel_system_id) VALUES (2, 30, 5, 4);


INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Solar Panel 2', 1, 0, 1, 'solarPanel.jpg');
INSERT INTO solar_panels (area, efficiency, id, solar_panel_system_id) VALUES (2.5, 35, 6, 4);


INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Solar Panel 3', 1, 0, 1, 'solarPanel.jpg');
INSERT INTO solar_panels (area, efficiency, id, solar_panel_system_id) VALUES (3, 32, 7, 4);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Air conditioning 1', 1, 0.5, 1, 'airConditioning.jpg');
INSERT INTO air_conditionings (id, current_temperature, min_temperature, max_temperature, current_mode, is_settings_active)
VALUES (8, 18.0, -5.0, 40.0, 'COOLING', true);
INSERT INTO supported_modes (air_conditioning_id, supported_mode) VALUES (8, 'COOLING');
INSERT INTO supported_modes (air_conditioning_id, supported_mode) VALUES (8, 'HEATING');
INSERT INTO supported_modes (air_conditioning_id, supported_mode) VALUES (8, 'AUTOMATIC');
INSERT INTO supported_modes (air_conditioning_id, supported_mode) VALUES (8, 'VENTILATION');

INSERT INTO air_conditioning_settings (air_conditioning_id, end_time, start_time, mode, temperature)
VALUES (8, '2023-12-17 12:30:00', '2023-12-17 13:30:00', 'COOLING', 18);
INSERT INTO air_conditioning_settings (air_conditioning_id, end_time, start_time, mode, temperature)
VALUES (8, '2023-12-17 15:30:00', '2023-12-17 16:30:00', 'HEATING', 20);
INSERT INTO air_conditioning_settings (air_conditioning_id, end_time, start_time, mode, temperature)
VALUES (8, '2023-12-17 17:30:00', '2023-12-17 18:30:00', 'AUTOMATIC', 19);
INSERT INTO air_conditioning_settings (air_conditioning_id, end_time, start_time, mode, temperature)
VALUES (8, '2023-12-17 20:30:00', '2023-12-17 21:30:00', 'VENTILATION', 25);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Home Battery 1', 2, 1, 1, 'homeBattery.jpg');
INSERT INTO home_batteries (capacity, current_energy, id) VALUES (10, 9, 9);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Sprinkler 1', 2, 0.1, 1, 'sprinkler.jpg');
INSERT INTO sprinklers (id, is_sprinkler_on) VALUES (10, false);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Washing machine 1', 1, 0.5, 1, 'washingMachine.jpg');
INSERT INTO washing_machines (id, is_settings_active)
VALUES (11, true);

INSERT INTO washing_machine_modes (duration, name, washing_machine_id)
VALUES (1, '30°C', 11);
INSERT INTO washing_machine_modes (duration, name, washing_machine_id)
VALUES (60, '90°C', 11);
INSERT INTO washing_machine_modes (duration, name, washing_machine_id)
VALUES (60, 'rinse', 11);

INSERT INTO washing_machine_settings (mode_id, start_time, washing_machine_id)
VALUES (1, '2024-01-27 20:30:00', 11);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Vehicle Charger 1', 2, 1, 1, 'vehicleCharger.jpg');
INSERT INTO vehicle_chargers (charging_power, id) VALUES (25, 12);
INSERT INTO vehicle_charger_sockets (name, is_active, charging_limit, vehicle_charger_id)
VALUES ('Vehicle Charger 1_Socket_1', false, 100, 12);
INSERT INTO vehicle_charger_sockets (name, is_active, charging_limit, vehicle_charger_id)
VALUES ('Vehicle Charger 1_Socket_2', false, 100, 12);

-- New devices

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Ambient sensor 2', 0, 0.001, 2, 'ambientSensor.jpg');
INSERT INTO ambient_sensors (temperature, humidity, id) VALUES (10.0, 10.0, 13);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Lamp 2', 1, 0.01, 2, 'lamp.jpg');
INSERT INTO lamps (is_automatic, current_brightness, is_light_on, id) VALUES (false, 1000.0, false, 14);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Gate 2', 1, 0.4, 2, 'gate.jpg');
INSERT INTO gates (is_open, mode, id) VALUES (false, 2, 15);
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (15, 'SM034RN');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (15, 'RU088RN');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (15, 'SM123AB');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (15, 'SM099ES');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (15, 'NS055RB');
INSERT INTO allowed_vehicles (gate_id, vehicle) VALUES (15, 'SM064RA');

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Solar Panel System 2', 1, 0, 2, 'solarPanelSystem.jpg');
INSERT INTO solar_panel_systems (id) VALUES (16);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Solar Panel 4', 1, 0, 2, 'solarPanel.jpg');
INSERT INTO solar_panels (area, efficiency, id, solar_panel_system_id) VALUES (2, 30, 17, 16);


INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Solar Panel 5', 1, 0, 2, 'solarPanel.jpg');
INSERT INTO solar_panels (area, efficiency, id, solar_panel_system_id) VALUES (2.5, 35, 18, 16);


INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Solar Panel 6', 1, 0, 2, 'solarPanel.jpg');
INSERT INTO solar_panels (area, efficiency, id, solar_panel_system_id) VALUES (3, 32, 19, 16);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Air conditioning 2', 1, 0.5, 2, 'airConditioning.jpg');
INSERT INTO air_conditionings (id, current_temperature, min_temperature, max_temperature, current_mode, is_settings_active)
VALUES (20, 18.0, -5.0, 40.0, 'COOLING', true);
INSERT INTO supported_modes (air_conditioning_id, supported_mode) VALUES (20, 'COOLING');
INSERT INTO supported_modes (air_conditioning_id, supported_mode) VALUES (20, 'HEATING');
INSERT INTO supported_modes (air_conditioning_id, supported_mode) VALUES (20, 'AUTOMATIC');
INSERT INTO supported_modes (air_conditioning_id, supported_mode) VALUES (20, 'VENTILATION');

INSERT INTO air_conditioning_settings (air_conditioning_id, end_time, start_time, mode, temperature)
VALUES (20, '2023-12-17 12:30:00', '2023-12-17 13:30:00', 'COOLING', 18);
INSERT INTO air_conditioning_settings (air_conditioning_id, end_time, start_time, mode, temperature)
VALUES (20, '2023-12-17 15:30:00', '2023-12-17 16:30:00', 'HEATING', 20);
INSERT INTO air_conditioning_settings (air_conditioning_id, end_time, start_time, mode, temperature)
VALUES (20, '2023-12-17 17:30:00', '2023-12-17 18:30:00', 'AUTOMATIC', 19);
INSERT INTO air_conditioning_settings (air_conditioning_id, end_time, start_time, mode, temperature)
VALUES (20, '2023-12-17 20:30:00', '2023-12-17 21:30:00', 'VENTILATION', 25);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Home Battery 2', 2, 1, 2, 'homeBattery.jpg');
INSERT INTO home_batteries (capacity, current_energy, id) VALUES (10, 9, 21);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Sprinkler 2', 2, 0.1, 2, 'sprinkler.jpg');
INSERT INTO sprinklers (id, is_sprinkler_on) VALUES (22, false);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Washing machine 2', 1, 0.5, 2, 'washingMachine.jpg');
INSERT INTO washing_machines (id, is_settings_active)
VALUES (23, true);

INSERT INTO washing_machine_modes (duration, name, washing_machine_id)
VALUES (1, '30°C', 23);
INSERT INTO washing_machine_modes (duration, name, washing_machine_id)
VALUES (60, '90°C', 23);
INSERT INTO washing_machine_modes (duration, name, washing_machine_id)
VALUES (60, 'rinse', 23);

INSERT INTO washing_machine_settings (mode_id, start_time, washing_machine_id)
VALUES (4, '2024-01-27 20:30:00', 23);

INSERT INTO devices (is_online, is_on, name, power_supply_type, consumption, real_estate_id, image_path)
VALUES (true, true, 'Vehicle Charger 2', 2, 1, 2, 'vehicleCharger.jpg');
INSERT INTO vehicle_chargers (charging_power, id) VALUES (25, 24);
INSERT INTO vehicle_charger_sockets (name, is_active, charging_limit, vehicle_charger_id)
VALUES ('Vehicle Charger 2_Socket_1', false, 100, 24);
INSERT INTO vehicle_charger_sockets (name, is_active, charging_limit, vehicle_charger_id)
VALUES ('Vehicle Charger 2_Socket_2', false, 100, 24);
