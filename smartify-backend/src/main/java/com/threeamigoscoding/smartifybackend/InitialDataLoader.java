package com.threeamigoscoding.smartifybackend;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.dto.realestate.CityDTO;
import com.threeamigoscoding.smartifybackend.dto.realestate.RealEstateDevicesDTO;
import com.threeamigoscoding.smartifybackend.mappers.realestate.AddressMapper;
import com.threeamigoscoding.smartifybackend.model.device.Device;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.*;
import com.threeamigoscoding.smartifybackend.model.device.home.*;
import com.threeamigoscoding.smartifybackend.model.device.outside.*;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.outside.LampRepositoryInflux;
import com.threeamigoscoding.smartifybackend.service.device.DeviceService;
import com.threeamigoscoding.smartifybackend.service.device.electroenergetic.*;
import com.threeamigoscoding.smartifybackend.service.device.home.AirConditioningService;
import com.threeamigoscoding.smartifybackend.service.device.home.AmbientSensorService;
import com.threeamigoscoding.smartifybackend.service.device.home.WashingMachineModeService;
import com.threeamigoscoding.smartifybackend.service.device.home.WashingMachineService;
import com.threeamigoscoding.smartifybackend.service.device.outside.GateService;
import com.threeamigoscoding.smartifybackend.service.device.outside.LampService;
import com.threeamigoscoding.smartifybackend.service.device.outside.SprinklerService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.service.user.UserService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import jakarta.persistence.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class InitialDataLoader implements CommandLineRunner {

    @Autowired
    private UserService userService;

    @Autowired
    private AmbientSensorService ambientSensorService;

    @Autowired
    private LampService lampService;

    @Autowired
    private GateService gateService;

    @Autowired
    private SolarPanelSystemService solarPanelSystemService;

    @Autowired
    private SolarPanelService solarPanelService;

    @Autowired
    private AirConditioningService airConditioningService;

    @Autowired
    private HomeBatteryService homeBatteryService;

    @Autowired
    private LampRepositoryInflux lampRepositoryInflux;

    @Autowired
    private RealEstateService realEstateService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private VehicleChargerService vehicleChargerService;

    @Autowired
    private VehicleChargerSocketService vehicleChargerSocketService;

    @Autowired
    private SprinklerService sprinklerService;

    @Autowired
    private WashingMachineService washingMachineService;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private WashingMachineModeService washingMachineModeService;

    @Override
    public void run(String... args) {
        userService.createSuperAdmin();

        // Home devices insert
        // insertAmbientSensors();
        //
        // insertAirConditioning();
        // insertWashingMachines();

        // Outside devices insert
        // insertLamps();
        // insertGates();
        //insertSprinklers();

        // Electro-energetic devices insert
        // insertSolarPanelSystems();
        // insertHomeBatteries();
        // insertVehicleChargers();

        initializeSimulator();
    }

    private void initializeSimulator() {
        try {
            String url = MyCredentials.simulatorServer;
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            sendRealEstateDevices(url, headers, restTemplate);

            // home
            sendAmbientSensors(url, headers, restTemplate);
            sendAirConditioning(url, headers, restTemplate);
            sendWashingMachines(url, headers, restTemplate);

            sendLamps(url, headers, restTemplate);
            sendGates(url, headers, restTemplate);
            sendSprinklers(url, headers, restTemplate);

            sendSolarPanelSystems(url, headers, restTemplate);
            sendHomeBatteries(url, headers, restTemplate);
            sendVehicleChargers(url, headers, restTemplate);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void sendAmbientSensors(String url, HttpHeaders headers, RestTemplate restTemplate) {
        List<AmbientSensor> ambientSensors = ambientSensorService.getAll();
        HttpEntity<List<AmbientSensor>> requestEntity = new HttpEntity<>(ambientSensors, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-ambient-sensors",
                requestEntity,
                String.class);
        ambientSensorService.subscribeToTopics(ambientSensors);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void sendLamps(String url, HttpHeaders headers, RestTemplate restTemplate){
        List<Lamp> lamps = lampService.getAll();
        HttpEntity<List<Lamp>> requestEntity = new HttpEntity<>(lamps, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-lamps",
                requestEntity,
                String.class);
        lampService.subscribeToTopics(lamps);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void sendGates(String url, HttpHeaders headers, RestTemplate restTemplate){
        List<Gate> gates = gateService.getAll();
        System.out.println(gates.get(0).getAllowedVehicles());
        HttpEntity<List<Gate>> requestEntity = new HttpEntity<>(gates, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-gates",
                requestEntity,
                String.class);
        gateService.subscribeToTopics(gates);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void sendSolarPanelSystems(String url, HttpHeaders headers, RestTemplate restTemplate) {
        List<SolarPanelSystem> solarPanelSystems = solarPanelSystemService.getAll();
        HttpEntity<List<SolarPanelSystem>> requestEntity = new HttpEntity<>(solarPanelSystems, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-solar-panel-systems",
                requestEntity,
                String.class);
        solarPanelSystemService.subscribeToTopics(solarPanelSystems);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void sendAirConditioning(String url, HttpHeaders headers, RestTemplate restTemplate) {
        List<AirConditioning> airConditioning = airConditioningService.getAll();
        HttpEntity<List<AirConditioning>> requestEntity = new HttpEntity<>(airConditioning, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-air-conditioning",
                requestEntity,
                String.class);
        airConditioningService.subscribeToTopics(airConditioning);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void sendSprinklers(String url, HttpHeaders headers, RestTemplate restTemplate){
        List<Sprinkler> sprinklers = sprinklerService.getAll();
        HttpEntity<List<Sprinkler>> requestEntity = new HttpEntity<>(sprinklers, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-sprinklers",
                requestEntity,
                String.class);
        sprinklerService.subscribeToTopics(sprinklers);
        System.out.println("Response: " + responseEntity.getBody());
    }
    
    private void sendHomeBatteries(String url, HttpHeaders headers, RestTemplate restTemplate) {
        List<HomeBattery> homeBatteries = homeBatteryService.getAll();
        HttpEntity<List<HomeBattery>> requestEntity = new HttpEntity<>(homeBatteries, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-home-batteries",
                requestEntity,
                String.class);
        homeBatteryService.subscribeToTopics(homeBatteries);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void sendVehicleChargers(String url, HttpHeaders headers, RestTemplate restTemplate) {
        List<VehicleCharger> vehicleChargers = vehicleChargerService.getAll();
        HttpEntity<List<VehicleCharger>> requestEntity = new HttpEntity<>(vehicleChargers, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-vehicle-chargers",
                requestEntity,
                String.class);
        vehicleChargerService.subscribeToTopics(vehicleChargers);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void sendWashingMachines(String url, HttpHeaders headers, RestTemplate restTemplate) {
        List<WashingMachine> washingMachines = washingMachineService.getAll();
        HttpEntity<List<WashingMachine>> requestEntity = new HttpEntity<>(washingMachines, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-washing-machines",
                requestEntity,
                String.class);
        washingMachineService.subscribeToTopics(washingMachines);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void sendRealEstateDevices(String url, HttpHeaders headers, RestTemplate restTemplate) {
        List<RealEstate> realEstates = realEstateService.getAll();
        List<RealEstateDevicesDTO> realEstateDevices = new ArrayList<>();
        for (RealEstate realEstate: realEstates) {
            Set<Integer> deviceIds = realEstate.getDevices().stream().map(Device::getId).collect(Collectors.toSet());
            CityDTO cityDTO = addressMapper.cityToCityDTO(realEstate.getAddress().getCity());
            RealEstateDevicesDTO realEstateDevicesDTO = new RealEstateDevicesDTO(
                    realEstate.getId(),
                    cityDTO,
                    deviceIds
            );
            realEstateDevices.add(realEstateDevicesDTO);
        }
        HttpEntity<List<RealEstateDevicesDTO>> requestEntity = new HttpEntity<>(realEstateDevices, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url + "set-devices-real-estates",
                requestEntity,
                String.class);
        System.out.println("Response: " + responseEntity.getBody());
    }

    private void insertAmbientSensors() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 1000; i++) {
            AmbientSensor ambientSensor = new AmbientSensor();
            ambientSensor.setOnline(true);
            ambientSensor.setOn(true);
            ambientSensor.setName("Ambient sensor");
            ambientSensor.setPowerSupplyType(PowerSupplyType.AUTONOMOUSLY);
            ambientSensor.setConsumption(0.01);
            ambientSensor.setImagePath("default.png");
            ambientSensor.setRealEstate(realEstate);
            ambientSensor.setTemperature(18D);
            ambientSensor.setHumidity(18D);
            ambientSensorService.save(ambientSensor);
        }
    }

    private void insertAirConditioning() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 100; i++) {
            AirConditioning airConditioning = new AirConditioning();

            airConditioning.setOnline(true);
            airConditioning.setOn(true);
            airConditioning.setName("Ambient sensor");
            airConditioning.setPowerSupplyType(PowerSupplyType.AUTONOMOUSLY);
            airConditioning.setConsumption(0.01);
            airConditioning.setImagePath("default.png");
            airConditioning.setRealEstate(realEstate);

            airConditioning.setCurrentTemperature(18D);
            airConditioning.setMinTemperature(10D);
            airConditioning.setMaxTemperature(30D);

            Set<AirConditioningMode> supportedModes = new HashSet<>();
            supportedModes.add(AirConditioningMode.COOLING);
            supportedModes.add(AirConditioningMode.HEATING);
            airConditioning.setSupportedModes(supportedModes);
            airConditioning.setCurrentMode(AirConditioningMode.COOLING);

            airConditioning.setSettingsActive(true);
            airConditioning.setCustomSettings(new HashSet<>());

            airConditioningService.save(airConditioning);
        }
    }

    private void insertWashingMachines() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 1000; i++) {
            WashingMachine washingMachine = new WashingMachine();

            washingMachine.setOnline(true);
            washingMachine.setOn(true);
            washingMachine.setName("Ambient sensor");
            washingMachine.setPowerSupplyType(PowerSupplyType.AUTONOMOUSLY);
            washingMachine.setConsumption(0.01);
            washingMachine.setImagePath("default.png");
            washingMachine.setRealEstate(realEstate);

            WashingMachineMode washingMachineMode = new WashingMachineMode();
            washingMachineMode.setName("RINSE");
            washingMachineMode.setDuration(1D);
            Set<WashingMachineMode> washingMachineModes = new HashSet<>();
            washingMachineModes.add(washingMachineMode);
            washingMachine.setSupportedModes(washingMachineModes);

            washingMachine.setSettingsActive(true);
            washingMachine.setCustomSettings(new HashSet<>());

            washingMachineService.save(washingMachine);
        }
    }

    private void insertSolarPanelSystems() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 1000; i++) {
            SolarPanelSystem solarPanelSystem = new SolarPanelSystem();

            solarPanelSystem.setOnline(true);
            solarPanelSystem.setOn(true);
            solarPanelSystem.setName("Solar Panel System Test");
            solarPanelSystem.setPowerSupplyType(PowerSupplyType.HOUSE_BATTERY);
            solarPanelSystem.setConsumption(0.0);
            solarPanelSystem.setImagePath("default.png");
            solarPanelSystem.setRealEstate(realEstate);

            SolarPanel solarPanel = new SolarPanel();
            solarPanel.setOnline(true);
            solarPanel.setOn(true);
            solarPanel.setName("Solar Panel System Test");
            solarPanel.setPowerSupplyType(PowerSupplyType.HOUSE_BATTERY);
            solarPanel.setConsumption(0.0);
            solarPanel.setImagePath("default.png");
            solarPanel.setRealEstate(realEstate);
            solarPanel.setArea(10.0);
            solarPanel.setEfficiency(30.0);

            Set<SolarPanel> solarPanels = new HashSet<>();
            solarPanels.add(solarPanel);

            solarPanelSystem.setSolarPanels(solarPanels);

            solarPanelService.save(solarPanel);
            solarPanelSystemService.save(solarPanelSystem);
        }
    }

    private void insertHomeBatteries() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 100; i++) {
            HomeBattery homeBattery = new HomeBattery();

            homeBattery.setOnline(true);
            homeBattery.setOn(true);
            homeBattery.setName("Home Battery Test");
            homeBattery.setPowerSupplyType(PowerSupplyType.HOUSE_BATTERY);
            homeBattery.setConsumption(1.0);
            homeBattery.setImagePath("default.png");
            homeBattery.setRealEstate(realEstate);

            homeBattery.setCapacity(10.0);
            homeBattery.setCurrentEnergy(9.0);

            homeBatteryService.save(homeBattery);
        }
    }

    private void insertVehicleChargers() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 1000; i++) {
            VehicleCharger vehicleCharger = new VehicleCharger();

            vehicleCharger.setOnline(true);
            vehicleCharger.setOn(true);
            vehicleCharger.setName("Vehicle Charger Test");
            vehicleCharger.setPowerSupplyType(PowerSupplyType.HOUSE_BATTERY);
            vehicleCharger.setConsumption(1.0);
            vehicleCharger.setImagePath("default.png");
            vehicleCharger.setRealEstate(realEstate);

            Set<VehicleChargerSocket> vehicleChargerSockets = new HashSet<>();

            VehicleChargerSocket vehicleChargerSocket = new VehicleChargerSocket();
            vehicleChargerSocket.setName("Vehicle Charger Test_Socket_1");
            vehicleChargerSocket.setActive(false);
            vehicleChargerSocket.setChargingLimit(100.0);
            vehicleChargerSockets.add(vehicleChargerSocket);

            vehicleCharger.setChargingPower(25.0);
            vehicleCharger.setVehicleChargerSockets(vehicleChargerSockets);

            vehicleChargerSocketService.save(vehicleChargerSocket);
            vehicleChargerService.save(vehicleCharger);
        }
    }

    private void insertLamps() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 1000; i++) {
            Lamp lamp = new Lamp();

            lamp.setOnline(true);
            lamp.setOn(true);
            lamp.setName("Lamp");
            lamp.setPowerSupplyType(PowerSupplyType.HOUSE_BATTERY);
            lamp.setConsumption(1.0);
            lamp.setImagePath("default.png");
            lamp.setRealEstate(realEstate);

            lamp.setCurrentBrightness(100D);
            lamp.setLightOn(true);
            lamp.setAutomatic(true);

            lampService.save(lamp);
        }
    }

    private void insertGates() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 500; i++) {
            Gate gate = new Gate();

            gate.setOnline(true);
            gate.setOn(true);
            gate.setName("Gate");
            gate.setPowerSupplyType(PowerSupplyType.HOUSE_BATTERY);
            gate.setConsumption(1.0);
            gate.setImagePath("default.png");
            gate.setRealEstate(realEstate);

            gate.setMode(GateMode.MANUAL);
            gate.setOpen(false);
            Set<String> vehicles = new HashSet<>();
            vehicles.add("SM123AB");
            gate.setAllowedVehicles(vehicles);

            gateService.save(gate);

        }
    }

    private void insertSprinklers() {
        RealEstate realEstate = realEstateService.findById(1);
        for (int i = 0; i < 1000; i++) {
            Sprinkler sprinkler = new Sprinkler();

            sprinkler.setOnline(true);
            sprinkler.setOn(true);
            sprinkler.setName("Lamp");
            sprinkler.setPowerSupplyType(PowerSupplyType.HOUSE_BATTERY);
            sprinkler.setConsumption(1.0);
            sprinkler.setImagePath("default.png");
            sprinkler.setRealEstate(realEstate);

            sprinkler.setSprinklerOn(true);
            sprinkler.setSprinklerSettings(new HashSet<>());

            sprinklerService.save(sprinkler);

        }
    }
}
