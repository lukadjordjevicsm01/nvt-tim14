package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.home.AirConditioningRecord;
import com.threeamigoscoding.smartifybackend.influx_records.home.WashingMachineRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class WashingMachineRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }

    private List<WashingMachineRecord> query(String fluxQuery, Integer id) {
        List<WashingMachineRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                result.add(new WashingMachineRecord(
                        id,
                        fluxRecord.getValueByKey("subject") == null ? "" :
                                Objects.requireNonNull(fluxRecord.getValueByKey("subject")).toString(),
                        fluxRecord.getValue() == null ? "" : ((String) fluxRecord.getValue()),
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli())
                ));
            }
        }
        return result;
    }

    public List<WashingMachineRecord> findByDateRange(String from, String to, String id) {
        String fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"id\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])",
                from, to, "washing machine", id);
        return this.query(fluxQuery, Integer.valueOf(id));
    }

}
