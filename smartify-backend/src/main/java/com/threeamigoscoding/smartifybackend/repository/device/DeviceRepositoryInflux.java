package com.threeamigoscoding.smartifybackend.repository.device;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.DeviceAvailabilityRecord;
import com.threeamigoscoding.smartifybackend.influx_records.outside.SprinklerEventRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class DeviceRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }

    private List<DeviceAvailabilityRecord> query(String fluxQuery, Integer sprinklerId) {
        List<DeviceAvailabilityRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                DeviceAvailabilityRecord deviceAvailabilityRecord = new DeviceAvailabilityRecord();
                deviceAvailabilityRecord.setIntervalStart(fluxRecord.getValueByKey("_start") == null ?
                        new Timestamp(new Date().getTime()) : new Timestamp(fluxRecord.getStart().toEpochMilli()));

                deviceAvailabilityRecord.setIntervalEnd(fluxRecord.getValueByKey("_stop") == null ?
                        new Timestamp(new Date().getTime()) : new Timestamp(fluxRecord.getStop().toEpochMilli()));

                deviceAvailabilityRecord.setOnlineTime(fluxRecord.getValueByKey("_online") == null ?
                        0 : ((Number) fluxRecord.getValueByKey("_online")).doubleValue());

                deviceAvailabilityRecord.setOfflineTime(fluxRecord.getValueByKey("_offline") == null ?
                        0 : ((Number) fluxRecord.getValueByKey("_offline")).doubleValue());
                result.add(deviceAvailabilityRecord);
            }
        }
        return result;
    }


    public List<DeviceAvailabilityRecord> findDeviceAvailabilityByDateRange(String from, String to, Integer deviceId,
                                                                String aggregationWindow) {
        String fluxQuery = String.format(
                "dataOnline = from(bucket: \"smartifydb\")\n" +
                        "  |> range(start: %s, stop: %s)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"DeviceStatus\" " +
                        "and r[\"deviceId\"] == \"%s\" and r[\"_value\"] == \"online\")" +

                "dataOffline = from(bucket: \"smartifydb\")\n" +
                        "  |> range(start: %s, stop: %s)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"DeviceStatus\" " +
                        "and r[\"deviceId\"] == \"%s\" and r[\"_value\"] == \"offline\")" +

                "aggregatedOnline = \n" +
                        "dataOnline\n" +
                        "    |> window(every: %s, createEmpty: true)\n" +
                        "    |> count()\n" +
                        "    |> group(columns: [\"_start\"])\n" +
                        "    |> rename(columns: {_value: \"_online\"})" +

                "aggregatedOffline = \n" +
                        "dataOffline\n" +
                        "    |> window(every: %s, createEmpty: true)\n" +
                        "    |> count()\n" +
                        "    |> group(columns: [\"_start\"])\n" +
                        "    |> rename(columns: {_value: \"_offline\"})" +

                "joinedData =  join(tables: {t1: aggregatedOnline, t2: aggregatedOffline}, on: [\"_start\", \"_stop\"])\n" +
                        "  |> sort(columns: [\"_start\"])" +
                        "  |> map(fn: (r) => ({\n" +
                        "    _start: r._start,\n" +
                        "    _stop: r._stop,\n" +
                        "    _online: if exists r._online then r._online else 0,\n" +
                        "    _offline: if exists r._offline then r._offline else 0\n" +
                        "  }))"+

                "yield joinedData",
        from, to, deviceId, from, to, deviceId, aggregationWindow, aggregationWindow);
        return this.query(fluxQuery, deviceId);
    }
}
