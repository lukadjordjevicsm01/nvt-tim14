package com.threeamigoscoding.smartifybackend.repository.device.outside;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.outside.GateEventRecord;
import com.threeamigoscoding.smartifybackend.influx_records.outside.SprinklerEventRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.w3c.dom.ls.LSInput;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SprinklerRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }

    private List<SprinklerEventRecord> query(String fluxQuery, Integer sprinklerId) {
        List<SprinklerEventRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                result.add(new SprinklerEventRecord(
                        sprinklerId,
                        fluxRecord.getValue() == null ? "" : ((String) fluxRecord.getValue()),
                        fluxRecord.getValueByKey("subject") == null ? "" : ((String) fluxRecord.getValueByKey("subject")),
                        fluxRecord.getValueByKey("setting") == null ? "" : ((String) fluxRecord.getValueByKey("setting")),
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli())
                ));
            }
        }
        return result;
    }


    public List<SprinklerEventRecord> findEventsByDateRange(String from, String to, Integer sprinklerId) {
        String fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"sprinklerId\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])",
                from, to, "sprinkler action", sprinklerId);
        return this.query(fluxQuery, sprinklerId);
    }
}
