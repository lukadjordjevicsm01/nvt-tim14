package com.threeamigoscoding.smartifybackend.repository.device.electroenergetic;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.SolarPanelSystemEventRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SolarPanelSystemRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    private List<SolarPanelSystemEventRecord> query(String fluxQuery, Integer solarPanelSystemId) {
        List<SolarPanelSystemEventRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable: tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord: records) {
                String valueByKey = (String) fluxRecord.getValueByKey("value");
                Double value = valueByKey == null ? null : Double.valueOf(valueByKey);
                if (value == -1.0) value = null;
                result.add(new SolarPanelSystemEventRecord(
                        solarPanelSystemId,
                        fluxRecord.getValue() == null ? "" : ((String) fluxRecord.getValue()),
                        fluxRecord.getValueByKey("subject") == null ? "" : ((String) fluxRecord.getValueByKey("subject")),
                        value,
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli())

                ));
            }
        }
        return result;
    }

    public List<SolarPanelSystemEventRecord> findEventsByDateRange(String from, String to, Integer solarPanelSystemId){
        String fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"solarPanelSystemId\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])",
                from, to, "Solar Panel System Action", solarPanelSystemId);
        return this.query(fluxQuery, solarPanelSystemId);
    }

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }

}
