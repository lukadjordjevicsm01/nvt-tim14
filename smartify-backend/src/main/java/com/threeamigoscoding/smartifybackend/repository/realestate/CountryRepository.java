package com.threeamigoscoding.smartifybackend.repository.realestate;

import com.threeamigoscoding.smartifybackend.model.realestate.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {


}
