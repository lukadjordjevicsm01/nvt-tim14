package com.threeamigoscoding.smartifybackend.repository.device.outside;

import com.threeamigoscoding.smartifybackend.model.device.outside.Sprinkler;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SprinklerRepository extends JpaRepository<Sprinkler, Integer> {
}
