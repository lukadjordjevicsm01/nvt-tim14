package com.threeamigoscoding.smartifybackend.repository.realestate;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstateStatus;
import com.threeamigoscoding.smartifybackend.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RealEstateRepository extends JpaRepository<RealEstate, Integer> {

    List<RealEstate> findAllByStatus(RealEstateStatus status);

    @Query(value = "SELECT u.realEstates from User u where u.id = :id")
    List<RealEstate> findAllByOwnerId(@Param("id")Integer id);

    @Query("SELECT DISTINCT re FROM RealEstate re " +
            "JOIN re.devices d " +
            "LEFT JOIN re.sharedWith reSharedWith " +
            "LEFT JOIN d.sharedWith deviceSharedWith " +
            "WHERE (reSharedWith IS NULL OR reSharedWith.id != :userId) " +
                "AND (deviceSharedWith IS NOT NULL AND deviceSharedWith.id = :userId)")
    List<RealEstate> findSharedDeviceRealEstates(Integer userId);

    @Query("SELECT re FROM RealEstate re " +
            "JOIN re.devices d " +
            "WHERE d.id = :deviceId")
    RealEstate findRealEstateByDevice(Integer deviceId);

}
