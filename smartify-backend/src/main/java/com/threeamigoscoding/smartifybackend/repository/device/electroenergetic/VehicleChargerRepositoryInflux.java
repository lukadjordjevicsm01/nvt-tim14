package com.threeamigoscoding.smartifybackend.repository.device.electroenergetic;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.MeasurementRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.SolarPanelSystemEventRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.VehicleChargerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VehicleChargerRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    private List<VehicleChargerRecord> query(String fluxQuery, Integer vehicleChargerId) {
        List<VehicleChargerRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable: tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord: records) {
                result.add(new VehicleChargerRecord(
                        vehicleChargerId,
                        fluxRecord.getValueByKey("vehicleChargerSocketName") == null ? "" : ((String) fluxRecord.getValueByKey("vehicleChargerSocketName")),
                        fluxRecord.getValueByKey("message") == null ? "" : ((String) fluxRecord.getValueByKey("message")),
                        fluxRecord.getValueByKey("subject") == null ? "" : ((String) fluxRecord.getValueByKey("subject")),
                        fluxRecord.getValue() == null ? "" : ((String) fluxRecord.getValue()),
                        null,
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli())
                ));
            }
        }
        return result;
    }

    public List<VehicleChargerRecord> findRecordsByDateRange(String from, String to, Integer vehicleChargerId) {
        String fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"vehicleChargerId\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])",
                from, to, "Vehicle Charger Charging Data", vehicleChargerId);
        return this.query(fluxQuery, vehicleChargerId);
    }

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }
}
