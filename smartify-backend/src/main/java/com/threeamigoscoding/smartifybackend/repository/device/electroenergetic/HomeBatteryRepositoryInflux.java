package com.threeamigoscoding.smartifybackend.repository.device.electroenergetic;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.ElectricityUsageDTO;
import com.threeamigoscoding.smartifybackend.influx_records.MeasurementRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.HomeBatteryConsumptionRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.HomeBatteryDistributionRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.SolarPanelSystemEventRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class HomeBatteryRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    private List<HomeBatteryConsumptionRecord> queryConsumption(String fluxQuery, Integer homeBatteryId) {
        List<HomeBatteryConsumptionRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable: tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord: records) {
                Double lastMinuteConsumption = (Double) fluxRecord.getValue();
                Integer realEstateId = null;
                Integer cityId = null;
                Object realEstateIdObj = fluxRecord.getValueByKey("realEstateId");
                Object cityIdObj = fluxRecord.getValueByKey("cityId");
                if (realEstateIdObj instanceof String) {
                    realEstateId = Integer.valueOf((String) realEstateIdObj);
                }
                if (cityIdObj instanceof String) {
                    cityId = Integer.valueOf((String) cityIdObj);
                }
                result.add(new HomeBatteryConsumptionRecord(
                        homeBatteryId,
                        lastMinuteConsumption,
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli()),
                        realEstateId,
                        cityId
                ));
            }
        }
        return result;
    }


    public List<MeasurementRecord> findRecordsByDateRange(String from, String to, Integer homeBatteryId){
        List<MeasurementRecord> measurementRecords = new ArrayList<>();
        String fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"homeBatteryId\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])",
                from, to, "Real Estate Last Consumption", homeBatteryId);
        List<HomeBatteryConsumptionRecord> homeBatteryConsumptionRecords = this.queryConsumption(fluxQuery, homeBatteryId);
        homeBatteryConsumptionRecords.forEach(homeBatteryConsumptionRecord -> {
            MeasurementRecord measurementRecord = new MeasurementRecord();
            measurementRecord.setMeasurement(homeBatteryConsumptionRecord.getLastMinuteConsumption());
            measurementRecord.setTimestamp(homeBatteryConsumptionRecord.getTimestamp());
            measurementRecords.add(measurementRecord);
        });
        return measurementRecords;
    }

    private List<HomeBatteryDistributionRecord> queryDistribution(String fluxQuery, Integer homeBatteryId) {
        List<HomeBatteryDistributionRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable: tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord: records) {
                Double distributionEnergy = (Double) fluxRecord.getValue();
                Integer realEstateId = null;
                Integer cityId = null;
                Object realEstateIdObj = fluxRecord.getValueByKey("realEstateId");
                Object cityIdObj = fluxRecord.getValueByKey("cityId");
                if (realEstateIdObj instanceof String) {
                    realEstateId = Integer.valueOf((String) realEstateIdObj);
                }
                if (cityIdObj instanceof String) {
                    cityId = Integer.valueOf((String) cityIdObj);
                }
                result.add(new HomeBatteryDistributionRecord(
                        homeBatteryId,
                        distributionEnergy,
                        fluxRecord.getValueByKey("message") == null ? "" : ((String) fluxRecord.getValueByKey("message")),
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli()),
                        realEstateId,
                        cityId
                ));
            }
        }
        return result;
    }

    public List<HomeBatteryDistributionRecord> findDistributionRecordsByDateRange
            (String from, String to, Integer homeBatteryId){
        String fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"homeBatteryId\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])",
                from, to, "Real Estate Distribution Energy", homeBatteryId);
        return this.queryDistribution(fluxQuery, homeBatteryId);

    }

    private Double queryElectricityUsageByRealEstateOrCity(String fluxQuery) {
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable: tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            FluxRecord fluxRecord = records.get(0);
            return (Double) fluxRecord.getValue();
        }
        return 0.0;
    }

    public ElectricityUsageDTO findElectricityUsageByRealEstate(String from, String to, Integer realEstateId) {
        ElectricityUsageDTO electricityUsageDTO = new ElectricityUsageDTO();
        String consumedFluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"realEstateId\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])" +
                        "|> sum(column: \"_value\")",
                from, to, "Real Estate Last Consumption", realEstateId);
        electricityUsageDTO.setConsumed(this.queryElectricityUsageByRealEstateOrCity(consumedFluxQuery));

        String producedFluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"realEstateId\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"message\"] == \"Energy given to electro distribution.\")" +
                        "|> sort(columns: [\"_time\"])" +
                        "|> sum(column: \"_value\")",
                from, to, "Real Estate Distribution Energy", realEstateId);
        electricityUsageDTO.setProduced(this.queryElectricityUsageByRealEstateOrCity(producedFluxQuery));

        return electricityUsageDTO;
    }

    public ElectricityUsageDTO findElectricityUsageByCity(String from, String to, Integer cityId) {
        ElectricityUsageDTO electricityUsageDTO = new ElectricityUsageDTO();

        String consumedFluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"cityId\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])" +
                        "|> sum(column: \"_value\")",
                from, to, "Real Estate Last Consumption", cityId);
        electricityUsageDTO.setConsumed(this.queryElectricityUsageByRealEstateOrCity(consumedFluxQuery));

        String producedFluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"cityId\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"message\"] == \"Energy given to electro distribution.\")" +
                        "|> sort(columns: [\"_time\"])" +
                        "|> sum(column: \"_value\")",
                from, to, "Real Estate Distribution Energy", cityId);
        electricityUsageDTO.setProduced(this.queryElectricityUsageByRealEstateOrCity(producedFluxQuery));

        return electricityUsageDTO;
    }

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }
}
