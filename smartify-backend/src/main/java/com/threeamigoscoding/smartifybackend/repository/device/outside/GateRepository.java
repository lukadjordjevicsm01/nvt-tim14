package com.threeamigoscoding.smartifybackend.repository.device.outside;

import com.threeamigoscoding.smartifybackend.model.device.outside.Gate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GateRepository extends JpaRepository<Gate, Integer> {
}
