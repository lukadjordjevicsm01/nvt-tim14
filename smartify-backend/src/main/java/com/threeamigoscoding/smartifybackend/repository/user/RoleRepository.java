package com.threeamigoscoding.smartifybackend.repository.user;

import com.threeamigoscoding.smartifybackend.model.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByName(String name);

}
