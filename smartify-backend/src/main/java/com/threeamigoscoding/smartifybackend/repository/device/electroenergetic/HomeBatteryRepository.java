package com.threeamigoscoding.smartifybackend.repository.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.HomeBattery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HomeBatteryRepository extends JpaRepository<HomeBattery, Integer> {
}
