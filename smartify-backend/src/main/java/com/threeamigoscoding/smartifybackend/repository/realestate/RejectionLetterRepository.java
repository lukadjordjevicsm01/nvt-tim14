package com.threeamigoscoding.smartifybackend.repository.realestate;

import com.threeamigoscoding.smartifybackend.model.realestate.RejectionLetter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RejectionLetterRepository extends JpaRepository<RejectionLetter, Integer> {
}
