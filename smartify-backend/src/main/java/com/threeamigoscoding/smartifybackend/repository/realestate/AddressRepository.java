package com.threeamigoscoding.smartifybackend.repository.realestate;

import com.threeamigoscoding.smartifybackend.model.realestate.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
}
