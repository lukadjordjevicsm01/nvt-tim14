package com.threeamigoscoding.smartifybackend.repository.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.VehicleChargerSocket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleChargerSocketRepository extends JpaRepository<VehicleChargerSocket, Integer> {
}
