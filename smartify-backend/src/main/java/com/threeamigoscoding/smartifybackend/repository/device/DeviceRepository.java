package com.threeamigoscoding.smartifybackend.repository.device;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Integer> {

    @Modifying
    @Transactional
    @Query("UPDATE Device d SET d.isOnline = false WHERE d.lastPing < :thresholdTime")
    void checkPings(Timestamp thresholdTime);

    @Transactional
    @Query("SELECT d.id from Device d WHERE d.isOnline = false ")
    List<Integer> getOfflineDevicesIds();

    @Transactional
    @Query("SELECT d.id from Device d  WHERE d.isOnline = true ")
    List<Integer> getOnlineDevicesIds();
    
    @Query(value = "SELECT r.devices from RealEstate r where r.id = :id")
    public List<Device> findAllByRealEstateId(@Param("id")Integer id);

}
