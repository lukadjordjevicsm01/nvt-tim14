package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioning;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirConditioningRepository extends JpaRepository<AirConditioning, Integer> {
}
