package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachineSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WashingMachineSettingRepository extends JpaRepository<WashingMachineSetting, Integer> {
}
