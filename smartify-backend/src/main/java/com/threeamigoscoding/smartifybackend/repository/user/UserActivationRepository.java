package com.threeamigoscoding.smartifybackend.repository.user;

import com.threeamigoscoding.smartifybackend.model.user.UserActivation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserActivationRepository extends JpaRepository<UserActivation, Integer> {
}
