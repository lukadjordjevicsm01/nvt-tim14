package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.home.AmbientSensorRecordDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class AmbientSensorRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    private List<AmbientSensorRecordDTO> query(String fluxQuery) {
        List<AmbientSensorRecordDTO> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                result.add(new AmbientSensorRecordDTO(
                        fluxRecord.getValue() == null ? 0 : ((double) fluxRecord.getValue()),
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli())
                ));
            }
        }
        return result;
    }

    public List<AmbientSensorRecordDTO> findByDateRange(String from,
                                                        String to,
                                                        String measurement,
                                                        String id,
                                                        String aggregationWindow) {
        String fluxQuery;
        if (!Objects.equals(aggregationWindow, "none"))
            fluxQuery = String.format(
                    "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                            "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                            "|> filter(fn: (r) => r[\"id\"] == \"%s\")" +
                            "|> aggregateWindow(every: %s, fn: mean, createEmpty: false)" +
                            "|> sort(columns: [\"_time\"])",
                    from, to, measurement, id, aggregationWindow);
        else
            fluxQuery = String.format(
                    "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                            "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                            "|> filter(fn: (r) => r[\"id\"] == \"%s\")" +
                            "|> sort(columns: [\"_time\"])",
                    from, to, measurement, id);
        return this.query(fluxQuery);
    }

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }

}
