package com.threeamigoscoding.smartifybackend.repository.device.outside;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.MeasurementRecord;
import com.threeamigoscoding.smartifybackend.influx_records.home.AmbientSensorRecordDTO;
import jakarta.persistence.Access;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import com.influxdb.client.write.Point;

@Repository
public class LampRepositoryInflux {
    @Autowired
    private InfluxDBClient influxDBClient;

    public void testSave(){
        WriteApiBlocking writeApi = this.influxDBClient.getWriteApiBlocking();
        Point point = Point.measurement("memory")
                .addField("name", "server1")
                .addField("free", 4743656L)
                .addField("used", 1015096L)
                .addField("buffer", 1010467L);
        writeApi.writePoint(point);
    }


    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }

    private List<MeasurementRecord> query(String fluxQuery) {
        List<MeasurementRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                result.add(new MeasurementRecord(
                        fluxRecord.getValue() == null ? 0 : ((double) fluxRecord.getValue()),
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli())
                ));
            }
        }
        return result;
    }

    public List<MeasurementRecord> findByDateRange(String from,
                                                   String to,
                                                   String measurement,
                                                   String id,
                                                   String aggregationWindow) {
        String fluxQuery;
        if (!Objects.equals(aggregationWindow, "none"))
            fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"lampId\"] == \"%s\")" +
                        "|> aggregateWindow(every: %s, fn: mean, createEmpty: false)" +
                        "|> sort(columns: [\"_time\"])",
                from, to, measurement, id, aggregationWindow);
        else
            fluxQuery = String.format(
                    "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                            "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                            "|> filter(fn: (r) => r[\"lampId\"] == \"%s\")" +
                            "|> sort(columns: [\"_time\"])",
                    from, to, measurement, id);
        return this.query(fluxQuery);
    }


}
