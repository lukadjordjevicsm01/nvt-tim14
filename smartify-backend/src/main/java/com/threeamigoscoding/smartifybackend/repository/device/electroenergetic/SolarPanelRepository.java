package com.threeamigoscoding.smartifybackend.repository.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.SolarPanel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SolarPanelRepository extends JpaRepository<SolarPanel, Integer> {
}
