package com.threeamigoscoding.smartifybackend.repository.device.outside;

import com.threeamigoscoding.smartifybackend.model.device.outside.SprinklerSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SprinklerSettingRepository extends JpaRepository<SprinklerSetting, Integer> {
}
