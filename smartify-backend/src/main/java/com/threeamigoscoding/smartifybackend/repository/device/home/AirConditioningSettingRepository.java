package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioningSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirConditioningSettingRepository extends JpaRepository<AirConditioningSetting, Integer> {
}
