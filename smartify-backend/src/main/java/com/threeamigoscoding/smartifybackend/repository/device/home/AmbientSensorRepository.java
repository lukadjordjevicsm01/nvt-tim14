package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.AmbientSensor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmbientSensorRepository extends JpaRepository<AmbientSensor, Integer> {
}
