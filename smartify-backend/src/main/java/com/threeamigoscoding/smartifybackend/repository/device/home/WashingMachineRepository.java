package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WashingMachineRepository extends JpaRepository<WashingMachine, Integer> {
}
