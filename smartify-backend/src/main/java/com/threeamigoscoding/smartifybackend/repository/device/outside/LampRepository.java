package com.threeamigoscoding.smartifybackend.repository.device.outside;

import com.threeamigoscoding.smartifybackend.model.device.outside.Lamp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LampRepository extends JpaRepository<Lamp, Integer> {
}
