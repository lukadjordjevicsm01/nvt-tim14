package com.threeamigoscoding.smartifybackend.repository.device.outside;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.home.AmbientSensorRecordDTO;
import com.threeamigoscoding.smartifybackend.influx_records.outside.GateEventRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class GateRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    private List<GateEventRecord> query(String fluxQuery, Integer gateId) {
        List<GateEventRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                result.add(new GateEventRecord(
                        gateId,
                        fluxRecord.getValue() == null ? "" : ((String) fluxRecord.getValue()),
                        fluxRecord.getValueByKey("subject") == null ? "" : ((String) fluxRecord.getValueByKey("subject")),
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli())
                ));
            }
        }
        return result;
    }

    public List<GateEventRecord> findEventsByDateRange(String from, String to, Integer gateId){
        String fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"gateId\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])",
                from, to, "Gate Action", gateId);
        return this.query(fluxQuery, gateId);
    }

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }

}
