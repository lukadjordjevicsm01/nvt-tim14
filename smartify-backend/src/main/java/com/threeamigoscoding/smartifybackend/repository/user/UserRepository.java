package com.threeamigoscoding.smartifybackend.repository.user;

import com.threeamigoscoding.smartifybackend.model.user.Role;
import com.threeamigoscoding.smartifybackend.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);

    @Query("SELECT re.owner FROM RealEstate re " +
            "JOIN re.devices d " +
            "WHERE d.id = :deviceId")
    User findOwnerByDevice(@Param("deviceId") Integer deviceId);
}
