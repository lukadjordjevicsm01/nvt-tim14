package com.threeamigoscoding.smartifybackend.repository.realestate;

import com.threeamigoscoding.smartifybackend.model.realestate.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {

    public List<City> findCitiesByCountry_CountryCode(String countryCode);

}
