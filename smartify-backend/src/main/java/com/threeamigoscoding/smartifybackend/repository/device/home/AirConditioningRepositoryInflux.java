package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.threeamigoscoding.smartifybackend.influx_records.home.AirConditioningRecord;
import com.threeamigoscoding.smartifybackend.influx_records.home.AmbientSensorRecordDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class AirConditioningRepositoryInflux {

    @Autowired
    private InfluxDBClient influxDBClient;

    public void save(Point point) {
        this.influxDBClient.getWriteApiBlocking().writePoint(point);
    }

    private List<AirConditioningRecord> query(String fluxQuery, Integer id) {
        List<AirConditioningRecord> result = new ArrayList<>();
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                Double currentTemperature = fluxRecord.getValue() == null ? 0 : ((double) fluxRecord.getValue());
                if (currentTemperature == -1000) currentTemperature = null;
                result.add(new AirConditioningRecord(
                        id,
                        fluxRecord.getValueByKey("subject") == null ? "" :
                                Objects.requireNonNull(fluxRecord.getValueByKey("subject")).toString(),
                        fluxRecord.getValueByKey("action") == null ? "" :
                                Objects.requireNonNull(fluxRecord.getValueByKey("action")).toString(),
                        currentTemperature,
                        fluxRecord.getTime() == null ? null : new Timestamp(fluxRecord.getTime().toEpochMilli())
                ));
            }
        }
        return result;
    }

    public List<AirConditioningRecord> findByDateRange(String from, String to, String id) {
        String fluxQuery = String.format(
                "from(bucket: \"smartifydb\") |> range(start: %s, stop: %s)" +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"%s\")" +
                        "|> filter(fn: (r) => r[\"id\"] == \"%s\")" +
                        "|> sort(columns: [\"_time\"])",
                from, to, "air conditioning", id);
        return this.query(fluxQuery, Integer.valueOf(id));
    }
}
