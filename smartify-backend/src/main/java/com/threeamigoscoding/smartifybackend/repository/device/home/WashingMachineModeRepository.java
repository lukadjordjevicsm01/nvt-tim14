package com.threeamigoscoding.smartifybackend.repository.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachineMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WashingMachineModeRepository extends JpaRepository<WashingMachineMode, Integer> {

}
