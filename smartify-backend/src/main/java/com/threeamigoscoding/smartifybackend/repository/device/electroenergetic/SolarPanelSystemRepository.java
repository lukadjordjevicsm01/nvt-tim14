package com.threeamigoscoding.smartifybackend.repository.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.SolarPanelSystem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SolarPanelSystemRepository extends JpaRepository<SolarPanelSystem, Integer> {
}
