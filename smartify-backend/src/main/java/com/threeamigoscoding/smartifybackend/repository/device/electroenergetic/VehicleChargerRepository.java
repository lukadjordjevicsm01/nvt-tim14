package com.threeamigoscoding.smartifybackend.repository.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.VehicleCharger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleChargerRepository extends JpaRepository<VehicleCharger, Integer> {
}
