package com.threeamigoscoding.smartifybackend.influx_records.electroenergetic;

import java.sql.Timestamp;

public class HomeBatteryConsumptionRecord {

    private Integer homeBatteryId;
    private Double lastMinuteConsumption;
    private Timestamp timestamp;
    private Integer realEstateId;
    private Integer cityId;

    //region Constructors

    public HomeBatteryConsumptionRecord() {

    }

    public HomeBatteryConsumptionRecord(Integer homeBatteryId, Double lastMinuteConsumption, Timestamp timestamp,
                                        Integer realEstateId, Integer cityId) {
        this.homeBatteryId = homeBatteryId;
        this.lastMinuteConsumption = lastMinuteConsumption;
        this.timestamp = timestamp;
        this.realEstateId = realEstateId;
        this.cityId = cityId;
    }

    //endregion

    //region Getters and Setters

    public Integer getHomeBatteryId() {
        return homeBatteryId;
    }

    public void setHomeBatteryId(Integer homeBatteryId) {
        this.homeBatteryId = homeBatteryId;
    }

    public Double getLastMinuteConsumption() {
        return lastMinuteConsumption;
    }

    public void setLastMinuteConsumption(Double lastMinuteConsumption) {
        this.lastMinuteConsumption = lastMinuteConsumption;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getRealEstateId() {
        return realEstateId;
    }

    public void setRealEstateId(Integer realEstateId) {
        this.realEstateId = realEstateId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
//endregion

}
