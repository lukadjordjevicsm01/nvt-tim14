package com.threeamigoscoding.smartifybackend.influx_records.electroenergetic;

import java.sql.Timestamp;

public class VehicleChargerRecord {

    private Integer vehicleChargerId;
    private String vehicleChargerSocketName;
    private String message;
    private String subject;
    private String chargingDuration;
    private Boolean finished;
    private Timestamp timestamp;

    //region Constructors

    public VehicleChargerRecord() {
    }

    public VehicleChargerRecord(Integer vehicleChargerId, String vehicleChargerSocketName, String message,
                                String subject, String chargingDuration, Boolean finished, Timestamp timestamp) {
        this.vehicleChargerId = vehicleChargerId;
        this.vehicleChargerSocketName = vehicleChargerSocketName;
        this.message = message;
        this.subject = subject;
        this.chargingDuration = chargingDuration;
        this.finished = finished;
        this.timestamp = timestamp;
    }

    //endregion

    //region Getters and Setters

    public Integer getVehicleChargerId() {
        return vehicleChargerId;
    }

    public void setVehicleChargerId(Integer vehicleChargerId) {
        this.vehicleChargerId = vehicleChargerId;
    }

    public String getVehicleChargerSocketName() {
        return vehicleChargerSocketName;
    }

    public void setVehicleChargerSocketName(String vehicleChargerSocketName) {
        this.vehicleChargerSocketName = vehicleChargerSocketName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getChargingDuration() {
        return chargingDuration;
    }

    public void setChargingDuration(String chargingDuration) {
        this.chargingDuration = chargingDuration;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
    //endregion

}
