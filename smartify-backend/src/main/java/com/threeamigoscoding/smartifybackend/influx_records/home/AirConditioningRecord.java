package com.threeamigoscoding.smartifybackend.influx_records.home;

import java.sql.Timestamp;

public class AirConditioningRecord {
    Integer id;
    String subject;
    String action;
    Double currentTemperature;
    Timestamp timestamp;

    public AirConditioningRecord() {}

    public AirConditioningRecord(Integer id,
                                 String subject,
                                 String action,
                                 Double currentTemperature,
                                 Timestamp timestamp) {
        this.id = id;
        this.subject = subject;
        this.action = action;
        this.currentTemperature = currentTemperature;
        this.timestamp = timestamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Double getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(Double currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
