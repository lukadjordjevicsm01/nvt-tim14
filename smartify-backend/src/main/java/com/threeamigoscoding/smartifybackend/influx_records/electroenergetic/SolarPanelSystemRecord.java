package com.threeamigoscoding.smartifybackend.influx_records.electroenergetic;

public class SolarPanelSystemRecord {
    Integer id;
    Double producedEnergy;

    public SolarPanelSystemRecord(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getProducedEnergy() {
        return producedEnergy;
    }

    public void setProducedEnergy(Double producedEnergy) {
        this.producedEnergy = producedEnergy;
    }
}
