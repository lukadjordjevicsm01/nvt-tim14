package com.threeamigoscoding.smartifybackend.influx_records.home;

import java.sql.Timestamp;

public class AmbientSensorRecordDTO {
    Integer id;
    Double measurement;
    Timestamp timestamp;

    public AmbientSensorRecordDTO() {

    }

    public AmbientSensorRecordDTO(Double measurement, Timestamp timestamp) {
        this.measurement = measurement;
        this.timestamp = timestamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Double temperature) {
        this.measurement = temperature;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
