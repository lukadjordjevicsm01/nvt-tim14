package com.threeamigoscoding.smartifybackend.influx_records.outside;

public class LampBrightnessRecord {

    private Double brightness;


    //region Constructors
    public LampBrightnessRecord() {
    }

    public LampBrightnessRecord(Double brightness) {
        this.brightness = brightness;
    }

    //endregion

    //region Getters and Setters

    public Double getBrightness() {
        return brightness;
    }

    public void setBrightness(Double brightness) {
        this.brightness = brightness;
    }

    //endregion

}
