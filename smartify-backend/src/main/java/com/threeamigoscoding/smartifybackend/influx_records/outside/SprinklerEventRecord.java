package com.threeamigoscoding.smartifybackend.influx_records.outside;

import java.sql.Timestamp;

public class SprinklerEventRecord {

    private Integer sprinklerId;
    private String action;
    private String subject;
    private String setting;
    private Timestamp timestamp;

    //region Constructors

    public SprinklerEventRecord(Integer sprinklerId, String action, String subject, String setting, Timestamp timestamp) {
        this.sprinklerId = sprinklerId;
        this.action = action;
        this.subject = subject;
        this.setting = setting;
        this.timestamp = timestamp;
    }

    public SprinklerEventRecord() {
    }

    //endregion

    //region Getters and Setters

    public Integer getSprinklerId() {
        return sprinklerId;
    }

    public void setSprinklerId(Integer sprinklerId) {
        this.sprinklerId = sprinklerId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSetting() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting = setting;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    //endregion

}
