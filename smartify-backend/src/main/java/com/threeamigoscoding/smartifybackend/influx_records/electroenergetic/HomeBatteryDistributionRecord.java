package com.threeamigoscoding.smartifybackend.influx_records.electroenergetic;

import java.sql.Timestamp;

public class HomeBatteryDistributionRecord {

    private Integer id;
    private Double distributionEnergy;
    private String message;
    private Timestamp timestamp;
    private Integer realEstateId;
    private Integer cityId;

    //region Constructors

    public HomeBatteryDistributionRecord() {}

    public HomeBatteryDistributionRecord(Integer id, Double distributionEnergy, String message, Timestamp timestamp,
                                         Integer realEstateId, Integer cityId) {
        this.id = id;
        this.distributionEnergy = distributionEnergy;
        this.message = message;
        this.timestamp = timestamp;
        this.realEstateId = realEstateId;
        this.cityId = cityId;
    }

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getDistributionEnergy() {
        return distributionEnergy;
    }

    public void setDistributionEnergy(Double distributionEnergy) {
        this.distributionEnergy = distributionEnergy;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getRealEstateId() {
        return realEstateId;
    }

    public void setRealEstateId(Integer realEstateId) {
        this.realEstateId = realEstateId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
    //endregion
}
