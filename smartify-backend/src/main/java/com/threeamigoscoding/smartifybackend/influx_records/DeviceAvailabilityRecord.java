package com.threeamigoscoding.smartifybackend.influx_records;

import java.sql.Timestamp;

public class DeviceAvailabilityRecord {

    private Double onlineTime;
    private Double offlineTime;
    private Double onlinePercentage;
    private Double offlinePercentage;
    private Timestamp intervalStart;
    private Timestamp intervalEnd;
    private String unit;


    //region Getters and Setters

    public Double getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Double onlineTime) {
        this.onlineTime = onlineTime;
    }

    public Double getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Double offlineTime) {
        this.offlineTime = offlineTime;
    }

    public Double getOnlinePercentage() {
        return onlinePercentage;
    }

    public void setOnlinePercentage(Double onlinePercentage) {
        this.onlinePercentage = onlinePercentage;
    }

    public Double getOfflinePercentage() {
        return offlinePercentage;
    }

    public void setOfflinePercentage(Double offlinePercentage) {
        this.offlinePercentage = offlinePercentage;
    }

    public Timestamp getIntervalStart() {
        return intervalStart;
    }

    public void setIntervalStart(Timestamp intervalStart) {
        this.intervalStart = intervalStart;
    }

    public Timestamp getIntervalEnd() {
        return intervalEnd;
    }

    public void setIntervalEnd(Timestamp intervalEnd) {
        this.intervalEnd = intervalEnd;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    //endregion


    @Override
    public String toString() {
        return "DeviceAvailabilityRecord{" +
                "onlineTime=" + onlineTime +
                ", offlineTime=" + offlineTime +
                ", onlinePercentage=" + onlinePercentage +
                ", offlinePercentage=" + offlinePercentage +
                ", intervalStart=" + intervalStart.toString() +
                ", intervalEnd=" + intervalEnd.toString() +
                ", unit='" + unit + '\'' +
                '}';
    }
}
