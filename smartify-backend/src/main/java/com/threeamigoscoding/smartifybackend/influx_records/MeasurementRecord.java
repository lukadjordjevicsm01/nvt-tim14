package com.threeamigoscoding.smartifybackend.influx_records;

import java.sql.Timestamp;

public class MeasurementRecord {

    Double measurement;
    Timestamp timestamp;


    public MeasurementRecord(Double measurement, Timestamp timestamp) {
        this.measurement = measurement;
        this.timestamp = timestamp;
    }

    public MeasurementRecord() {
    }


    public Double getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Double measurement) {
        this.measurement = measurement;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
