package com.threeamigoscoding.smartifybackend.influx_records.home;

import java.sql.Timestamp;

public class AmbientSensorRecord {
    Integer id;
    Double temperature;
    Double humidity;
    Timestamp timestamp;

    public AmbientSensorRecord() {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
