package com.threeamigoscoding.smartifybackend.influx_records.electroenergetic;

import java.sql.Timestamp;

public class SolarPanelSystemEventRecord {

    private Integer solarPanelSystemId;
    private String action;
    private String subject;
    private Double value;
    private Timestamp timestamp;

    //region Constructors

    public SolarPanelSystemEventRecord() {}

    public SolarPanelSystemEventRecord(Integer solarPanelSystemId, String action, String subject, Double value,
                                       Timestamp timestamp) {
        this.solarPanelSystemId = solarPanelSystemId;
        this.action = action;
        this.subject = subject;
        this.value = value;
        this.timestamp = timestamp;
    }

    //endregion

    //region Getters and Setters

    public Integer getSolarPanelSystemId() {
        return solarPanelSystemId;
    }

    public void setSolarPanelSystemId(Integer solarPanelSystemId) {
        this.solarPanelSystemId = solarPanelSystemId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    //endregion
}
