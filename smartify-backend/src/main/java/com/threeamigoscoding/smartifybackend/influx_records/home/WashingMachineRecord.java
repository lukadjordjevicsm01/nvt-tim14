package com.threeamigoscoding.smartifybackend.influx_records.home;

import java.sql.Timestamp;

public class WashingMachineRecord {
    Integer id;
    String subject;
    String action;
    Timestamp timestamp;

    public WashingMachineRecord() {

    }

    public WashingMachineRecord(Integer id, String subject, String action, Timestamp timestamp) {
        this.id = id;
        this.subject = subject;
        this.action = action;
        this.timestamp = timestamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
