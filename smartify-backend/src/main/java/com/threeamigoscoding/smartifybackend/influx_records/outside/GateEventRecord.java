package com.threeamigoscoding.smartifybackend.influx_records.outside;

import java.sql.Timestamp;

public class GateEventRecord {

    private Integer gateId;
    private String action;
    private String subject;
    private Timestamp timestamp;
    //region Constructors

    public GateEventRecord() {
    }

    public GateEventRecord(Integer gateId, String action, String subject, Timestamp timestamp) {
        this.gateId = gateId;
        this.action = action;
        this.subject = subject;
        this.timestamp = timestamp;
    }

    //endregion

    //region Getters and Setters

    public Integer getGateId() {
        return gateId;
    }

    public void setGateId(Integer gateId) {
        this.gateId = gateId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    //endregion

}
