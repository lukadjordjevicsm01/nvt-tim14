package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;

public class HomeBatteryDTO extends DeviceDTO {

    private Double capacity;

    private Double currentEnergy;

    //region Constructors

    public HomeBatteryDTO(){}

    //endregion

    //region Getters and Setters

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public Double getCurrentEnergy() {
        return currentEnergy;
    }

    public void setCurrentEnergy(Double currentEnergy) {
        this.currentEnergy = currentEnergy;
    }

    //endregion
}
