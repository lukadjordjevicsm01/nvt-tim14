package com.threeamigoscoding.smartifybackend.dto.realestate;

import com.threeamigoscoding.smartifybackend.model.realestate.RealEstateType;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.web.multipart.MultipartFile;

public class RealEstateRegistrationDTO {

    @NotNull(message = "You must provide real estate type when registering your real estate")
    private RealEstateType realEstateType;

    private AddressInDTO address;

    @Min(value = 0, message = "You cannot have less then 0 floors")
    @Max(value = 100, message = "You cannot have more then 100 floors")
    private Integer floors;

    @NotNull(message = "Cannot know whose real estate is this")
    private Integer userId;

    @NotNull(message = "Your real estate must have a name")
    @Size(min = 1, message = "Your real estate must have a name")
    private String name;

    @NotNull(message = "You must upload a picture when registering your real estate.")
    private MultipartFile realEstatePicture;


    //region Constructors

    public RealEstateRegistrationDTO(RealEstateType realEstateType, AddressInDTO addressDTO, Integer floors, Integer userId, String name, MultipartFile picture) {
        this.realEstateType = realEstateType;
        this.address = addressDTO;
        this.floors = floors;
        this.userId = userId;
        this.name = name;
        this.realEstatePicture = picture;
    }

    public RealEstateRegistrationDTO() {
    }

    //endregion

    //region Getters and Setters

    public RealEstateType getRealEstateType() {
        return realEstateType;
    }

    public void setRealEstateType(RealEstateType realEstateType) {
        this.realEstateType = realEstateType;
    }

    public AddressInDTO getAddress() {
        return address;
    }

    public void setAddress(AddressInDTO address) {
        this.address = address;
    }

    public Integer getFloors() {
        return floors;
    }

    public void setFloors(Integer floors) {
        this.floors = floors;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getRealEstatePicture() {
        return realEstatePicture;
    }

    public void setRealEstatePicture(MultipartFile realEstatePicture) {
        this.realEstatePicture = realEstatePicture;
    }

    //endregion

}
