package com.threeamigoscoding.smartifybackend.dto.mqtt_model;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MqttMessageDto {

    private String command;

    private String payload;

    //region Constructors
    public MqttMessageDto() {
    }

    public MqttMessageDto(String command, String payload) {
        this.command = command;
        this.payload = payload;
    }

    //endregion

    //region Getters and Setters

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    //endregion

}
