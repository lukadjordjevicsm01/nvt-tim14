package com.threeamigoscoding.smartifybackend.dto.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioningMode;

import java.sql.Time;
import java.sql.Timestamp;

public class AirConditioningSettingDTO {

    private AirConditioningMode mode;

    private Timestamp startTime;

    private Timestamp endTime;

    private Double temperature;

    //region Constructors

    public AirConditioningSettingDTO(){}

    //endregion

    //region Getters and Setters

    public AirConditioningMode getMode() {
        return mode;
    }

    public void setMode(AirConditioningMode mode) {
        this.mode = mode;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    //endregion
}
