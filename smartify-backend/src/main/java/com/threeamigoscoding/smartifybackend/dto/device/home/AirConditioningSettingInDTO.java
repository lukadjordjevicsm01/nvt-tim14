package com.threeamigoscoding.smartifybackend.dto.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioningMode;

import java.sql.Timestamp;

public class AirConditioningSettingInDTO {
    private AirConditioningMode mode;

    private long startTime;

    private long endTime;

    private Double temperature;

    public AirConditioningMode getMode() {
        return mode;
    }

    public void setMode(AirConditioningMode mode) {
        this.mode = mode;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }
}
