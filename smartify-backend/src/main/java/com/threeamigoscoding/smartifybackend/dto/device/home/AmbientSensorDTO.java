package com.threeamigoscoding.smartifybackend.dto.device.home;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;

public class AmbientSensorDTO extends DeviceDTO {

    private Double temperature;

    private Double humidity;

    //region Constructors

    public AmbientSensorDTO() {}

    //endregion

    //region Getters and Setters

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    //endregion

}
