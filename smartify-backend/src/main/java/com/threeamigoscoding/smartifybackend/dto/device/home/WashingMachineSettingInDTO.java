package com.threeamigoscoding.smartifybackend.dto.device.home;

import java.sql.Timestamp;

public class WashingMachineSettingInDTO {
    private WashingMachineModeDTO mode;

    private long startTime;

    public WashingMachineModeDTO getMode() {
        return mode;
    }

    public void setMode(WashingMachineModeDTO mode) {
        this.mode = mode;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
