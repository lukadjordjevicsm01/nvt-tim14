package com.threeamigoscoding.smartifybackend.dto.device.home;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;

import java.util.HashSet;
import java.util.Set;

public class WashingMachineDTO extends DeviceDTO {

    private WashingMachineModeDTO currentMode;

    private Set<WashingMachineModeDTO> supportedModes;

    private Boolean isSettingsActive;

    private Set<WashingMachineSettingDTO> customSettings;

    //region Constructors

    public WashingMachineDTO(){}

    //endregion

    //region Getters and Setters

    public WashingMachineModeDTO getCurrentMode() {
        return currentMode;
    }

    public void setCurrentMode(WashingMachineModeDTO currentMode) {
        this.currentMode = currentMode;
    }

    public Set<WashingMachineModeDTO> getSupportedModes() {
        return supportedModes;
    }

    public void setSupportedModes(Set<WashingMachineModeDTO> supportedModes) {
        this.supportedModes = supportedModes;
    }

    public Boolean getSettingsActive() {
        return isSettingsActive;
    }

    public void setSettingsActive(Boolean settingsActive) {
        isSettingsActive = settingsActive;
    }

    public Set<WashingMachineSettingDTO> getCustomSettings() {
        return customSettings;
    }

    public void setCustomSettings(Set<WashingMachineSettingDTO> customSettings) {
        this.customSettings = customSettings;
    }

    //endregion

}
