package com.threeamigoscoding.smartifybackend.dto.realestate;

import com.threeamigoscoding.smartifybackend.model.realestate.RealEstateStatus;
import jakarta.validation.constraints.NotNull;

public class StatusChangeDTO {

    @NotNull(message = "You must provide real estate that you want to change status for")
    private Integer realEstateId;

    @NotNull(message = "You must provide status")
    private RealEstateStatus status;

    private String reason;

    //region Constructors

    public StatusChangeDTO(Integer realEstateId, RealEstateStatus status, String reason) {
        this.realEstateId = realEstateId;
        this.status = status;
        this.reason = reason;
    }

    public StatusChangeDTO() {
    }

    //endregion

    //region Getters and Setters

    public Integer getRealEstateId() {
        return realEstateId;
    }

    public void setRealEstateId(Integer realEstateId) {
        this.realEstateId = realEstateId;
    }

    public RealEstateStatus getStatus() {
        return status;
    }

    public void setStatus(RealEstateStatus status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    //endregion

}
