package com.threeamigoscoding.smartifybackend.dto.device.outside;


import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;

import java.util.HashSet;
import java.util.Set;

public class SprinklerDTO extends DeviceDTO {

    private Set<SprinklerSettingDTO> sprinklerSettings = new HashSet<>();

    private Boolean isSprinklerOn;

    //region Constructors

    public SprinklerDTO() {}

    //endregion

    //region Getters and Setters

    public Set<SprinklerSettingDTO> getSprinklerSettings() {
        return sprinklerSettings;
    }

    public void setSprinklerSettings(Set<SprinklerSettingDTO> sprinklerSettings) {
        this.sprinklerSettings = sprinklerSettings;
    }

    public Boolean getSprinklerOn() {
        return isSprinklerOn;
    }

    public void setSprinklerOn(Boolean sprinkleOn) {
        isSprinklerOn = sprinkleOn;
    }

    //endregion

}
