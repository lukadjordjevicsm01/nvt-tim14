package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;

public class ElectricityUsageDTO {

    private Double consumed;
    private Double produced;

    public ElectricityUsageDTO(Double consumed, Double produced) {
        this.consumed = consumed;
        this.produced = produced;
    }

    public ElectricityUsageDTO() {
    }

    public Double getConsumed() {
        return consumed;
    }

    public void setConsumed(Double consumed) {
        this.consumed = consumed;
    }

    public Double getProduced() {
        return produced;
    }

    public void setProduced(Double produced) {
        this.produced = produced;
    }
}
