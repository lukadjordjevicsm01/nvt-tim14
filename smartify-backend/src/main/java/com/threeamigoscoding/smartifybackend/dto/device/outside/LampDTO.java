package com.threeamigoscoding.smartifybackend.dto.device.outside;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;

public class LampDTO extends DeviceDTO {

    private Double currentBrightness;

    private Boolean isAutomatic;

    private Boolean isLightOn;

    //region Constructors

    public LampDTO() {}

    //endregion

    //region Getters and Setters

    public Double getCurrentBrightness() {
        return currentBrightness;
    }

    public void setCurrentBrightness(Double currentBrightness) {
        this.currentBrightness = currentBrightness;
    }

    public Boolean getAutomatic() {
        return isAutomatic;
    }

    public void setAutomatic(Boolean automatic) {
        isAutomatic = automatic;
    }

    public Boolean getLightOn() {
        return isLightOn;
    }

    public void setLightOn(Boolean lightOn) {
        isLightOn = lightOn;
    }

    //endregion
}
