package com.threeamigoscoding.smartifybackend.dto.device.outside;

public class SprinklerSwitchDTO {

    private boolean sprinklerOn;

    //region Constructors

    public SprinklerSwitchDTO(boolean sprinklerOn) {
        this.sprinklerOn = sprinklerOn;
    }

    public SprinklerSwitchDTO() {
    }

    //endregion

    //region Getters and Setters

    public boolean isSprinklerOn() {
        return sprinklerOn;
    }

    public void setSprinklerOn(boolean sprinklerOn) {
        this.sprinklerOn = sprinklerOn;
    }


    //endregion

}
