package com.threeamigoscoding.smartifybackend.dto.realestate;

import java.util.HashSet;
import java.util.Set;

public class RealEstateDevicesDTO {
    private Integer realEstateId;
    private CityDTO city;
    private Set<Integer> devicesIds = new HashSet<>();

    public RealEstateDevicesDTO() {
    }

    public RealEstateDevicesDTO(Integer realEstateId, CityDTO city, Set<Integer> devicesIds) {
        this.realEstateId = realEstateId;
        this.city = city;
        this.devicesIds = devicesIds;
    }

    public Integer getRealEstateId() {
        return realEstateId;
    }

    public void setRealEstateId(Integer realEstateId) {
        this.realEstateId = realEstateId;
    }

    public CityDTO getCity() {
        return city;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }

    public Set<Integer> getDevicesIds() {
        return devicesIds;
    }

    public void setDevicesIds(Set<Integer> devicesIds) {
        this.devicesIds = devicesIds;
    }
}
