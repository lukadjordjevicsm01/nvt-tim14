package com.threeamigoscoding.smartifybackend.dto.realestate;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class CountryDTO {

    @NotNull(message = "Country needs to have a name")
    private String name;

    @NotNull(message = "Country needs to have country code")
    @Size(min = 2, max = 2, message = "The country code must be exactly 2 characters long")
    private String countryCode;

    //region Constructors

    public CountryDTO(String name, String countryCode) {
        this.name = name;
        this.countryCode = countryCode;
    }

    public CountryDTO() {
    }

    //endregion

    //region Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    //endregion

}
