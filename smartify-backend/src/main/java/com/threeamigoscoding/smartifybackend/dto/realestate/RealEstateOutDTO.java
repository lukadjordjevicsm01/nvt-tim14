package com.threeamigoscoding.smartifybackend.dto.realestate;

import com.threeamigoscoding.smartifybackend.dto.user.UserDTO;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstateStatus;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstateType;

public class RealEstateOutDTO {

    private Integer id;

    private RealEstateType realEstateType;

    private RealEstateStatus status;

    private Integer floors;

    private String name;

    private AddressOutDTO address;

    private UserDTO userDTO;

    //region Constructors

    public RealEstateOutDTO(Integer id, RealEstateType realEstateType, RealEstateStatus status, Integer floors,
                            String name, AddressOutDTO address, UserDTO userDTO) {
        this.id = id;
        this.realEstateType = realEstateType;
        this.status = status;
        this.floors = floors;
        this.name = name;
        this.address = address;
        this.userDTO = userDTO;
    }

    public RealEstateOutDTO() {
    }

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RealEstateType getRealEstateType() {
        return realEstateType;
    }

    public void setRealEstateType(RealEstateType realEstateType) {
        this.realEstateType = realEstateType;
    }

    public RealEstateStatus getStatus() {
        return status;
    }

    public void setStatus(RealEstateStatus status) {
        this.status = status;
    }

    public Integer getFloors() {
        return floors;
    }

    public void setFloors(Integer floors) {
        this.floors = floors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AddressOutDTO getAddress() {
        return address;
    }

    public void setAddress(AddressOutDTO address) {
        this.address = address;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    //endregion

}

