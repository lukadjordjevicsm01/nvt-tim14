package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;

public class ChargingDataDTO {
    private Integer vehicleChargerId;
    private String chargeSocketName;
    private Double batteryCapacity;
    private Double currentBatteryPercentage;
    private Double chargePercentageLimit;
    private String userEmail;

    public ChargingDataDTO() {
    }

    public Integer getVehicleChargerId() {
        return vehicleChargerId;
    }

    public void setVehicleChargerId(Integer vehicleChargerId) {
        this.vehicleChargerId = vehicleChargerId;
    }

    public String getChargeSocketName() {
        return chargeSocketName;
    }

    public void setChargeSocketName(String chargeSocketName) {
        this.chargeSocketName = chargeSocketName;
    }

    public Double getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Double batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Double getCurrentBatteryPercentage() {
        return currentBatteryPercentage;
    }

    public void setCurrentBatteryPercentage(Double currentBatteryPercentage) {
        this.currentBatteryPercentage = currentBatteryPercentage;
    }

    public Double getChargePercentageLimit() {
        return chargePercentageLimit;
    }

    public void setChargePercentageLimit(Double chargePercentageLimit) {
        this.chargePercentageLimit = chargePercentageLimit;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
