package com.threeamigoscoding.smartifybackend.dto.device;

import org.springframework.web.multipart.MultipartFile;

public class DeviceImageDTO {

    private Integer id;
    private MultipartFile image;

    public DeviceImageDTO() {}

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    //endregion
}
