package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;

public class CurrentBatteryPercentageDTO {

    private Integer vehicleChargerId;
    private String chargeSocketName;
    private Double currentBatteryPercentage;

    //region Constructors

    public CurrentBatteryPercentageDTO() {
    }

    public CurrentBatteryPercentageDTO(Integer vehicleChargerId, String chargeSocketName,
                                       Double currentBatteryPercentage) {
        this.vehicleChargerId = vehicleChargerId;
        this.chargeSocketName = chargeSocketName;
        this.currentBatteryPercentage = currentBatteryPercentage;
    }

    //endregion

    //region Getters and Setters

    public Integer getVehicleChargerId() {
        return vehicleChargerId;
    }

    public void setVehicleChargerId(Integer vehicleChargerId) {
        this.vehicleChargerId = vehicleChargerId;
    }

    public String getChargeSocketName() {
        return chargeSocketName;
    }

    public void setChargeSocketName(String chargeSocketName) {
        this.chargeSocketName = chargeSocketName;
    }

    public Double getCurrentBatteryPercentage() {
        return currentBatteryPercentage;
    }

    public void setCurrentBatteryPercentage(Double currentBatteryPercentage) {
        this.currentBatteryPercentage = currentBatteryPercentage;
    }

    //endregion
}
