package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;

import java.util.Set;

public class SolarPanelSystemDTO extends DeviceDTO {

    private Set<SolarPanelDTO> solarPanels;

    //region Constructors

    public SolarPanelSystemDTO(){}

    //endregion

    //region Getters and Setters

    public Set<SolarPanelDTO> getSolarPanels() {
        return solarPanels;
    }

    public void setSolarPanels(Set<SolarPanelDTO> solarPanels) {
        this.solarPanels = solarPanels;
    }

    //endregion

}
