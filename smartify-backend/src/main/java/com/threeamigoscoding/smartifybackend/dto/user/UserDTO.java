package com.threeamigoscoding.smartifybackend.dto.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import org.springframework.web.multipart.MultipartFile;

public class UserDTO {

    private Integer id;
    @Pattern(regexp = "^[a-zA-Z]*$", message = "The name must contain only letters")
    private String name;
    @Pattern(regexp = "^[a-zA-Z]*$", message = "The surname must contain only letters")
    private String surname;
    private String email;
    private String password;
    private MultipartFile profilePicture;

    public UserDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MultipartFile getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(MultipartFile profilePicture) {
        this.profilePicture = profilePicture;
    }
}
