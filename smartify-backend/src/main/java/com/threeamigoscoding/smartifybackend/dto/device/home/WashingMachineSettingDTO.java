package com.threeamigoscoding.smartifybackend.dto.device.home;

import java.sql.Timestamp;

public class WashingMachineSettingDTO {

    private WashingMachineModeDTO mode;

    private Timestamp startTime;

    //region Constructors

    public WashingMachineSettingDTO() {}

    //endregion

    //region Getters and Setters

    public WashingMachineModeDTO getMode() {
        return mode;
    }

    public void setMode(WashingMachineModeDTO mode) {
        this.mode = mode;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    //endregion
}
