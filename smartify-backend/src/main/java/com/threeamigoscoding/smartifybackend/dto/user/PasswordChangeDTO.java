package com.threeamigoscoding.smartifybackend.dto.user;

import jakarta.validation.constraints.Pattern;

public class PasswordChangeDTO {
    private String email;
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d).*$",
            message = "Password must contain at least 1 number and 1 letter")
    private String newPassword;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
