package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;


public class VehicleChargerSocketDTO {

    private String name;

    private Boolean isActive;

    private Double chargingLimit;

    //region Constructors

    public VehicleChargerSocketDTO(String name, Double chargingLimit) {
        this.name = name;
        this.isActive = false;
        this.chargingLimit = chargingLimit;
    }

    public VehicleChargerSocketDTO(){}

    //endregion

    //region Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Double getChargingLimit() {
        return chargingLimit;
    }

    public void setChargingLimit(Double chargingLimit) {
        this.chargingLimit = chargingLimit;
    }

    //endregion
}
