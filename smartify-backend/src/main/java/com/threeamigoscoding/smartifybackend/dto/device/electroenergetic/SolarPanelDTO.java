package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;

public class SolarPanelDTO extends DeviceDTO {

    private Double area;

    private Double efficiency;

    //region Constructors

    public SolarPanelDTO() {}

    //endregion

    //region Getters and Setters

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(Double efficiency) {
        this.efficiency = efficiency;
    }

    //endregion

}
