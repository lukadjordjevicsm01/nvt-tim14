package com.threeamigoscoding.smartifybackend.dto.mqtt_model;

public class MqttMessageObjectDto {

    private String command;

    private Object payload;

    //region Constructors
    public MqttMessageObjectDto() {
    }

    public MqttMessageObjectDto(String command, Object payload) {
        this.command = command;
        this.payload = payload;
    }

    //endregion

    //region Getters and Setters

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    //endregion

}
