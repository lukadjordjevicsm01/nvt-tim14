package com.threeamigoscoding.smartifybackend.dto.device.outside;

import jakarta.persistence.criteria.CriteriaBuilder;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.util.Set;

public class SprinklerSettingDTO {
    private Integer id;

    private Timestamp startTime;

    private Timestamp endTime;

    private Boolean isRepeating;

    private Set<DayOfWeek> repeatDays;

    //region Constructors

    public SprinklerSettingDTO() {}

    //endregion

    //region Getters and Setters

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Set<DayOfWeek> getRepeatDays() {
        return repeatDays;
    }

    public void setRepeatDays(Set<DayOfWeek> repeatDays) {
        this.repeatDays = repeatDays;
    }

    public Boolean getRepeating() {
        return isRepeating;
    }

    public void setRepeating(Boolean repeating) {
        isRepeating = repeating;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //endregion


    @Override
    public String toString() {
        return "SprinklerSettingDTO{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", isRepeating=" + isRepeating +
                ", repeatDays=" + repeatDays +
                '}';
    }
}
