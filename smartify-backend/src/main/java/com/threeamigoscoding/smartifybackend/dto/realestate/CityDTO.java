package com.threeamigoscoding.smartifybackend.dto.realestate;

import jakarta.validation.constraints.NotNull;

public class CityDTO {

    private Integer id;

    @NotNull(message = "City needs to have a name")
    private String name;

    @NotNull(message = "City needs to have a zip code")
    private String zipCode;

    //region Constructors

    public CityDTO(Integer id, String name, String zipCode) {
        this.id = id;
        this.name = name;
        this.zipCode = zipCode;
    }

    public CityDTO() {
    }

    //endregion

    //region Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    //endregion

}
