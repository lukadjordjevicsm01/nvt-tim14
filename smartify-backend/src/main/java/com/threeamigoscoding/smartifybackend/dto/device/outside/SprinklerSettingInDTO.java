package com.threeamigoscoding.smartifybackend.dto.device.outside;

import java.time.DayOfWeek;
import java.util.Set;

public class SprinklerSettingInDTO {

    private String addedBy;
    private long startTime;

    private long endTime;

    private Boolean isRepeating;

    private Set<DayOfWeek> repeatDays;


    //region Getters and Setters

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public Boolean getRepeating() {
        return isRepeating;
    }

    public void setRepeating(Boolean repeating) {
        isRepeating = repeating;
    }

    public Set<DayOfWeek> getRepeatDays() {
        return repeatDays;
    }

    public void setRepeatDays(Set<DayOfWeek> repeatDays) {
        this.repeatDays = repeatDays;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    //endregion


    @Override
    public String toString() {
        return "SprinklerSettingInDTO{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", isRepeating=" + isRepeating +
                ", repeatDays=" + repeatDays +
                '}';
    }
}
