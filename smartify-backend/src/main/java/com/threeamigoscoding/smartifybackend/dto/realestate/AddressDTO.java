package com.threeamigoscoding.smartifybackend.dto.realestate;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;

public class AddressDTO {

    @NotNull(message = "Address needs to have longitude")
    @DecimalMin(value = "-180.0", inclusive = true, message = "Longitude must be greater than or equal to -180")
    @DecimalMax(value = "180.0", inclusive = true, message = "Longitude must be less than or equal to 180")
    protected Double longitude;

    @NotNull(message = "Address needs to have latitude")
    @DecimalMin(value = "-90.0", inclusive = true, message = "Latitude must be greater than or equal to -90")
    @DecimalMax(value = "90.0", inclusive = true, message = "Latitude must be less than or equal to 90")
    protected Double latitude;

    protected String streetName;


    //region Constructors

    public AddressDTO(Double longitude, Double latitude, String streetName) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.streetName = streetName;
    }

    public AddressDTO() {
    }

    //endregion

    //region Getters and Setters
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }


    //endregion
}
