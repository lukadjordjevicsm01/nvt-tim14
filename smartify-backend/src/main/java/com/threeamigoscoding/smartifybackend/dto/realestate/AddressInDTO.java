package com.threeamigoscoding.smartifybackend.dto.realestate;

import jakarta.validation.constraints.NotNull;

public class AddressInDTO extends AddressDTO {

    @NotNull(message = "You need to provide the id for the city")
    private Integer cityId;

    //region Constructors

    public AddressInDTO(Double longitude, Double latitude, String streetName, Integer cityId) {
        super(longitude, latitude, streetName);
        this.cityId = cityId;
    }

    public AddressInDTO(Integer cityId) {
        this.cityId = cityId;
    }

    public AddressInDTO() {
    }
    //endregion

    //region Getters and Setters

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    //endregion

}
