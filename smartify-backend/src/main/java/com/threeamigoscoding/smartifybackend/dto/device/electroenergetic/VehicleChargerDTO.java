package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;
import java.util.HashSet;
import java.util.Set;

public class VehicleChargerDTO extends DeviceDTO {

    private Double chargingPower;

    private Integer socketNumber;

    private Set<VehicleChargerSocketDTO> vehicleChargerSockets;

    //region Constructors

    public VehicleChargerDTO() {}

    //endregion

    //region Getters and Setters

    public Double getChargingPower() {
        return chargingPower;
    }

    public void setChargingPower(Double chargingPower) {
        this.chargingPower = chargingPower;
    }

    public Integer getSocketNumber() {
        return socketNumber;
    }

    public void setSocketNumber(Integer socketNumber) {
        this.socketNumber = socketNumber;
    }

    public Set<VehicleChargerSocketDTO> getVehicleChargerSockets() {
        return vehicleChargerSockets;
    }

    public void setVehicleChargerSockets(Set<VehicleChargerSocketDTO> vehicleChargerSockets) {
        this.vehicleChargerSockets = vehicleChargerSockets;
    }

    //endregion
}
