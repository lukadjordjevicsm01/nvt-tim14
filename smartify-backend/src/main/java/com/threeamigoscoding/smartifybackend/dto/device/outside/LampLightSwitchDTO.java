package com.threeamigoscoding.smartifybackend.dto.device.outside;

public class LampLightSwitchDTO {

    private boolean lightOn;

    //region Constructors

    public LampLightSwitchDTO(boolean lightOn) {
        this.lightOn = lightOn;
    }

    public LampLightSwitchDTO() {
    }

    //endregion

    //region Getters and Setters

    public boolean isLightOn() {
        return lightOn;
    }

    public void setLightOn(boolean lightOn) {
        this.lightOn = lightOn;
    }

    //endregion

}
