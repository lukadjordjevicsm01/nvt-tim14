package com.threeamigoscoding.smartifybackend.dto.device.home;

import java.sql.Timestamp;

public class WashingMachineActionDTO {
    Integer id;
    String washingMachineModeName;
    Double duration;
    String email;
    Timestamp timestamp;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWashingMachineModeName() {
        return washingMachineModeName;
    }

    public void setWashingMachineModeName(String washingMachineModeName) {
        this.washingMachineModeName = washingMachineModeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
