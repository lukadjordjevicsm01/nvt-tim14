package com.threeamigoscoding.smartifybackend.dto.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioningMode;

public class AirConditioningActionDTO {
    Integer id;
    AirConditioningMode airConditioningMode;
    Double currentTemperature;
    String email;

    public AirConditioningActionDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AirConditioningMode getAirConditioningMode() {
        return airConditioningMode;
    }

    public void setAirConditioningMode(AirConditioningMode airConditioningMode) {
        this.airConditioningMode = airConditioningMode;
    }

    public Double getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(Double currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
