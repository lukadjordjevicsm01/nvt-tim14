package com.threeamigoscoding.smartifybackend.dto.device;

import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;
import org.springframework.web.multipart.MultipartFile;


public class DeviceDTO {

    private Integer id;

    private Boolean isOnline;

    private Boolean isOn;

    private String name;

    private PowerSupplyType powerSupplyType;

    private Double consumption;

    private String imagePath;

    private String deviceType;

    //region Constructors

    public DeviceDTO(){}

    //endregion

    //region Getters and Setters


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public Boolean getOn() {
        return isOn;
    }

    public void setOn(Boolean on) {
        isOn = on;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Double getConsumption() {
        return consumption;
    }

    public void setConsumption(Double consumption) {
        this.consumption = consumption;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    //endregion
}
