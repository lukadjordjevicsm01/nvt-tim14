package com.threeamigoscoding.smartifybackend.dto;

import com.fasterxml.jackson.databind.ObjectMapper;

public class WebSocketMessageDTO {

    private String command;

    private Object payload;

    public WebSocketMessageDTO(String command, Object payload) {
        this.command = command;
        this.payload = payload;
    }

    public WebSocketMessageDTO() {
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
