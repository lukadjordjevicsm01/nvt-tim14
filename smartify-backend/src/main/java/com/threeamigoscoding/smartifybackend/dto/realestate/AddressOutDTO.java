package com.threeamigoscoding.smartifybackend.dto.realestate;

public class AddressOutDTO extends AddressDTO{

    private CityDTO cityDTO;

    private CountryDTO countryDTO;

    //region Constructors

    public AddressOutDTO(Double longitude, Double latitude, String streetName, CityDTO cityDTO, CountryDTO countryDTO) {
        super(longitude, latitude, streetName);
        this.cityDTO = cityDTO;
        this.countryDTO = countryDTO;
    }

    public AddressOutDTO(CityDTO cityDTO, CountryDTO countryDTO) {
        this.cityDTO = cityDTO;
        this.countryDTO = countryDTO;
    }

    public AddressOutDTO() {
    }

    //endregion

    //region Getters and Setters

    public CityDTO getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }

    public CountryDTO getCountryDTO() {
        return countryDTO;
    }

    public void setCountryDTO(CountryDTO countryDTO) {
        this.countryDTO = countryDTO;
    }

    //endregion

}
