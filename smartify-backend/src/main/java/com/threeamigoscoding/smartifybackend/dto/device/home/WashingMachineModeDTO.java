package com.threeamigoscoding.smartifybackend.dto.device.home;


public class WashingMachineModeDTO {

    private String name;

    private Double duration;

    //region Constructors

    public WashingMachineModeDTO(){}

    //endregion

    //region Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    //endregion

}
