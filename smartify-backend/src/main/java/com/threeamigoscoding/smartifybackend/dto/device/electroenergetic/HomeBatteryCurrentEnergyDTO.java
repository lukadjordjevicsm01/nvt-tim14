package com.threeamigoscoding.smartifybackend.dto.device.electroenergetic;

public class HomeBatteryCurrentEnergyDTO {

    private Integer id;
    private Double currentEnergy;

    //region Constructors

    public HomeBatteryCurrentEnergyDTO() {
    }

    public HomeBatteryCurrentEnergyDTO(Integer id, Double currentEnergy) {
        this.id = id;
        this.currentEnergy = currentEnergy;
    }

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCurrentEnergy() {
        return currentEnergy;
    }

    public void setCurrentEnergy(Double currentEnergy) {
        this.currentEnergy = currentEnergy;
    }

    //endregion


}
