package com.threeamigoscoding.smartifybackend.dto.device.home;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.PowerSupplyType;
import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioningMode;

import java.util.HashSet;
import java.util.Set;

public class AirConditioningDTO extends DeviceDTO {

    private Double currentTemperature;

    private Double minTemperature;

    private Double maxTemperature;

    private AirConditioningMode currentMode;

    private Set<AirConditioningMode> supportedModes;

    private Boolean isSettingsActive;

    private Set<AirConditioningSettingDTO> customSettings;

    //region Constructors

    public AirConditioningDTO(){}

    //endregion

    //region Getters and Setters

    public Double getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(Double currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public Double getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(Double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public Double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(Double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public AirConditioningMode getCurrentMode() {
        return currentMode;
    }

    public void setCurrentMode(AirConditioningMode currentMode) {
        this.currentMode = currentMode;
    }

    public Set<AirConditioningMode> getSupportedModes() {
        return supportedModes;
    }

    public void setSupportedModes(Set<AirConditioningMode> supportedModes) {
        this.supportedModes = supportedModes;
    }

    public Boolean getSettingsActive() {
        return isSettingsActive;
    }

    public void setSettingsActive(Boolean settingsActive) {
        isSettingsActive = settingsActive;
    }

    public Set<AirConditioningSettingDTO> getCustomSettings() {
        return customSettings;
    }

    public void setCustomSettings(Set<AirConditioningSettingDTO> customSettings) {
        this.customSettings = customSettings;
    }

    //endregion
}
