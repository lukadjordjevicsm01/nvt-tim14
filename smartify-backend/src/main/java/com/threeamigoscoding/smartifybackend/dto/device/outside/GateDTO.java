package com.threeamigoscoding.smartifybackend.dto.device.outside;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.outside.GateMode;

import java.util.HashSet;
import java.util.Set;

public class GateDTO extends DeviceDTO {

    private Boolean isOpen;

    private GateMode mode;

    private Set<String> allowedVehicles = new HashSet<>();

    //region Constructors

    public GateDTO() {}

    //endregion

    //region Getters and Setters

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public GateMode getMode() {
        return mode;
    }

    public void setMode(GateMode aPublic) {
        mode = aPublic;
    }

    public Set<String> getAllowedVehicles() {
        return allowedVehicles;
    }

    public void setAllowedVehicles(Set<String> allowedVehicles) {
        this.allowedVehicles = allowedVehicles;
    }

    //endregion
}
