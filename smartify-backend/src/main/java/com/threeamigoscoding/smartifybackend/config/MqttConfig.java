package com.threeamigoscoding.smartifybackend.config;

import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class MqttConfig {
    private final String broker = MyCredentials.broker;

    @Bean(name = "deviceClient")
    public IMqttClient deviceClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

    @Bean(name = "ambientSensorMqttClient")
    public IMqttClient ambientSensorMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

     @Bean(name = "airConditioningClient")
    public IMqttClient airConditioningMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

    @Bean(name = "washingMachineClient")
    public IMqttClient washingMachineMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

    @Bean(name = "lampMqttClient")
    public IMqttClient lampMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

    @Bean(name = "gateMqttClient")
    public IMqttClient gateMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

    @Bean(name = "solarPanelSystemMqttClient")
    public IMqttClient solarPanelSystemMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

    @Bean(name = "homeBatteryMqttClient")
    public IMqttClient homeBatteryMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

    @Bean(name = "vehicleChargerMqttClient")
    public IMqttClient vehicleChargerMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }


    @Bean(name = "sprinklerMqttClient")
    public IMqttClient sprinklerMqttClient() throws Exception {
        MqttClient client = new MqttClient(this.broker, UUID.randomUUID().toString(), new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setAutomaticReconnect(true);
//        options.setUserName(this.username);
//        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

}
