package com.threeamigoscoding.smartifybackend.service.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachineMode;
import com.threeamigoscoding.smartifybackend.repository.device.home.WashingMachineModeRepository;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WashingMachineModeService {

    @Autowired
    public WashingMachineModeRepository washingMachineModeRepository;

    public void save(WashingMachineMode washingMachineMode) {
        washingMachineModeRepository.save(washingMachineMode);
    }

    public WashingMachineMode getWashingMachineMode(Integer id) {
        return washingMachineModeRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Washing machine mode does not exist."));
    }

}
