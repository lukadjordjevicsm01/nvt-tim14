package com.threeamigoscoding.smartifybackend.service.device.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.device.home.AirConditioningActionDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.AirConditioningDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.AirConditioningSettingInDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.influx_records.home.AirConditioningRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.home.AirConditioningMapper;
import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioning;
import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioningSetting;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.home.AirConditioningRepository;
import com.threeamigoscoding.smartifybackend.repository.device.home.AirConditioningRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.InvalidOperationException;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

@Service
public class AirConditioningService {

    public AirConditioningRepository airConditioningRepository;

    private final RealEstateService realEstateService;

    private final AirConditioningMapper airConditioningMapper;

    private final IMqttClient airConditioningMqttClient;

    private final ObjectMapper jsonMapper;

    private final AirConditioningRepositoryInflux airConditioningRepositoryInflux;

    private final AirConditioningSettingService airConditioningSettingService;

    private final PingService pingService;

    private final AccessControl accessControl;

    @Autowired
    public AirConditioningService(
            AirConditioningRepository airConditioningRepository,
            RealEstateService realEstateService,
            AirConditioningMapper airConditioningMapper,
            @Qualifier("airConditioningClient") IMqttClient ambientSensorMqttClient,
            ObjectMapper jsonMapper,
            AirConditioningRepositoryInflux airConditioningRepositoryInflux,
            AirConditioningSettingService airConditioningSettingService,
            PingService pingService,
            AccessControl accessControl
    ) {
        this.airConditioningRepository = airConditioningRepository;
        this.realEstateService = realEstateService;
        this.airConditioningMapper = airConditioningMapper;
        this.airConditioningMqttClient = ambientSensorMqttClient;
        this.jsonMapper = jsonMapper;
        this.airConditioningRepositoryInflux = airConditioningRepositoryInflux;
        this.airConditioningSettingService = airConditioningSettingService;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();
    }

    public AirConditioning create(String token, AirConditioningDTO airConditioningDTO, Integer realEstateId) {
        AirConditioning airConditioning = airConditioningMapper.dtoToEntity(airConditioningDTO);
        RealEstate realEstate = realEstateService.findById(realEstateId);
        accessControl.validateRealEstateOwner(token, realEstate);
        airConditioning.setOn(true);
        airConditioning.setOnline(true);
        airConditioning.setCurrentMode(airConditioningDTO.getSupportedModes().iterator().next());
        airConditioning.setCurrentTemperature(20D);
        airConditioning.setCustomSettings(new HashSet<>());
        airConditioning.setRealEstate(realEstate);

        airConditioningRepository.save(airConditioning);

//        realEstate.getDevices().add(airConditioning);
//        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, airConditioning.getId());
        addToSimulator(airConditioning);

        return airConditioning;
    }

    private void addToSimulator(AirConditioning airConditioning) {
        String url = MyCredentials.simulatorServer + "add-air-conditioning";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<AirConditioning> requestEntity = new HttpEntity<>(airConditioning, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            airConditioningMqttClient.subscribe("air-conditioning/" + airConditioning.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public void save(AirConditioning airConditioning) {
        airConditioningRepository.save(airConditioning);
    }

    public List<AirConditioning> getAll() {
        return airConditioningRepository.findAll();
    }

    public AirConditioning findById(Integer id) {
        return airConditioningRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Air conditioning does not exist.")
        );
    }

    public AirConditioning getAirConditioning(String token, Integer airConditioningId) {
        AirConditioning airConditioning = findById(airConditioningId);
        accessControl.validateDeviceAccess(token, airConditioning);
        return airConditioning;
    }

    private void setMqttCallback() {
        airConditioningMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                try {
                    MqttMessageDto mqttMessageDto =
                            jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                    if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                        setLastPing(Integer.parseInt(topic.split("/")[1]));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "record")) {
                        AirConditioningRecord airConditioningRecord =
                                jsonMapper.readValue(mqttMessageDto.getPayload(), AirConditioningRecord.class);
                        recordData(airConditioningRecord);
                        AirConditioning airConditioning = findById(airConditioningRecord.getId());
                        airConditioning.setCurrentTemperature(airConditioningRecord.getCurrentTemperature());
                        airConditioningRepository.save(airConditioning);
                    }
                    else if (Objects.equals(mqttMessageDto.getCommand(), "test"))
                        System.out.println(mqttMessageDto.getPayload());
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    private void recordData(AirConditioningRecord airConditioningRecord) {
        Point airConditioning = Point.measurement("air conditioning")
                .addField("current_temperature", airConditioningRecord.getCurrentTemperature())
                .addTag("id", airConditioningRecord.getId().toString())
                .addTag("subject", airConditioningRecord.getSubject())
                .addTag("action", airConditioningRecord.getAction());

        airConditioningRepositoryInflux.save(airConditioning);
    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }

    public void subscribeToTopics(List<AirConditioning> airConditioning) {
        airConditioning.forEach(ac -> {
            try {
                airConditioningMqttClient.subscribe("air-conditioning/" + ac.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public List<AirConditioningRecord> findByDateRange(
            String token,
            String from,
            String to,
            String airConditioningId
    ) {
        AirConditioning airConditioning = findById(Integer.valueOf(airConditioningId));
        accessControl.validateDeviceAccess(token, airConditioning);
        return airConditioningRepositoryInflux.findByDateRange(from, to, airConditioningId);
    }

    public void saveSwitchRecord(Integer id, Boolean isOn, String userEmail) {
        String action = "Device turned off";
        if (isOn) {
            action = "Device turned on";
        }
        AirConditioningRecord airConditioningRecord = new AirConditioningRecord();
        airConditioningRecord.setAction(action);
        airConditioningRecord.setSubject(userEmail);
        airConditioningRecord.setId(id);
        airConditioningRecord.setCurrentTemperature(-1000D);

        recordData(airConditioningRecord);
    }

    public void setAction(String token, Integer airConditioningId, AirConditioningActionDTO airConditioningActionDTO)
            throws JsonProcessingException, MqttException {
        AirConditioning airConditioning = findById(airConditioningId);
        accessControl.validateDeviceAccess(token, airConditioning);
        airConditioning.setCurrentMode(airConditioningActionDTO.getAirConditioningMode());
        airConditioning.setCurrentTemperature(airConditioningActionDTO.getCurrentTemperature());
        airConditioningRepository.save(airConditioning);

        String actionPayload = jsonMapper.writeValueAsString(airConditioningActionDTO);
        MqttMessageDto payload = new MqttMessageDto("action", actionPayload);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());

        airConditioningMqttClient.publish("air-conditioning-simulator/" + airConditioningId, mqttMessage);
    }

    public void addSetting(
            String token,
            Integer airConditioningId,
            AirConditioningSettingInDTO airConditioningSettingDTO
    ) throws JsonProcessingException, MqttException {
        AirConditioning airConditioning = findById(airConditioningId);
        accessControl.validateDeviceAccess(token, airConditioning);

        for (AirConditioningSetting airConditioningSetting : airConditioning.getCustomSettings()) {
            if (timeRangeInvalid(new Timestamp(airConditioningSettingDTO.getStartTime()),
                    new Timestamp(airConditioningSettingDTO.getEndTime()),
                    airConditioningSetting.getStartTime(), airConditioningSetting.getEndTime()))
                throw new InvalidOperationException("Setting alredy exists for chosen time range.");
        }

        AirConditioningSetting airConditioningSetting = new AirConditioningSetting();
        airConditioningSetting.setMode(airConditioningSettingDTO.getMode());
        airConditioningSetting.setStartTime(new Timestamp(airConditioningSettingDTO.getStartTime()));
        airConditioningSetting.setEndTime(new Timestamp(airConditioningSettingDTO.getEndTime()));
        airConditioningSetting.setTemperature(airConditioningSettingDTO.getTemperature());

        airConditioningSettingService.save(airConditioningSetting);
        airConditioning.getCustomSettings().add(airConditioningSetting);
        airConditioningRepository.save(airConditioning);

        String settingPayload = jsonMapper.writeValueAsString(airConditioningSetting);
        MqttMessageDto payload = new MqttMessageDto("setting", settingPayload);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());

        airConditioningMqttClient.publish("air-conditioning-simulator/" + airConditioningId, mqttMessage);

    }

    private static boolean timeRangeInvalid(Timestamp start1, Timestamp end1, Timestamp start2, Timestamp end2) {
        // Extract time components from Timestamps
        Calendar cal1Start = Calendar.getInstance();
        cal1Start.setTimeInMillis(start1.getTime());
        int hour1Start = cal1Start.get(Calendar.HOUR_OF_DAY);
        int minute1Start = cal1Start.get(Calendar.MINUTE);
        int second1Start = cal1Start.get(Calendar.SECOND);

        Calendar cal1End = Calendar.getInstance();
        cal1End.setTimeInMillis(end1.getTime());
        int hour1End = cal1End.get(Calendar.HOUR_OF_DAY);
        int minute1End = cal1End.get(Calendar.MINUTE);
        int second1End = cal1End.get(Calendar.SECOND);

        Calendar cal2Start = Calendar.getInstance();
        cal2Start.setTimeInMillis(start2.getTime());
        int hour2Start = cal2Start.get(Calendar.HOUR_OF_DAY);
        int minute2Start = cal2Start.get(Calendar.MINUTE);
        int second2Start = cal2Start.get(Calendar.SECOND);

        Calendar cal2End = Calendar.getInstance();
        cal2End.setTimeInMillis(end2.getTime());
        int hour2End = cal2End.get(Calendar.HOUR_OF_DAY);
        int minute2End = cal2End.get(Calendar.MINUTE);
        int second2End = cal2End.get(Calendar.SECOND);

        // Check if the time ranges overlap or one is inside the other
        return (hour1Start < hour2End ||
                (
                        hour1Start == hour2End &&
                                (minute1Start < minute2End ||
                                        (minute1Start == minute2End && second1Start <= second2End))))
                &&
                (hour1End > hour2Start ||
                        (hour1End == hour2Start &&
                                (minute1End > minute2Start ||
                                        (minute1End == minute2Start && second1End >= second2Start))));
    }
}
