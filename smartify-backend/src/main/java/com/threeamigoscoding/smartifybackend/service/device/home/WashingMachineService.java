package com.threeamigoscoding.smartifybackend.service.device.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.WebSocketMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.WashingMachineActionDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.WashingMachineDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.WashingMachineModeDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.WashingMachineSettingInDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.influx_records.home.AirConditioningRecord;
import com.threeamigoscoding.smartifybackend.influx_records.home.AmbientSensorRecordDTO;
import com.threeamigoscoding.smartifybackend.influx_records.home.WashingMachineRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.home.WashingMachineMapper;
import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioning;
import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachine;
import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachineMode;
import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachineSetting;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.home.WashingMachineRepository;
import com.threeamigoscoding.smartifybackend.repository.device.home.WashingMachineRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.InvalidOperationException;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

@Service
public class WashingMachineService {

    public WashingMachineRepository washingMachineRepository;
    public WashingMachineRepositoryInflux washingMachineRepositoryInflux;
    private final RealEstateService realEstateService;
    private final WashingMachineModeService washingMachineModeService;
    private final WashingMachineSettingService washingMachineSettingService;
    private final WashingMachineMapper washingMachineMapper;
    private final IMqttClient washingMachineMqttClient;
    private final ObjectMapper jsonMapper;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final PingService pingService;
    private final AccessControl accessControl;

    @Autowired
    public WashingMachineService(
            WashingMachineRepository washingMachineRepository,
            RealEstateService realEstateService,
            WashingMachineModeService washingMachineModeService,
            WashingMachineSettingService washingMachineSettingService,
            WashingMachineMapper washingMachineMapper,
            @Qualifier("washingMachineClient") IMqttClient washingMachineMqttClient,
            ObjectMapper jsonMapper,
            WashingMachineRepositoryInflux washingMachineRepositoryInflux,
            SimpMessagingTemplate simpMessagingTemplate,
            PingService pingService,
            AccessControl accessControl
    ) {
        this.washingMachineRepository = washingMachineRepository;
        this.realEstateService = realEstateService;
        this.washingMachineModeService = washingMachineModeService;
        this.washingMachineSettingService = washingMachineSettingService;
        this.washingMachineMapper = washingMachineMapper;
        this.washingMachineMqttClient = washingMachineMqttClient;
        this.jsonMapper = jsonMapper;
        this.washingMachineRepositoryInflux = washingMachineRepositoryInflux;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();
    }

    public WashingMachine create(String token, WashingMachineDTO washingMachineDTO, Integer realEstateId) {
        WashingMachine washingMachine = washingMachineMapper.dtoToEntity(washingMachineDTO);
        RealEstate realEstate = realEstateService.findById(realEstateId);
        accessControl.validateRealEstateOwner(token, realEstate);
        washingMachine.setRealEstate(realEstate);
        washingMachine.setCustomSettings(new HashSet<>());
        for (WashingMachineMode washingMachineMode : washingMachine.getSupportedModes()) {
            washingMachineModeService.save(washingMachineMode);
        }

        washingMachineRepository.save(washingMachine);

//        realEstate.getDevices().add(washingMachine);
//        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, washingMachine.getId());
        addToSimulator(washingMachine);

        return washingMachine;
    }

    private void addToSimulator(WashingMachine washingMachine) {
        String url = MyCredentials.simulatorServer + "add-washing-machine";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<WashingMachine> requestEntity = new HttpEntity<>(washingMachine, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            washingMachineMqttClient.subscribe("washing-machine/" + washingMachine.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public void save(WashingMachine washingMachine) {
        washingMachineRepository.save(washingMachine);
    }

    public List<WashingMachine> getAll() {
        return washingMachineRepository.findAll();
    }

    public WashingMachine findById(Integer id) {
        return washingMachineRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Washing machine does not exist.")
        );
    }

    public WashingMachineDTO getWashingMachine(String token, Integer washingMachineId) {
        WashingMachine washingMachine = findById(washingMachineId);
        accessControl.validateDeviceAccess(token, washingMachine);
        return washingMachineMapper.endtityToDto(washingMachine);
    }

    public WashingMachineModeDTO getCurrentMode(String token, Integer washingMachineId) {
        WashingMachine washingMachine = findById(washingMachineId);
        accessControl.validateDeviceAccess(token, washingMachine);
        WashingMachineMode washingMachineMode = washingMachine.getCurrentMode();
        WashingMachineModeDTO washingMachineModeDTO = new WashingMachineModeDTO();
        washingMachineModeDTO.setName(washingMachineMode.getName());
        washingMachineModeDTO.setDuration(washingMachineMode.getDuration());
        return washingMachineModeDTO;
    }

    public void subscribeToTopics(List<WashingMachine> washingMachines) {
        washingMachines.forEach(wm -> {
            try {
                washingMachineMqttClient.subscribe("washing-machine/" + wm.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void setMqttCallback() {
        washingMachineMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) {
                try {
                    MqttMessageDto mqttMessageDto =
                            jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                    int washingMachineId = Integer.parseInt(topic.split("/")[1]);
                    WashingMachine washingMachine =  findById(washingMachineId);
                    if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                        setLastPing(Integer.parseInt(topic.split("/")[1]));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "record"))
                        processRecord(washingMachine, mqttMessageDto);
                    else if (Objects.equals(mqttMessageDto.getCommand(), "notify"))
                        processNotify(washingMachine);
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    public void setAction(String token, Integer washingMachineId, WashingMachineActionDTO washingMachineActionDTO)
            throws JsonProcessingException, MqttException {
        WashingMachine washingMachine = findById(washingMachineId);
        accessControl.validateDeviceAccess(token, washingMachine);

        String actionPayload = jsonMapper.writeValueAsString(washingMachineActionDTO);
        MqttMessageDto payload = new MqttMessageDto("action", actionPayload);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());

        washingMachineMqttClient.publish("washing-machine-simulator/" + washingMachineId, mqttMessage);
    }

    public void saveSwitchRecord(Integer id, Boolean isOn, String userEmail) {
        String action = "Device turned off";
        if (isOn) {
            action = "Device turned on";
        }
        WashingMachineRecord washingMachineRecord = new WashingMachineRecord();
        washingMachineRecord.setAction(action);
        washingMachineRecord.setSubject(userEmail);
        washingMachineRecord.setId(id);

        recordData(washingMachineRecord);
    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }

    private void processRecord(WashingMachine washingMachine, MqttMessageDto mqttMessageDto)
            throws JsonProcessingException {
        WashingMachineRecord washingMachineRecord =
                jsonMapper.readValue(mqttMessageDto.getPayload(), WashingMachineRecord.class);
        recordData(washingMachineRecord);

        washingMachine.setCurrentMode(washingMachine.getSupportedModes().stream().filter(
                (mode) -> Objects.equals(mode.getName(), washingMachineRecord.getAction())
        ).findFirst().get());
        washingMachineRepository.save(washingMachine);

        WebSocketMessageDTO wsModePayload =
                new WebSocketMessageDTO("value", washingMachineRecord.getAction());
        String wsModePayloadJson = jsonMapper.writeValueAsString(wsModePayload);
        simpMessagingTemplate.convertAndSend(
                "/washing-machine/" + washingMachine.getId(),
                wsModePayloadJson);
        System.out.println("/washing-machine/" + washingMachine.getId());
    }

    private void processNotify(WashingMachine washingMachine) throws JsonProcessingException {
        washingMachine.setCurrentMode(null);
        washingMachineRepository.save(washingMachine);
        WebSocketMessageDTO wsModePayload =
                new WebSocketMessageDTO("value", "Idle");
        String wsModePayloadJson = jsonMapper.writeValueAsString(wsModePayload);
        simpMessagingTemplate.convertAndSend(
                "/washing-machine/" + washingMachine.getId(),
                wsModePayloadJson);
    }

    private void recordData(WashingMachineRecord washingMachineRecord) {
        Point airConditioning = Point.measurement("washing machine")
                .addField("action", washingMachineRecord.getAction())
                .addTag("id", washingMachineRecord.getId().toString())
                .addTag("subject", washingMachineRecord.getSubject());

        washingMachineRepositoryInflux.save(airConditioning);
    }

    public List<WashingMachineRecord> findByDateRange(String token, String from, String to, String washingMachineId) {
        WashingMachine washingMachine = findById(Integer.valueOf(washingMachineId));
        accessControl.validateDeviceAccess(token, washingMachine);
        return washingMachineRepositoryInflux.findByDateRange(from, to, washingMachineId);
    }

    public void addSetting(String token, Integer washingMachineId, WashingMachineSettingInDTO washingMachineInDTO)
            throws JsonProcessingException, MqttException {
        WashingMachine washingMachine = findById(washingMachineId);
        accessControl.validateDeviceAccess(token, washingMachine);
        LocalDateTime start1 = (new Timestamp(washingMachineInDTO.getStartTime()).toLocalDateTime());
        LocalDateTime end1 = start1.plusMinutes((washingMachineInDTO.getMode().getDuration()).longValue());
        for (WashingMachineSetting washingMachineSetting : washingMachine.getCustomSettings()) {
            LocalDateTime start2 = washingMachineSetting.getStartTime().toLocalDateTime();
            LocalDateTime end2 = start2.plusMinutes((washingMachineSetting.getMode().getDuration()).longValue());
            if (dateRangesOverlap(start1, end1, start2, end2))
                throw new InvalidOperationException("Setting alredy exists for chosen time range.");
        }

        WashingMachineSetting washingMachineSetting = new WashingMachineSetting();
        washingMachineSetting.setStartTime(new Timestamp(washingMachineInDTO.getStartTime()));
        washingMachineSetting.setMode(washingMachine.getSupportedModes().stream().filter(
                (mode) -> Objects.equals(mode.getName(), washingMachineInDTO.getMode().getName())
        ).findFirst().get());

        washingMachineSettingService.save(washingMachineSetting);
        washingMachine.getCustomSettings().add(washingMachineSetting);
        washingMachineRepository.save(washingMachine);

        String settingPayload = jsonMapper.writeValueAsString(washingMachineSetting);
        MqttMessageDto payload = new MqttMessageDto("setting", settingPayload);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());

        washingMachineMqttClient.publish("washing-machine-simulator/" + washingMachineId, mqttMessage);
    }

    public static boolean dateRangesOverlap(LocalDateTime start1, LocalDateTime end1,
                                            LocalDateTime start2, LocalDateTime end2) {
        // Scenario 1: Range 1 is completely before Range 2
        if (end1.isBefore(start2) || end1.isEqual(start2)) {
            return false;
        }

        // Scenario 2: Range 1 is completely after Range 2
        if (start1.isAfter(end2) || start1.isEqual(end2)) {
            return false;
        }

        // Scenario 3: Range 1 starts before Range 2 but ends within Range 2
        if (start1.isBefore(start2) && end1.isBefore(end2)) {
            return true;
        }

        // Scenario 4: Range 1 starts before Range 2 and ends after Range 2
        if (start1.isBefore(start2) && end1.isAfter(end2)) {
            return true;
        }

        // Scenario 5: Range 1 is completely within Range 2
        if (start1.isAfter(start2) && end1.isBefore(end2)) {
            return true;
        }

        // Scenario 6: Range 1 starts within Range 2 but ends after Range 2
        if (start1.isAfter(start2) && start1.isBefore(end2) && end1.isAfter(end2)) {
            return true;
        }

        // Scenario 7: Range 1 starts at the same time as Range 2
        if (start1.equals(start2)) {
            return true;
        }

        // Scenario 8: Range 1 ends at the same time as Range 2
        if (end1.equals(end2)) {
            return true;
        }

        // No overlapping scenario found
        return false;
    }
}
