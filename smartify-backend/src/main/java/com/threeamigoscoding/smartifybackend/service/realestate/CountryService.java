package com.threeamigoscoding.smartifybackend.service.realestate;

import com.threeamigoscoding.smartifybackend.dto.realestate.CountryDTO;
import com.threeamigoscoding.smartifybackend.mappers.realestate.AddressMapper;
import com.threeamigoscoding.smartifybackend.repository.realestate.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private AddressMapper addressMapper;

    public List<CountryDTO> findAll(){
        return countryRepository.findAll().stream().map(c -> addressMapper.countryToCountryDTO(c)).toList();
    }

}
