package com.threeamigoscoding.smartifybackend.service.device.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.WebSocketMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.AmbientSensorDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.influx_records.home.AmbientSensorRecord;
import com.threeamigoscoding.smartifybackend.influx_records.home.AmbientSensorRecordDTO;
import com.threeamigoscoding.smartifybackend.mappers.device.home.AmbientSensorMapper;
import com.threeamigoscoding.smartifybackend.model.device.home.AmbientSensor;
import com.threeamigoscoding.smartifybackend.model.device.outside.Lamp;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.home.AmbientSensorRepository;
import com.threeamigoscoding.smartifybackend.repository.device.home.AmbientSensorRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Service
public class AmbientSensorService {

    private final AmbientSensorRepository ambientSensorRepository;
    private final RealEstateService realEstateService;
    private final AmbientSensorMapper ambientSensorMapper;
    private final IMqttClient ambientSensorMqttClient;
    private final ObjectMapper jsonMapper;
    private final AmbientSensorRepositoryInflux ambientSensorRepositoryInflux;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final PingService pingService;
    private final AccessControl accessControl;

    @Autowired
    public AmbientSensorService(
            AmbientSensorRepository ambientSensorRepository,
            RealEstateService realEstateService,
            AmbientSensorMapper ambientSensorMapper,
            @Qualifier("ambientSensorMqttClient") IMqttClient ambientSensorMqttClient,
            ObjectMapper jsonMapper, AmbientSensorRepositoryInflux ambientSensorRepositoryInflux,
            SimpMessagingTemplate simpMessagingTemplate,
            PingService pingService,
            AccessControl accessControl
    ) {
        this.ambientSensorRepository = ambientSensorRepository;
        this.realEstateService = realEstateService;
        this.ambientSensorMapper = ambientSensorMapper;
        this.ambientSensorMqttClient = ambientSensorMqttClient;
        this.jsonMapper = jsonMapper;
        this.ambientSensorRepositoryInflux = ambientSensorRepositoryInflux;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();
    }

    public AmbientSensor create(String token, AmbientSensorDTO ambientSensorDTO, Integer realEstateId) {
        RealEstate realEstate = realEstateService.findById(realEstateId);
        accessControl.validateRealEstateOwner(token, realEstate);
        AmbientSensor ambientSensor = ambientSensorMapper.dtoToEntity(ambientSensorDTO);
        ambientSensor.setOnline(true);
        ambientSensor.setOn(true);
        ambientSensor.setRealEstate(realEstate);

        this.ambientSensorRepository.save(ambientSensor);

//        realEstate.getDevices().add(ambientSensor);
//        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, ambientSensor.getId());
        addToSimulator(ambientSensor);

        return ambientSensor;
    }

    private void addToSimulator(AmbientSensor ambientSensor) {
        String url = MyCredentials.simulatorServer + "add-ambient-sensor";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<AmbientSensor> requestEntity = new HttpEntity<>(ambientSensor, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            ambientSensorMqttClient.subscribe("ambient-sensor/" + ambientSensor.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public List<AmbientSensor> getAll() {
        return ambientSensorRepository.findAll();
    }

    public void save(AmbientSensor ambientSensor) {
        ambientSensorRepository.save(ambientSensor);
    }

    public AmbientSensor findById(Integer id) {
        return ambientSensorRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Ambient sensor does not exist."));
    }

    public AmbientSensorDTO getAmbientSensor(String token, Integer id) {
        AmbientSensor ambientSensor = findById(id);
        accessControl.validateDeviceAccess(token, ambientSensor);
        return ambientSensorMapper.entityToDto(ambientSensor);
    }

    public void subscribeToTopics(List<AmbientSensor> ambientSensors) {
        ambientSensors.forEach(ambientSensor -> {
            try {
                ambientSensorMqttClient.subscribe("ambient-sensor/" + ambientSensor.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void setMqttCallback() {
        ambientSensorMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) {
                try{
                    MqttMessageDto mqttMessageDto =
                            jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                    if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                        setLastPing(Integer.parseInt(topic.split("/")[1]));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "record"))
                        recordData(mqttMessageDto.getPayload());
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    public List<AmbientSensorRecordDTO> findByDateRange(
            String token,
            String from,
            String to,
            String measurement,
            Integer id,
            String aggregateWindow
    ) {
        AmbientSensor ambientSensor = findById(id);
        accessControl.validateDeviceAccess(token, ambientSensor);
        return ambientSensorRepositoryInflux.findByDateRange(from, to, measurement, id.toString(), aggregateWindow);
    }

    private void recordData(String payload) throws JsonProcessingException {
        AmbientSensorRecord ambientSensorRecord = jsonMapper.readValue(payload, AmbientSensorRecord.class);
        Point temperature = Point.measurement("temperature")
                .addField("temperature", ambientSensorRecord.getTemperature())
                .addTag("id", ambientSensorRecord.getId().toString());
        ambientSensorRepositoryInflux.save(temperature);

        WebSocketMessageDTO wsTemperaturePayload =
                new WebSocketMessageDTO("value", new AmbientSensorRecordDTO(
                        ambientSensorRecord.getTemperature(), new Timestamp(System.currentTimeMillis())));
        String wsTemperaturePayloadJson = jsonMapper.writeValueAsString(wsTemperaturePayload);
        this.simpMessagingTemplate.convertAndSend(
                "/ambient-sensor/temperature/" + ambientSensorRecord.getId(),
                wsTemperaturePayloadJson);

        Point humidity = Point.measurement("humidity")
                .addField("humidity", ambientSensorRecord.getHumidity())
                .addTag("id", ambientSensorRecord.getId().toString());
        ambientSensorRepositoryInflux.save(humidity);

        WebSocketMessageDTO wsHumidityPayload =
                new WebSocketMessageDTO("value", new AmbientSensorRecordDTO(
                        ambientSensorRecord.getHumidity(), new Timestamp(System.currentTimeMillis())));
        String wsHumidityPayloadJson = jsonMapper.writeValueAsString(wsHumidityPayload);
        this.simpMessagingTemplate.convertAndSend(
                "/ambient-sensor/humidity/" + ambientSensorRecord.getId(),
                wsHumidityPayloadJson);
    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }
}
