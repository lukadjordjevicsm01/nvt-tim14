package com.threeamigoscoding.smartifybackend.service.user;

import com.threeamigoscoding.smartifybackend.model.user.UserActivation;
import com.threeamigoscoding.smartifybackend.repository.user.UserActivationRepository;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class UserActivationService {

    @Autowired
    private UserActivationRepository userActivationRepository;

    public UserActivation save(UserActivation userActivation) {
        return this.userActivationRepository.save(userActivation);
    }

    public UserActivation findById(Integer id) {
        return userActivationRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("User activation does not exist!"));
    }

    public Boolean isActivationValid(UserActivation userActivation) {
        return userActivation.getExpirDate().isAfter(LocalDate.now());
    }

}
