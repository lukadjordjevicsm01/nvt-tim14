package com.threeamigoscoding.smartifybackend.service.device.electroenergetic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.WebSocketMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.ChargingDataDTO;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.CurrentBatteryPercentageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.VehicleChargerDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageObjectDto;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.SolarPanelSystemEventRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.VehicleChargerRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.electroenergetic.VehicleChargerMapper;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.HomeBattery;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.VehicleCharger;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.VehicleChargerSocket;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.HomeBatteryRepositoryInflux;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.VehicleChargerRepository;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.VehicleChargerRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Service
public class VehicleChargerService {

    private final VehicleChargerRepository vehicleChargerRepository;
    private final RealEstateService realEstateService;
    private final VehicleChargerSocketService vehicleChargerSocketService;
    private final VehicleChargerMapper vehicleChargerMapper;
    private final IMqttClient vehicleChargerMqttClient;
    private final ObjectMapper jsonMapper;
    private final VehicleChargerRepositoryInflux vehicleChargerRepositoryInflux;
    private final SimpMessagingTemplate simpMessagingTemplate;

    private final PingService pingService;

    private final AccessControl accessControl;


    @Autowired
    public VehicleChargerService(VehicleChargerRepository vehicleChargerRepository,
                                 RealEstateService realEstateService,
                                 VehicleChargerSocketService vehicleChargerSocketService,
                                 VehicleChargerMapper vehicleChargerMapper,
                                 @Qualifier("vehicleChargerMqttClient") IMqttClient vehicleChargerMqttClient,
                                 ObjectMapper jsonMapper,
                                 VehicleChargerRepositoryInflux vehicleChargerRepositoryInflux,
                                 SimpMessagingTemplate simpMessagingTemplate,
                                 PingService pingService,
                                 AccessControl accessControl) {
        this.vehicleChargerRepository = vehicleChargerRepository;
        this.realEstateService = realEstateService;
        this.vehicleChargerSocketService = vehicleChargerSocketService;
        this.vehicleChargerMapper = vehicleChargerMapper;
        this.vehicleChargerMqttClient = vehicleChargerMqttClient;
        this.jsonMapper = jsonMapper;
        this.vehicleChargerRepositoryInflux = vehicleChargerRepositoryInflux;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();
    }


    public VehicleCharger create(VehicleChargerDTO vehicleChargerDTO, Integer realEstateId, String token) {
        VehicleCharger vehicleCharger = vehicleChargerMapper.dtoToEntity(vehicleChargerDTO);
        RealEstate realEstate = realEstateService.findById(realEstateId);

        accessControl.validateRealEstateOwner(token, realEstate);

        vehicleCharger.setRealEstate(realEstate);

        for (int i = 1; i <=  vehicleChargerDTO.getSocketNumber(); i++) {
            String name = vehicleCharger.getName() + "_Socket_" + i;
            VehicleChargerSocket vehicleChargerSocket = new VehicleChargerSocket();
            vehicleChargerSocket.setName(name);
            vehicleChargerSocket.setActive(false);
            vehicleChargerSocket.setChargingLimit(100.0);
            vehicleChargerSocketService.save(vehicleChargerSocket);
            vehicleCharger.getVehicleChargerSockets().add(vehicleChargerSocket);
        }

        vehicleChargerRepository.save(vehicleCharger);

        realEstate.getDevices().add(vehicleCharger);
        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, vehicleCharger.getId());
        addToSimulator(vehicleCharger);

        return vehicleCharger;

    }

    private void addToSimulator(VehicleCharger vehicleCharger) {
        String url = MyCredentials.simulatorServer + "add-vehicle-charger";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<VehicleCharger> requestEntity = new HttpEntity<>(vehicleCharger, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            vehicleChargerMqttClient.subscribe("vehicle-charger/" + vehicleCharger.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public List<VehicleCharger> getAll() {
        return vehicleChargerRepository.findAll();
    }

    public VehicleCharger findById(Integer id) {
        return vehicleChargerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Vehicle Charger does not exist."));
    }

    public void save(VehicleCharger vehicleCharger) {
        vehicleChargerRepository.save(vehicleCharger);
    }

    public VehicleChargerDTO getVehicleCharger(Integer id, String token) {
        VehicleCharger vehicleCharger = findById(id);
        accessControl.validateDeviceAccess(token, vehicleCharger);
        return vehicleChargerMapper.entityToDto(vehicleCharger);
    }

    public void subscribeToTopics(List<VehicleCharger> vehicleChargers) {
        vehicleChargers.forEach(vehicleCharger -> {
            try {
                vehicleChargerMqttClient.subscribe("vehicle-charger/" + vehicleCharger.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void setMqttCallback() {
        vehicleChargerMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) {
                try {
                    MqttMessageDto mqttMessageDto =
                            jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                    if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                        setLastPing(Integer.parseInt(topic.split("/")[1]));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "charging-data"))
                        saveChargingRecord(jsonMapper
                                .readValue(mqttMessageDto.getPayload(), VehicleChargerRecord.class));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "current-battery-percentage"))
                        sendCurrentBatteryPercentage(jsonMapper
                                .readValue(mqttMessageDto.getPayload(), CurrentBatteryPercentageDTO.class));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }

    public void switchChargingState(ChargingDataDTO chargingDataDTO, Boolean activeStatus, String token) throws
            JsonProcessingException, MqttException {
        VehicleCharger vehicleCharger = changeChargingSocketState(chargingDataDTO.getVehicleChargerId(),
                chargingDataDTO.getChargeSocketName(), activeStatus);

        accessControl.validateDeviceAccess(token, vehicleCharger);

        String command = "finish-charging";
        if (activeStatus)
            command = "start-charging";
        MqttMessageObjectDto payload = new MqttMessageObjectDto(command, chargingDataDTO);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        vehicleChargerMqttClient.publish(
                "vehicle-charger-simulator/" + vehicleCharger.getId().toString(),
                mqttMessage);
    }

    private void saveChargingRecord(VehicleChargerRecord vehicleChargerRecord) throws JsonProcessingException {
        Point point = Point.measurement("Vehicle Charger Charging Data")
                .addTag("vehicleChargerId", vehicleChargerRecord.getVehicleChargerId().toString())
                .addTag("vehicleChargerSocketName", vehicleChargerRecord.getVehicleChargerSocketName())
                .addTag("message", vehicleChargerRecord.getMessage())
                .addTag("subject", vehicleChargerRecord.getSubject())
                .addField("chargingDuration", vehicleChargerRecord.getChargingDuration());
        vehicleChargerRepositoryInflux.save(point);
        if (vehicleChargerRecord.getFinished()) {
            WebSocketMessageDTO wsChargingPayload =
                    new WebSocketMessageDTO("charging-finished", vehicleChargerRecord);
            String wsChargingPayloadJson = jsonMapper.writeValueAsString(wsChargingPayload);
            this.simpMessagingTemplate.convertAndSend(
                    "/vehicle-charger/" + vehicleChargerRecord.getVehicleChargerId(), wsChargingPayloadJson);

            changeChargingSocketState(vehicleChargerRecord.getVehicleChargerId(),
                    vehicleChargerRecord.getVehicleChargerSocketName(), false);
        }
    }

    private VehicleCharger changeChargingSocketState(Integer vehicleChargerId, String chargeSocketName,
                                                     Boolean activeStatus) {
        VehicleCharger vehicleCharger = findById(vehicleChargerId);
        for (VehicleChargerSocket vehicleChargerSocket: vehicleCharger.getVehicleChargerSockets())
            if (vehicleChargerSocket.getName().equals(chargeSocketName)) {
                vehicleChargerSocket.setActive(activeStatus);
                vehicleChargerSocketService.save(vehicleChargerSocket);
                break;
            }
        save(vehicleCharger);
        return vehicleCharger;
    }

    private void sendCurrentBatteryPercentage(CurrentBatteryPercentageDTO currentBatteryPercentageDTO) throws
            JsonProcessingException {
        WebSocketMessageDTO wsChargingPayload =
                new WebSocketMessageDTO("current-battery-percentage", currentBatteryPercentageDTO);
        String wsChargingPayloadJson = jsonMapper.writeValueAsString(wsChargingPayload);
        this.simpMessagingTemplate.convertAndSend(
                "/vehicle-charger/" + currentBatteryPercentageDTO.getVehicleChargerId(), wsChargingPayloadJson);
    }

    public List<VehicleChargerRecord> findRecordsByDateRange(String from,
                                                             String to,
                                                             Integer vehicleChargerId,
                                                             String token) {
        accessControl.validateDeviceAccess(token, findById(vehicleChargerId));
        return vehicleChargerRepositoryInflux.findRecordsByDateRange(from, to, vehicleChargerId);
    }
}
