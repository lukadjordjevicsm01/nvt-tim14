package com.threeamigoscoding.smartifybackend.service.device.outside;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.WebSocketMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.LampDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.LampLightSwitchDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.influx_records.MeasurementRecord;
import com.threeamigoscoding.smartifybackend.influx_records.outside.LampBrightnessRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.outside.LampMapper;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.SolarPanelSystem;
import com.threeamigoscoding.smartifybackend.model.device.home.AmbientSensor;
import com.threeamigoscoding.smartifybackend.model.device.outside.Lamp;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.outside.LampRepository;
import com.threeamigoscoding.smartifybackend.repository.device.outside.LampRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Service
public class LampService {

    public LampRepository lampRepository;
    private final RealEstateService realEstateService;
    private final LampMapper lampMapper;
    private final IMqttClient lampMqttClient;
    private final ObjectMapper jsonMapper;
    private final LampRepositoryInflux lampRepositoryInflux;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final PingService pingService;
    private final AccessControl accessControl;


    @Autowired
    public LampService(LampRepository lampRepository,
                       RealEstateService realEstateService,
                       LampMapper lampMapper,
                       @Qualifier("lampMqttClient") IMqttClient lampMqttClient,
                       ObjectMapper jsonMapper,
                       LampRepositoryInflux lampRepositoryInflux,
                       SimpMessagingTemplate simpMessagingTemplate,
                       PingService pingService,
                       AccessControl accessControl){
        this.lampRepository = lampRepository;
        this.realEstateService = realEstateService;
        this.lampMapper = lampMapper;
        this.lampMqttClient = lampMqttClient;
        this.jsonMapper= jsonMapper;
        this.lampRepositoryInflux = lampRepositoryInflux;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();
    }

    public Lamp create(LampDTO lampDTO, Integer realEstateId, String token) {
        Lamp lamp = lampMapper.dtoToEntity(lampDTO);
        RealEstate realEstate = realEstateService.findById(realEstateId);

        accessControl.validateRealEstateOwner(token, realEstate);

        lamp.setRealEstate(realEstate);

        lampRepository.save(lamp);

//        realEstate.getDevices().add(lamp);
//        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, lamp.getId());
        addToSimulator(lamp);

        return lamp;
    }

    private void addToSimulator(Lamp lamp) {
        String url = MyCredentials.simulatorServer + "add-lamp";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<Lamp> requestEntity = new HttpEntity<>(lamp, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            lampMqttClient.subscribe("lamp/" + lamp.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public void save(Lamp lamp) {
        lampRepository.save(lamp);
    }

    public List<Lamp> getAll() {
        return lampRepository.findAll();
    }

    public void subscribeToTopics(List<Lamp> lamps) {
        lamps.forEach(lamp -> {
            try {
                lampMqttClient.subscribe("lamp/" + lamp.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void setMqttCallback() {
        lampMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                MqttMessageDto mqttMessageDto =
                        jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                    setLastPing(Integer.parseInt(topic.split("/")[1]));
                else if (Objects.equals(mqttMessageDto.getCommand(), "record-brightness"))
                    recordBrightness(Integer.parseInt(topic.split("/")[1]), mqttMessageDto.getPayload());
                else if (Objects.equals(mqttMessageDto.getCommand(), "lamp-light-switch")){
                    LampLightSwitchDTO lampLightSwitchDTO
                            = jsonMapper.readValue(mqttMessageDto.getPayload(), LampLightSwitchDTO.class);
                    switchLight(Integer.parseInt(topic.split("/")[1]), lampLightSwitchDTO.isLightOn(),
                            true, "none");
                }


            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    public void switchLight(Integer id, boolean on, boolean automatic, String token) throws JsonProcessingException, MqttException {
        Lamp lamp = getActualLamp(id);
        if (!token.equals("none")) accessControl.validateDeviceAccess(token, lamp);
        lamp.setLightOn(on);
        lampRepository.save(lamp);

        if (automatic){
            WebSocketMessageDTO payload = new WebSocketMessageDTO("switchLight",
                    new LampLightSwitchDTO(on));
            String payloadJson = jsonMapper.writeValueAsString(payload);
            this.simpMessagingTemplate.convertAndSend("/lamp/" + id , payloadJson);
            return;
        }

        MqttMessageDto payloadMQTT = new MqttMessageDto("switchLight", "{\"lightOn\": "+ on + "}");
        String payloadJson = jsonMapper.writeValueAsString(payloadMQTT);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        lampMqttClient.publish("lamp-simulator/" + lamp.getId().toString(),
                mqttMessage
        );
    }

    public void switchMode(Integer lampId, boolean automatic, String token) throws JsonProcessingException, MqttException {
        Lamp lamp = getActualLamp(lampId);
        accessControl.validateDeviceAccess(token, lamp);
        lamp.setAutomatic(automatic);
        lampRepository.save(lamp);
        MqttMessageDto payload = new MqttMessageDto("switchMode", "");
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        lampMqttClient.publish("lamp-simulator/" + lamp.getId().toString(),
                mqttMessage
        );
    }

    private void recordBrightness(Integer id, String payload) throws JsonProcessingException {
        LampBrightnessRecord lampBrightnessRecord = jsonMapper.readValue(payload, LampBrightnessRecord.class);
        Point brightness = Point.measurement("Brightness")
                .addField("brightness", lampBrightnessRecord.getBrightness())
                .addTag("lampId", id.toString());
        lampRepositoryInflux.save(brightness);
        //this.simpMessagingTemplate.convertAndSend("/lamp/" + id , new WebSocketMessageDTO("aaa", "bbb"));
    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }

    private Lamp getActualLamp(Integer lampId){
        return lampRepository.findById(lampId).orElseThrow(() -> new ResourceNotFoundException("Lamp does not exist."));
    }

    public LampDTO getLamp(Integer lampId, String token) {
        Lamp lamp = getActualLamp(lampId);
        accessControl.validateDeviceAccess(token, lamp);
        return lampMapper.entityToDto(lamp);
    }


    public List<MeasurementRecord> findByDateRange(String from,
                                                   String to,
                                                   String measurement,
                                                   String id,
                                                   String aggregateWindow,
                                                   String token) {
        accessControl.validateDeviceAccess(token, getActualLamp(Integer.valueOf(id)));
        return lampRepositoryInflux.findByDateRange(from, to, measurement, id, aggregateWindow);
    }
}
