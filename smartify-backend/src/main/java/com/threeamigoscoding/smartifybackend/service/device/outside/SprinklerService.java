package com.threeamigoscoding.smartifybackend.service.device.outside;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.WebSocketMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerSettingDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerSettingInDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerSwitchDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.influx_records.outside.SprinklerEventRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.outside.SprinklerMapper;
import com.threeamigoscoding.smartifybackend.model.device.outside.Gate;
import com.threeamigoscoding.smartifybackend.model.device.outside.Sprinkler;
import com.threeamigoscoding.smartifybackend.model.device.outside.SprinklerSetting;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.outside.SprinklerRepository;
import com.threeamigoscoding.smartifybackend.repository.device.outside.SprinklerRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.InvalidOperationException;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SprinklerService {

    public SprinklerRepository sprinklerRepository;

    private RealEstateService realEstateService;

    private SprinklerMapper sprinklerMapper;

    private final IMqttClient sprinklerMqttClient;

    private final ObjectMapper jsonMapper;

    private SprinklerSettingService sprinklerSettingService;

    private SimpMessagingTemplate simpMessagingTemplate;

    private SprinklerRepositoryInflux sprinklerRepositoryInflux;

    private final PingService pingService;

    private final AccessControl accessControl;


    @Autowired
    public SprinklerService(SprinklerRepository sprinklerRepository,
                            RealEstateService realEstateService,
                            SprinklerMapper sprinklerMapper,
                            @Qualifier("sprinklerMqttClient") IMqttClient sprinklerMqttClient,
                            ObjectMapper jsonMapper,
                            SprinklerSettingService sprinklerSettingService,
                            SimpMessagingTemplate simpMessagingTemplate,
                            SprinklerRepositoryInflux sprinklerRepositoryInflux,
                            PingService pingService,
                            AccessControl accessControl){

        this.sprinklerRepository = sprinklerRepository;
        this.realEstateService = realEstateService;
        this.sprinklerMapper = sprinklerMapper;
        this.sprinklerMqttClient = sprinklerMqttClient;
        this.jsonMapper = jsonMapper;
        this.sprinklerSettingService = sprinklerSettingService;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.sprinklerRepositoryInflux = sprinklerRepositoryInflux;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();
    }

    public Sprinkler create(SprinklerDTO sprinklerDTO, Integer realEstateId, String token) {
        Sprinkler sprinkler = sprinklerMapper.dtoToEntity(sprinklerDTO);
        RealEstate realEstate = realEstateService.findById(realEstateId);

        accessControl.validateRealEstateOwner(token, realEstate);

        sprinkler.setRealEstate(realEstate);
        sprinkler.setSprinklerSettings(new HashSet<>());

        sprinklerRepository.save(sprinkler);

//        realEstate.getDevices().add(sprinkler);
//        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, sprinkler.getId());
        addToSimulator(sprinkler);

        return sprinkler;
    }

    private void addToSimulator(Sprinkler sprinkler) {
        String url = MyCredentials.simulatorServer + "add-sprinkler";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<Sprinkler> requestEntity = new HttpEntity<>(sprinkler, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            sprinklerMqttClient.subscribe("sprinkler/" + sprinkler.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public List<Sprinkler> getAll() {
        return sprinklerRepository.findAll();
    }

    public void subscribeToTopics(List<Sprinkler> sprinklers) {
        sprinklers.forEach(ambientSensor -> {
            try {
                sprinklerMqttClient.subscribe("sprinkler/" + ambientSensor.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void setMqttCallback() {
        sprinklerMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) {
                try{
                    MqttMessageDto mqttMessageDto =
                            jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                    if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                        setLastPing(Integer.parseInt(topic.split("/")[1]));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "switch-sprinkler")){
                        SprinklerSwitchDTO sprinklerSwitchDTO
                                = jsonMapper.readValue(mqttMessageDto.getPayload(), SprinklerSwitchDTO.class);
                        changeSprinklerState(Integer.parseInt(topic.split("/")[1]),
                                sprinklerSwitchDTO.isSprinklerOn(), true, "none");
                        registerSprinklerEvent(Integer.parseInt(topic.split("/")[1]),
                                "Sprinkler turned " + (sprinklerSwitchDTO.isSprinklerOn() ? "on" : "off"),
                                "System", "");
                    }


                } catch (Exception e){
                    System.out.println(e.getMessage());
                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }

    private Sprinkler getActualSprinkler(Integer sprinklerId){
        return sprinklerRepository.findById(sprinklerId).orElseThrow(() ->
                new ResourceNotFoundException("Sprinkler does not exist."));
    }

    public SprinklerDTO getSprinkler(Integer sprinklerId, String token){
        Sprinkler sprinkler = getActualSprinkler(sprinklerId);
        accessControl.validateDeviceAccess(token, sprinkler);
        return sprinklerMapper.entityToDto(sprinkler);
    }

    public void changeSprinklerState(Integer sprinklerId, boolean on, boolean automatic, String token)
            throws JsonProcessingException, MqttException {
        Sprinkler sprinkler = getActualSprinkler(sprinklerId);
        if (!token.equals("none")) accessControl.validateDeviceAccess(token, sprinkler);
        sprinkler.setSprinklerOn(on);
        sprinklerRepository.save(sprinkler);

        if (automatic){
            WebSocketMessageDTO payload = new WebSocketMessageDTO("switchSprinkler",
                    new SprinklerSwitchDTO(on));
            String payloadJson = jsonMapper.writeValueAsString(payload);
            this.simpMessagingTemplate.convertAndSend("/sprinkler/" + sprinklerId , payloadJson);
        }

        MqttMessageDto payloadMQTT = new MqttMessageDto("switchSprinkler", "{\"sprinklerOn\": "+ on + "}");
        String payloadJson = jsonMapper.writeValueAsString(payloadMQTT);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        sprinklerMqttClient.publish("sprinkler-simulator/" + sprinkler.getId().toString(),
                mqttMessage
        );

    }

    public void registerSprinklerEvent(Integer sprinklerId, String action, String subject, String setting){
        Point sprinkleEvent = Point.measurement("sprinkler action")
                .addField("action", action)
                .addTag("sprinklerId", sprinklerId.toString())
                .addTag("subject", subject)
                .addTag("setting", setting);
        sprinklerRepositoryInflux.save(sprinkleEvent);
        //mozda socket
    }

    public SprinklerSettingDTO addSetting(Integer sprinklerId, SprinklerSettingInDTO sprinklerSettingDTO, String token)
            throws JsonProcessingException, MqttException {
        Sprinkler sprinkler = getActualSprinkler(sprinklerId);

        accessControl.validateDeviceAccess(token, sprinkler);

        checkForOverlap(sprinkler.getSprinklerSettings(), sprinklerSettingDTO);

        SprinklerSetting sprinklerSetting = new SprinklerSetting();
        sprinklerSetting.setStartTime(new Timestamp(sprinklerSettingDTO.getStartTime()));
        sprinklerSetting.setEndTime(new Timestamp(sprinklerSettingDTO.getEndTime()));
        sprinklerSetting.setIsRepeating(true);
        sprinklerSetting.setRepeatDays(sprinklerSettingDTO.getRepeatDays());

        sprinklerSettingService.save(sprinklerSetting);

        sprinkler.getSprinklerSettings().add(sprinklerSetting);
        sprinklerRepository.save(sprinkler);

        String settingPayload = jsonMapper.writeValueAsString(sprinklerSetting);
        MqttMessageDto payload = new MqttMessageDto("setting", settingPayload);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());

        sprinklerMqttClient.publish("sprinkler-simulator/" + sprinklerId, mqttMessage);

        String action = "New setting added";
        String setting = sprinklerSettingService.settingToString(sprinklerSetting);
        registerSprinklerEvent(sprinklerId, action, sprinklerSettingDTO.getAddedBy(), setting);

        return sprinklerMapper.sprinklerSettingEntityToDto(sprinklerSetting);

    }

    public void updateSetting(Integer sprinklerId, Integer settingId, SprinklerSettingInDTO sprinklerSettingDTO,
                              String token)
            throws JsonProcessingException, MqttException {

        Sprinkler sprinkler = getActualSprinkler(sprinklerId);
        accessControl.validateDeviceAccess(token, sprinkler);

        Set<SprinklerSetting> settingsToCheckOverlap = sprinkler.getSprinklerSettings().stream().filter(setting ->
                !setting.getId().equals(settingId)).collect(Collectors.toSet());

        SprinklerSetting sprinklerSetting = sprinklerSettingService.getSprinklerSetting(settingId);

        checkForOverlap(settingsToCheckOverlap, sprinklerSettingDTO);

        sprinklerSetting.setStartTime(new Timestamp(sprinklerSettingDTO.getStartTime()));
        sprinklerSetting.setEndTime(new Timestamp(sprinklerSettingDTO.getEndTime()));
        sprinklerSetting.setRepeatDays(sprinklerSettingDTO.getRepeatDays());

        sprinklerSettingService.save(sprinklerSetting);

        String settingPayload = jsonMapper.writeValueAsString(sprinklerSetting);
        MqttMessageDto payload = new MqttMessageDto("settingUpdate", settingPayload);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        sprinklerMqttClient.publish("sprinkler-simulator/" + sprinklerId, mqttMessage);

        String action = "Setting modified";
        String setting = sprinklerSettingService.settingToString(sprinklerSetting);
        registerSprinklerEvent(sprinklerId, action, sprinklerSettingDTO.getAddedBy(), setting);

    }

    public void deleteSetting(Integer sprinklerId, Integer settingId, String email, String token)
            throws JsonProcessingException, MqttException {

        accessControl.validateDeviceOwner(token, getActualSprinkler(sprinklerId));
        SprinklerSetting sprinklerSetting = sprinklerSettingService.getSprinklerSetting(settingId);
        String setting = sprinklerSettingService.settingToString(sprinklerSetting);

        sprinklerSettingService.deleteById(settingId);

        MqttMessageDto payload = new MqttMessageDto("settingDelete", "{\"id\": "+ settingId + "}");
        String payloadJson = jsonMapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        sprinklerMqttClient.publish("sprinkler-simulator/" + sprinklerId, mqttMessage);

        String action = "Setting removed";
        registerSprinklerEvent(sprinklerId, action, email, setting);
    }

    public List<SprinklerEventRecord> findEventsByDateRange(String from, String to, Integer sprinklerId, String token){
        accessControl.validateDeviceAccess(token, getActualSprinkler(sprinklerId));
        return sprinklerRepositoryInflux.findEventsByDateRange(from, to, sprinklerId);
    }

    private static boolean timeRangeInvalid(Timestamp start1, Timestamp end1, Timestamp start2, Timestamp end2) {
        // Extract time components from Timestamps
        Calendar cal1Start = Calendar.getInstance();
        cal1Start.setTimeInMillis(start1.getTime());
        int hour1Start = cal1Start.get(Calendar.HOUR_OF_DAY);
        int minute1Start = cal1Start.get(Calendar.MINUTE);

        Calendar cal1End = Calendar.getInstance();
        cal1End.setTimeInMillis(end1.getTime());
        int hour1End = cal1End.get(Calendar.HOUR_OF_DAY);
        int minute1End = cal1End.get(Calendar.MINUTE);

        Calendar cal2Start = Calendar.getInstance();
        cal2Start.setTimeInMillis(start2.getTime());
        int hour2Start = cal2Start.get(Calendar.HOUR_OF_DAY);
        int minute2Start = cal2Start.get(Calendar.MINUTE);

        Calendar cal2End = Calendar.getInstance();
        cal2End.setTimeInMillis(end2.getTime());
        int hour2End = cal2End.get(Calendar.HOUR_OF_DAY);
        int minute2End = cal2End.get(Calendar.MINUTE);

        // Check if the time ranges overlap or one is inside the other
        return (hour1Start < hour2End || (hour1Start == hour2End && (minute1Start < minute2End)))
                &&
                (hour1End > hour2Start || (hour1End == hour2Start && (minute1End > minute2Start)));
    }

    private static void checkForOverlap(Set<SprinklerSetting> sprinklerSettings, SprinklerSettingInDTO sprinklerSettingDTO){
        for (SprinklerSetting sprinklerSetting : sprinklerSettings){
            for (DayOfWeek dayOfWeek : sprinklerSetting.getRepeatDays()){
                if (sprinklerSettingDTO.getRepeatDays().contains(dayOfWeek) &&
                        timeRangeInvalid(new Timestamp(sprinklerSettingDTO.getStartTime()),
                                new Timestamp(sprinklerSettingDTO.getEndTime()),
                                sprinklerSetting.getStartTime(),
                                sprinklerSetting.getEndTime())){
                    throw new InvalidOperationException("Setting overlaps with existing setting.");

                }
            }
        }
    }

    public void save(Sprinkler sprinkler) {
        sprinklerRepository.save(sprinkler);
    }
}
