package com.threeamigoscoding.smartifybackend.service.realestate;

import com.threeamigoscoding.smartifybackend.model.realestate.RejectionLetter;
import com.threeamigoscoding.smartifybackend.repository.realestate.RejectionLetterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RejectionLetterService {
    @Autowired
    private RejectionLetterRepository rejectionLetterRepository;

    public void save(RejectionLetter rejectionLetter){
        rejectionLetterRepository.save(rejectionLetter);
    }
}
