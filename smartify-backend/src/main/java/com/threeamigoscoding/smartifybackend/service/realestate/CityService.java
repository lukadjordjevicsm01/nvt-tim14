package com.threeamigoscoding.smartifybackend.service.realestate;

import com.threeamigoscoding.smartifybackend.dto.realestate.CityDTO;
import com.threeamigoscoding.smartifybackend.mappers.realestate.AddressMapper;
import com.threeamigoscoding.smartifybackend.model.realestate.City;
import com.threeamigoscoding.smartifybackend.repository.realestate.CityRepository;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private AddressMapper addressMapper;

    public City findById(Integer id){
        return cityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("City with id: " + id +
                "does not exist!"));
    }

    public List<CityDTO> findByCountry(String countryCode){
        return cityRepository.findCitiesByCountry_CountryCode(countryCode).stream()
                .map(city -> addressMapper.cityToCityDTO(city)).toList();
    }

    public List<CityDTO> findAll(){
        return cityRepository.findAll().stream().map(city -> addressMapper.cityToCityDTO(city)).toList();
    }

}
