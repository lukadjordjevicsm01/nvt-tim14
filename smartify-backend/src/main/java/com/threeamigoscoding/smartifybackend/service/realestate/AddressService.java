package com.threeamigoscoding.smartifybackend.service.realestate;

import com.threeamigoscoding.smartifybackend.model.realestate.Address;
import com.threeamigoscoding.smartifybackend.repository.realestate.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    public void save(Address address){
        addressRepository.save(address);
    }

}
