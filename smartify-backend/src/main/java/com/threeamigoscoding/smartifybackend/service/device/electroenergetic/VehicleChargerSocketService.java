package com.threeamigoscoding.smartifybackend.service.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.VehicleChargerSocket;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.VehicleChargerSocketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleChargerSocketService {

    @Autowired
    public VehicleChargerSocketRepository vehicleChargerSocketRepository;

    public void save(VehicleChargerSocket vehicleChargerSocket) {
        vehicleChargerSocketRepository.save(vehicleChargerSocket);
    }
}
