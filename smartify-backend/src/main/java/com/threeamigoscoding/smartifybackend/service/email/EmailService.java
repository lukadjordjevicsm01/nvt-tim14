package com.threeamigoscoding.smartifybackend.service.email;

import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class EmailService {

    @Autowired
    private TemplateEngine templateEngine;

    public void sendConfirmationEmail(String to, Integer activationId) {
        Email from = new Email("threeamigoscoding@gmail.com");
        String subject = "Account activation";
        String body = "Thank you for registering to our website. Please activate your " +
                "account by clicking on the button below.";
        Email toEmail = new Email(to);

        String htmlContent = prepareHtmlContent(subject, body, true,
                "http://localhost:8080/api/user/activate/" + activationId, "Activate");

        Content content = new Content("text/html", htmlContent);

        Mail mail = new Mail(from, subject, toEmail, content);

        sendEmail(mail);
    }

    public void sendSimpleEmail(String to, String subject, String htmlContent) {
        Email from = new Email("threeamigoscoding@gmail.com");
        String mailSubject = subject;
        Email toEmail = new Email(to);

        Content content = new Content("text/html", htmlContent);
        Mail mail = new Mail(from, mailSubject, toEmail, content);

        sendEmail(mail);
    }

    private void sendEmail(Mail mail) {
        SendGrid sg = new SendGrid(MyCredentials.sendGridKey);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String prepareHtmlContent(String subject, String body, boolean showButton, String url, String btnText){
        Context thymeleafContext = new Context();
        thymeleafContext.setVariable("subject", subject);
        thymeleafContext.setVariable("body", body);
        thymeleafContext.setVariable("showButton", showButton);
        thymeleafContext.setVariable("url", url);
        thymeleafContext.setVariable("btnText", btnText);
        return templateEngine.process("simple-template", thymeleafContext);
    }

}