package com.threeamigoscoding.smartifybackend.service.device;

import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.model.device.Device;
import com.threeamigoscoding.smartifybackend.repository.device.DeviceRepository;
import com.threeamigoscoding.smartifybackend.repository.device.DeviceRepositoryInflux;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class PingService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceRepositoryInflux deviceRepositoryInflux;


    public void setLastPing(Integer id){
        Device device = deviceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Device does not exist."));

        device.setLastPing(new Timestamp(System.currentTimeMillis()));
        device.setOnline(true);
        deviceRepository.save(device);
    }



}
