package com.threeamigoscoding.smartifybackend.service.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioningSetting;
import com.threeamigoscoding.smartifybackend.repository.device.home.AirConditioningSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AirConditioningSettingService {

    @Autowired
    public AirConditioningSettingRepository airConditioningSettingRepository;

    public AirConditioningSetting save(AirConditioningSetting airConditioningSetting) {
        return airConditioningSettingRepository.save(airConditioningSetting);
    }
}
