package com.threeamigoscoding.smartifybackend.service.realestate;

import com.threeamigoscoding.smartifybackend.dto.realestate.RealEstateDevicesDTO;
import com.threeamigoscoding.smartifybackend.dto.realestate.RealEstateOutDTO;
import com.threeamigoscoding.smartifybackend.dto.realestate.RealEstateRegistrationDTO;
import com.threeamigoscoding.smartifybackend.dto.realestate.StatusChangeDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserOutDTO;
import com.threeamigoscoding.smartifybackend.mappers.realestate.AddressMapper;
import com.threeamigoscoding.smartifybackend.mappers.realestate.RealEstateOutMapper;
import com.threeamigoscoding.smartifybackend.mappers.realestate.RealEstateRegistrationMapper;
import com.threeamigoscoding.smartifybackend.mappers.user.UserMapper;
import com.threeamigoscoding.smartifybackend.model.device.Device;
import com.threeamigoscoding.smartifybackend.model.realestate.*;
import com.threeamigoscoding.smartifybackend.model.user.Role;
import com.threeamigoscoding.smartifybackend.model.user.User;
import com.threeamigoscoding.smartifybackend.repository.realestate.RealEstateRepository;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.DeviceService;
import com.threeamigoscoding.smartifybackend.service.email.EmailService;
import com.threeamigoscoding.smartifybackend.service.user.UserService;
import com.threeamigoscoding.smartifybackend.util.exceptions.InvalidOperationException;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Objects;

@Service
public class RealEstateService {

    @Autowired
    private RealEstateRepository realEstateRepository;

    @Autowired
    private CityService cityService;

    @Autowired
    private RejectionLetterService rejectionLetterService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private UserService userService;

    @Autowired
    private RealEstateRegistrationMapper realEstateRegistrationMapper;

    @Autowired
    private RealEstateOutMapper realEstateOutMapper;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AccessControl accessControl;

    public RealEstate registerRealEstate(RealEstateRegistrationDTO realEstateRegistrationDTO) throws IOException {
        System.out.println(realEstateRegistrationDTO.getRealEstateType());
        User user = userService.findById(realEstateRegistrationDTO.getUserId());
        City city = cityService.findById(realEstateRegistrationDTO.getAddress().getCityId());
        RealEstate realEstate = realEstateRegistrationMapper.dtoToEntity(realEstateRegistrationDTO);
        realEstate.getAddress().setCity(city);
        addressService.save(realEstate.getAddress());

        String fileName = realEstateRegistrationDTO.getRealEstatePicture().getOriginalFilename();
        assert fileName != null;
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        String imageName =  user.getEmail() + "_" + System.currentTimeMillis() + "." + extension;
        String filePath =
                "src/main/resources/images/realestate/" + imageName;
        Files.write(Path.of(filePath), realEstateRegistrationDTO.getRealEstatePicture().getBytes());

        realEstate.setPicturePath(imageName);
        realEstate.setStatus(RealEstateStatus.PENDING);
        realEstateRepository.save(realEstate);

        user.getRealEstates().add(realEstate);
        userService.save(user);

        return realEstate;
    }

    public void changeRealEstateStatus(StatusChangeDTO statusChangeDTO){
        RealEstate realEstate = realEstateRepository.findById(statusChangeDTO.getRealEstateId()).orElseThrow(
                ResourceNotFoundException::new
        );
        if (realEstate.getStatus() != RealEstateStatus.PENDING) throw new InvalidOperationException
                ("Cannot change real estate status if the current status is not pending!");
        realEstate.setStatus(statusChangeDTO.getStatus());
        realEstateRepository.save(realEstate);

        String to = realEstate.getOwner().getEmail();
        String subject = "Real estate " + (statusChangeDTO.getStatus() ==
                RealEstateStatus.ACCEPTED ? "accepted" : "rejected");
        String body = "Real estate " + realEstate.getName() + " at " + realEstate.getAddress().getStreetName() + " was "
                +  (statusChangeDTO.getStatus() == RealEstateStatus.ACCEPTED ? "accepted" : "rejected") + ". \n" +
                (statusChangeDTO.getStatus() == RealEstateStatus.ACCEPTED ? "" : "Reason: " + statusChangeDTO.getReason());

        String htmlContent = emailService.prepareHtmlContent(subject, body, false, "", "");

        emailService.sendSimpleEmail(to, subject, htmlContent);

        if (statusChangeDTO.getStatus() != RealEstateStatus.REJECTED) return;

        RejectionLetter rejectionLetter = new RejectionLetter(realEstate, statusChangeDTO.getReason());
        rejectionLetterService.save(rejectionLetter);

    }

    public RealEstate findById(Integer id) {
        return realEstateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Real Estate with id: "
                + id + "does not exist!"));
    }

    public void save(RealEstate realEstate) {
        realEstateRepository.save(realEstate);
    }


    public List<RealEstateOutDTO> findAllByOwner(String token, Integer ownerId) {
        accessControl.validateUserId(token, ownerId);
        return realEstateRepository.findAllByOwnerId(ownerId).stream().map(this::mapToOutDto).toList();
    }

    public List<RealEstateOutDTO> findAllByStatus(RealEstateStatus status){
        return realEstateRepository.findAllByStatus(status).stream().map(this::mapToOutDto).toList();
    }

    private RealEstateOutDTO mapToOutDto(RealEstate realEstate){
        RealEstateOutDTO realEstateOutDTO = realEstateOutMapper.entityToDTO(realEstate);

        realEstateOutDTO.getAddress().setCityDTO(addressMapper.cityToCityDTO(realEstate.getAddress().getCity()));
        realEstateOutDTO.getAddress().setCountryDTO
                (addressMapper.countryToCountryDTO(realEstate.getAddress().getCity().getCountry()));
        realEstateOutDTO.setUserDTO(userMapper.entityToDto(realEstate.getOwner()));
        return realEstateOutDTO;
    }

    public List<RealEstate> getAll() {
        return realEstateRepository.findAll();
    }

    public void addToRealEstatesSimulation(RealEstate realEstate, Integer deviceId) {
        String url = "http://localhost:5000/add-device-real-estate";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        Set<Integer> devicesIds = new HashSet<>();
        devicesIds.add(deviceId);

        RealEstateDevicesDTO realEstateDevicesDTO = new RealEstateDevicesDTO();
        realEstateDevicesDTO.setRealEstateId(realEstate.getId());
        realEstateDevicesDTO.setCity(addressMapper.cityToCityDTO(realEstate.getAddress().getCity()));
        realEstateDevicesDTO.setDevicesIds(devicesIds);

        HttpEntity<RealEstateDevicesDTO> requestEntity = new HttpEntity<>(realEstateDevicesDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                url,
                requestEntity,
                String.class);
        System.out.println("Response: " + responseEntity.getBody());
    }
        
    public List<UserOutDTO> getSharedWith(String token, Integer id) {
        RealEstate realEstate = findById(id);
        accessControl.validateRealEstateOwner(token, realEstate);
        return realEstate.getSharedWith().stream().map(u -> userMapper.entityToOutDto(u)).toList();
    }

    public UserOutDTO grantPermission(String token, Integer realEstateId, String userEmail) {
        RealEstate realEstate = findById(realEstateId);
        accessControl.validateRealEstateOwner(token, realEstate);
        User user = userService.findByEmail(userEmail);
        if (user == null)
            throw new ResourceNotFoundException("User with given email does not exist.");
        for (Role role : user.getRoles())
            if (Objects.equals(role.getName(), "ROLE_ADMIN"))
                throw new InvalidOperationException("User is an admin.");
        if (user.equals(realEstate.getOwner()))
            throw new InvalidOperationException("User is the owner of the real estate.");
        if (user.getSharedRealEstates().contains(realEstate))
            throw new InvalidOperationException("User already has permissions for this real estate.");

        user.getSharedRealEstates().add(realEstate);
        realEstate.getDevices().forEach(device -> {
            user.getSharedDevices().add(device);
        });

        userService.save(user);
        return userMapper.entityToOutDto(user);
    }

    public UserOutDTO revokePermission(String token, Integer realEstateId, String userEmail) {
        RealEstate realEstate = findById(realEstateId);
        accessControl.validateRealEstateOwner(token, realEstate);
        User user = userService.findByEmail(userEmail);
        if (user == null)
            throw new ResourceNotFoundException("User with given email does not exist.");
        for (Role role : user.getRoles())
            if (Objects.equals(role.getName(), "ROLE_ADMIN"))
                throw new InvalidOperationException("User is an admin.");
        if (user.equals(realEstate.getOwner()))
            throw new InvalidOperationException("User is the owner of the real estate.");
        if (!user.getSharedRealEstates().contains(realEstate))
            throw new InvalidOperationException("User does not have permissions for this real estate.");

        user.getSharedRealEstates().add(realEstate);
        realEstate.getDevices().forEach(device -> {
            user.getSharedDevices().remove(device);
        });
        user.getSharedRealEstates().remove(realEstate);

        userService.save(user);
        return userMapper.entityToOutDto(user);
    }

    public RealEstate findRealEstateByDevice(Integer deviceId) {
        return realEstateRepository.findRealEstateByDevice(deviceId);
    }

    public List<RealEstateOutDTO> getSharedRealEstates(String token, Integer userId) {
        User user = userService.findById(userId);
        accessControl.validateUserId(token, userId);
        return user
                .getSharedRealEstates()
                .stream()
                .map(this::mapToOutDto)
                .toList();
    }

    public List<RealEstateOutDTO> getSharedDeviceRealEstates(String token, Integer userId) {
        List<RealEstateOutDTO> realEstates = new ArrayList<>();
        User user = userService.findById(userId);
        accessControl.validateUserId(token, userId);
        user.getSharedDevices()
                .forEach((device) -> {
                    RealEstate realEstate = device.getRealEstate();
                    if (!realEstate.getSharedWith().contains(user) &&
                            realEstates.stream().noneMatch(dto -> (Objects.equals(dto.getId(), realEstate.getId()))))
                        realEstates.add(mapToOutDto(realEstate));
                });
        return realEstates;
    }

}
