package com.threeamigoscoding.smartifybackend.service.user;

import com.threeamigoscoding.smartifybackend.dto.realestate.RealEstateOutDTO;
import com.threeamigoscoding.smartifybackend.dto.user.PasswordChangeDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserTokenState;
import com.threeamigoscoding.smartifybackend.mappers.user.UserMapper;
import com.threeamigoscoding.smartifybackend.model.user.Role;
import com.threeamigoscoding.smartifybackend.model.user.User;
import com.threeamigoscoding.smartifybackend.model.user.UserActivation;
import com.threeamigoscoding.smartifybackend.repository.user.RoleRepository;
import com.threeamigoscoding.smartifybackend.repository.user.UserRepository;
import com.threeamigoscoding.smartifybackend.service.email.EmailService;
import com.threeamigoscoding.smartifybackend.util.TokenUtils;
import com.threeamigoscoding.smartifybackend.util.exceptions.AlreadyExistisException;
import com.threeamigoscoding.smartifybackend.util.exceptions.InvalidOperationException;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserActivationService userActivationService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private EmailService emailService;


    public User findById(Integer id){
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User with id: " + id + "does not exist!"));
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    public void save(User user){
        userRepository.save(user);
    }

    public void createSuperAdmin() {
        if (userRepository.findByEmail("admin") != null)
            return;

        User admin = new User();
        admin.setName("Super");
        admin.setSurname("Admin");
        admin.setEmail("admin");
        admin.setVerified(true);
        admin.setProfilePicturePath("src/main/resources/images/admin/admin.svg");
        admin.setPassword(generateSuperadminPassword());
        admin.setLastPasswordResetDate(new Timestamp(0));
        System.out.println(admin.getLastPasswordResetDate());

        Role adminRole = roleRepository.findByName("ROLE_ADMIN");
        Role superadminRole = roleRepository.findByName("ROLE_SUPERADMIN");
        admin.setRoles(List.of(adminRole, superadminRole));

        userRepository.save(admin);
    }

    private String generateSuperadminPassword() {
        String password = RandomStringUtils.randomAlphanumeric(16);
        // emailService.sendSimpleEmail("miloscuturic8@gmail.com", "Superadmin credentials",
        //        "Username: admin" + "\n" +
        //              "Password: " + password);
        System.out.println("Superadmin username: admin");
        System.out.println("Superadmin password: " + password);
        saveCredentialsToFile(password);
        return passwordEncoder.encode(password);
    }

    private void saveCredentialsToFile(String password) {
        File file = new File("src/main/resources/superadmin_credentials.txt");

        try (FileWriter writer = new FileWriter(file)) {
            writer.write("Username: admin" + "\n");
            writer.write("Password: " + password + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public UserDTO createAdmin(UserDTO userDTO) throws IOException {
        User validation = userRepository.findByEmail(userDTO.getEmail());
        if (validation != null)
            throw new AlreadyExistisException("User with this email already exists.");

        User admin = userMapper.dtoToEntity(userDTO);
        List<Role> roles = new ArrayList<>(Collections.singletonList(roleRepository.findByName("ROLE_ADMIN")));
        admin.setRoles(roles);
        admin.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        String filePath = savePicture(userDTO);
        admin.setProfilePicturePath(filePath);
        admin.setLastPasswordResetDate(new Timestamp(0));
        admin.setVerified(true);

        userRepository.save(admin);

        // emailService.sendSimpleEmail(admin.getEmail(), "Your credentials",
        //      "Your password is: " + userDTO.getPassword());

        return userMapper.entityToDto(admin);
    }

    public UserTokenState login(UserDTO authenticationRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationRequest.getEmail(), authenticationRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        User user = (User) authentication.getPrincipal();
        if (!user.getVerified())
            throw new InvalidOperationException("Account is not verified");

        String jwt = tokenUtils.generateToken(user);
        int expiresIn = tokenUtils.getExpiredIn();

        return new UserTokenState(jwt, expiresIn);
    }

    public UserDTO createUserActivation(UserDTO userDTO) throws IOException {
        if (Objects.equals(userDTO.getEmail(), "admin"))
            throw new InvalidOperationException("Invalid username.");

        User validation = userRepository.findByEmail(userDTO.getEmail());
        if (validation != null && validation.getVerified())
            throw new AlreadyExistisException("User with this email already exists.");
        else if (validation != null && !validation.getVerified())
            throw new AlreadyExistisException("Check your email for verification.");

        User user = userMapper.dtoToEntity(userDTO);
        user.setVerified(false);

        List<Role> roles = new ArrayList<>(Collections.singletonList(roleRepository.findByName("ROLE_USER")));
        user.setRoles(roles);
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));

        UserActivation userActivation = new UserActivation(user, LocalDate.now(), LocalDate.now().plusDays(2));

        String filePath = savePicture(userDTO);
        user.setProfilePicturePath(filePath);

        userRepository.save(user);
        userActivationService.save(userActivation);
        emailService.sendConfirmationEmail(user.getEmail(), userActivation.getId());
        return userMapper.entityToDto(user);
    }

    public void activateUserAccount(Integer userActivationId) {
        UserActivation userActivation = userActivationService.findById(userActivationId);
        if (!userActivationService.isActivationValid(userActivation))
            throw new InvalidOperationException("Activation expired");

        User user = userActivation.getUser();
        user.setVerified(true);
        user.setLastPasswordResetDate(new Timestamp(System.currentTimeMillis()));
        userRepository.save(user);

        String content = emailService.prepareHtmlContent("Account activated",
                "Your account activation was successful!", true, "http://localhost:5173/login",
                "Log In");
        emailService.sendSimpleEmail(userActivation.getUser().getEmail(),
                "Account activated", content);

        userMapper.entityToDto(user);
    }

    public void changePassword(PasswordChangeDTO passwordChangeDTO) {
        User user = userRepository.findByEmail(passwordChangeDTO.getEmail());
        if (user == null)
            throw new ResourceNotFoundException("User does not exist.");
        if (passwordEncoder.matches(passwordChangeDTO.getNewPassword(), user.getPassword()))
            throw new InvalidOperationException("New password must be different then the old one.");
        user.setPassword(passwordEncoder.encode(passwordChangeDTO.getNewPassword()));
        user.setLastPasswordResetDate(new Timestamp(System.currentTimeMillis()));
        save(user);
    }

    public User findOwnerByDevice(Integer deviceId) {
        return userRepository.findOwnerByDevice(deviceId);
    }

    private String savePicture(UserDTO userDTO) throws IOException {
        String fileName = userDTO.getProfilePicture().getOriginalFilename();
        assert fileName != null;
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        String imageName = userDTO.getEmail() + "_" + System.currentTimeMillis() + "." + extension;
        String filePath = "src/main/resources/images/user/" + imageName;
        Files.write(Path.of(filePath), userDTO.getProfilePicture().getBytes());
        return imageName;
    }
}
