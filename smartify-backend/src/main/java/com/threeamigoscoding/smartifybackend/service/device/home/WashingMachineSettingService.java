package com.threeamigoscoding.smartifybackend.service.device.home;

import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachineSetting;
import com.threeamigoscoding.smartifybackend.repository.device.home.WashingMachineSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WashingMachineSettingService {

    @Autowired
    public WashingMachineSettingRepository washingMachineSettingRepository;

    public void save(WashingMachineSetting washingMachineSetting) {
        washingMachineSettingRepository.save(washingMachineSetting);
    }
}
