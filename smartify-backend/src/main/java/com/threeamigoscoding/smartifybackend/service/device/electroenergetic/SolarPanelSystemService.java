package com.threeamigoscoding.smartifybackend.service.device.electroenergetic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.SolarPanelSystemDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.SolarPanelSystemEventRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.SolarPanelSystemRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.electroenergetic.SolarPanelSystemMapper;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.SolarPanel;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.SolarPanelSystem;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.SolarPanelSystemRepository;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.SolarPanelSystemRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Service
public class SolarPanelSystemService {

    private final SolarPanelSystemRepository solarPanelSystemRepository ;
    private final RealEstateService realEstateService;
    private final SolarPanelService solarPanelService;
    private final SolarPanelSystemMapper solarPanelSystemMapper;
    private final IMqttClient solarPanelSystemMqttClient;
    private final ObjectMapper jsonMapper;
    private final SolarPanelSystemRepositoryInflux solarPanelSystemRepositoryInflux;
    private final PingService pingService;
    private final AccessControl accessControl;

    @Autowired
    public SolarPanelSystemService(SolarPanelSystemRepository solarPanelSystemRepository,
                                   RealEstateService realEstateService,
                                   SolarPanelService solarPanelService,
                                   SolarPanelSystemMapper solarPanelSystemMapper,
                                   @Qualifier("solarPanelSystemMqttClient") IMqttClient solarPanelSystemMqttClient,
                                   ObjectMapper jsonMapper,
                                   SolarPanelSystemRepositoryInflux solarPanelSystemRepositoryInflux,
                                   PingService pingService,
                                   AccessControl accessControl) {
        this.solarPanelSystemRepository = solarPanelSystemRepository;
        this.realEstateService = realEstateService;
        this.solarPanelService = solarPanelService;
        this.solarPanelSystemMapper = solarPanelSystemMapper;
        this.solarPanelSystemMqttClient = solarPanelSystemMqttClient;
        this.jsonMapper = jsonMapper;
        this.solarPanelSystemRepositoryInflux = solarPanelSystemRepositoryInflux;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();


    }

    public SolarPanelSystem create(SolarPanelSystemDTO solarPanelSystemDTO, Integer realEstateId, String token) {
        SolarPanelSystem solarPanelSystem = solarPanelSystemMapper.dtoToEntity(solarPanelSystemDTO);
        RealEstate realEstate = realEstateService.findById(realEstateId);

        accessControl.validateRealEstateOwner(token, realEstate);

        solarPanelSystem.setRealEstate(realEstate);
        Double solarPanelSystemConsumption = 0.0;
        for (SolarPanel solarPanel : solarPanelSystem.getSolarPanels()) {
            solarPanel.setImagePath("solarPanel.jpg");
            solarPanelService.save(solarPanel);
            realEstate.getDevices().add(solarPanel);
            solarPanelSystemConsumption += solarPanel.getConsumption();
        }
        solarPanelSystem.setConsumption(solarPanelSystemConsumption);

        solarPanelSystemRepository.save(solarPanelSystem);

//        realEstate.getDevices().add(solarPanelSystem);
//        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, solarPanelSystem.getId());
        addToSimulator(solarPanelSystem);

        return solarPanelSystem;
    }

    public void save(SolarPanelSystem solarPanelSystem) {
        solarPanelSystemRepository.save(solarPanelSystem);
    }

    public List<SolarPanelSystem> getAll() {
        return solarPanelSystemRepository.findAll();
    }

    public SolarPanelSystem findById(Integer id) {
        return solarPanelSystemRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Solar Panel System does not exist."));
    }

    public SolarPanelSystemDTO getSolarPanelSystem(Integer id, String token) {
        SolarPanelSystem solarPanelSystem = findById(id);
        accessControl.validateDeviceAccess(token, solarPanelSystem);
        return solarPanelSystemMapper.entityToDto(solarPanelSystem);
    }

    private void addToSimulator(SolarPanelSystem solarPanelSystem) {
        String url = MyCredentials.simulatorServer + "add-solar-panel-system";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<SolarPanelSystem> requestEntity = new HttpEntity<>(solarPanelSystem, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            solarPanelSystemMqttClient.subscribe("solar-panel-system/" + solarPanelSystem.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public void subscribeToTopics(List<SolarPanelSystem> solarPanelSystems) {
        solarPanelSystems.forEach(solarPanelSystem -> {
            try {
                solarPanelSystemMqttClient.subscribe("solar-panel-system/" + solarPanelSystem.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void setMqttCallback() {
        solarPanelSystemMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) {
                try{
                    MqttMessageDto mqttMessageDto =
                            jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                    if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                        setLastPing(Integer.parseInt(topic.split("/")[1]));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "record"))
                        recordData(mqttMessageDto.getPayload());
                    else if (Objects.equals(mqttMessageDto.getCommand(), "event"))
                        registerSolarPanelSystemEvent(jsonMapper
                                .readValue(mqttMessageDto.getPayload(), SolarPanelSystemEventRecord.class));

                } catch (Exception e){
                    System.out.println(e.getMessage());
                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    private void recordData(String payload) throws JsonProcessingException {
        SolarPanelSystemRecord solarPanelSystemRecord = jsonMapper.readValue(payload, SolarPanelSystemRecord.class);
        Point producedEnergy = Point.measurement("producedEnergy")
                .addField("producedEnergy", solarPanelSystemRecord.getProducedEnergy())
                .addTag("id", solarPanelSystemRecord.getId().toString());
        solarPanelSystemRepositoryInflux.save(producedEnergy);
    }

    private void registerSolarPanelSystemEvent(SolarPanelSystemEventRecord solarPanelSystemEventRecord) {
        Point point = Point.measurement("Solar Panel System Action")
                .addField("action", solarPanelSystemEventRecord.getAction())
                .addTag("value", solarPanelSystemEventRecord.getValue().toString())
                .addTag("solarPanelSystemId", solarPanelSystemEventRecord.getSolarPanelSystemId().toString())
                .addTag("subject", solarPanelSystemEventRecord.getSubject());
        solarPanelSystemRepositoryInflux.save(point);
    }

    public void saveSwitchRecord(Integer solarPanelSystemId, boolean isOn, String subject){
        String action = "Device turned off";
        if (isOn) {
            action = "Device turned on";
        }
        SolarPanelSystemEventRecord solarPanelSystemEventRecord = new SolarPanelSystemEventRecord(
                solarPanelSystemId, action, subject, -1.0, null
        );
        registerSolarPanelSystemEvent(solarPanelSystemEventRecord);
    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }

    public List<SolarPanelSystemEventRecord> findEventsByDateRange(String from,
                                                                   String to,
                                                                   Integer solarPanelSystemId,
                                                                   String token) {
        accessControl.validateDeviceAccess(token, findById(solarPanelSystemId));
        return solarPanelSystemRepositoryInflux.findEventsByDateRange(from, to, solarPanelSystemId);
    }
}
