package com.threeamigoscoding.smartifybackend.service.device;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.dto.device.DeviceImageDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.dto.user.UserDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserOutDTO;
import com.threeamigoscoding.smartifybackend.influx_records.DeviceAvailabilityRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.DeviceMapper;
import com.threeamigoscoding.smartifybackend.mappers.user.UserMapper;
import com.threeamigoscoding.smartifybackend.model.device.Device;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.model.user.Role;
import com.threeamigoscoding.smartifybackend.model.user.User;
import com.threeamigoscoding.smartifybackend.repository.device.DeviceRepository;
import com.threeamigoscoding.smartifybackend.repository.device.DeviceRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.electroenergetic.SolarPanelSystemService;
import com.threeamigoscoding.smartifybackend.service.device.home.AirConditioningService;
import com.threeamigoscoding.smartifybackend.service.device.home.WashingMachineService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.service.user.UserService;
import com.threeamigoscoding.smartifybackend.util.exceptions.InvalidOperationException;
import com.threeamigoscoding.smartifybackend.util.DevicePingChecker;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

@Service
public class DeviceService {

    @Autowired
    public DeviceRepository deviceRepository;
    @Autowired
    public DeviceMapper deviceMapper;
    @Autowired
    private ObjectMapper mapper;

    @Autowired
    @Qualifier("deviceClient")
    public IMqttClient deviceClient;

    @Autowired
    private SolarPanelSystemService solarPanelSystemService;

    @Autowired
    private AirConditioningService airConditioningService;

    @Autowired
    private WashingMachineService washingMachineService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private RealEstateService realEstateService;

    @Autowired
    private DeviceRepositoryInflux deviceRepositoryInflux;

    @Autowired
    private AccessControl accessControl;

    public List<Device> getAll() {
        return deviceRepository.findAll();
    }

    public void checkPings() {
        Timestamp thresholdTime = new Timestamp(System.currentTimeMillis() - (30 * 1000));
        //Timestamp thresholdTime = new Timestamp(System.currentTimeMillis());
        deviceRepository.checkPings(thresholdTime);

        List<Integer> offlineDevicesIds = deviceRepository.getOfflineDevicesIds();
        List<Integer> onlineDevicesIds = deviceRepository.getOnlineDevicesIds();

        for (Integer id: offlineDevicesIds) {
            Point deviceStatusChange = Point.measurement("DeviceStatus")
                    .addField("status", "offline")
                    .addTag("deviceId", id.toString());
            deviceRepositoryInflux.save(deviceStatusChange);
        }
        for (Integer id: onlineDevicesIds) {
            Point deviceStatusChange = Point.measurement("DeviceStatus")
                    .addField("status", "online")
                    .addTag("deviceId", id.toString());
            deviceRepositoryInflux.save(deviceStatusChange);
        }
    }

    public Device findById(Integer id) {
        return deviceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Device with given id was not found."));
    }
    
    public DeviceDTO addImage(String token, DeviceImageDTO deviceImageDTO) throws IOException {
        Device device = deviceRepository.findById(deviceImageDTO.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Device with given id was not found."));
        accessControl.validateDeviceOwner(token, device);
        String filePath = saveImage(deviceImageDTO);
        device.setImagePath(filePath);
        deviceRepository.save(device);
        return deviceMapper.entityToDto(device);
    }

    private String saveImage(DeviceImageDTO deviceImageDTO) throws IOException {
        String fileName = deviceImageDTO.getImage().getOriginalFilename();
        assert fileName != null;
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        String imageName = deviceImageDTO.getId() + "_" + System.currentTimeMillis() + "." + extension;
        String filePath = "src/main/resources/images/device/" + imageName;
        Files.write(Path.of(filePath), deviceImageDTO.getImage().getBytes());
        return imageName;
    }

    public List<DeviceDTO> getDevicesByRealEstate(String token, Integer realEstateId) {
        return deviceRepository.findAllByRealEstateId(realEstateId)
                .stream()
                .map(device -> {
                    accessControl.validateDeviceAccess(token, device);
                    DeviceDTO deviceDTO = deviceMapper.entityToDto(device);
                    deviceDTO.setDeviceType(device.getClass().getSimpleName());
                    return deviceDTO;
                })
                .toList();
    }

    public DeviceDTO switchDevice(String token, Integer deviceId, String userEmail)
            throws JsonProcessingException, MqttException {
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException("Device with given id was not found."));
        accessControl.validateDeviceAccess(token, device);
        device.setOn(!device.getOn());
        deviceRepository.save(device);
        MqttMessageDto payload = new MqttMessageDto("switch", "Activity switch");
        String payloadJson = mapper.writeValueAsString(payload);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        deviceClient.publish(
                device.getClass()
                        .getSimpleName()
                        .replaceAll("([a-z0-9])([A-Z])", "$1-$2")
                        .toLowerCase() + "-simulator/" + device.getId().toString(),
                        mqttMessage
                );
        saveSwitchRecord(device, userEmail);
        return deviceMapper.entityToDto(device);
    }

    private void saveSwitchRecord(Device device, String userEmail) throws JsonProcessingException {
        if (device.getClass().getSimpleName().equals("SolarPanelSystem")) {
            solarPanelSystemService.saveSwitchRecord(device.getId(), device.getOn(), userEmail);
        } else if (device.getClass().getSimpleName().equals("AirConditioning")) {
            System.out.println("USO");
            airConditioningService.saveSwitchRecord(device.getId(), device.getOn(), userEmail);
        } else if (device.getClass().getSimpleName().equals("WashingMachine")) {
            washingMachineService.saveSwitchRecord(device.getId(), device.getOn(), userEmail);
        }
    }

    public List<UserOutDTO> getSharedWith(String token, Integer deviceId) {
        Device device = findById(deviceId);
        accessControl.validateDeviceOwner(token, device);
        return device.getSharedWith().stream().map(u -> userMapper.entityToOutDto(u)).toList();
    }

    public UserOutDTO grantPermission(String token, Integer deviceId, String userEmail) {
        Device device = findById(deviceId);
        accessControl.validateDeviceOwner(token, device);
        User user = userService.findByEmail(userEmail);
        if (user == null)
            throw new ResourceNotFoundException("User with given email does not exist.");
        for (Role role : user.getRoles())
            if (Objects.equals(role.getName(), "ROLE_ADMIN"))
                throw new InvalidOperationException("User is an admin.");
        if (user.equals(userService.findOwnerByDevice(deviceId)))
            throw new InvalidOperationException("User is the owner of the device.");
        if (user.getSharedDevices().contains(device))
            throw new InvalidOperationException("User already has permissions for this device.");

        user.getSharedDevices().add(device);
        userService.save(user);
        return userMapper.entityToOutDto(user);
    }

    public UserOutDTO revokePermission(String token, Integer deviceId, String userEmail) {
        Device device = findById(deviceId);
        accessControl.validateDeviceOwner(token, device);
        User user = userService.findByEmail(userEmail);
        if (user == null)
            throw new ResourceNotFoundException("User with given email does not exist.");
        if (user.equals(userService.findOwnerByDevice(deviceId)))
            throw new InvalidOperationException("User is the owner of the device.");
        if (!user.getSharedDevices().contains(device))
            throw new InvalidOperationException("User does not have permissions for this device.");

        user.getSharedDevices().remove(device);
        user.getSharedRealEstates().remove(device.getRealEstate());
        userService.save(user);
        return userMapper.entityToOutDto(user);
    }

    public List<DeviceDTO> getSharedDevicesByRealEstate(String token, Integer realEstateId, Integer userId) {
        accessControl.validateUserId(token, userId);
        RealEstate realEstate = realEstateService.findById(realEstateId);
        User user = userService.findById(userId);
        List<DeviceDTO> devices = new ArrayList<>();
        realEstate.getDevices().forEach(device -> {
            if (device.getSharedWith().contains(user)) {
                accessControl.validateDeviceAccess(token, device);
                DeviceDTO deviceDTO = deviceMapper.entityToDto(device);
                deviceDTO.setDeviceType(device.getClass().getSimpleName());
                devices.add(deviceDTO);
            }
        });
        return devices;
    }

    public UserOutDTO getDeviceOwner(Integer deviceId) {
        return userMapper.entityToOutDto(userService.findOwnerByDevice(deviceId));
    }

    public List<DeviceAvailabilityRecord> findDeviceAvailabilityByDateRange(
            String token,
            String from,
            String to,
            Integer deviceId,
            String aggregationWindow){

        accessControl.validateDeviceAccess(token, findById(deviceId));
        List<DeviceAvailabilityRecord> deviceAvailabilityRecords =
                deviceRepositoryInflux.findDeviceAvailabilityByDateRange(from, to, deviceId, aggregationWindow);

        int divider = 1;
        String unit = "";
        if (aggregationWindow.equals("1h")){
            divider = 60;
            unit = "minutes";
        } else {
            divider = 60 * 60;
            unit = "hours";
        }

        for (DeviceAvailabilityRecord deviceAvailabilityRecord: deviceAvailabilityRecords){
            deviceAvailabilityRecord.setOnlineTime(deviceAvailabilityRecord.getOnlineTime()
                    * DevicePingChecker.checkingFrequencySeconds / divider);

            deviceAvailabilityRecord.setOfflineTime(deviceAvailabilityRecord.getOfflineTime()
                    * DevicePingChecker.checkingFrequencySeconds / divider);

            Instant intervalStart = deviceAvailabilityRecord.getIntervalStart().toInstant();
            Instant intervalEnd = deviceAvailabilityRecord.getIntervalEnd().toInstant();

            long difference;
            if (unit.equals("minutes")){
                //difference in minutes
                difference = Duration.between(intervalStart, intervalEnd).toMinutes();
            } else {
                //difference in hours
                difference = Duration.between(intervalStart, intervalEnd).toHours();
            }
            deviceAvailabilityRecord.setUnit(unit);
            if (difference == 0) {
                deviceAvailabilityRecord.setOnlinePercentage(0.0);
                deviceAvailabilityRecord.setOfflinePercentage(0.0);
                continue;
            };
            deviceAvailabilityRecord.setOnlinePercentage((deviceAvailabilityRecord.getOnlineTime() / difference) * 100);
            deviceAvailabilityRecord.setOfflinePercentage((deviceAvailabilityRecord.getOfflineTime() / difference) * 100);

        }

        return deviceAvailabilityRecords;
    }

}
