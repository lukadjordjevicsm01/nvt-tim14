package com.threeamigoscoding.smartifybackend.service.device.outside;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.WebSocketMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.GateDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.influx_records.outside.GateEventRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.outside.GateMapper;
import com.threeamigoscoding.smartifybackend.model.device.outside.Gate;
import com.threeamigoscoding.smartifybackend.model.device.outside.GateMode;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.outside.GateRepository;
import com.threeamigoscoding.smartifybackend.repository.device.outside.GateRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class GateService {

    public GateRepository gateRepository;

    private final RealEstateService realEstateService;

    private final GateMapper gateMapper;

    private final IMqttClient gateMqttClient;

    private final ObjectMapper jsonMapper;

    private final GateRepositoryInflux gateRepositoryInflux;

    private final SimpMessagingTemplate simpMessagingTemplate;

    private final PingService pingService;

    private final AccessControl accessControl;

    @Autowired
    public GateService(GateRepository gateRepository,
                       RealEstateService realEstateService,
                       GateMapper gateMapper,
                       @Qualifier("gateMqttClient") IMqttClient gateMqttClient,
                       ObjectMapper jsonMapper,
                       GateRepositoryInflux gateRepositoryInflux,
                       SimpMessagingTemplate simpMessagingTemplate,
                       PingService pingService,
                       AccessControl accessControl){

        this.gateRepository = gateRepository;
        this.realEstateService = realEstateService;
        this.gateMapper = gateMapper;
        this.gateMqttClient = gateMqttClient;
        this.jsonMapper = jsonMapper;
        this.gateRepositoryInflux = gateRepositoryInflux;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();
    }

    public Gate create(GateDTO gateDTO, Integer realEstateId, String token) {
        Gate gate = gateMapper.dtoToEntity(gateDTO);
        gate.setMode(GateMode.PUBLIC);

        RealEstate realEstate = realEstateService.findById(realEstateId);

        accessControl.validateRealEstateOwner(token, realEstate);
        gate.setRealEstate(realEstate);

        gateRepository.save(gate);

//        realEstate.getDevices().add(gate);
//        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, gate.getId());
        addToSimulator(gate);

        return gate;
    }

    private void addToSimulator(Gate gate) {
        String url = MyCredentials.simulatorServer + "add-gate";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<Gate> requestEntity = new HttpEntity<>(gate, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            gateMqttClient.subscribe("gate/" + gate.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public List<Gate> getAll() {
        return gateRepository.findAll();
    }

    public void subscribeToTopics(List<Gate> gates) {
        gates.forEach(gate -> {
            try {
                gateMqttClient.subscribe("gate/" + gate.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void setMqttCallback() {
        gateMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) {
                try{
                    MqttMessageDto mqttMessageDto =
                            jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                    if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                        setLastPing(Integer.parseInt(topic.split("/")[1]));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "approach-enter"))
                        registerGateEvent(jsonMapper.readValue(mqttMessageDto.getPayload(), GateEventRecord.class));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "open")){
                        GateEventRecord gateEventRecord =
                                jsonMapper.readValue(mqttMessageDto.getPayload(), GateEventRecord.class);
                        changeGateState(gateEventRecord.getGateId(), true, true, "none");
                        registerGateEvent(gateEventRecord);
                    }
                    else if (Objects.equals(mqttMessageDto.getCommand(), "close")){
                        GateEventRecord gateEventRecord =
                                jsonMapper.readValue(mqttMessageDto.getPayload(), GateEventRecord.class);
                        changeGateState(gateEventRecord.getGateId(), false, true, "none");
                        registerGateEvent(gateEventRecord);
                    }
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    public void changeGateStateManual(Integer gateId, boolean open, String email, String token)
            throws MqttException, JsonProcessingException {
        changeGateState(gateId, open, false, token);
        String gateState = open ? "opened" : "closed";
        registerGateEvent(new GateEventRecord(gateId, "Gate manually " + gateState, email, null));
    }

    private void changeGateState(Integer gateId, boolean open, boolean automatic, String token)
            throws JsonProcessingException, MqttException {
        Gate gate = getActualGate(gateId);
        if (!token.equals("none")) accessControl.validateDeviceAccess(token, gate);
        gate.setOpen(open);
        gateRepository.save(gate);
        if (!automatic) {
            MqttMessageDto payloadMQTT = new MqttMessageDto(
                    "switch-gate-state", "{\"open\": "+ open + "}");
            String payloadJson = jsonMapper.writeValueAsString(payloadMQTT);
            MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
            gateMqttClient.publish("gate-simulator/" + gate.getId().toString(),
                    mqttMessage
            );
        }
    }

    private void registerGateEvent(GateEventRecord gateEventRecord) throws JsonProcessingException {
        Point temperature = Point.measurement("Gate Action")
                .addField("action", gateEventRecord.getAction())
                .addTag("gateId", gateEventRecord.getGateId().toString())
                .addTag("subject", gateEventRecord.getSubject());
        gateRepositoryInflux.save(temperature);
        gateEventRecord.setTimestamp(new Timestamp(new Date().getTime()));
        WebSocketMessageDTO payload = new WebSocketMessageDTO("event", gateEventRecord);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        this.simpMessagingTemplate.convertAndSend("/gate/" + gateEventRecord.getGateId() , payloadJson);
    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }

    public List<GateEventRecord> findEventsByDateRange(String from, String to, Integer gateId, String token){
        accessControl.validateDeviceAccess(token, getActualGate(gateId));
        return gateRepositoryInflux.findEventsByDateRange(from, to, gateId);
    }

    public GateDTO getGate(Integer gateId, String token){
        Gate gate = getActualGate(gateId);
        accessControl.validateDeviceAccess(token, gate);
        return gateMapper.entityToDto(gate);
    }

    private Gate getActualGate(Integer gateId){
        return gateRepository.findById(gateId).orElseThrow(() ->
                new ResourceNotFoundException("Gate does not exist."));
    }

    public void updateVehicles(Integer gateId, List<String> vehicles, String token)
            throws JsonProcessingException, MqttException {
        //mozda updateovati da bude query u repozitorijumu
        Gate gate = getActualGate(gateId);

        accessControl.validateDeviceAccess(token, gate);

        gate.setAllowedVehicles(new HashSet<>(vehicles));
        gateRepository.save(gate);
        MqttMessageDto payloadMQTT = new MqttMessageDto(
                "update-allowed-vehicles", jsonMapper.writeValueAsString(vehicles));
        String payloadJson = jsonMapper.writeValueAsString(payloadMQTT);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        gateMqttClient.publish("gate-simulator/" + gate.getId().toString(),
                mqttMessage
        );
    }

    public void updateMode(Integer gateId, GateMode gateMode, String email, String token)
            throws JsonProcessingException, MqttException {
        Gate gate = getActualGate(gateId);
        accessControl.validateDeviceAccess(token, gate);
        gate.setMode(gateMode);
        gate.setOpen(false);
        gateRepository.save(gate);

        MqttMessageDto payloadMQTTMode = new MqttMessageDto(
                "update-mode", jsonMapper.writeValueAsString(gateMode));
        String payloadJson = jsonMapper.writeValueAsString(payloadMQTTMode);
        MqttMessage mqttMessage = new MqttMessage(payloadJson.getBytes());
        gateMqttClient.publish("gate-simulator/" + gate.getId().toString(),
                mqttMessage
        );

        registerGateEvent(new GateEventRecord(gateId, "Gate mode changed to " + gateMode.toString().toLowerCase(),
                email, null));

        MqttMessageDto payloadMQTTState = new MqttMessageDto(
                "switch-gate-state", "{\"open\": "+ false + "}");
        payloadJson = jsonMapper.writeValueAsString(payloadMQTTState);
        mqttMessage = new MqttMessage(payloadJson.getBytes());
        gateMqttClient.publish("gate-simulator/" + gate.getId().toString(),
                mqttMessage
        );
    }


    public void save(Gate gate) {
        gateRepository.save(gate);
    }
}
