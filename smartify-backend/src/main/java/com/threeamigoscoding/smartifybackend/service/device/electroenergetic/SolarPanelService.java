package com.threeamigoscoding.smartifybackend.service.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.SolarPanel;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.SolarPanelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SolarPanelService {

    @Autowired
    public SolarPanelRepository solarPanelRepository;

    public void save(SolarPanel solarPanel) {
        solarPanelRepository.save(solarPanel);
    }

}
