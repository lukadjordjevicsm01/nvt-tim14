package com.threeamigoscoding.smartifybackend.service.device.electroenergetic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.influxdb.client.write.Point;
import com.threeamigoscoding.smartifybackend.dto.WebSocketMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.ElectricityUsageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.HomeBatteryCurrentEnergyDTO;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.HomeBatteryDTO;
import com.threeamigoscoding.smartifybackend.dto.mqtt_model.MqttMessageDto;
import com.threeamigoscoding.smartifybackend.influx_records.MeasurementRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.HomeBatteryConsumptionRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.HomeBatteryDistributionRecord;
import com.threeamigoscoding.smartifybackend.mappers.device.electroenergetic.HomeBatteryMapper;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.HomeBattery;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.HomeBatteryRepository;
import com.threeamigoscoding.smartifybackend.repository.device.electroenergetic.HomeBatteryRepositoryInflux;
import com.threeamigoscoding.smartifybackend.security.auth.AccessControl;
import com.threeamigoscoding.smartifybackend.service.device.PingService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import com.threeamigoscoding.smartifybackend.util.MyCredentials;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Service
public class HomeBatteryService {

    private final HomeBatteryRepository homeBatteryRepository;
    private final RealEstateService realEstateService;
    private final HomeBatteryMapper homeBatteryMapper;
    private final IMqttClient homeBatteryMqttClient;
    private final ObjectMapper jsonMapper;
    private final HomeBatteryRepositoryInflux homeBatteryRepositoryInflux;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final PingService pingService;
    private final AccessControl accessControl;


    @Autowired
    public HomeBatteryService(HomeBatteryRepository homeBatteryRepository,
                              RealEstateService realEstateService,
                              HomeBatteryMapper homeBatteryMapper,
                              @Qualifier("homeBatteryMqttClient") IMqttClient homeBatteryMqttClient,
                              ObjectMapper jsonMapper,
                              HomeBatteryRepositoryInflux homeBatteryRepositoryInflux,
                              SimpMessagingTemplate simpMessagingTemplate,
                              PingService pingService,
                              AccessControl accessControl) {
        this.homeBatteryRepository = homeBatteryRepository;
        this.realEstateService = realEstateService;
        this.homeBatteryMapper = homeBatteryMapper;
        this.homeBatteryMqttClient = homeBatteryMqttClient;
        this.jsonMapper = jsonMapper;
        this.homeBatteryRepositoryInflux = homeBatteryRepositoryInflux;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.pingService = pingService;
        this.accessControl = accessControl;
        setMqttCallback();
    }

    public HomeBattery create(HomeBatteryDTO homeBatteryDTO, Integer realEstateId, String token) {
        HomeBattery homeBattery = homeBatteryMapper.dtoToEntity(homeBatteryDTO);
        RealEstate realEstate = realEstateService.findById(realEstateId);
        accessControl.validateRealEstateOwner(token, realEstate);
        homeBattery.setRealEstate(realEstate);
        homeBatteryRepository.save(homeBattery);

//        realEstate.getDevices().add(homeBattery);
//        realEstateService.save(realEstate);

        realEstateService.addToRealEstatesSimulation(realEstate, homeBattery.getId());
        addToSimulator(homeBattery);

        return homeBattery;
    }

    public List<HomeBattery> getAll() {
        return homeBatteryRepository.findAll();
    }

    public HomeBattery findById(Integer id) {
        return homeBatteryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Home Battery does not exist."));
    }

    public HomeBatteryDTO getHomeBattery(Integer id, String token) {
        HomeBattery homeBattery = findById(id);
        accessControl.validateDeviceAccess(token, homeBattery);
        return homeBatteryMapper.entityToDto(homeBattery);
    }

    private void addToSimulator(HomeBattery homeBattery) {
        String url = MyCredentials.simulatorServer + "add-home-battery";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<HomeBattery> requestEntity = new HttpEntity<>(homeBattery, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        try {
            homeBatteryMqttClient.subscribe("home-battery/" + homeBattery.getId());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Response: " + responseEntity.getBody());
    }

    public void subscribeToTopics(List<HomeBattery> homeBatteries) {
        homeBatteries.forEach(homeBattery -> {
            try {
                homeBatteryMqttClient.subscribe("home-battery/" + homeBattery.getId());
            } catch (MqttException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void setMqttCallback() {
        homeBatteryMqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                throwable.getStackTrace();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) {
                try{
                    MqttMessageDto mqttMessageDto =
                            jsonMapper.readValue(new String(mqttMessage.getPayload()), MqttMessageDto.class);
                    if (Objects.equals(mqttMessageDto.getCommand(), "ping"))
                        setLastPing(Integer.parseInt(topic.split("/")[1]));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "change-current-energy"))
                        changeCurrentEnergy(jsonMapper
                                .readValue(mqttMessageDto.getPayload(), HomeBatteryCurrentEnergyDTO.class));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "distribution"))
                        saveDistributedEnergy(jsonMapper
                                .readValue(mqttMessageDto.getPayload(), HomeBatteryDistributionRecord.class));
                    else if (Objects.equals(mqttMessageDto.getCommand(), "consumption"))
                        saveLastConsumption(jsonMapper
                                .readValue(mqttMessageDto.getPayload(), HomeBatteryConsumptionRecord.class));

                } catch (Exception e){
                    System.out.println(e.getMessage());
                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                System.out.println(iMqttDeliveryToken.getMessageId());
            }
        });
    }

    private void changeCurrentEnergy(HomeBatteryCurrentEnergyDTO homeBatteryCurrentEnergyDTO) throws JsonProcessingException {
        HomeBattery homeBattery = findById(homeBatteryCurrentEnergyDTO.getId());
        homeBattery.setCurrentEnergy(homeBatteryCurrentEnergyDTO.getCurrentEnergy());
        homeBatteryRepository.save(homeBattery);
        WebSocketMessageDTO payload = new WebSocketMessageDTO("currentEnergy", homeBatteryCurrentEnergyDTO);
        String payloadJson = jsonMapper.writeValueAsString(payload);
        this.simpMessagingTemplate.convertAndSend("/home-battery/" + homeBatteryCurrentEnergyDTO.getId() , payloadJson);
    }

    private void saveDistributedEnergy(HomeBatteryDistributionRecord homeBatteryDistributionRecord) {
        Point point = Point.measurement("Real Estate Distribution Energy")
                .addField("distributionEnergy", homeBatteryDistributionRecord.getDistributionEnergy())
                .addTag("id", homeBatteryDistributionRecord.getId().toString())
                .addTag("message", homeBatteryDistributionRecord.getMessage())
                .addTag("realEstateId", homeBatteryDistributionRecord.getRealEstateId().toString())
                .addTag("cityId", homeBatteryDistributionRecord.getCityId().toString());
        homeBatteryRepositoryInflux.save(point);
    }

    private void saveLastConsumption(HomeBatteryConsumptionRecord homeBatteryConsumptionRecord) throws JsonProcessingException {
        Point point = Point.measurement("Real Estate Last Consumption")
                .addField("lastMinuteConsumption", homeBatteryConsumptionRecord.getLastMinuteConsumption())
                .addTag("homeBatteryId", homeBatteryConsumptionRecord.getHomeBatteryId().toString())
                .addTag("realEstateId", homeBatteryConsumptionRecord.getRealEstateId().toString())
                .addTag("cityId", homeBatteryConsumptionRecord.getCityId().toString());
        homeBatteryRepositoryInflux.save(point);
        MeasurementRecord measurementRecord = new MeasurementRecord(
                homeBatteryConsumptionRecord.getLastMinuteConsumption(),
                homeBatteryConsumptionRecord.getTimestamp());
        WebSocketMessageDTO wsConsumptionPayload =
                new WebSocketMessageDTO("value", measurementRecord);
        String wsConsumptionPayloadJson = jsonMapper.writeValueAsString(wsConsumptionPayload);
        this.simpMessagingTemplate.convertAndSend(
                "/home-battery/" + homeBatteryConsumptionRecord.getHomeBatteryId(), wsConsumptionPayloadJson);

    }

    private void setLastPing(Integer id) {
        pingService.setLastPing(id);
    }

    public List<MeasurementRecord> findConsumptionRecordsByDateRange(String from, String to,
                                                                     Integer homeBatteryId,
                                                                     String token) {
        accessControl.validateDeviceAccess(token, findById(homeBatteryId));
        return homeBatteryRepositoryInflux.findRecordsByDateRange(from, to, homeBatteryId);
    }

    public List<HomeBatteryDistributionRecord> findDistributionRecordsByDateRange(String from, String to,
                                                                                  Integer homeBatteryId,
                                                                                  String token){
        accessControl.validateDeviceAccess(token, findById(homeBatteryId));
        return homeBatteryRepositoryInflux.findDistributionRecordsByDateRange(from, to, homeBatteryId);

    }

    public ElectricityUsageDTO findElectricityUsageByRealEstate(String from, String to, Integer realEstateId) {
        return homeBatteryRepositoryInflux.findElectricityUsageByRealEstate(from, to, realEstateId);
    }

    public ElectricityUsageDTO findElectricityUsageByCity(String from, String to, Integer cityId) {
        return homeBatteryRepositoryInflux.findElectricityUsageByCity(from, to, cityId);
    }

    public void save(HomeBattery homeBattery) {
        homeBatteryRepository.save(homeBattery);
    }
}
