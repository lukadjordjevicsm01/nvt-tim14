package com.threeamigoscoding.smartifybackend.service.device.outside;

import com.threeamigoscoding.smartifybackend.model.device.outside.SprinklerSetting;
import com.threeamigoscoding.smartifybackend.repository.device.outside.SprinklerSettingRepository;
import com.threeamigoscoding.smartifybackend.util.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.TreeSet;

@Service
public class SprinklerSettingService {

    @Autowired
    public SprinklerSettingRepository sprinklerSettingRepository;

    private final SimpleDateFormat hourMinuteFormat = new SimpleDateFormat("HH:mm");

    public SprinklerSetting save(SprinklerSetting sprinklerSetting){
        return sprinklerSettingRepository.save(sprinklerSetting);
    }

    public String settingToString(SprinklerSetting sprinklerSetting){
        String timeString = hourMinuteFormat.format(sprinklerSetting.getStartTime()) +
                " - " + hourMinuteFormat.format(sprinklerSetting.getEndTime());
        if (sprinklerSetting.getRepeatDays().size() == 7)
            return "Every day " + timeString;

        TreeSet<DayOfWeek> sortedDayOfWeekSet = new TreeSet<>(sprinklerSetting.getRepeatDays());
        StringBuilder result = new StringBuilder();
        for (DayOfWeek dayOfWeek : sortedDayOfWeekSet) {
            result.append(dayOfWeek.getDisplayName(TextStyle.SHORT, java.util.Locale.ENGLISH)).append(" ");
        }

        return result.toString() + timeString;
    }

    public SprinklerSetting getSprinklerSetting(Integer settingId) {
        return sprinklerSettingRepository.findById(settingId).orElseThrow(() ->
                new ResourceNotFoundException("Sprinkler setting not found."));
    }

    public void deleteById(Integer settingId){
        sprinklerSettingRepository.deleteById(settingId);
    }
}
