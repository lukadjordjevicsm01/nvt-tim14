package com.threeamigoscoding.smartifybackend.security.auth;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.model.user.User;
import com.threeamigoscoding.smartifybackend.util.TokenUtils;
import com.threeamigoscoding.smartifybackend.util.exceptions.AuthorizationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class AccessControl {

    @Autowired
    TokenUtils tokenUtils;

    public void validateDeviceOwner(String token, Device device) {
        String userEmail = tokenUtils.getUsernameFromToken(token.split(" ")[1]);
        if (!Objects.equals(userEmail, device.getRealEstate().getOwner().getEmail()))
            throw new AuthorizationException("You don't have access to this device!");
    }

    public void validateDeviceAccess(String token, Device device) {
        String userEmail = tokenUtils.getUsernameFromToken(token.split(" ")[1]);
        if (!Objects.equals(userEmail, device.getRealEstate().getOwner().getEmail())
                && !device.getSharedWith().stream().map(User::getEmail).toList().contains(userEmail))
            throw new AuthorizationException("You don't have access to this device!");
    }

    public void validateRealEstateOwner(String token, RealEstate realEstate) {
        String userEmail = tokenUtils.getUsernameFromToken(token.split(" ")[1]);
        if (!Objects.equals(userEmail, realEstate.getOwner().getEmail()))
            throw new AuthorizationException("You don't have access to this real estate!");
    }

    public void validateRealEstateAccess(String token, RealEstate realEstate) {
        String userEmail = tokenUtils.getUsernameFromToken(token.split(" ")[1]);
        if (!Objects.equals(userEmail, realEstate.getOwner().getEmail())
                && !realEstate.getSharedWith().stream().map(User::getEmail).toList().contains(userEmail))
            throw new AuthorizationException("You don't have access to this real estate!");
    }

    public void validateUserId(String token, Integer userId) {
        Integer id = tokenUtils.getUserIdFromToken(token.split(" ")[1]);
        if (!Objects.equals(id, userId))
            throw new AuthorizationException("Unauthorized!");
    }

}
