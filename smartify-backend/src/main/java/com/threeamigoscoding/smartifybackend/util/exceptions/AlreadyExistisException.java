package com.threeamigoscoding.smartifybackend.util.exceptions;

public class AlreadyExistisException extends RuntimeException {
    public AlreadyExistisException(String message) {
        super(message);
    }

    public AlreadyExistisException() {

    }
}
