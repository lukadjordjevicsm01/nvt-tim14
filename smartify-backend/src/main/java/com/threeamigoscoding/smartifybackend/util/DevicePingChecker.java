package com.threeamigoscoding.smartifybackend.util;

import com.threeamigoscoding.smartifybackend.service.device.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DevicePingChecker {

    @Autowired
    private DeviceService deviceService;

    public static final int checkingFrequencySeconds = 30;

    // 30 minutes in milliseconds = 1800000
    @Scheduled(fixedRate = checkingFrequencySeconds * 1000, initialDelay = 10000)
    public void checkPings() {
        deviceService.checkPings();
    }

}
