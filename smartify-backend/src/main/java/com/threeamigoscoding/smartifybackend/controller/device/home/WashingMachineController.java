package com.threeamigoscoding.smartifybackend.controller.device.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.threeamigoscoding.smartifybackend.dto.ResponseMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.*;
import com.threeamigoscoding.smartifybackend.influx_records.home.WashingMachineRecord;
import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachine;
import com.threeamigoscoding.smartifybackend.service.device.home.WashingMachineService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/washingMachine")
public class WashingMachineController {

    @Autowired
    WashingMachineService washingMachineService;

    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<WashingMachine> createWashingMachine(
            @RequestHeader (name="Authorization") String token,
            @RequestBody WashingMachineDTO washingMachineDTO,
            @PathVariable Integer realEstateId
    ) {
        WashingMachine washingMachine = washingMachineService.create(token, washingMachineDTO, realEstateId);
        return new ResponseEntity<>(washingMachine, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getWashingMachine/{washingMachineId}")
    public ResponseEntity<WashingMachineDTO> getAirConditioning(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer washingMachineId
    ){
        return ResponseEntity.ok(washingMachineService.getWashingMachine(token, washingMachineId));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/setAction/{washingMachineId}")
    public ResponseEntity<ResponseMessageDTO> setAction(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer washingMachineId,
            @RequestBody WashingMachineActionDTO washingMachineActionDTO
    ) throws JsonProcessingException, MqttException {
        washingMachineService.setAction(token, washingMachineId, washingMachineActionDTO);
        return ResponseEntity.ok(new ResponseMessageDTO("Action excecuted successfully."));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getRecords/{washingMachineId}")
    public ResponseEntity<List<WashingMachineRecord>> getRecords(
            @RequestHeader (name="Authorization") String token,
            @PathVariable String washingMachineId,
            @RequestParam String from,
            @RequestParam String to
    ) {
        return ResponseEntity.ok(washingMachineService.findByDateRange(token, from, to, washingMachineId));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/addSetting/{washingMachineId}")
    public ResponseEntity<ResponseMessageDTO> addSetting(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer washingMachineId,
            @RequestBody WashingMachineSettingInDTO washingMachineInDTO
    ) throws JsonProcessingException, MqttException {
        washingMachineService.addSetting(token, washingMachineId, washingMachineInDTO);
        return ResponseEntity.ok(new ResponseMessageDTO("Setting added."));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getCurrentMode/{washingMachineId}")
    public ResponseEntity<WashingMachineModeDTO> getCurrentMode(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer washingMachineId
    ) {
        return ResponseEntity.ok(washingMachineService.getCurrentMode(token, washingMachineId));
    }
}
