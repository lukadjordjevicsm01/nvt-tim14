package com.threeamigoscoding.smartifybackend.controller.device.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.threeamigoscoding.smartifybackend.dto.ResponseMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.AirConditioningActionDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.AirConditioningDTO;
import com.threeamigoscoding.smartifybackend.dto.device.home.AirConditioningSettingInDTO;
import com.threeamigoscoding.smartifybackend.influx_records.home.AirConditioningRecord;
import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioning;
import com.threeamigoscoding.smartifybackend.service.device.home.AirConditioningService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/airConditioning")
public class AirConditioningController {

    @Autowired
    AirConditioningService airConditioningService;

    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<AirConditioning> createAirConditioning(
            @RequestHeader (name="Authorization") String token,
            @RequestBody AirConditioningDTO airConditioningDTO,
            @PathVariable Integer realEstateId
    ){
        AirConditioning airConditioning = airConditioningService.create(token, airConditioningDTO, realEstateId);
        return ResponseEntity.ok(airConditioning);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getAirConditioning/{airConditioningId}")
    public ResponseEntity<AirConditioning> getAirConditioning(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer airConditioningId
    ) {
        return ResponseEntity.ok(airConditioningService.getAirConditioning(token, airConditioningId));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getRecords/{airConditioningId}")
    public ResponseEntity<List<AirConditioningRecord>> getRecords(
            @RequestHeader (name="Authorization") String token,
            @PathVariable String airConditioningId,
            @RequestParam String from,
            @RequestParam String to
    ) {
        return ResponseEntity.ok(airConditioningService.findByDateRange(token, from, to, airConditioningId));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/setAction/{airConditioningId}")
    public ResponseEntity<ResponseMessageDTO> setAction(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer airConditioningId,
            @RequestBody AirConditioningActionDTO airConditioningActionDTO
    ) throws JsonProcessingException, MqttException {
        airConditioningService.setAction(token, airConditioningId, airConditioningActionDTO);
        return ResponseEntity.ok(new ResponseMessageDTO("Action excecuted successfully."));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/addSetting/{airConditioningId}")
    public ResponseEntity<ResponseMessageDTO> addSetting(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer airConditioningId,
            @RequestBody AirConditioningSettingInDTO airConditioningSettingDTO)
            throws JsonProcessingException, MqttException {
        airConditioningService.addSetting(token, airConditioningId, airConditioningSettingDTO);
        return ResponseEntity.ok(new ResponseMessageDTO("Setting added."));
    }
}
