package com.threeamigoscoding.smartifybackend.controller.realestate;

import com.threeamigoscoding.smartifybackend.dto.ResponseMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.realestate.*;
import com.threeamigoscoding.smartifybackend.dto.user.UserOutDTO;
import com.threeamigoscoding.smartifybackend.model.realestate.Address;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstateStatus;
import com.threeamigoscoding.smartifybackend.service.realestate.CityService;
import com.threeamigoscoding.smartifybackend.service.realestate.CountryService;
import com.threeamigoscoding.smartifybackend.service.realestate.RealEstateService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "api/realEstate")
public class RealEstateController {


    @Autowired
    private RealEstateService realEstateService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CityService cityService;

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/register")
    public ResponseEntity<RealEstate> registerRealEstate(@ModelAttribute @Valid RealEstateRegistrationDTO realEstateRegistrationDTO)
            throws IOException {
        RealEstate realEstate = realEstateService.registerRealEstate(realEstateRegistrationDTO);
        return new ResponseEntity<>(realEstate, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/changeStatus")
    public ResponseEntity<ResponseMessageDTO> changeStatus(@RequestBody @Valid StatusChangeDTO statusChangeDTO){
        realEstateService.changeRealEstateStatus(statusChangeDTO);
        return new ResponseEntity<>(new ResponseMessageDTO("Success"), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/{userId}")
    public ResponseEntity<List<RealEstateOutDTO>> findAllRealEstateForUser(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer userId
    ){
        return new ResponseEntity<>(realEstateService.findAllByOwner(token, userId), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/pending")
    public ResponseEntity<List<RealEstateOutDTO>> findAllPendingRealEstates(){
        return new ResponseEntity<>(realEstateService.findAllByStatus(RealEstateStatus.PENDING), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/accepted")
    public ResponseEntity<List<RealEstateOutDTO>> findAllAcceptedRealEstates(){
        return new ResponseEntity<>(realEstateService.findAllByStatus(RealEstateStatus.ACCEPTED), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping(value = "/countries")
    public ResponseEntity<List<CountryDTO>> findAllCountries(){
        return new ResponseEntity<>(countryService.findAll(), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping(value = "/cities/{countryCode}")
    public ResponseEntity<List<CityDTO>> findAllCitiesByCountry(@PathVariable String countryCode){
        return new ResponseEntity<>(cityService.findByCountry(countryCode), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping(value = "/cities")
    public ResponseEntity<List<CityDTO>> findAllCities(){
        return new ResponseEntity<>(cityService.findAll(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value="/getSharedWith/{realEstateId}")
    public ResponseEntity<List<UserOutDTO>> getSharedWith(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer realEstateId
    ) {
        return ResponseEntity.ok(realEstateService.getSharedWith(token, realEstateId));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value="/grantPermission/{deviceId}")
    public ResponseEntity<UserOutDTO> grantPermission(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer deviceId,
            @RequestParam String userEmail
    ) {
        return ResponseEntity.ok(realEstateService.grantPermission(token, deviceId, userEmail));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value="/revokePermission/{deviceId}")
    public ResponseEntity<UserOutDTO> revokePermission(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer deviceId,
            @RequestParam String userEmail
    ) {
        return ResponseEntity.ok(realEstateService.revokePermission(token, deviceId, userEmail));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getSharedRealEstates/{userId}")
    public ResponseEntity<List<RealEstateOutDTO>> getSharedRealEstates(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer userId
    ){
        return ResponseEntity.ok(realEstateService.getSharedRealEstates(token, userId));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getSharedDeviceRealEstates/{userId}")
    public ResponseEntity<List<RealEstateOutDTO>> getSharedDeviceRealEstates(
            @RequestHeader (name="Authorization") String token,

            @PathVariable Integer userId
    ){
        return ResponseEntity.ok(realEstateService.getSharedDeviceRealEstates(token, userId));
    }

}
