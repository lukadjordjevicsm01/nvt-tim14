package com.threeamigoscoding.smartifybackend.controller.device.outside;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.threeamigoscoding.smartifybackend.dto.ResponseMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.GateDTO;
import com.threeamigoscoding.smartifybackend.influx_records.outside.GateEventRecord;
import com.threeamigoscoding.smartifybackend.model.device.outside.Gate;
import com.threeamigoscoding.smartifybackend.model.device.outside.GateMode;
import com.threeamigoscoding.smartifybackend.service.device.outside.GateService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/gate")
public class GateController {

    @Autowired
    GateService gateService;

    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<Gate> createGate(@RequestBody GateDTO gateDTO,
                                           @PathVariable Integer realEstateId,
                                           @RequestHeader (name="Authorization") String token){
        Gate gate = gateService.create(gateDTO, realEstateId, token);
        return new ResponseEntity<>(gate, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = "/getGateEvents/{gateId}")
    public ResponseEntity<List<GateEventRecord>> getGateEvents(@PathVariable Integer gateId,
                                                               @RequestParam String from,
                                                               @RequestParam String to,
                                                               @RequestHeader (name="Authorization") String token){
        return new ResponseEntity<>(gateService.findEventsByDateRange(from, to, gateId, token), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = "/getGate/{gateId}")
    public ResponseEntity<GateDTO> getGate(@PathVariable Integer gateId,
                                           @RequestHeader (name="Authorization") String token){
        return new ResponseEntity<>(gateService.getGate(gateId, token), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @PostMapping(value = "/updateVehicles/{gateId}")
    public ResponseEntity<ResponseMessageDTO> updateVehicles(@PathVariable Integer gateId,
                                                             @RequestBody List<String> vehicles,
                                                             @RequestHeader (name="Authorization") String token)
            throws JsonProcessingException, MqttException {
        gateService.updateVehicles(gateId, vehicles, token);
        return new ResponseEntity<>(new ResponseMessageDTO("Allowed vehicles list updated successfully!"), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @PostMapping(value = "/updateMode/{gateId}")
    public ResponseEntity<ResponseMessageDTO> updateMode(@PathVariable Integer gateId,
                                                         @RequestParam GateMode gateMode,
                                                         @RequestParam String email,
                                                         @RequestHeader (name="Authorization") String token
                                                         )
            throws JsonProcessingException, MqttException {
        gateService.updateMode(gateId, gateMode, email, token);
        return new ResponseEntity<>(new ResponseMessageDTO("Gate mode updated successfully!"), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @PostMapping(value = "/changeGateState/{gateId}")
    public ResponseEntity<ResponseMessageDTO> changeGateState(@PathVariable Integer gateId,
                                                              @RequestParam boolean open,
                                                              @RequestParam String email,
                                                              @RequestHeader (name="Authorization") String token)
            throws JsonProcessingException, MqttException {
        gateService.changeGateStateManual(gateId, open, email, token);
        return new ResponseEntity<>(new ResponseMessageDTO("Gate mode updated successfully!"), HttpStatus.OK);
    }
}
