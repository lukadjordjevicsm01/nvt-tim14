package com.threeamigoscoding.smartifybackend.controller.device.outside;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.threeamigoscoding.smartifybackend.dto.ResponseMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerSettingDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerSettingInDTO;
import com.threeamigoscoding.smartifybackend.influx_records.outside.SprinklerEventRecord;
import com.threeamigoscoding.smartifybackend.model.device.outside.Sprinkler;
import com.threeamigoscoding.smartifybackend.service.device.outside.SprinklerService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/sprinkler")
public class SprinklerController {

    @Autowired
    SprinklerService sprinklerService;

    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<Sprinkler> createSprinkler(@RequestBody SprinklerDTO sprinklerDTO,
                                                     @PathVariable Integer realEstateId,
                                                     @RequestHeader (name="Authorization") String token){
        Sprinkler sprinkler = sprinklerService.create(sprinklerDTO, realEstateId, token);
        return ResponseEntity.ok(sprinkler);

    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/get-sprinkler/{sprinklerId}")
    public ResponseEntity<SprinklerDTO> getSprinkler(@PathVariable Integer sprinklerId,
                                                     @RequestHeader (name="Authorization") String token){
        return ResponseEntity.ok(sprinklerService.getSprinkler(sprinklerId, token));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/addSetting/{sprinklerId}")
    public ResponseEntity<SprinklerSettingDTO> addSetting(@PathVariable Integer sprinklerId,
                                                          @RequestBody SprinklerSettingInDTO sprinklerSettingDTO,
                                                          @RequestHeader (name="Authorization") String token)
            throws MqttException, JsonProcessingException {

        System.out.println(sprinklerSettingDTO);
        return ResponseEntity.ok(sprinklerService.addSetting(sprinklerId, sprinklerSettingDTO, token));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/updateSetting/{settingId}/{sprinklerId}")
    public ResponseEntity<ResponseMessageDTO> updateSetting(@PathVariable Integer settingId,
                                                            @PathVariable Integer sprinklerId,
                                                            @RequestBody SprinklerSettingInDTO sprinklerSettingDTO,
                                                            @RequestHeader (name="Authorization") String token)
            throws JsonProcessingException, MqttException {

        sprinklerService.updateSetting(sprinklerId, settingId, sprinklerSettingDTO, token);
        return ResponseEntity.ok(new ResponseMessageDTO("Setting updated."));
    }

    @PreAuthorize("hasRole('USER')")
    @DeleteMapping(value = "/deleteSetting/{settingId}/{sprinklerId}")
    public ResponseEntity<ResponseMessageDTO> deleteSetting(@PathVariable Integer settingId,
                                                            @PathVariable Integer sprinklerId,
                                                            @RequestParam String email,
                                                            @RequestHeader (name="Authorization") String token)
            throws JsonProcessingException, MqttException {

        sprinklerService.deleteSetting(sprinklerId, settingId, email, token);
        return ResponseEntity.ok(new ResponseMessageDTO("Setting removed."));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/switchSprinkler/{sprinklerId}")
    public ResponseEntity<ResponseMessageDTO> switchSprinkler(@PathVariable Integer sprinklerId,
                                                              @RequestParam String email,
                                                              @RequestParam boolean sprinklerOn,
                                                              @RequestHeader (name="Authorization") String token)
            throws MqttException, JsonProcessingException {

        sprinklerService.changeSprinklerState(sprinklerId, sprinklerOn, false, token);
        String action = "Sprinkler turned " + (sprinklerOn ? "on" : "off");
        sprinklerService.registerSprinklerEvent(sprinklerId, action, email, "");
        return ResponseEntity.ok(new ResponseMessageDTO("Sprinkler turned " + (sprinklerOn ? "on" : "off")));
    }

    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = "/getSprinklerEvents/{sprinklerId}")
    public ResponseEntity<List<SprinklerEventRecord>> getSprinklerEvents(@PathVariable Integer sprinklerId,
                                                                         @RequestParam String from,
                                                                         @RequestParam String to,
                                                                         @RequestHeader (name="Authorization") String token) {
        return ResponseEntity.ok(sprinklerService.findEventsByDateRange(from, to, sprinklerId, token));
    }

}
