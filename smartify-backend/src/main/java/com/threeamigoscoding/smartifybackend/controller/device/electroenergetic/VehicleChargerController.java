package com.threeamigoscoding.smartifybackend.controller.device.electroenergetic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.ChargingDataDTO;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.VehicleChargerDTO;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.VehicleChargerRecord;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.VehicleCharger;
import com.threeamigoscoding.smartifybackend.service.device.electroenergetic.VehicleChargerService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/vehicleCharger")
public class VehicleChargerController {

    @Autowired
    VehicleChargerService vehicleChargerService;

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<VehicleCharger> createVehicleCharger(@RequestBody VehicleChargerDTO vehicleChargerDTO,
                                                               @PathVariable Integer realEstateId,
                                                               @RequestHeader (name="Authorization") String token){
        VehicleCharger vehicleCharger = vehicleChargerService.create(vehicleChargerDTO, realEstateId, token);
        return new ResponseEntity<>(vehicleCharger, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getVehicleCharger/{vehicleChargerId}")
    public ResponseEntity<VehicleChargerDTO> getVehicleCharger(@PathVariable Integer vehicleChargerId,
                                                               @RequestHeader (name="Authorization") String token) {
        VehicleChargerDTO vehicleCharger = vehicleChargerService.getVehicleCharger(vehicleChargerId, token);
        return new ResponseEntity<>(vehicleCharger, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/startCharging")
    public ResponseEntity<ChargingDataDTO> startCharging(@RequestBody ChargingDataDTO chargingDataDTO,
                                                         @RequestHeader (name="Authorization") String token)
            throws JsonProcessingException, MqttException {
        vehicleChargerService.switchChargingState(chargingDataDTO, true, token);
        return new ResponseEntity<>(chargingDataDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/finishCharging")
    public ResponseEntity<ChargingDataDTO> finishCharging(@RequestBody ChargingDataDTO chargingDataDTO,
                                                          @RequestHeader (name="Authorization") String token)
            throws JsonProcessingException, MqttException {
        vehicleChargerService.switchChargingState(chargingDataDTO, false, token);
        return new ResponseEntity<>(chargingDataDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getVehicleChargerRecords/{vehicleChargerId}")
    public ResponseEntity<List<VehicleChargerRecord>> getVehicleChargerRecords(@PathVariable Integer vehicleChargerId,
                                                                               @RequestParam String from,
                                                                               @RequestParam String to,
                                                                               @RequestHeader (name="Authorization") String token){
        return new ResponseEntity<>(vehicleChargerService
                .findRecordsByDateRange(from, to, vehicleChargerId, token), HttpStatus.OK);
    }

}
