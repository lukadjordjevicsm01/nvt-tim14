package com.threeamigoscoding.smartifybackend.controller.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.ElectricityUsageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.HomeBatteryDTO;
import com.threeamigoscoding.smartifybackend.influx_records.MeasurementRecord;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.HomeBatteryDistributionRecord;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.HomeBattery;
import com.threeamigoscoding.smartifybackend.service.device.electroenergetic.HomeBatteryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/homeBattery")
public class HomeBatteryController {

    @Autowired
    HomeBatteryService homeBatteryService;

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<HomeBattery> createHomeBattery(@RequestBody HomeBatteryDTO homeBatteryDTO,
                                                         @PathVariable Integer realEstateId,
                                                         @RequestHeader (name="Authorization") String token) {
        HomeBattery homeBattery = homeBatteryService.create(homeBatteryDTO, realEstateId, token);
        return new ResponseEntity<>(homeBattery, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = "/getConsumptionRecords/{homeBatteryId}")
    public ResponseEntity<List<MeasurementRecord>> getConsumptionRecords(@PathVariable Integer homeBatteryId,
                                                                         @RequestParam String from,
                                                                         @RequestParam String to,
                                                                         @RequestHeader (name="Authorization") String token){
        return new ResponseEntity<>(homeBatteryService
                .findConsumptionRecordsByDateRange(from, to, homeBatteryId, token), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = "/getDistributionRecords/{homeBatteryId}")
    public ResponseEntity<List<HomeBatteryDistributionRecord>> getDistributionRecords(@PathVariable Integer homeBatteryId,
                                                                                      @RequestParam String from,
                                                                                      @RequestParam String to,
                                                                                      @RequestHeader (name="Authorization") String token){
        return new ResponseEntity<>(homeBatteryService
                .findDistributionRecordsByDateRange(from, to, homeBatteryId, token), HttpStatus.OK);
    }



    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = "/getHomeBattery/{homeBatteryId}")
    public ResponseEntity<HomeBatteryDTO> getHomeBattery(@PathVariable Integer homeBatteryId,
                                                         @RequestHeader (name="Authorization") String token) {
        HomeBatteryDTO homeBattery = homeBatteryService.getHomeBattery(homeBatteryId, token);
        return new ResponseEntity<>(homeBattery, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/getElectricityUsageByRealEstate/{realEstateId}")
    public ResponseEntity<ElectricityUsageDTO> getElectricityUsageByRealEstate(@PathVariable Integer realEstateId,
                                                                               @RequestParam String from,
                                                                               @RequestParam String to){
        return new ResponseEntity<>(homeBatteryService
                .findElectricityUsageByRealEstate(from, to, realEstateId), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/getElectricityUsageByCity/{cityId}")
    public ResponseEntity<ElectricityUsageDTO> getElectricityUsageByCity(@PathVariable Integer cityId,
                                                             @RequestParam String from,
                                                             @RequestParam String to){
        return new ResponseEntity<>(homeBatteryService
                .findElectricityUsageByCity(from, to, cityId), HttpStatus.OK);
    }

}
