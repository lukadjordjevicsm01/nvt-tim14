package com.threeamigoscoding.smartifybackend.controller.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.SolarPanelSystemDTO;
import com.threeamigoscoding.smartifybackend.influx_records.electroenergetic.SolarPanelSystemEventRecord;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.SolarPanelSystem;
import com.threeamigoscoding.smartifybackend.service.device.electroenergetic.SolarPanelSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/solarPanelSystem")
public class SolarPanelSystemController {

    @Autowired
    private SolarPanelSystemService solarPanelSystemService;

    @PreAuthorize("hasAnyRole('USER')")
    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<SolarPanelSystem> createSolarPanelSystem(@RequestBody SolarPanelSystemDTO solarPanelSystemDTO,
                                                                   @PathVariable Integer realEstateId,
                                                                   @RequestHeader (name="Authorization") String token) {
        SolarPanelSystem solarPanelSystem = solarPanelSystemService.create(solarPanelSystemDTO, realEstateId, token);
        return new ResponseEntity<>(solarPanelSystem, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = "/getSolarPanelSystemEvents/{solarPanelSystemId}")
    public ResponseEntity<List<SolarPanelSystemEventRecord>> getSolarPanelSystemEvents(@PathVariable Integer solarPanelSystemId,
                                                                                       @RequestParam String from,
                                                                                       @RequestParam String to,
                                                                                       @RequestHeader (name="Authorization") String token){
        return new ResponseEntity<>(solarPanelSystemService
                .findEventsByDateRange(from, to, solarPanelSystemId, token), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = "/getSolarPanelSystem/{solarPanelSystemId}")
    public ResponseEntity<SolarPanelSystemDTO> getSolarPanelSystem(@PathVariable Integer solarPanelSystemId,
                                                                   @RequestHeader (name="Authorization") String token) {
        SolarPanelSystemDTO solarPanelSystem = solarPanelSystemService.getSolarPanelSystem(solarPanelSystemId, token);
        return new ResponseEntity<>(solarPanelSystem, HttpStatus.OK);
    }
}
