package com.threeamigoscoding.smartifybackend.controller.user;

import com.threeamigoscoding.smartifybackend.dto.ResponseMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.user.PasswordChangeDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserTokenState;
import com.threeamigoscoding.smartifybackend.service.user.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(value = "api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/login", consumes = "application/json")
    public ResponseEntity<UserTokenState> login(@Valid @RequestBody UserDTO authenticationRequest) {
        return ResponseEntity.ok(userService.login(authenticationRequest));
    }

    @PostMapping(value = "/register")
    public ResponseEntity<UserDTO> register(@Valid @ModelAttribute UserDTO userDTO) throws IOException {
        return ResponseEntity.ok(userService.createUserActivation(userDTO));
    }

    @PreAuthorize("hasRole('SUPERADMIN')")
    @PostMapping(value = "/register-admin")
    public ResponseEntity<UserDTO> registerAdmin(@Valid @ModelAttribute UserDTO userDTO) throws IOException {
        return ResponseEntity.ok(userService.createAdmin(userDTO));
    }

    @GetMapping(value = "/activate/{activation-code}")
    public ResponseEntity<String> activateAccount(@PathVariable("activation-code") Integer activationId) {
        userService.activateUserAccount(activationId);
        return ResponseEntity.ok("Account successfully verified.");
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/change-password")
    public ResponseEntity<ResponseMessageDTO> changePassword(@Valid @RequestBody PasswordChangeDTO passwordChangeDTO) {
        userService.changePassword(passwordChangeDTO);
        return ResponseEntity.ok(new ResponseMessageDTO("Password successfully changed."));
    }

}
