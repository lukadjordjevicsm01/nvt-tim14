package com.threeamigoscoding.smartifybackend.controller.device;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.dto.device.DeviceImageDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserOutDTO;
import com.threeamigoscoding.smartifybackend.influx_records.DeviceAvailabilityRecord;
import com.threeamigoscoding.smartifybackend.service.device.DeviceService;
import jakarta.validation.Valid;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/addImage")
    public ResponseEntity<DeviceDTO> addImage(
            @RequestHeader (name="Authorization") String token,
            @Valid @ModelAttribute DeviceImageDTO deviceImageDTO)
            throws IOException {
        return ResponseEntity.ok(deviceService.addImage(token, deviceImageDTO));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value="/{realEstateId}")
    public ResponseEntity<List<DeviceDTO>> getDevicesByRealEstate(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer realEstateId) {
        return ResponseEntity.ok(deviceService.getDevicesByRealEstate(token, realEstateId));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value="/switch/{deviceId}")
    public ResponseEntity<DeviceDTO> switchDevice(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer deviceId,
            @RequestParam String userEmail)
            throws JsonProcessingException, MqttException {
        return ResponseEntity.ok(deviceService.switchDevice(token, deviceId, userEmail));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value="/getSharedWith/{deviceId}")
    public ResponseEntity<List<UserOutDTO>> getSharedWith(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer deviceId) {
        return ResponseEntity.ok(deviceService.getSharedWith(token, deviceId));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value="/grantPermission/{deviceId}")
    public ResponseEntity<UserOutDTO> grantPermission(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer deviceId,
            @RequestParam String userEmail) {
        return ResponseEntity.ok(deviceService.grantPermission(token, deviceId, userEmail));
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value="/revokePermission/{deviceId}")
    public ResponseEntity<UserOutDTO> revokePermission(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer deviceId,
            @RequestParam String userEmail) {
        return ResponseEntity.ok(deviceService.revokePermission(token, deviceId, userEmail));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value="/getSharedDevicesByRealEstate/{realEstateId}/{userId}")
    public ResponseEntity<List<DeviceDTO>> getSharedDevicesByRealEstate(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer realEstateId,
            @PathVariable Integer userId) {
        return ResponseEntity.ok(deviceService.getSharedDevicesByRealEstate(token, realEstateId, userId));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value="/getDeviceOwner/{deviceId}")
    public ResponseEntity<UserOutDTO> getDeviceOwner(@PathVariable Integer deviceId) {
        return ResponseEntity.ok(deviceService.getDeviceOwner(deviceId));
    }

    @GetMapping(value="/availabilityRecords/{deviceId}")
    public ResponseEntity<List<DeviceAvailabilityRecord>> findDeviceAvailabilityByDateRange(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer deviceId,
            @RequestParam String from,
            @RequestParam String to,
            @RequestParam String aggregationWindow) {
        List<DeviceAvailabilityRecord> deviceAvailabilityRecords =
                deviceService.findDeviceAvailabilityByDateRange(token, from, to, deviceId, aggregationWindow);
        return ResponseEntity.ok(deviceAvailabilityRecords);
    }

}
