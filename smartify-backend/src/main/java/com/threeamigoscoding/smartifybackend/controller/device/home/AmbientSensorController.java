package com.threeamigoscoding.smartifybackend.controller.device.home;

import com.threeamigoscoding.smartifybackend.dto.device.home.AmbientSensorDTO;
import com.threeamigoscoding.smartifybackend.influx_records.home.AmbientSensorRecordDTO;
import com.threeamigoscoding.smartifybackend.model.device.home.AmbientSensor;
import com.threeamigoscoding.smartifybackend.service.device.home.AmbientSensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/ambientSensor")
public class AmbientSensorController {

    @Autowired
    AmbientSensorService ambientSensorService;

    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<AmbientSensor> createAmbientSensor(
            @RequestHeader (name="Authorization") String token,
            @RequestBody AmbientSensorDTO ambientSensorDTO,
            @PathVariable Integer realEstateId
    ) {
        AmbientSensor ambientSensor = ambientSensorService.create(token, ambientSensorDTO, realEstateId);
        return ResponseEntity.ok(ambientSensor);
    }

    @GetMapping(value = "/getAmbientSensor/{ambentSensorId}")
    public ResponseEntity<AmbientSensorDTO> getAmbientSensor(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer ambentSensorId
    ) {
        AmbientSensorDTO ambientSensor = ambientSensorService.getAmbientSensor(token, ambentSensorId);
        return new ResponseEntity<>(ambientSensor, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/get-ambient-sensor-records/{ambentSensorId}")
    public ResponseEntity<List<AmbientSensorRecordDTO>> getAmbientSensorRecords(
            @RequestHeader (name="Authorization") String token,
            @PathVariable Integer ambentSensorId,
            @RequestParam String from,
            @RequestParam String to,
            @RequestParam String measurement,
            @RequestParam String aggregationWindow
    ) {
        return ResponseEntity.ok(ambientSensorService
                .findByDateRange(token, from, to, measurement, ambentSensorId, aggregationWindow));
    }

}
