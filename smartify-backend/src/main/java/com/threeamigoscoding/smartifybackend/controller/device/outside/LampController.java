package com.threeamigoscoding.smartifybackend.controller.device.outside;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.threeamigoscoding.smartifybackend.dto.ResponseMessageDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.LampDTO;
import com.threeamigoscoding.smartifybackend.influx_records.MeasurementRecord;
import com.threeamigoscoding.smartifybackend.model.device.outside.Lamp;
import com.threeamigoscoding.smartifybackend.service.device.outside.LampService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/device/lamp")
public class LampController {

    @Autowired
    LampService lampService;

    @PostMapping(value = "/create/{realEstateId}")
    public ResponseEntity<Lamp> createLamp(@RequestBody LampDTO lampDTO,
                                           @PathVariable Integer realEstateId,
                                           @RequestHeader (name="Authorization") String token){
        Lamp lamp = lampService.create(lampDTO, realEstateId, token);
        return new ResponseEntity<>(lamp, HttpStatus.OK);
    }

    @GetMapping(value = "/getLamp/{lampId}")
    public ResponseEntity<LampDTO> getLamp(@PathVariable Integer lampId,
                                           @RequestHeader (name="Authorization") String token) {
        LampDTO lamp = lampService.getLamp(lampId, token);
        return new ResponseEntity<>(lamp, HttpStatus.OK);
    }

    @PostMapping(value = "/switchLight/{lampId}")
    public ResponseEntity<ResponseMessageDTO> switchLight(@PathVariable Integer lampId,
                                                          @RequestParam boolean on,
                                                          @RequestHeader (name="Authorization") String token)
            throws MqttException, JsonProcessingException {
        lampService.switchLight(lampId, on, false, token);
        String lightOn = on ? "on" : "off";
        return new ResponseEntity<>(new ResponseMessageDTO("Lamp light successfully turned " + lightOn), HttpStatus.OK);
    }

    @PostMapping(value = "/switchMode/{lampId}")
    public ResponseEntity<ResponseMessageDTO> switchMode(@PathVariable Integer lampId,
                                                         @RequestParam boolean automatic,
                                                         @RequestHeader (name="Authorization") String token)
            throws MqttException, JsonProcessingException {
        lampService.switchMode(lampId, automatic, token);
        String lampMode = automatic ? "automatic" : "manual";
        return new ResponseEntity<>(new ResponseMessageDTO("Lamp mode successfully set to " + lampMode), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/get-lamp-records/{lampId}")
    public ResponseEntity<List<MeasurementRecord>> getLampRecords(@PathVariable Integer lampId,
                                                                  @RequestParam String from,
                                                                  @RequestParam String to,
                                                                  @RequestParam String measurement,
                                                                  @RequestParam String aggregationWindow,
                                                                  @RequestHeader (name="Authorization") String token
                                                                  ) {
        return new ResponseEntity<>(lampService
                .findByDateRange(from, to, measurement, lampId.toString(), aggregationWindow, token),
                HttpStatus.OK);
    }
}
