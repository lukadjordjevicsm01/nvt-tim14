package com.threeamigoscoding.smartifybackend.model.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "solar_panel_systems")
public class SolarPanelSystem extends Device {

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "solar_panel_system_id")
    private Set<SolarPanel> solarPanels = new HashSet<>();

    //region Constructors

    public SolarPanelSystem(Set<SolarPanel> solarPanels) {
        this.solarPanels = solarPanels;
    }

    public SolarPanelSystem(){}

    //endregion

    //region Getters and Setters

    public Set<SolarPanel> getSolarPanels() {
        return solarPanels;
    }

    public void setSolarPanels(Set<SolarPanel> solarPanels) {
        this.solarPanels = solarPanels;
    }

    //endregion

}
