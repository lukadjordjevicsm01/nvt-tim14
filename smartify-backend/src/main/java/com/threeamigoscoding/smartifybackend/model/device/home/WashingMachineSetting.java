package com.threeamigoscoding.smartifybackend.model.device.home;

import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(name = "washing_machine_settings")
public class WashingMachineSetting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "mode_id")
    private WashingMachineMode mode;

    @Column
    private Timestamp startTime;

    //region Constructors

    public WashingMachineSetting(Integer id, WashingMachineMode mode, Timestamp startTime) {
        this.id = id;
        this.mode = mode;
        this.startTime = startTime;
    }

    public WashingMachineSetting(){}

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public WashingMachineMode getMode() {
        return mode;
    }

    public void setMode(WashingMachineMode mode) {
        this.mode = mode;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    //endregion


    @Override
    public String toString() {
        return "WashingMachineSetting{" +
                "id=" + id +
                ", mode=" + mode.getId() +
                ", startTime=" + startTime +
                '}';
    }
}
