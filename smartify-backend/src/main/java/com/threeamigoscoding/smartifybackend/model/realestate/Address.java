package com.threeamigoscoding.smartifybackend.model.realestate;

import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "addresses")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "city_id")
    @NotNull(message = "Address needs to have a city")
    private City city;

    @Column
    @NotNull(message = "Address needs to have longitude")
    @DecimalMin(value = "-180.0", inclusive = true, message = "Longitude must be greater than or equal to -180")
    @DecimalMax(value = "180.0", inclusive = true, message = "Longitude must be less than or equal to 180")
    private Double longitude;

    @Column
    @NotNull(message = "Address needs to have latitude")
    @DecimalMin(value = "-90.0", inclusive = true, message = "Latitude must be greater than or equal to -90")
    @DecimalMax(value = "90.0", inclusive = true, message = "Latitude must be less than or equal to 90")
    private Double latitude;

    @Column
    private String streetName;


    //region Constructors

    public Address() {
    }

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    //endregion

}
