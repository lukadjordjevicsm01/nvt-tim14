package com.threeamigoscoding.smartifybackend.model.device.home;

import jakarta.persistence.*;

@Entity
@Table(name = "washing_machine_modes")
public class WashingMachineMode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column
    private Double duration;

    //region Constructors

    public WashingMachineMode(Integer id, String name, Double duration) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }

    public WashingMachineMode(){}

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    //endregion


    @Override
    public String toString() {
        return "WashingMachineMode{" +
                "name='" + name + '\'' +
                ", duration=" + duration +
                '}';
    }
}
