package com.threeamigoscoding.smartifybackend.model.realestate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.threeamigoscoding.smartifybackend.model.device.Device;
import com.threeamigoscoding.smartifybackend.model.user.User;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "real_estate")
public class RealEstate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotNull
    private RealEstateType realEstateType;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "address_id")
    private Address address;

    @Column(nullable = false)
    private String picturePath;

    @Column
    @NotNull
    private RealEstateStatus status;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH, mappedBy = "realEstate")
    //@JoinColumn(name = "real_estate_id")
    private Set<Device> devices = new HashSet<>();

    @Column
    @Min(value = 0, message = "You cannot have less then 0 floors")
    @Max(value = 100, message = "You cannot have more then 100 floors")
    private Integer floors;

    @Column(name = "real_estate_name")
    @NotNull(message = "Your real estate must have a name")
    @Size(min = 1, message = "Your real estate must have a name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "owner_id")
    private User owner;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH, mappedBy = "sharedRealEstates")
    private Set<User> sharedWith = new HashSet<>();


    //region Constructors

    public RealEstate(Integer id, RealEstateType realEstateType, Address address, String picturePath, RealEstateStatus status,
                      Set<Device> devices, Integer floors, String name, User owner) {
        this.id = id;
        this.realEstateType = realEstateType;
        this.address = address;
        this.picturePath = picturePath;
        this.status = status;
        this.devices = devices;
        this.floors = floors;
        this.name = name;
        this.owner = owner;
    }

    public RealEstate() {
    }

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RealEstateType getRealEstateType() {
        return realEstateType;
    }

    public void setRealEstateType(RealEstateType realEstateType) {
        this.realEstateType = realEstateType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public RealEstateStatus getStatus() {
        return status;
    }

    public void setStatus(RealEstateStatus status) {
        this.status = status;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }

    public Integer getFloors() {
        return floors;
    }

    public void setFloors(Integer floors) {
        this.floors = floors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }


    //endregion


    @Override
    public String toString() {
        return "RealEstate{" +
                "id=" + id +
                ", realEstateType=" + realEstateType +
                ", status=" + status +
                ", floors=" + floors +
                '}';
    }

    public Set<User> getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(Set<User> sharedWith) {
        this.sharedWith = sharedWith;
    }
}
