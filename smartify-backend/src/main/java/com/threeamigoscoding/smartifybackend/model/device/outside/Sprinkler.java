package com.threeamigoscoding.smartifybackend.model.device.outside;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;

import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sprinklers")
public class Sprinkler extends Device {

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "sprinkler_id")
    private Set<SprinklerSetting> sprinklerSettings = new HashSet<>();

    @Column
    private Boolean isSprinklerOn;

    //region Constructors

    public Sprinkler(Set<SprinklerSetting> sprinklerSettings, Boolean isSprinklerOn) {
        this.sprinklerSettings = sprinklerSettings;
        this.isSprinklerOn = isSprinklerOn;
    }

    public Sprinkler(){}

    //endregion

    //region Getters and Setters

    public Set<SprinklerSetting> getSprinklerSettings() {
        return sprinklerSettings;
    }

    public void setSprinklerSettings(Set<SprinklerSetting> sprinklerSettings) {
        this.sprinklerSettings = sprinklerSettings;
    }

    public Boolean getSprinklerOn() {
        return isSprinklerOn;
    }

    public void setSprinklerOn(Boolean sprinklerOn) {
        isSprinklerOn = sprinklerOn;
    }

    //endregion

}
