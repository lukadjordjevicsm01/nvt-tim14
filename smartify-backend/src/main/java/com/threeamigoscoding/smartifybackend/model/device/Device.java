package com.threeamigoscoding.smartifybackend.model.device;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import com.threeamigoscoding.smartifybackend.model.user.User;
import jakarta.persistence.*;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import static jakarta.persistence.InheritanceType.JOINED;

@Entity
@Table(name = "devices")
@Inheritance(strategy = JOINED)
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Boolean isOnline;

    @Column
    private Boolean isOn;

    @Column
    private String name;

    @Column
    private PowerSupplyType powerSupplyType;

    @Column
    private Double consumption;

    @Column
    private Timestamp lastPing;

    @Column
    private String imagePath;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH, mappedBy = "sharedDevices")
    private Set<User> sharedWith = new HashSet<>();

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "real_estate_id")
    private RealEstate realEstate;

    //region Constructors

    public Device(Integer id, Boolean isOnline, Boolean isOn, String name, PowerSupplyType powerSupplyType,
                  Double consumption, Timestamp lastPing, String imagePath) {
        this.id = id;
        this.isOnline = isOnline;
        this.isOn = isOn;
        this.name = name;
        this.powerSupplyType = powerSupplyType;
        this.consumption = consumption;
        this.lastPing = lastPing;
        this.imagePath = imagePath;
    }

    public Device() {
    }

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public Boolean getOn() {
        return isOn;
    }

    public void setOn(Boolean on) {
        isOn = on;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Double getConsumption() {
        return consumption;
    }

    public void setConsumption(Double consumption) {
        this.consumption = consumption;
    }

    public Timestamp getLastPing() {
        return lastPing;
    }

    public void setLastPing(Timestamp lastPing) {
        this.lastPing = lastPing;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Set<User> getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(Set<User> sharedWith) {
        this.sharedWith = sharedWith;
    }

    public RealEstate getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(RealEstate realEstate) {
        this.realEstate = realEstate;
    }

    //endregion
}
