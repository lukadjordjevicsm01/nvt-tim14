package com.threeamigoscoding.smartifybackend.model.user;

import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
@Table(name = "user_activations")
public class UserActivation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(nullable = false)
    private LocalDate date;

    @Column(nullable = false)
    private LocalDate expirDate;

    public UserActivation(User user, LocalDate date, LocalDate lifetime) {
        this.id = id;
        this.user = user;
        this.date = date;
        this.expirDate = lifetime;
    }

    public UserActivation() {
    }

    //region Getters and Setters


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getExpirDate() {
        return expirDate;
    }

    public void setExpirDate(LocalDate expirDate) {
        this.expirDate = expirDate;
    }

    //endregion
}
