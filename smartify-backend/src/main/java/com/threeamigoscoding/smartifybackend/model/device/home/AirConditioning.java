package com.threeamigoscoding.smartifybackend.model.device.home;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "air_conditionings")
public class AirConditioning extends Device {
    @Column
    private Double currentTemperature;

    @Column
    private Double minTemperature;

    @Column
    private Double maxTemperature;

    @Column
    @Enumerated(EnumType.STRING)
    private AirConditioningMode currentMode;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "supported_modes", joinColumns = @JoinColumn(name = "air_conditioning_id"))
    @Column(name = "supported_mode")
    @Fetch(FetchMode.JOIN)
    private Set<AirConditioningMode> supportedModes = new HashSet<>();

    @Column
    private Boolean isSettingsActive;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "air_conditioning_id")
    private Set<AirConditioningSetting> customSettings = new HashSet<>();

    //region Constructors

    public AirConditioning(Double currentTemperature, Double minTemperature, Double maxTemperature,
                           AirConditioningMode currentMode, Set<AirConditioningMode> supportedModes,
                           Boolean isSettingsActive, Set<AirConditioningSetting> customSettings) {
        this.currentTemperature = currentTemperature;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
        this.currentMode = currentMode;
        this.supportedModes = supportedModes;
        this.isSettingsActive = isSettingsActive;
        this.customSettings = customSettings;
    }

    public AirConditioning(){}

    //endregion

    //region Getters and Setters

    public Double getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(Double currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public Double getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(Double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public Double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(Double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public AirConditioningMode getCurrentMode() {
        return currentMode;
    }

    public void setCurrentMode(AirConditioningMode currentMode) {
        this.currentMode = currentMode;
    }

    public Set<AirConditioningMode> getSupportedModes() {
        return supportedModes;
    }

    public void setSupportedModes(Set<AirConditioningMode> supportedModes) {
        this.supportedModes = supportedModes;
    }

    public Boolean getSettingsActive() {
        return isSettingsActive;
    }

    public void setSettingsActive(Boolean settingsActive) {
        isSettingsActive = settingsActive;
    }

    public Set<AirConditioningSetting> getCustomSettings() {
        return customSettings;
    }

    public void setCustomSettings(Set<AirConditioningSetting> customSettings) {
        this.customSettings = customSettings;
    }

    //endregion
}
