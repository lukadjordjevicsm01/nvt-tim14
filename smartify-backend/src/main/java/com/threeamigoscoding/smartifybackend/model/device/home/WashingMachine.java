package com.threeamigoscoding.smartifybackend.model.device.home;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "washing_machines")
public class WashingMachine extends Device {

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "mode_id")
    private WashingMachineMode currentMode;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "washing_machine_id")
    private Set<WashingMachineMode> supportedModes = new HashSet<>();

    @Column
    private Boolean isSettingsActive;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "washing_machine_id")
    private Set<WashingMachineSetting> customSettings = new HashSet<>();

    //region Constructors

    public WashingMachine(WashingMachineMode currentMode, Set<WashingMachineMode> supportedModes,
                          Boolean isSettingsActive, Set<WashingMachineSetting> customSettings) {
        this.currentMode = currentMode;
        this.supportedModes = supportedModes;
        this.isSettingsActive = isSettingsActive;
        this.customSettings = customSettings;
    }

    public WashingMachine(){}

    //endregion

    //region Getters and Setters

    public WashingMachineMode getCurrentMode() {
        return currentMode;
    }

    public void setCurrentMode(WashingMachineMode currentMode) {
        this.currentMode = currentMode;
    }

    public Set<WashingMachineMode> getSupportedModes() {
        return supportedModes;
    }

    public void setSupportedModes(Set<WashingMachineMode> supportedModes) {
        this.supportedModes = supportedModes;
    }

    public Boolean getSettingsActive() {
        return isSettingsActive;
    }

    public void setSettingsActive(Boolean settingsActive) {
        isSettingsActive = settingsActive;
    }

    public Set<WashingMachineSetting> getCustomSettings() {
        return customSettings;
    }

    public void setCustomSettings(Set<WashingMachineSetting> customSettings) {
        this.customSettings = customSettings;
    }

    //endregion
}
