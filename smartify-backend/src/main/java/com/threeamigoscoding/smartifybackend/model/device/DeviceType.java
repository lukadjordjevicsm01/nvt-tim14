package com.threeamigoscoding.smartifybackend.model.device;

public enum DeviceType {
    HOUSE,
    OUTSIDE,
    ELECTRO_ENERGETIC
}
