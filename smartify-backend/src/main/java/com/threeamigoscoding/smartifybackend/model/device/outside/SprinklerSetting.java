package com.threeamigoscoding.smartifybackend.model.device.outside;

import jakarta.persistence.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sprinkler_settings")
public class SprinklerSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Timestamp startTime;

    @Column
    private Timestamp endTime;

    @Column
    private Boolean isRepeating;

    @ElementCollection
    @CollectionTable(name = "repeat_days", joinColumns = @JoinColumn(name = "sprinkler_setting_id"))
    @Column(name = "day_of_week")
    @Fetch(FetchMode.JOIN)
    private Set<DayOfWeek> repeatDays = new HashSet<>();

    //region Getters and Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Boolean getIsRepeating() {
        return isRepeating;
    }

    public void setIsRepeating(Boolean isRepeating) {
        this.isRepeating = isRepeating;
    }

    public Set<DayOfWeek> getRepeatDays() {
        return repeatDays;
    }

    public void setRepeatDays(Set<DayOfWeek> repeatDays) {
        this.repeatDays = repeatDays;
    }

    //endregion
}
