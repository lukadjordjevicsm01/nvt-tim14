package com.threeamigoscoding.smartifybackend.model.device.home;

import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(name = "air_conditioning_settings")
public class AirConditioningSetting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @Enumerated(EnumType.STRING)
    private AirConditioningMode mode;

    @Column
    private Timestamp startTime;

    @Column
    private Timestamp endTime;

    @Column
    private Double temperature;

    //region Constructors

    public AirConditioningSetting(Integer id, AirConditioningMode mode, Timestamp startTime, Timestamp endTime) {
        this.id = id;
        this.mode = mode;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public AirConditioningSetting(){}

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AirConditioningMode getMode() {
        return mode;
    }

    public void setMode(AirConditioningMode mode) {
        this.mode = mode;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    //endregion
}
