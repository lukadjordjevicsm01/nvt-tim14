package com.threeamigoscoding.smartifybackend.model.realestate;

public enum RealEstateType {
    HOUSE,
    APARTMENT
}
