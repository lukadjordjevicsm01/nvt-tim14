package com.threeamigoscoding.smartifybackend.model.realestate;

public enum RealEstateStatus {

    PENDING,
    ACCEPTED,
    REJECTED

}
