package com.threeamigoscoding.smartifybackend.model.device.home;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;

@Entity
@Table(name = "ambient_sensors")
public class AmbientSensor extends Device {

    @Column
    private Double temperature;

    @Column
    private Double humidity;

    //region Constructors

    public AmbientSensor(Double temperature, Double humidity) {
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public AmbientSensor(){}

    //endregion

    //region Getters and Setters

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    //endregion
}
