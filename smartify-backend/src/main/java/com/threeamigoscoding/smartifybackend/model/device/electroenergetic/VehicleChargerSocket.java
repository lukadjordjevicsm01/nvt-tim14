package com.threeamigoscoding.smartifybackend.model.device.electroenergetic;

import jakarta.persistence.*;

@Entity
@Table(name = "vehicle_charger_sockets")
public class VehicleChargerSocket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column
    private Boolean isActive;

    @Column
    private Double chargingLimit;

    //region Constructors

    public VehicleChargerSocket(Integer id, String name, Boolean isActive, Double chargingLimit) {
        this.id = id;
        this.name = name;
        this.isActive = isActive;
        this.chargingLimit = chargingLimit;
    }

    public VehicleChargerSocket(){}

    //endregion

    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Double getChargingLimit() {
        return chargingLimit;
    }

    public void setChargingLimit(Double chargingLimit) {
        this.chargingLimit = chargingLimit;
    }

    //endregion

}
