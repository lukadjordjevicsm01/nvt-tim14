package com.threeamigoscoding.smartifybackend.model.device.outside;

public enum GateMode {

    MANUAL,
    PUBLIC,
    PRIVATE

}
