package com.threeamigoscoding.smartifybackend.model.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "vehicle_chargers")
public class VehicleCharger extends Device {

    @Column
    private Double chargingPower;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "vehicle_charger_id")
    private Set<VehicleChargerSocket> vehicleChargerSockets = new HashSet<>();

    //region Constructors

    public VehicleCharger(Double chargingPower, Set<VehicleChargerSocket> vehicleChargerSockets) {
        this.chargingPower = chargingPower;
        this.vehicleChargerSockets = vehicleChargerSockets;
    }

    public VehicleCharger(){}

    //endregion

    //region Getters and Setters

    public Double getChargingPower() {
        return chargingPower;
    }

    public void setChargingPower(Double chargingPower) {
        this.chargingPower = chargingPower;
    }

    public Set<VehicleChargerSocket> getVehicleChargerSockets() {
        return vehicleChargerSockets;
    }

    public void setVehicleChargerSockets(Set<VehicleChargerSocket> vehicleChargerSockets) {
        this.vehicleChargerSockets = vehicleChargerSockets;
    }

    //endregion
}
