package com.threeamigoscoding.smartifybackend.model.device.home;

public enum AirConditioningMode {
    COOLING,
    HEATING,
    AUTOMATIC,
    VENTILATION
}
