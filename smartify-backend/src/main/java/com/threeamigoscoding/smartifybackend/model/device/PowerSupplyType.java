package com.threeamigoscoding.smartifybackend.model.device;

public enum PowerSupplyType {

    AUTONOMOUSLY,
    HOUSE_BATTERY,
    MAINS

}
