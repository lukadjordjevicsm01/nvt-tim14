package com.threeamigoscoding.smartifybackend.model.realestate;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "rejection_letters")
public class RejectionLetter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "realEstate_id")
    @NotNull(message = "Rejection letter is not usable without real estate")
    private RealEstate realEstate;

    @Column
    @NotNull(message = "Real estate rejection letter must come with a reason")
    @Size(min = 1, message = "Real estate rejection letter must come with a reason")
    private String reason;

    //region Constructors

    public RejectionLetter(RealEstate realEstate, String reason) {
        this.realEstate = realEstate;
        this.reason = reason;
    }

    public RejectionLetter() {
    }

    //endregion


    //region Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RealEstate getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(RealEstate realEstate) {
        this.realEstate = realEstate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    //endregion

}
