package com.threeamigoscoding.smartifybackend.model.device.outside;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "gates")
public class Gate extends Device {
    @Column
    private Boolean isOpen;

    @Column
    private GateMode mode;

    @ElementCollection
    @CollectionTable(name = "allowed_vehicles", joinColumns = @JoinColumn(name = "gate_id"))
    @Column(name = "vehicle")
    @Fetch(FetchMode.JOIN)
    private Set<String> allowedVehicles = new HashSet<>();

    //region Constructors

    public Gate(Boolean isOpen, GateMode isPublic, Set<String> allowedVehicles) {
        this.isOpen = isOpen;
        this.mode = isPublic;
        this.allowedVehicles = allowedVehicles;
    }

    public Gate(){}

    //endregion

    //region Getters and Setters

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public GateMode getMode() {
        return mode;
    }

    public void setMode(GateMode aPublic) {
        mode = aPublic;
    }

    public Set<String> getAllowedVehicles() {
        return allowedVehicles;
    }

    public void setAllowedVehicles(Set<String> allowedVehicles) {
        this.allowedVehicles = allowedVehicles;
    }

    //endregion

}
