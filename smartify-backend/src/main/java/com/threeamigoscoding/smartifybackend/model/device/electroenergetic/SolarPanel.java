package com.threeamigoscoding.smartifybackend.model.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;

@Entity
@Table(name = "solar_panels")
public class SolarPanel extends Device {

    @Column
    private Double area;

    @Column
    private Double efficiency;

    //region Constructors

    public SolarPanel(Double area, Double efficiency) {
        this.area = area;
        this.efficiency = efficiency;
    }

    public SolarPanel(){}

    //endregion

    //region Getters and Setters

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(Double efficiency) {
        this.efficiency = efficiency;
    }

    //endregion
}
