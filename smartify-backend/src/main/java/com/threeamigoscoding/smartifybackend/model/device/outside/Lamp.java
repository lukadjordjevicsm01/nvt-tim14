package com.threeamigoscoding.smartifybackend.model.device.outside;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;

@Entity
@Table(name = "lamps")
public class Lamp extends Device {
    @Column
    private Double currentBrightness;

    @Column
    private Boolean isAutomatic;

    @Column
    private Boolean isLightOn;

    //region Constructors

    public Lamp(Double currentBrightness, Boolean isAutomatic, Boolean isLightOn) {
        this.currentBrightness = currentBrightness;
        this.isAutomatic = isAutomatic;
        this.isLightOn = isLightOn;
    }

    public Lamp(){}

    //endregion

    //region Getters and Setters

    public Double getCurrentBrightness() {
        return currentBrightness;
    }

    public void setCurrentBrightness(Double currentBrightness) {
        this.currentBrightness = currentBrightness;
    }

    public Boolean getAutomatic() {
        return isAutomatic;
    }

    public void setAutomatic(Boolean automatic) {
        isAutomatic = automatic;
    }

    public Boolean getLightOn() {
        return isLightOn;
    }

    public void setLightOn(Boolean lightOn) {
        isLightOn = lightOn;
    }

    //endregion

}
