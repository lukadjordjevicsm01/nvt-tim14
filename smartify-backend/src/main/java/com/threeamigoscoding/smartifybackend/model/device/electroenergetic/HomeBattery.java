package com.threeamigoscoding.smartifybackend.model.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.model.device.Device;
import jakarta.persistence.*;

@Entity
@Table(name = "home_batteries")
public class HomeBattery extends Device {

    @Column
    private Double capacity;

    @Column
    private Double currentEnergy;

    //region Constructors

    public HomeBattery(Double capacity, Double currentEnergy) {
        this.capacity = capacity;
        this.currentEnergy = currentEnergy;
    }

    public HomeBattery(){}

    //endregion

    //region Getters and Setters

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public Double getCurrentEnergy() {
        return currentEnergy;
    }

    public void setCurrentEnergy(Double currentEnergy) {
        this.currentEnergy = currentEnergy;
    }

    //endregion
}
