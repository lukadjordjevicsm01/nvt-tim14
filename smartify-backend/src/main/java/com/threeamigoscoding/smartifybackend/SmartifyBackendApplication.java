package com.threeamigoscoding.smartifybackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class SmartifyBackendApplication {

	@Bean
	public ObjectMapper objectMapper(){
		return new ObjectMapper();
	}
	public static void main(String[] args) {
		SpringApplication.run(SmartifyBackendApplication.class, args);
	}

}
