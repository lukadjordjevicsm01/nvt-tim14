package com.threeamigoscoding.smartifybackend.mappers.realestate;

import com.threeamigoscoding.smartifybackend.dto.realestate.CityDTO;
import com.threeamigoscoding.smartifybackend.dto.realestate.CountryDTO;
import com.threeamigoscoding.smartifybackend.model.realestate.City;
import com.threeamigoscoding.smartifybackend.model.realestate.Country;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    CountryDTO countryToCountryDTO(Country country);

    CityDTO cityToCityDTO(City city);

}
