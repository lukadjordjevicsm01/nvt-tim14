package com.threeamigoscoding.smartifybackend.mappers.device.home;

import com.threeamigoscoding.smartifybackend.dto.device.home.AirConditioningDTO;
import com.threeamigoscoding.smartifybackend.model.device.home.AirConditioning;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AirConditioningMapper {

    AirConditioning dtoToEntity(AirConditioningDTO airConditioningDTO);

}
