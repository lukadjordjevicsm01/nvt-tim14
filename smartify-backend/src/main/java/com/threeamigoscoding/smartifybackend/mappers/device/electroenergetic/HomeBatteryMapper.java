package com.threeamigoscoding.smartifybackend.mappers.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.HomeBatteryDTO;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.HomeBattery;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HomeBatteryMapper {

    HomeBattery dtoToEntity(HomeBatteryDTO homeBatteryDTO);

    HomeBatteryDTO entityToDto(HomeBattery homeBattery);

}
