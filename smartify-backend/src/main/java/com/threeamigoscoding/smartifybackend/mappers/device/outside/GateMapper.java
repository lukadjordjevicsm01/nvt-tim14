package com.threeamigoscoding.smartifybackend.mappers.device.outside;

import com.threeamigoscoding.smartifybackend.dto.device.outside.GateDTO;
import com.threeamigoscoding.smartifybackend.model.device.outside.Gate;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GateMapper {

    Gate dtoToEntity(GateDTO gateDTO);
    GateDTO entityToDto(Gate gate);
}
