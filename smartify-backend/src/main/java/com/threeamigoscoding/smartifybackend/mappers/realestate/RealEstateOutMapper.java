package com.threeamigoscoding.smartifybackend.mappers.realestate;

import com.threeamigoscoding.smartifybackend.dto.realestate.RealEstateOutDTO;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RealEstateOutMapper {

    public RealEstateOutDTO entityToDTO(RealEstate realEstate);

}
