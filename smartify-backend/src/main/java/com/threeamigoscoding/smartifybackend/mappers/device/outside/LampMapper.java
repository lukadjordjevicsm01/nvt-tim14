package com.threeamigoscoding.smartifybackend.mappers.device.outside;

import com.threeamigoscoding.smartifybackend.dto.device.outside.LampDTO;
import com.threeamigoscoding.smartifybackend.model.device.outside.Lamp;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LampMapper {

    Lamp dtoToEntity(LampDTO lampDTO);
    LampDTO entityToDto(Lamp lamp);
}
