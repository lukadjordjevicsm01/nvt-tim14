package com.threeamigoscoding.smartifybackend.mappers.device.home;

import com.threeamigoscoding.smartifybackend.dto.device.home.WashingMachineDTO;
import com.threeamigoscoding.smartifybackend.model.device.home.WashingMachine;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface WashingMachineMapper {
    WashingMachine dtoToEntity(WashingMachineDTO washingMachineDTO);

    WashingMachineDTO endtityToDto(WashingMachine washingMachine);
}
