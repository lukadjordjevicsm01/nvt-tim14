package com.threeamigoscoding.smartifybackend.mappers.device.home;

import com.threeamigoscoding.smartifybackend.dto.device.home.AmbientSensorDTO;
import com.threeamigoscoding.smartifybackend.model.device.home.AmbientSensor;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AmbientSensorMapper {

    AmbientSensor dtoToEntity(AmbientSensorDTO ambientSensorDTO);

    AmbientSensorDTO entityToDto(AmbientSensor ambientSensor);
}
