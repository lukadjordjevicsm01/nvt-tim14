package com.threeamigoscoding.smartifybackend.mappers.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.VehicleChargerDTO;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.VehicleCharger;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VehicleChargerMapper {

    VehicleCharger dtoToEntity(VehicleChargerDTO vehicleChargerDTO);
    VehicleChargerDTO entityToDto(VehicleCharger vehicleCharger);
}
