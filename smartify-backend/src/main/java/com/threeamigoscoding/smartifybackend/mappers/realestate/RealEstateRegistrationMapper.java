package com.threeamigoscoding.smartifybackend.mappers.realestate;

import com.threeamigoscoding.smartifybackend.dto.realestate.RealEstateRegistrationDTO;
import com.threeamigoscoding.smartifybackend.model.realestate.RealEstate;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RealEstateRegistrationMapper {

    RealEstate dtoToEntity(RealEstateRegistrationDTO realEstateRegistrationDTO);
}
