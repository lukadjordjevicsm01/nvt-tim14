package com.threeamigoscoding.smartifybackend.mappers.device.outside;

import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerDTO;
import com.threeamigoscoding.smartifybackend.dto.device.outside.SprinklerSettingDTO;
import com.threeamigoscoding.smartifybackend.model.device.outside.Sprinkler;
import com.threeamigoscoding.smartifybackend.model.device.outside.SprinklerSetting;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SprinklerMapper {

    Sprinkler dtoToEntity(SprinklerDTO sprinklerDTO);
    SprinklerDTO entityToDto(Sprinkler sprinkler);

    SprinklerSettingDTO sprinklerSettingEntityToDto(SprinklerSetting sprinklerSetting);

}
