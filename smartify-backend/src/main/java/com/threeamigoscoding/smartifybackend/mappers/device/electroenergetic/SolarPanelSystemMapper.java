package com.threeamigoscoding.smartifybackend.mappers.device.electroenergetic;

import com.threeamigoscoding.smartifybackend.dto.device.electroenergetic.SolarPanelSystemDTO;
import com.threeamigoscoding.smartifybackend.model.device.electroenergetic.SolarPanelSystem;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SolarPanelSystemMapper {

    SolarPanelSystem dtoToEntity(SolarPanelSystemDTO solarPanelSystemDTO);
    SolarPanelSystemDTO entityToDto(SolarPanelSystem solarPanelSystem);

}
