package com.threeamigoscoding.smartifybackend.mappers.user;

import com.threeamigoscoding.smartifybackend.dto.user.UserDTO;
import com.threeamigoscoding.smartifybackend.dto.user.UserOutDTO;
import com.threeamigoscoding.smartifybackend.model.user.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User dtoToEntity(UserDTO userDTO);

    UserDTO entityToDto(User user);

    UserOutDTO entityToOutDto(User user);

}
