package com.threeamigoscoding.smartifybackend.mappers.device;

import com.threeamigoscoding.smartifybackend.dto.device.DeviceDTO;
import com.threeamigoscoding.smartifybackend.model.device.Device;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DeviceMapper {

    Device dtoToEntity(DeviceDTO deviceDTO);
    DeviceDTO entityToDto(Device device);

}
