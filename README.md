# Smartify
## Introduction
Smartify is a sophisticated software solution designed to facilitate the monitoring of smart homes within smart cities. This project entails the development of a user-friendly platform accessible to homeowners for real-time monitoring of their properties. The system supports a diverse range of smart devices, enabling users to track essential metrics such as temperature, humidity, and electricity consumption. Moreover, it provides features for both manual and automatic control of devices, offering convenience and efficiency to users. Administrators can leverage the platform to monitor energy consumption across real estates or entire cities, contributing to better resource management and sustainability efforts. 

## User Roles
The platform distinguishes between the following types of users:
### Regular Registered User:
- Can register a new smart property (house or apartment) and smart devices within their property.
- Monitors the operation of various devices within their smart property and manages them.
### Administrator:
- Has real-time monitoring and management privileges using a variety of graphical elements.
- Cannot execute any functionalities reserved for regular users, while regular users cannot access functionalities exclusive to administrators.
### Unauthenticated User:
- Has the option to register and log in to the system.

## Smart Device Categories
For the purpose of this project and task delegation during the software development, smart devices are categorized into the following 3 groups:
### Smart Home Appliances 
- Devices located within the home/apartment.
### External Smart Devices 
- Devices situated outside the home/apartment (on balconies, in gardens, etc.).
### Major Power Consumption Devices
- Devices representing significant producers or consumers of electrical energy.

## Smart Device Functionalities:

### Common to All Devices:
  - Periodically reports online status to the platform.
  - Communicates with the platform via MQTT protocol.
  
### Smart Home Appliances:
- Ambient sensor:
  - Reports current room temperature and humidity periodically.
- Air conditioning:
  - Supports various operating modes and temperature ranges.
  - Users can start/stop device operation, change operating modes, and set temperatures.
  - Users can define special operating schedules.
- Washing machine:
  - Supports multiple washing cycles.
  - Users can start a cycle or schedule its start time.
   
### External Smart Devices:
- Lamp:
  - Users can turn the lamp on/off.
  - Lamp has an automatic mode based on ambient light levels.
- Vehicle gate:
  - Users can open/close the gate and switch between public and private modes.
  - Gate has a camera and proximity sensor for vehicle detection.
- Sprinkler system:
  - Users can turn the system on/off and define special operating modes or schedules.

### Major Power Consumption Devices:
- Solar panel system:
  - Consists of multiple panels producing electricity based on weather conditions.
  - Periodically reports energy production.
- Home battery:
  - Stores excess energy from solar panels.
  - Reports current usage and status periodically.
- Vehicle charger:
  - Supports charging multiple vehicles simultaneously.
  - Records charging start and stop times, along with energy consumption.
  - Stops charging when the specified battery level is reached.

## Tech Stack
<div align="center">
  <a href="https://github-readme-tech-stack.vercel.app">
    <img src="https://github-readme-tech-stack.vercel.app/api/cards?title=Tech+Stack&align=center&titleAlign=center&lineCount=2&bg=%230D1117&badge=%23161B22&border=%2321262D&titleColor=%2358A6FF&line1=spring%2Cspring+boot%2C4ee248%3Breact%2Creact%2C58a6ff%3Bpython%2Cpython%2Cffdd54%3Bdocker%2Cdocker%2C0db7ed%3B&line2=nginx%2Cnginx%2C35ac2b%3Bmysql%2Cmysql%2C00758F%3Binfluxdb%2Cinfluxdb%2C612791%3Bmui%2Cmui%2C2b80be%3B" alt="Tech Stack" />
  </a>
</div>

## Requirements for the system
- Docker
- Nodejs
- Python 3.10+
- Java openJDK-17 + 
- Maven
- MySQL Server running with remote access allowed

## Smartify System Setup Guide

To run the Smartify system, follow the instructions below based on your preferred option.

### Option 1: Run Every Component Using Docker

1. Position to the root directory of the system:

   ```bash
   cd path_to_downloaded_repo/nvt-tim14
   ```

2. Build the React app:

   ```bash
   cd smartify-frontend
   vite build
   ```

3. Build the Spring Boot app:

   ```bash
   cd smartify-backend
   ./mvnw clean install
   ```

4. Run the system using Docker:

   ```bash
   docker-compose up
   ```

### Option 2: Run Backend and Simulator Components on Local Machine

1. Position to the root directory of the system:

   ```bash
   cd path_to_downloaded_repo/nvt-tim14
   ```

2. Build the React app:

   ```bash
   cd smartify-frontend
   vite build
   ```

3. Remove backend and simulator services from `docker-compose.yml`.

4. Run Docker Compose:

   ```bash
   docker-compose up
   ```

5. Position to the simulator folder and set up the virtual environment:

   ```bash
   cd smartify-simulator
   virtualenv venv
   ```

6. Activate the virtual environment:

   ```bash
   venv\Scripts\activate
   ```

7. Install simulator requirements:

   ```bash
   pip install -r requirements.txt
   ```

8. Run the simulator app:

   ```bash
   python run.py
   ```

9. Position to the backend folder and run the backend component:

   ```bash
   cd smartify-backend
   mvn spring-boot:run
   ```

Type `localhost:80` into your browser, and you are good to go! The Smartify system should now be running on your local machine.

## OpenAPI specification
To view our open API specification visit http://localhost:8080/swagger-ui/index.html

## Authors
- [Miloš Čuturić](https://github.com/cuturic01)
- [Luka Đorđević](https://github.com/lukaDjordjevic01)
- [Marko Janošević](https://github.com/janosevicsm)

## Academic Context
Smartify was developed for the purposes of the course [Advanced Web Technologies](http://www.ftn.uns.ac.rs/812852485/napredne-veb-tehnologije).
### Course Assistant
- Vanja Mijatov
### Course Professor
- Branko Milosavljević