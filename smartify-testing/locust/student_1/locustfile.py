from locust import HttpUser, task, between
import os
import random
import string


class ClientDbExampleUser(HttpUser):
    wait_time = between(1, 5)
    host = "http://localhost:8080/api"
    headers = None

    def login(self, email="marko@mail.com", password="marko123"):
        login_response = self.client.post(f"/user/login", json={"email": email, "password": password})
        if login_response.status_code == 200:
            auth_token = login_response.json().get("accessToken")
            headers = {"Authorization": f"Bearer {auth_token}"}
            return headers

    # @task
    def login_scenario(self):
        self.login()

    # @task
    def register_scenario(self):
        random_mail = ''.join(random.choices(string.ascii_letters + string.digits, k=10))
        random_mail += "@mail.com"
        form_data = {
            "name": "User",
            "surname": "User",
            "email": random_mail,
            "password": "User12345678!",
        }
        file_path = './user.jpg'
        with open(file_path, 'rb') as image_file:
            profile_picture = {'profilePicture': (os.path.basename(file_path), image_file)}

            self.client.post(f"/user/register", data=form_data, files=profile_picture)

    # @task
    def register_admin_scenario(self):
        headers = self.login("admin", "rn3u0aRpftrtIDYm")
        random_mail = ''.join(random.choices(string.ascii_letters + string.digits, k=10))
        random_mail += "@mail.com"
        form_data = {
            "name": "User",
            "surname": "User",
            "email": random_mail,
            "password": "User12345678!",
        }
        file_path = './user.jpg'
        with open(file_path, 'rb') as image_file:
            profile_picture = {'profilePicture': (os.path.basename(file_path), image_file)}
            self.client.post(f"/user/register-admin", headers=headers, data=form_data, files=profile_picture)

    # @task
    def ambient_sensor_events_scenario(self):
        headers = self.login()
        ambient_sensor_response = self.client.get(f"/device/ambientSensor/getAmbientSensor/1", headers=headers)
        if ambient_sensor_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()",
                "measurement": "temperature",
                "aggregationWindow": "none",
            }
            self.client.get(f"/device/ambientSensor/get-ambient-sensor-records/1", headers=headers, params=params)
        else:
            print("Failed to get ambient sensor data")

    # @task
    def air_conditioning_events_scenario(self):
        headers = self.login()
        air_conditioning_response = self.client.get(f"/device/airConditioning/getAirConditioning/8", headers=headers)
        if air_conditioning_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()"
            }
            self.client.get(f"/device/airConditioning/getRecords/8", headers=headers, params=params)
        else:
            print("Failed to get air conditioning data")

    # @task
    def set_air_conditioning_mode_scenario(self):
        headers = self.login()
        air_conditioning_response = self.client.get(f"/device/airConditioning/getAirConditioning/8", headers=headers)
        if air_conditioning_response.status_code == 200:
            body = {
                "airConditioningMode": "COOLING",
                "currentTemperature": 20,
                "email": "marko@mail.com"
            }
            self.client.post(f"/device/airConditioning/setAction/8", headers=headers, json=body)
        else:
            print("Failed to get air conditioning data")

    # @task
    def washing_machine_events_scenario(self):
        headers = self.login()
        washing_machine_response = self.client.get(f"/device/washingMachine/getWashingMachine/11", headers=headers)
        if washing_machine_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()"
            }
            self.client.get(f"/device/washingMachine/getRecords/11", headers=headers, params=params)
        else:
            print("Failed to get washing machine data")

    # @task
    def set_washing_machine_mode_scenario(self):
        headers = self.login()
        washing_machine_response = self.client.get(f"/device/washingMachine/getWashingMachine/11", headers=headers)
        if washing_machine_response.status_code == 200:
            body = {
                "washingMachineModeName": "rinse",
                "duration": 20,
                "email": "marko@mail.com"
            }
            self.client.post(f"/device/washingMachine/setAction/11", headers=headers, json=body)
        else:
            print("Failed to get washing machine data")

    # @task
    def grant_permission_scenario(self):
        headers = self.login()
        params = {
            "userEmail": "pera@mail.com"
        }
        self.client.post(f"/device/grantPermission/11", headers=headers, params=params)

    # @task
    def get_shared_with_by_real_estate_scenario(self):
        headers = self.login()
        self.client.get(f"/realEstate/getSharedWith/1", headers=headers)

# ------------------------------------------ PERFORMANCE TESTING -------------------------------------------------------
    # @task
    def device_switch_performance(self):
        headers = self.login()
        params = {
            "userEmail": "marko@mail.com"
        }
        self.client.post(f"/device/switch/1", headers=headers, params=params)

    # @task
    def air_conditioning_set_action_performance(self):
        body = {
            "airConditioningMode": "COOLING",
            "currentTemperature": 20,
            "email": "marko@mail.com"
        }
        self.client.post(f"/device/airConditioning/setAction/8", headers=self.headers, json=body)

    # @task
    def washing_machine_set_action_performance(self):
        body = {
            "washingMachineModeName": "rinse",
            "duration": 20,
            "email": "marko@mail.com"
        }
        self.client.post(f"/device/washingMachine/setAction/11", headers=self.headers, json=body)

    def on_start(self):
        self.headers = self.login()
