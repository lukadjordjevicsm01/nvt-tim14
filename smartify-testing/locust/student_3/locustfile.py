from locust import HttpUser, task, between


class ClientDbExampleUser(HttpUser):
    wait_time = between(1, 5)
    host = "http://localhost:8080/api"
    headers = None

    def login(self, email="marko@mail.com", password="marko123"):
        login_response = self.client.post(f"/user/login", json={"email": email, "password": password})
        if login_response.status_code == 200:
            auth_token = login_response.json().get("accessToken")
            headers = {"Authorization": f"Bearer {auth_token}"}
            return headers

    # @task
    def ambient_sensor_create_scenario(self):
        headers = self.login()
        body = {
            "isOnline": True,
            "isOn:": True,
            "name": "Ambient Sensor Test",
            "powerSupplyType": "AUTONOMOUSLY",
            "consumption": 0.01,
            "imagePath": "",
            "deviceType": ""
        }
        self.client.post(f"/device/ambientSensor/create/1", json=body, headers=headers)

    # @task
    def lamp_create_scenario(self):
        headers = self.login()
        body = {
            "isOnline": True,
            "isOn:": True,
            "name": "Lamp Test",
            "powerSupplyType": "AUTONOMOUSLY",
            "consumption": 0.1,
            "imagePath": "",
            "deviceType": "",
            "currentBrightness": 0,
            "isAutomatic": False,
            "isLightOn": False
        }
        self.client.post(f"/device/lamp/create/1", json=body, headers=headers)

    # @task
    def vehicle_charger_create_scenario(self):
        headers = self.login()
        body = {
            "isOnline": True,
            "isOn:": True,
            "name": "Vehicle Charger Test",
            "powerSupplyType": "HOUSE_BATTERY",
            "consumption": 1,
            "imagePath": "",
            "deviceType": "",
            "chargingPower": 10,
            "socketNumber": 3,
            "vehicleChargerSockets": []
        }
        self.client.post(f"/device/vehicleCharger/create/1", json=body, headers=headers)

    # @task
    def get_solar_panel_system_events_scenario(self):
        headers = self.login()
        solar_panel_system_response = self.client.get(f"/device/solarPanelSystem/getSolarPanelSystem/4",
                                                      headers=headers)
        if solar_panel_system_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()",
            }
            self.client.get(f"/device/solarPanelSystem/getSolarPanelSystemEvents/4", headers=headers, params=params)
        else:
            print("Failed to get solar panel system data")

    # @task
    def get_home_battery_consumption_records_scenario(self):
        headers = self.login()
        home_battery_response = self.client.get(f"/device/homeBattery/getHomeBattery/9", headers=headers)
        if home_battery_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()",
            }
            self.client.get(f"/device/homeBattery/getConsumptionRecords/9", headers=headers, params=params)
        else:
            print("Failed to get home battery data")

    # @task
    def get_vehicle_charger_events_scenario(self):
        headers = self.login()
        vehicle_charger_response = self.client.get(f"/device/vehicleCharger/getVehicleCharger/12", headers=headers)
        if vehicle_charger_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()",
            }
            self.client.get(f"/device/vehicleCharger/getVehicleChargerRecords/12", headers=headers, params=params)
        else:
            print("Failed to get vehicle charger data")

    # @task
    def start_charging_scenario(self):
        headers = self.login()
        vehicle_charger_response = self.client.get(f"/device/vehicleCharger/getVehicleCharger/12", headers=headers)
        if vehicle_charger_response.status_code == 200:
            body = {
                "vehicleChargerId": 12,
                "chargeSocketName": "Vehicle Charger 1_Socket_1",
                "batteryCapacity": 100,
                "currentBatteryPercentage": 0,
                "chargePercentageLimit": 100,
                "userEmail": "marko@mail.com"
            }

            self.client.post(f"/device/vehicleCharger/startCharging", headers=headers, json=body)
        else:
            print("Failed to get vehicle charger data")

    # @task
    def stop_charging_scenario(self):
        headers = self.login()
        vehicle_charger_response = self.client.get(f"/device/vehicleCharger/getVehicleCharger/12", headers=headers)
        if vehicle_charger_response.status_code == 200:
            body = {
                "vehicleChargerId": 12,
                "chargeSocketName": "Vehicle Charger 1_Socket_1",
                "batteryCapacity": 100,
                "currentBatteryPercentage": 0,
                "chargePercentageLimit": 100,
                "userEmail": "marko@mail.com"
            }

            self.client.post(f"/device/vehicleCharger/finishCharging", headers=headers, json=body)
        else:
            print("Failed to get vehicle charger data")

    # @task
    def get_electricity_usage_by_real_estate_scenario(self):
        headers = self.login("milos@mail.com", "milos123")
        real_estates_response = self.client.get(f"/realEstate/accepted", headers=headers)
        if real_estates_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()",
            }
            self.client.get(f"/device/homeBattery/getElectricityUsageByRealEstate/1", headers=headers, params=params)
        else:
            print("Failed to real estates data")

    # @task
    def get_electricity_usage_by_city_scenario(self):
        headers = self.login("milos@mail.com", "milos123")
        cities_response = self.client.get(f"/realEstate/cities", headers=headers)
        if cities_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()",
            }
            self.client.get(f"/device/homeBattery/getElectricityUsageByCity/1", headers=headers, params=params)
        else:
            print("Failed to get cities data")

# ------------------------------------------ PERFORMANCE TESTING -------------------------------------------------------
    # @task
    def device_switch_performance(self):
        params = {
            "userEmail": "marko@mail.com"
        }
        self.client.post(f"/device/switch/4", headers=self.headers, params=params)

    # @task
    def start_charging_performance(self):
        body = {
            "vehicleChargerId": 12,
            "chargeSocketName": "Vehicle Charger 1_Socket_1",
            "batteryCapacity": 100,
            "currentBatteryPercentage": 0,
            "chargePercentageLimit": 100,
            "userEmail": "marko@mail.com"
        }
        self.client.post(f"/device/vehicleCharger/startCharging", headers=self.headers, json=body)

    @task
    def stop_charging_performance(self):
        body = {
            "vehicleChargerId": 12,
            "chargeSocketName": "Vehicle Charger 1_Socket_1",
            "batteryCapacity": 100,
            "currentBatteryPercentage": 0,
            "chargePercentageLimit": 100,
            "userEmail": "marko@mail.com"
        }

        self.client.post(f"/device/vehicleCharger/finishCharging", headers=self.headers, json=body)

    def on_start(self):
        self.headers = self.login()
