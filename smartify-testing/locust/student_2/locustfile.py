from locust import HttpUser, task, between
import os


class ClientDbExampleUser(HttpUser):
    wait_time = between(1, 5)
    host = "http://localhost:8080/api"
    headers = None

    def login(self, email="marko@mail.com", password="marko123"):
        login_response = self.client.post(f"/user/login", json={"email": email, "password": password})
        if login_response.status_code == 200:
            auth_token = login_response.json().get("accessToken")
            headers = {"Authorization": f"Bearer {auth_token}"}
            return headers

    # @task
    def real_estate_registration_scenario(self):
        headers = self.login()
        form_data = {
            "realEstateType": "APARTMENT",
            "address.cityId": 1,
            "address.longitude": 45.6789,
            "address.latitude": -78.9012,
            "address.streetName": "Example Street",
            "floors": 1,
            "userId": 3,
            "name": "My first real estate"
        }
        file_path = './apartment.jpg'
        with open(file_path, 'rb') as image_file:
            real_estate_picture = {'realEstatePicture': (os.path.basename(file_path), image_file)}

            self.client.post(f"/realEstate/register", data=form_data, headers=headers, files=real_estate_picture)

    # @task
    def get_lamp_brightness_scenario(self):
        headers = self.login()
        lamp_response = self.client.get(f"/device/lamp/getLamp/2", headers=headers)
        if lamp_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()",
                "measurement": "Brightness",
                "aggregationWindow": "none",
            }
            self.client.get(f"/device/lamp/get-lamp-records/2", headers=headers, params=params)
        else:
            print("Failed to get lamp data")

    # @task
    def lamp_mode_change_scenario(self):
        headers = self.login()
        lamp_response = self.client.get(f"/device/lamp/getLamp/2", headers=headers)
        if lamp_response.status_code == 200:
            params = {
                "automatic": False,
            }
            self.client.post(f"/device/lamp/switchMode/2", headers=headers, params=params)
        else:
            print("Failed to get lamp data")

    # @task
    def gate_events_scenario(self):
        headers = self.login()
        gate_response = self.client.get(f"/device/gate/getGate/3", headers=headers)
        if gate_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()"
            }
            self.client.get(f"/device/gate/getGateEvents/3", headers=headers, params=params)
        else:
            print("Failed to get gate data")

    # @task
    def change_gate_mode_scenario(self):
        headers = self.login()
        gate_response = self.client.get(f"/device/gate/getGate/3", headers=headers)
        if gate_response.status_code == 200:
            params = {
                "gateMode": "MANUAL",
                "email": "marko@mail.com"
            }
            self.client.post(f"/device/gate/updateMode/3", headers=headers, params=params)
        else:
            print("Failed to get gate data")

    # @task
    def update_allowed_vehicles_scenario(self):
        headers = self.login()
        gate_response = self.client.get(f"/device/gate/getGate/3", headers=headers)
        if gate_response.status_code == 200:
            body = ["SM123AB", "BG123AB", "NS123AB"]
            self.client.post(f"/device/gate/updateVehicles/3", headers=headers, json=body)
        else:
            print("Failed to get gate data")

    # @task
    def sprinkler_events_scenario(self):
        headers = self.login()
        sprinkler_response = self.client.get(f"/device/sprinkler/get-sprinkler/10", headers=headers)
        if sprinkler_response.status_code == 200:
            params = {
                "from": "-1h",
                "to": "now()"
            }
            self.client.get(f"/device/sprinkler/getSprinklerEvents/10", headers=headers, params=params)
        else:
            print("Failed to get sprinkler data")

    # @task
    def sprinkler_switch_scenario(self):
        headers = self.login()
        sprinkler_response = self.client.get(f"/device/sprinkler/get-sprinkler/10", headers=headers)
        if sprinkler_response.status_code == 200:
            params = {
                "email": "marko@mail.com",
                "sprinklerOn": True
            }
            self.client.post(f"/device/sprinkler/switchSprinkler/10", headers=headers, params=params)
        else:
            print("Failed to get sprinkler data")

    # @task
    def show_device_availability_scenario(self):
        headers = self.login()
        sprinkler_response = self.client.get(f"/device/sprinkler/get-sprinkler/10", headers=headers)
        if sprinkler_response.status_code == 200:
            params = {
                "from": "-6h",
                "to": "now()",
                "aggregationWindow": "1h",
            }
            self.client.get(f"/device/availabilityRecords/10", headers=headers, params=params)
        else:
            print("Failed to get sprinkler data")

    # @task
    def get_real_estates_by_onwer_scenario(self):
        headers = self.login()
        self.client.get(f"/realEstate/3", headers=headers)

# ------------------------------------------ PERFORMANCE TESTING -------------------------------------------------------
    # @task
    def lamp_switch_mode_performance(self):
        params = {
            "automatic": False,
        }
        self.client.post(f"/device/lamp/switchMode/2", headers=self.headers, params=params)

    # @task
    def lamp_switch_light_performance(self):
        params = {
            "on": True,
        }
        self.client.post(f"/device/lamp/switchLight/2", headers=self.headers, params=params)

    # @task
    def update_allowed_vehicles_performance(self):
        body = ["SM123AB", "BG123AB", "NS123AB"]
        self.client.post(f"/device/gate/updateVehicles/3", headers=self.headers, json=body)

    # @task
    def change_gate_state_performance(self):
        params = {
            "open": True,
            "email": "marko@mail.com"
        }
        self.client.post(f"/device/gate/changeGateState/3", headers=self.headers, params=params)

    # @task
    def change_gate_mode_performance(self):
        params = {
            "gateMode": "MANUAL",
            "email": "marko@mail.com"
        }
        self.client.post(f"/device/gate/updateMode/3", headers=self.headers, params=params)

    @task
    def sprinkler_switch_performance(self):
        params = {
            "email": "marko@mail.com",
            "sprinklerOn": True
        }
        self.client.post(f"/device/sprinkler/switchSprinkler/10", headers=self.headers, params=params)

    def on_start(self):
        self.headers = self.login()
