import time

from flask import Flask, render_template, request
from flask_socketio import SocketIO, join_room, leave_room, send

app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins="http://localhost:5173")


@socketio.on('subscribe')
def handle_subscribe(data):
    topic = data['topic']
    print(f'Client subscribed to topic: {topic}')
    join_room(topic)
    i = 1
    while True:
        # handle_send_message({"topic": topic, "message": i})
        send("Client " + str(i) + ' has entered ' + topic, to=topic)
        time.sleep(2)
        i += 1


@socketio.on('unsubscribe')
def handle_unsubscribe(data):
    topic = data['topic']
    print(f'Client unsubscribed from topic: {topic}')
    leave_room(topic)


@socketio.on('send_message')
def handle_send_message(data):
    topic = data['topic']
    message = data['message']
    # print(f'Received message on topic {topic}: {message}')
    socketio.emit('message', {'topic': topic, 'message': message}, room=topic)


if __name__ == '__main__':
    socketio.run(app, debug=True)
