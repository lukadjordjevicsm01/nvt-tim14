import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import time
from flask import Flask

from controller.air_conditioning_controller import air_conditioning_controller
from controller.ambient_sensor_controller import ambient_sensor_controller

from controller.gate_controller import gate_controller
from controller.lamp_controller import lamp_controller
from controller.sprinkler_controller import sprinkler_controller

from controller.solar_panel_system_controller import solar_panel_system_controller
from controller.home_battery_controller import home_battery_controller
from controller.vehicle_charger_controller import vehicle_charger_controller
from controller.washing_machine_controller import washing_machine_controller
from controller.device_controller import device_controller

app = Flask(__name__)

app.register_blueprint(ambient_sensor_controller)
app.register_blueprint(air_conditioning_controller)
app.register_blueprint(washing_machine_controller)

app.register_blueprint(lamp_controller)
app.register_blueprint(gate_controller)
app.register_blueprint(sprinkler_controller)

app.register_blueprint(solar_panel_system_controller)
app.register_blueprint(home_battery_controller)
app.register_blueprint(vehicle_charger_controller)

app.register_blueprint(device_controller)


def on_message_received(client, userdata, message):
    print(f'Message received on topic {message.topic}: {message.payload.decode("utf-8")}')


def before_first_request():
    subscribe_to_topic()
    while True:
        message = "Hello from first Smartify simulator!"
        publish.single("smartify", message, hostname="localhost")
        time.sleep(5)


def subscribe_to_topic():
    client = mqtt.Client()
    client.on_message = on_message_received
    client.connect('localhost')
    client.subscribe('smartify/spring')
    client.loop_start()


if __name__ == '__main__':
    # before_first_request()
    app.run(host="0.0.0.0", debug=False)
