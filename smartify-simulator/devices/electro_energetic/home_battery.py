import json
import time as py_time

from paho.mqtt import publish

from globals import (AMBIENT_SENSORS, AIR_CONDITIONING, WASHING_MACHINES,
                     LAMPS, GATES, SPRINKLERS,
                     SOLAR_PANEL_SYSTEMS, HOME_BATTERIES, VEHICLE_CHARGERS,
                     DEVICES_REAL_ESTATES, MQTT_HOST, MQTT_PORT)


def proccess_mqtt_message(client, userdata, message):
    home_battery_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        HOME_BATTERIES[home_battery_id]['on'] = not HOME_BATTERIES[home_battery_id]['on']


def home_battery_simulation(home_battery, stop_event):
    while True:
        if not home_battery["on"]:
            print("Home Battery off")
            py_time.sleep(3)
            continue

        last_minute_energy_produced = 0
        for solar_panel_system in SOLAR_PANEL_SYSTEMS.values():
            if (DEVICES_REAL_ESTATES[solar_panel_system['id']]['realEstateId']
                    == DEVICES_REAL_ESTATES[home_battery['id']]['realEstateId']):
                last_minute_energy_produced += solar_panel_system["lastProducedEnergy"]

        last_minute_consumption = get_last_minute_consumption(home_battery["id"])

        energy_difference = last_minute_energy_produced - last_minute_consumption

        home_battery["currentEnergy"] += energy_difference / len(HOME_BATTERIES.keys())

        if home_battery["currentEnergy"] < 0:
            distribution_energy = 0 - home_battery["currentEnergy"]
            message = "Energy taken from electro distribution."
            home_battery["currentEnergy"] = 0
        elif home_battery["currentEnergy"] > home_battery["capacity"]:
            distribution_energy = home_battery["currentEnergy"] - home_battery["capacity"]
            message = "Energy given to electro distribution."
            home_battery["currentEnergy"] = home_battery["capacity"]
        else:
            distribution_energy = 0
            message = "Energy given to electro distribution."

        payload_distribution_energy = {
            "id": home_battery["id"],
            "distributionEnergy": distribution_energy,
            "message": message,
            "realEstateId": DEVICES_REAL_ESTATES[home_battery["id"]]["realEstateId"],
            "cityId": DEVICES_REAL_ESTATES[home_battery["id"]]["city"]["id"]
        }

        publish.single("home-battery/" + str(home_battery['id']),
                       json.dumps({"command": "distribution", "payload": json.dumps(payload_distribution_energy)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)

        payload_current_energy = {
            "id": home_battery["id"],
            "currentEnergy": home_battery["currentEnergy"]
        }

        publish.single("home-battery/" + str(home_battery['id']),
                       json.dumps({"command": "change-current-energy", "payload": json.dumps(payload_current_energy)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)

        payload_last_minute_consumption = {
            "homeBatteryId": home_battery["id"],
            "lastMinuteConsumption": last_minute_consumption,
            "realEstateId": DEVICES_REAL_ESTATES[home_battery["id"]]["realEstateId"],
            "cityId": DEVICES_REAL_ESTATES[home_battery["id"]]["city"]["id"]
        }

        publish.single("home-battery/" + str(home_battery['id']),
                       json.dumps({"command": "consumption", "payload": json.dumps(payload_last_minute_consumption)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)

        if stop_event.is_set():
            break
        py_time.sleep(60)


def get_last_minute_consumption(home_battery_id):
    device_dictionaries = [AMBIENT_SENSORS, AIR_CONDITIONING, WASHING_MACHINES,
                           LAMPS, GATES, SPRINKLERS,
                           SOLAR_PANEL_SYSTEMS, HOME_BATTERIES, VEHICLE_CHARGERS]
    consumption_sum = 0
    for device_dictionary in device_dictionaries:
        for device in device_dictionary.values():
            if (DEVICES_REAL_ESTATES[home_battery_id]['realEstateId'] ==
                    DEVICES_REAL_ESTATES[device['id']]['realEstateId']
                    and device["on"] and device["powerSupplyType"] != "AUTONOMOUSLY"):
                consumption_sum += device["consumption"]
    consumption_sum /= 60
    return consumption_sum
