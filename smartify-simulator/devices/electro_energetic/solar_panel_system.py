import json
import random
import time as py_time
from datetime import datetime, time as dt_time

from paho.mqtt import publish

from globals import SOLAR_PANEL_SYSTEMS, MQTT_HOST, MQTT_PORT


def proccess_mqtt_message(client, userdata, message):
    solar_panel_system_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        SOLAR_PANEL_SYSTEMS[solar_panel_system_id]['on'] = not SOLAR_PANEL_SYSTEMS[solar_panel_system_id]['on']


def solar_panel_system_simulation(solar_panel_system, stop_event):
    start_time, end_time, sunny_hours = get_sunny_hours_time()
    while True:
        if not solar_panel_system["on"]:
            print("Solar Panel System off")
            py_time.sleep(3)
            continue

        produced_energy = 0
        current_time = datetime.now().time()
        if start_time <= current_time <= end_time:
            for solar_panel in solar_panel_system["solarPanels"]:
                if not solar_panel["on"]:
                    continue
                panel_produced_energy = solar_panel["area"] * (solar_panel["efficiency"] / 100) * sunny_hours / 24 / 60
                panel_produced_energy *= random.uniform(0.95, 1.05)
                produced_energy += panel_produced_energy

        solar_panel_system["lastProducedEnergy"] = produced_energy

        payload_record = {
            "id": solar_panel_system['id'],
            "producedEnergy": produced_energy,
        }

        publish.single("solar-panel-system/" + str(solar_panel_system['id']),
                       json.dumps({"command": "record", "payload": json.dumps(payload_record)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)

        payload_event = {
            "solarPanelSystemId": solar_panel_system['id'],
            "action": "Energy produced",
            "subject": solar_panel_system["name"],
            "value": produced_energy
        }

        publish.single("solar-panel-system/" + str(solar_panel_system['id']),
                       json.dumps({"command": "event", "payload": json.dumps(payload_event)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)

        if stop_event.is_set():
            break
        py_time.sleep(60)


def get_sunny_hours_time():
    current_month = datetime.now().month
    if current_month == 1:
        start_time = dt_time(10, 40)
        end_time = dt_time(13, 0)
    elif current_month == 2:
        # start_time = dt_time(10, 20)
        # end_time = dt_time(13, 40)
        start_time = dt_time(0, 0)
        end_time = dt_time(23, 59)
    elif current_month == 3:
        start_time = dt_time(10, 0)
        end_time = dt_time(14, 50)
    elif current_month == 4:
        start_time = dt_time(9, 40)
        end_time = dt_time(16, 0)
    elif current_month == 5:
        start_time = dt_time(9, 30)
        end_time = dt_time(17, 0)
    elif current_month == 6:
        start_time = dt_time(9, 0)
        end_time = dt_time(17, 40)
    elif current_month == 7:
        start_time = dt_time(8, 30)
        end_time = dt_time(18, 0)
    elif current_month == 8:
        start_time = dt_time(8, 40)
        end_time = dt_time(17, 40)
    elif current_month == 9:
        start_time = dt_time(9, 0)
        end_time = dt_time(16, 50)
    elif current_month == 10:
        start_time = dt_time(10, 0)
        end_time = dt_time(15, 30)
    elif current_month == 11:
        start_time = dt_time(10, 30)
        end_time = dt_time(13, 30)
    else:
        start_time = dt_time(10, 40)
        end_time = dt_time(13, 0)
    # else:
    #     start_time = dt_time(0, 0)
    #     end_time = dt_time(23, 59)
    start_datetime = datetime.combine(datetime.today(), start_time)
    end_datetime = datetime.combine(datetime.today(), end_time)
    sunny_hours = (end_datetime - start_datetime).total_seconds() / 60 / 60
    return start_time, end_time, sunny_hours
