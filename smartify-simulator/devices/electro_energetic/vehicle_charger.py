import json
import time as py_time
from datetime import datetime

from paho.mqtt import publish
from globals import VEHICLE_CHARGERS, CHARGING_DATA, MQTT_HOST, MQTT_PORT


def proccess_mqtt_message(client, userdata, message):
    vehicle_charger_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        VEHICLE_CHARGERS[vehicle_charger_id]['on'] = not VEHICLE_CHARGERS[vehicle_charger_id]['on']
    if payload["command"] == 'start-charging':
        start_charging(payload["payload"])
    if payload["command"] == 'finish-charging':
        data = payload["payload"]
        finish_charging(CHARGING_DATA[data['chargeSocketName']], data['chargeSocketName'],
                        VEHICLE_CHARGERS[data["vehicleChargerId"]], data["userEmail"])
        switch_charge_socket_state(data["vehicleChargerId"], data['chargeSocketName'])


def vehicle_charger_simulation(vehicle_charger, stop_event):
    energy_per_minute = vehicle_charger['chargingPower'] / 60
    while True:
        if not vehicle_charger['on']:
            print("Vehicle Charger off")
            py_time.sleep(3)
            continue

        for vehicle_charger_socket in vehicle_charger['vehicleChargerSockets']:
            if vehicle_charger_socket['active']:
                data = CHARGING_DATA[vehicle_charger_socket['name']]
                percentage_charged = energy_per_minute / data['batteryCapacity'] * 1000
                data['currentBatteryPercentage'] += percentage_charged
                if data['currentBatteryPercentage'] >= data['chargePercentageLimit']:
                    finish_charging(data, vehicle_charger_socket['name'], vehicle_charger, "System")
                    vehicle_charger_socket['active'] = False
                else:
                    payload_battery_percentage = {
                        "vehicleChargerId": vehicle_charger['id'],
                        "chargeSocketName": vehicle_charger_socket['name'],
                        "currentBatteryPercentage": data['currentBatteryPercentage'],
                    }

                    publish.single("vehicle-charger/" + str(vehicle_charger['id']),
                                   json.dumps({
                                       "command": "current-battery-percentage",
                                       "payload": json.dumps(payload_battery_percentage)
                                   }),
                                   hostname=MQTT_HOST, port=MQTT_PORT)

        py_time.sleep(10)
        if stop_event.is_set():
            break


def start_charging(data):
    switch_charge_socket_state(data["vehicleChargerId"], data['chargeSocketName'])
    CHARGING_DATA[data['chargeSocketName']] = {
        "active": True,
        "batteryCapacity": data['batteryCapacity'],
        "currentBatteryPercentage": data['currentBatteryPercentage'],
        "chargePercentageLimit": data['chargePercentageLimit'],
        "userEmail": data['userEmail'],
        "startTime": datetime.now()
    }
    payload_charging_data = {
        "vehicleChargerId": data["vehicleChargerId"],
        "vehicleChargerSocketName": data['chargeSocketName'],
        "message": "Vehicle battery started charging.",
        "subject": data['userEmail'],
        "chargingDuration": "",
        "finished": False
    }

    publish.single("vehicle-charger/" + str(data["vehicleChargerId"]),
                   json.dumps({
                       "command": "charging-data",
                       "payload": json.dumps(payload_charging_data)
                   }),
                   hostname=MQTT_HOST, port=MQTT_PORT)


def finish_charging(data, vehicle_charger_socket_name, vehicle_charger, subject):
    data['active'] = False
    if subject == "System":
        data['currentBatteryPercentage'] = data['chargePercentageLimit']

    end_time = datetime.now()
    seconds_difference = (end_time - data['startTime']).total_seconds()
    hours_elapsed = int(seconds_difference // 3600)
    minutes_elapsed = int((seconds_difference % 3600) // 60)
    seconds_elapsed = int(seconds_difference % 60)
    charging_duration = "{:02}:{:02}:{:02}".format(hours_elapsed, minutes_elapsed, seconds_elapsed)

    payload_charging_data = {
        "vehicleChargerId": vehicle_charger['id'],
        "vehicleChargerSocketName": vehicle_charger_socket_name,
        "message": "Vehicle battery finished charging.",
        "subject": subject,
        "chargingDuration": charging_duration,
        "finished": True
    }

    publish.single("vehicle-charger/" + str(vehicle_charger['id']),
                   json.dumps({
                       "command": "charging-data",
                       "payload": json.dumps(payload_charging_data)
                   }),
                   hostname=MQTT_HOST, port=MQTT_PORT)


def switch_charge_socket_state(charger_id, socket_name):
    for vehicle_charger_socket in VEHICLE_CHARGERS[charger_id]['vehicleChargerSockets']:
        if vehicle_charger_socket['name'] == socket_name:
            vehicle_charger_socket['active'] = not vehicle_charger_socket['active']
