import json
import random
import time
from datetime import datetime
from paho.mqtt import publish

from globals import SPRINKLERS, MQTT_HOST, MQTT_PORT


def process_mqtt_message(client, userdata, message):
    sprinkler_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        SPRINKLERS[sprinkler_id]['on'] = not SPRINKLERS[sprinkler_id]['on']
    elif payload["command"] == 'setting':
        payload = json.loads(payload['payload'])
        SPRINKLERS[sprinkler_id]["sprinklerSettings"].append(payload)
        print(SPRINKLERS[sprinkler_id]["sprinklerSettings"])
    elif payload["command"] == 'settingDelete':
        payload = json.loads(payload['payload'])
        print(payload["id"])
        remove_object_by_id(SPRINKLERS[sprinkler_id]["sprinklerSettings"], payload["id"])
        print(SPRINKLERS[sprinkler_id]["sprinklerSettings"])
    elif payload["command"] == 'settingUpdate':
        payload = json.loads(payload['payload'])
        print(payload["id"])
        update_sprinkler_setting(sprinkler_id, payload)
        print(SPRINKLERS[sprinkler_id]["sprinklerSettings"])
    elif payload["command"] == 'switchSprinkler':
        SPRINKLERS[sprinkler_id]["sprinklerOn"] = json.loads(payload["payload"])["sprinklerOn"]
        print(SPRINKLERS[sprinkler_id]["sprinklerOn"])


def sprinkler_simulation(sprinkler, stop_event):
    while True:
        if not sprinkler["on"] or len(sprinkler["sprinklerSettings"]) == 0:
            time.sleep(0.2)
            continue

        command = check_sprinkler_settings(sprinkler)
        if command == "no_action":
            time.sleep(5)
            continue

        payload = {
            "sprinklerOn": command == "on"
        }
        publish.single(f'sprinkler/{sprinkler["id"]}',
                       json.dumps({"command": "switch-sprinkler", "payload": json.dumps(payload)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)


def check_sprinkler_settings(sprinkler):
    current_day = datetime.now().strftime("%A")
    current_time = datetime.now().time()

    for setting in sprinkler["sprinklerSettings"]:
        repeat_days = [day.lower() for day in setting["repeatDays"]]
        if current_day.lower() not in repeat_days:
            continue

        start_time = datetime.fromtimestamp(int(setting["startTime"]) / 1000)
        start_hour, start_minute = start_time.hour, start_time.minute
        print(start_hour, start_minute)
        if current_time.hour == start_hour and current_time.minute == start_minute and not sprinkler["sprinklerOn"]:
            sprinkler["sprinklerOn"] = True
            return "on"

        end_time = datetime.fromtimestamp(int(setting["endTime"]) / 1000)
        end_hour, end_minute = end_time.hour, end_time.minute
        if current_time.hour == end_hour and current_time.minute == end_minute and sprinkler["sprinklerOn"]:
            sprinkler["sprinklerOn"] = False
            return "off"

    return "no_action"


def update_sprinkler_setting(sprinkler_id, payload):
    remove_object_by_id(SPRINKLERS[sprinkler_id]["sprinklerSettings"], payload["id"])
    SPRINKLERS[sprinkler_id]["sprinklerSettings"].append(payload)

def remove_object_by_id(objects_list, target_id):
    objects_list[:] = [obj for obj in objects_list if obj.get("id") != target_id]