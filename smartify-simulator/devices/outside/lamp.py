import json
import time
from datetime import datetime

from influxdb_client import Point
from paho.mqtt import publish

from globals import INFLUX_CLIENT, SYNCHRONOUS, INFLUX_ORG, LAMPS, MQTT_HOST, MQTT_PORT
import random


def process_mqtt_message(client, userdata, message):
    lamp_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        LAMPS[lamp_id]['on'] = not LAMPS[lamp_id]['on']
    elif payload["command"] == 'switchLight':
        LAMPS[lamp_id]['lightOn'] = json.loads(payload["payload"])["lightOn"]
    elif payload["command"] == 'switchMode':
        LAMPS[lamp_id]['automatic'] = not LAMPS[lamp_id]['automatic']


def lamp_simulation(lamp, stop_event):
    # current_brightness = 350.0
    while True:
        if stop_event.is_set():
            break

        if not lamp["on"]:
            time.sleep(3)
            continue

        # current_brightness = simulate_brightness(current_brightness)

        current_brightness = get_brightness()

        payload = {
            "brightness": current_brightness
        }
        publish.single("lamp/" + str(lamp['id']),
                       json.dumps({"command": "record-brightness", "payload": json.dumps(payload)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)
        # print(f'published: {payload}')
        if not lamp['automatic']:
            time.sleep(60)
            continue

        if current_brightness > 600 and lamp["lightOn"]:
            publish.single("lamp/" + str(lamp['id']),
                           json.dumps({"command": "lamp-light-switch", "payload": json.dumps({"lightOn": False})}),
                           hostname=MQTT_HOST, port=MQTT_PORT)
            lamp["lightOn"] = False
        elif current_brightness < 400 and not lamp["lightOn"]:
            publish.single("lamp/" + str(lamp['id']),
                           json.dumps({"command": "lamp-light-switch", "payload": json.dumps({"lightOn": True})}),
                           hostname=MQTT_HOST, port=MQTT_PORT)
            lamp["lightOn"] = True

        time.sleep(60)


# def simulate_brightness(current_brightness):
#     mean_variation = 0
#     std_deviation = 50
#
#     brightness_variation = random.gauss(mean_variation, std_deviation)
#
#     current_brightness = max(0.0, current_brightness + brightness_variation)
#
#     return current_brightness


def get_brightness():
    current_month = datetime.now().month
    current_hour = datetime.now().hour

    if current_month == 1:
        if 7 <= current_hour <= 10 or 14 <= current_hour <= 16:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 2:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 17:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 3:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 18:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 4:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 19:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 5:
        if 5 <= current_hour <= 10 or 14 <= current_hour <= 20:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 6:
        if 5 <= current_hour <= 10 or 14 <= current_hour <= 20:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 7:
        if 5 <= current_hour <= 10 or 14 <= current_hour <= 20:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 8:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 20:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 9:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 19:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 10:
        if 7 <= current_hour <= 10 or 14 <= current_hour <= 18:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 11:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 16:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    else:
        if 7 <= current_hour <= 10 or 14 <= current_hour <= 16:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
