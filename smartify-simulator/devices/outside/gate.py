import json
import random
import time

from paho.mqtt import publish

from globals import GATES, MQTT_HOST, MQTT_PORT


def process_mqtt_message(client, userdata, message):
    gate_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        GATES[gate_id]['on'] = not GATES[gate_id]['on']
    elif payload["command"] == 'switch-gate-state':
        GATES[gate_id]['open'] = json.loads(payload["payload"])["open"]
    elif payload["command"] == 'update-allowed-vehicles':
        GATES[gate_id]['allowedVehicles'] = json.loads(payload["payload"])
    elif payload["command"] == 'update-mode':
        GATES[gate_id]['mode'] = json.loads(payload["payload"])


def gate_simulation(gate, stop_event):
    vehicles_inside = []
    last_exited = time.time()
    while True:
        if not gate["on"]:
            continue

        vehicle = generate_random_vehicle(gate, vehicles_inside)
        if vehicle is not None:
            publish_event(gate, "Vehicle approached", vehicle, "approach-enter")
            time.sleep(3)
            # region Entry
            if gate["open"]:
                publish_event(gate, "Vehicle entered on manually opened gate", vehicle, "approach-enter")
                vehicles_inside.append(vehicle)
                pass
            else:
                if is_vehicle_allowed(gate, vehicle):
                    publish_event(gate, "Gate opened for vehicle to enter", vehicle, "open")
                    gate["open"] = True
                    time.sleep(5)
                    publish_event(gate, "Vehicle entered and gate closed", vehicle, "close")
                    gate["open"] = False
                    vehicles_inside.append(vehicle)
                else:
                    time.sleep(10)
                    if gate["open"]:
                        publish_event(gate, "Vehicle entered on manually opened gate", vehicle, "approach-enter")
                        vehicles_inside.append(vehicle)
                    else:
                        publish_event(gate, "Vehicle went away", vehicle, "approach-enter")

            # endregion

        # region Exit
        if (time.time() - last_exited) / 60 <= 0.2:
            continue
        last_exited = time.time()

        if len(vehicles_inside) == 0:
            time.sleep(10)
            continue

        vehicle_to_exit = random.choice(vehicles_inside)
        if gate["open"]:
            publish_event(gate, "Vehicle left on manually opened gate", vehicle_to_exit, "approach-enter")
            vehicles_inside.remove(vehicle_to_exit)
        else:
            publish_event(gate, "Gate opened for vehicle to leave", vehicle_to_exit, "open")
            gate["open"] = True
            time.sleep(5)
            publish_event(gate, "Vehicle left and gate closed", vehicle_to_exit, "close")
            gate["open"] = False
            vehicles_inside.remove(vehicle_to_exit)
        # endregion

        time.sleep(10)


def generate_random_vehicle(gate, vehicles_inside):
    available_vehicles = set(gate["allowedVehicles"]) - set(vehicles_inside)
    if len(available_vehicles) == 0:
        return None

    if random.choice([True, False]):
        return random.choice(list(available_vehicles))
    else:
        return generate_not_allowed_vehicle()


def is_vehicle_allowed(gate, vehicle):
    if gate["mode"].lower() == "public":
        return True
    elif gate["mode"].lower() == "private" and vehicle in gate["allowedVehicles"]:
        return True
    else:
        return False


def generate_not_allowed_vehicle():
    return f"{chr(random.randint(65, 90))}{chr(random.randint(65, 90))}{random.randint(100, 999)}"


def publish_event(gate, action, subject, command):
    payload = {
        "gateId": gate["id"],
        "action": action,
        "subject": subject
    }
    publish.single(f'gate/{gate["id"]}',
                   json.dumps({"command": command, "payload": json.dumps(payload)}),
                   hostname=MQTT_HOST, port=MQTT_PORT)
    #print(f'Action: {action} Subject: {subject} Command: {command}')
