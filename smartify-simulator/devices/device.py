import json
import random
import time

from paho.mqtt import publish
from globals import MQTT_HOST, MQTT_PORT


def heartbeat(device_id, device_type, stop_event):
    while not stop_event.is_set():
        publish.single(f'{device_type}/{device_id}',
                       json.dumps({'command': 'ping', 'payload': f"Device {device_id} heartbeat"}),
                       hostname=MQTT_HOST, port=MQTT_PORT)

        sleep_durations = [10, 50]
        probabilities = [1.0, 0.00]
        sleep_duration = random.choices(sleep_durations, probabilities)[0]
        time.sleep(sleep_duration)
