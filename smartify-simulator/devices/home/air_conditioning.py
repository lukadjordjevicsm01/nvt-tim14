from datetime import datetime, time, timedelta
import json
import time as py_time

from paho.mqtt import publish

from globals import AIR_CONDITIONING, MQTT_HOST, MQTT_PORT


def process_mqtt_message(client, userdata, message):
    air_conditioning_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        AIR_CONDITIONING[air_conditioning_id]['on'] = not AIR_CONDITIONING[air_conditioning_id]['on']
    elif payload["command"] == 'action':
        payload = json.loads(payload['payload'])
        print(payload)
        record_payload = {
            "id": air_conditioning_id,
            "action": f"{payload['airConditioningMode']}",
            "subject": payload['email'],
            "currentTemperature": payload['currentTemperature']
        }
        publish.single("air-conditioning/" + str(air_conditioning_id),
                       json.dumps({"command": "record", "payload": json.dumps(record_payload)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)
    elif payload["command"] == 'setting':
        payload = json.loads(payload['payload'])
        print(AIR_CONDITIONING[air_conditioning_id])
        AIR_CONDITIONING[air_conditioning_id]['customSettings'].append(payload)


def air_conditioning_simulation(air_conditioning, stop_event):
    print(f"Air conditioning {air_conditioning['id']} simulation started")
    while not stop_event.is_set():
        if not air_conditioning["on"]:
            print("Air conditioning off")
            py_time.sleep(5)
            continue

        for setting in air_conditioning["customSettings"]:
            now = datetime.now()
            if "lastUsedTime" in setting and now - setting['lastUsedTime'] <= timedelta(hours=24):
                continue

            # region date normalization
            start_datetime = datetime.fromtimestamp(setting["startTime"]/1000)
            end_datetime = datetime.fromtimestamp(setting["endTime"]/1000)
            start_time = time(start_datetime.hour, start_datetime.minute)
            end_time = time(end_datetime.hour, end_datetime.minute)

            start_date = datetime(now.year, now.month, now.day, start_time.hour, start_time.minute)
            end_date = datetime(now.year, now.month, now.day, end_time.hour, end_time.minute)
            if start_time > end_time:
                end_date = datetime(end_date.year, end_date.month, end_date.day + 1, end_date.hour, end_date.minute)
            # endregion

            if start_date < now < end_date:
                payload = {
                    "id": air_conditioning['id'],
                    "subject": 'system',
                    "action": f"{setting['mode']}",
                    "currentTemperature": setting['temperature']
                }

                publish.single("air-conditioning/" + str(air_conditioning['id']),
                               json.dumps({"command": "record", "payload": json.dumps(payload)}),
                               hostname=MQTT_HOST, port=MQTT_PORT)
                setting['lastUsedTime'] = now

        py_time.sleep(5)

