import json
import threading
import time as py_time
from datetime import datetime, time, timedelta

from paho.mqtt import publish

from globals import WASHING_MACHINES, MQTT_HOST, MQTT_PORT


def process_mqtt_message(client, userdata, message):
    washing_machine_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        WASHING_MACHINES[washing_machine_id]['on'] = not WASHING_MACHINES[washing_machine_id]['on']
    elif payload["command"] == 'action':
        payload = json.loads(payload['payload'])
        print(payload)
        record_payload = {
            "id": washing_machine_id,
            "action": f"{payload['washingMachineModeName']}",
            "subject": payload['email']
        }
        publish.single("washing-machine/" + str(washing_machine_id),
                       json.dumps({"command": "record", "payload": json.dumps(record_payload)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)
        WASHING_MACHINES[washing_machine_id]['currentMode'] = {}
        WASHING_MACHINES[washing_machine_id]['currentMode']['name'] = payload['washingMachineModeName']
        WASHING_MACHINES[washing_machine_id]['currentMode']['duration'] = payload['duration']
    elif payload["command"] == 'setting':
        payload = json.loads(payload['payload'])
        WASHING_MACHINES[washing_machine_id]['customSettings'].append(payload)
        print(WASHING_MACHINES[washing_machine_id])


def washing_machine_simulation(washing_machine, stop_event):
    print(f"Washing machine {washing_machine['id']} simulation started")
    while not stop_event.is_set():
        if not washing_machine["on"]:
            print("Washing machine off")
            py_time.sleep(5)
            continue

        if washing_machine['currentMode'] and washing_machine['currentMode']['name'] != "":
            if "startTime" not in washing_machine['currentMode']:
                washing_machine['currentMode']["startTime"] = datetime.now()
                washing_machine['currentMode']["endTime"] = (washing_machine['currentMode']["startTime"] +
                                                             timedelta(minutes=washing_machine['currentMode']["duration"]))

            if washing_machine['currentMode']["startTime"] <= datetime.now() < washing_machine['currentMode']["endTime"]:
                py_time.sleep(5)
                continue
            else:
                washing_machine['currentMode']["name"] = ""
                publish.single("washing-machine/" + str(washing_machine["id"]),
                               json.dumps({"command": "notify"}),
                               hostname=MQTT_HOST, port=MQTT_PORT)

        for setting in washing_machine["customSettings"]:
            if washing_machine['currentMode'] and washing_machine['currentMode']['name'] != "":
                break

            now = datetime.now()

            if ("done" in setting and "endTime" in setting
                    and setting['endTime'] < now
                    and not setting['done']):
                setting['done'] = True
                publish.single("washing-machine/" + str(washing_machine["id"]),
                               json.dumps({"command": "notify"}),
                               hostname=MQTT_HOST, port=MQTT_PORT)

            if "done" in setting and setting['done']:
                continue

            if "endTime" in setting and setting['endTime'] > now:
                continue

            start_datetime = datetime.fromtimestamp(setting["startTime"] / 1000)
            end_datetime = start_datetime + timedelta(minutes=setting["mode"]["duration"])
            if start_datetime < now < end_datetime:
                record_payload = {
                    "id": washing_machine["id"],
                    "action": f"{setting['mode']['name']}",
                    "subject": "system"
                }
                publish.single("washing-machine/" + str(washing_machine["id"]),
                               json.dumps({"command": "record", "payload": json.dumps(record_payload)}),
                               hostname=MQTT_HOST, port=MQTT_PORT)
                setting['endTime'] = end_datetime
                setting['done'] = False

        py_time.sleep(5)
