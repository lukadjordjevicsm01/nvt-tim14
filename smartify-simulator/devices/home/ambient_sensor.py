import json
import random
import time
from datetime import datetime

from paho.mqtt import publish

from globals import AMBIENT_SENSORS, MQTT_HOST, MQTT_PORT


def process_mqtt_message(client, userdata, message):
    ambient_sensor_id = int(message.topic.split("/")[-1])
    payload = json.loads(message.payload.decode("utf-8"))
    if payload["command"] == 'switch':
        AMBIENT_SENSORS[ambient_sensor_id]['on'] = not AMBIENT_SENSORS[ambient_sensor_id]['on']


def ambient_sensor_simulation(ambient_sensor, stop_event):
    while True:
        if not ambient_sensor["on"]:
            print("Ambient sensor off")
            time.sleep(60)
            continue

        temperature, humidity = calculate_temperature_humidity()

        temperature_change = random.uniform(-2, 2)
        humidity_change = random.uniform(-3, 3)

        temperature += temperature_change
        humidity += humidity_change

        ambient_sensor['temperature'] = round(temperature, 2)
        ambient_sensor['humidity'] = round(humidity, 2)

        payload = {
                "id": ambient_sensor['id'],
                "temperature": ambient_sensor['temperature'],
                "humidity": ambient_sensor['humidity'],
            }

        publish.single("ambient-sensor/" + str(ambient_sensor['id']),
                       json.dumps({"command": "record", "payload": json.dumps(payload)}),
                       hostname=MQTT_HOST, port=MQTT_PORT)
        if stop_event.is_set():
            break
        time.sleep(60)


def calculate_temperature_humidity():
    month = datetime.now().month
    hour = datetime.now().hour
    if month == 1:
        humidity = 79
        if 7 <= hour <= 16:
            temperature = 4.5
        else:
            temperature = -2.6
    elif month == 2:
        humidity = 70
        if 6 <= hour <= 17:
            temperature = 7.4
        else:
            temperature = -1.4
    elif month == 3:
        humidity = 63
        if 5 <= hour <= 18:
            temperature = 12.8
        else:
            temperature = 2.2
    elif month == 4:
        humidity = 60
        if 6 <= hour <= 19:
            temperature = 18.4
        else:
            temperature = 6.9
    elif month == 5:
        humidity = 60
        if 5 <= hour <= 20:
            temperature = 23
        else:
            temperature = 11.5
    elif month == 6:
        humidity = 64
        if 5 <= hour <= 20:
            temperature = 15.2
        else:
            temperature = 26.8
    elif month == 7:
        humidity = 60
        if 5 <= hour <= 20:
            temperature = 29
        else:
            temperature = 16.6
    elif month == 8:
        humidity = 60
        if 5 <= hour <= 19:
            temperature = 29.3
        else:
            temperature = 16.7
    elif month == 9:
        humidity = 65
        if 6 <= hour <= 18:
            temperature = 24
        else:
            temperature = 12.3
    elif month == 10:
        humidity = 70
        if 7 <= hour <= 17:
            temperature = 18.4
        else:
            temperature = 7.5
    elif month == 11:
        humidity = 78
        if 6 <= hour <= 16:
            temperature = 11.9
        else:
            temperature = 3.2
    else:
        humidity = 80
        if 7 <= hour <= 16:
            temperature = 5.5
        else:
            temperature = -1.2

    return temperature, humidity

