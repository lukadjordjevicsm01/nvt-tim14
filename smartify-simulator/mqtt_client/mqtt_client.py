import paho.mqtt.client as mqtt
from globals import MQTT_HOST, MQTT_PORT


def create_mqtt_client(on_message_received):
    client = mqtt.Client()
    client.on_message = on_message_received
    client.connect(host=MQTT_HOST, port=MQTT_PORT)
    client.loop_start()
    return client
