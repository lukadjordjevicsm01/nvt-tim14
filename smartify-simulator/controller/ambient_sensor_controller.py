import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.home.ambient_sensor import ambient_sensor_simulation, process_mqtt_message
from globals import AMBIENT_SENSORS, AMBIENT_SENSORS_THREADS
from mqtt_client.mqtt_client import create_mqtt_client

ambient_sensor_controller = Blueprint('ambient-sensor-controller', __name__)

ambient_sensor_client = create_mqtt_client(process_mqtt_message)
base_topic = "ambient-sensor-simulator"


@ambient_sensor_controller.route('/set-ambient-sensors', methods=['POST'])
def set_ambient_sensors():
    try:
        ambient_sensors = request.get_json()
        for ambient_sensor in ambient_sensors:
            AMBIENT_SENSORS[ambient_sensor['id']] = ambient_sensor
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(ambient_sensor['id'], 'ambient-sensor', stop_event))
            thread = threading.Thread(target=ambient_sensor_simulation,
                                      args=(AMBIENT_SENSORS[ambient_sensor['id']], stop_event))
            AMBIENT_SENSORS_THREADS[ambient_sensor['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            ambient_sensor_client.subscribe(f'{base_topic}/{ambient_sensor["id"]}')

        return jsonify({'message': 'Sensors posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@ambient_sensor_controller.route('/add-ambient-sensor', methods=['POST'])
def add_ambient_sensor():
    try:
        ambient_sensor = request.get_json()
        AMBIENT_SENSORS[ambient_sensor['id']] = ambient_sensor
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(ambient_sensor['id'], 'ambient-sensor', stop_event))
        thread = threading.Thread(target=ambient_sensor_simulation,
                                  args=(AMBIENT_SENSORS[ambient_sensor['id']], stop_event))
        AMBIENT_SENSORS_THREADS[ambient_sensor['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()
        ambient_sensor_client.subscribe(f'{base_topic}/{ambient_sensor["id"]}')
        print("Sensor added.")
        return jsonify({'message': 'Sensor added.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500