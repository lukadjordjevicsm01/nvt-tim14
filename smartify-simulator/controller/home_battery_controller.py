import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.electro_energetic.home_battery import home_battery_simulation, proccess_mqtt_message
from globals import HOME_BATTERIES, HOME_BATTERIES_THREADS
from mqtt_client.mqtt_client import create_mqtt_client

home_battery_controller = Blueprint('home-battery-controller', __name__)

home_battery_client = create_mqtt_client(proccess_mqtt_message)
base_topic = "home-battery-simulator"


@home_battery_controller.route('/set-home-batteries', methods=['POST'])
def set_home_batteries():
    try:
        home_batteries = request.get_json()
        for home_battery in home_batteries:
            HOME_BATTERIES[home_battery['id']] = home_battery
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(home_battery['id'], 'home-battery', stop_event))
            thread = threading.Thread(target=home_battery_simulation,
                                      args=(HOME_BATTERIES[home_battery['id']], stop_event))
            HOME_BATTERIES_THREADS[home_battery['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            home_battery_client.subscribe(f'{base_topic}/{home_battery["id"]}')

        return jsonify({'message': 'Home Batteries posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@home_battery_controller.route('/add-home-battery', methods=['POST'])
def add_home_battery():
    try:
        home_battery = request.get_json()
        HOME_BATTERIES[home_battery['id']] = home_battery
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(home_battery['id'], 'home_battery', stop_event))
        thread = threading.Thread(target=home_battery_simulation,
                                  args=(HOME_BATTERIES[home_battery['id']], stop_event))
        HOME_BATTERIES_THREADS[home_battery['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()

        home_battery_client.subscribe(f'{base_topic}/{home_battery["id"]}')
        return jsonify({'message': 'Home Battery posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
