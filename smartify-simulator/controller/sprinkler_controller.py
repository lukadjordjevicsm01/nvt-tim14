import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.outside.sprinkler import sprinkler_simulation, process_mqtt_message
from globals import SPRINKLERS, SPRINKLERS_THREADS

from mqtt_client.mqtt_client import create_mqtt_client

sprinkler_controller = Blueprint('sprinkler-controller', __name__)

sprinkler_client = create_mqtt_client(process_mqtt_message)
base_topic = "sprinkler-simulator"


@sprinkler_controller.route('/set-sprinklers', methods=['POST'])
def set_sprinklers():
    try:
        sprinklers = request.get_json()
        for sprinkler in sprinklers:
            SPRINKLERS[sprinkler['id']] = sprinkler
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(sprinkler['id'], 'sprinkler', stop_event))
            thread = threading.Thread(target=sprinkler_simulation,
                                      args=(SPRINKLERS[sprinkler['id']], stop_event))
            SPRINKLERS_THREADS[sprinkler['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            sprinkler_client.subscribe(f'{base_topic}/{sprinkler["id"]}')

        return jsonify({'message': 'Sprinklers posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@sprinkler_controller.route('/add-sprinkler', methods=['POST'])
def add_sprinkler():
    try:
        sprinkler = request.get_json()

        SPRINKLERS[sprinkler['id']] = sprinkler
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(sprinkler['id'], 'sprinkler', stop_event))
        thread = threading.Thread(target=sprinkler_simulation,
                                  args=(SPRINKLERS[sprinkler['id']], stop_event))
        SPRINKLERS_THREADS[sprinkler['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()

        sprinkler_client.subscribe(f'{base_topic}/{sprinkler["id"]}')
        return jsonify({'message': 'Sprinkler posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500