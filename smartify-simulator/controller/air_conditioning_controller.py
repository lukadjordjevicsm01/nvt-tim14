import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.home.air_conditioning import process_mqtt_message, air_conditioning_simulation
from globals import AIR_CONDITIONING, AIR_CONDITIONING_THREADS
from mqtt_client.mqtt_client import create_mqtt_client

air_conditioning_controller = Blueprint('air-conditioning-controller', __name__)

air_conditioning_client = create_mqtt_client(process_mqtt_message)
base_topic = "air-conditioning-simulator"


@air_conditioning_controller.route('/set-air-conditioning', methods=['POST'])
def set_air_conditioning():
    try:
        air_condtitionings = request.get_json()
        for air_condtitioning in air_condtitionings:
            AIR_CONDITIONING[air_condtitioning['id']] = air_condtitioning
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(air_condtitioning['id'], 'air-conditioning', stop_event))
            thread = threading.Thread(target=air_conditioning_simulation,
                                      args=(AIR_CONDITIONING[air_condtitioning['id']], stop_event))
            AIR_CONDITIONING_THREADS[air_condtitioning['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            air_conditioning_client.subscribe(f'{base_topic}/{air_condtitioning["id"]}')

        return jsonify({'message': 'Air conditioners posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@air_conditioning_controller.route('/add-air-conditioning', methods=['POST'])
def add_air_conditioning():
    try:
        air_condtitioning = request.get_json()
        AIR_CONDITIONING[air_condtitioning['id']] = air_condtitioning
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(air_condtitioning['id'], 'air-conditioning', stop_event))
        thread = threading.Thread(target=air_conditioning_simulation,
                                  args=(AIR_CONDITIONING[air_condtitioning['id']], stop_event))
        AIR_CONDITIONING_THREADS[air_condtitioning['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()

        air_conditioning_client.subscribe(f'{base_topic}/{air_condtitioning["id"]}')

        return jsonify({'message': 'Air conditioner posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
