import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.home.washing_machine import process_mqtt_message, washing_machine_simulation
from globals import WASHING_MACHINES, WASHING_MACHINES_THREADS

from mqtt_client.mqtt_client import create_mqtt_client

washing_machine_controller = Blueprint('washing_machine_controller', __name__)

washing_machine_client = create_mqtt_client(process_mqtt_message)
base_topic = "washing-machine-simulator"


@washing_machine_controller.route('/set-washing-machines', methods=['POST'])
def set_washing_machies():
    try:
        washing_machines = request.get_json()
        for washing_machine in washing_machines:
            WASHING_MACHINES[washing_machine['id']] = washing_machine
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(washing_machine['id'], 'washing-machine', stop_event))
            thread = threading.Thread(target=washing_machine_simulation,
                                      args=(WASHING_MACHINES[washing_machine['id']], stop_event))
            WASHING_MACHINES_THREADS[washing_machine['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            washing_machine_client.subscribe(f'{base_topic}/{washing_machine["id"]}')

        return jsonify({'message': 'Washing machines posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@washing_machine_controller.route('/add-washing-machine', methods=['POST'])
def add_air_conditioning():
    try:
        washing_machine = request.get_json()
        WASHING_MACHINES[washing_machine['id']] = washing_machine
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(washing_machine['id'], 'washing_machine', stop_event))
        thread = threading.Thread(target=washing_machine_simulation,
                                  args=(WASHING_MACHINES[washing_machine['id']], stop_event))
        WASHING_MACHINES_THREADS[washing_machine['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()

        washing_machine_client.subscribe(f'{base_topic}/{washing_machine["id"]}')

        return jsonify({'message': 'Air conditioner posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
