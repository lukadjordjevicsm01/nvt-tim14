from flask import Blueprint, request, jsonify
from globals import DEVICES_REAL_ESTATES

device_controller = Blueprint('device-controller', __name__)


@device_controller.route('/set-devices-real-estates', methods=['POST'])
def set_devices_real_estates():
    try:
        real_estates = request.get_json()
        for real_estate in real_estates:
            for device_id in real_estate['devicesIds']:
                DEVICES_REAL_ESTATES[device_id] = {
                    'realEstateId': real_estate['realEstateId'],
                    'city': real_estate['city']
                }
        return jsonify({'message': 'Devices Real Estates posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@device_controller.route('/add-device-real-estate', methods=['POST'])
def add_device_real_estate():
    try:
        real_estate = request.get_json()
        for device_id in real_estate['devicesIds']:
            DEVICES_REAL_ESTATES[device_id] = {
                'realEstateId': real_estate['realEstateId'],
                'city': real_estate['city']
            }
        return jsonify({'message': 'Device Real Estate posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
