import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.electro_energetic.vehicle_charger import vehicle_charger_simulation, proccess_mqtt_message
from globals import VEHICLE_CHARGERS, VEHICLE_CHARGERS_THREADS
from mqtt_client.mqtt_client import create_mqtt_client

vehicle_charger_controller = Blueprint('vehicle-charger-controller', __name__)

vehicle_charger_client = create_mqtt_client(proccess_mqtt_message)
base_topic = "vehicle-charger-simulator"


@vehicle_charger_controller.route('/set-vehicle-chargers', methods=['POST'])
def set_vehicle_chargers():
    try:
        vehicle_chargers = request.get_json()
        for vehicle_charger in vehicle_chargers:
            VEHICLE_CHARGERS[vehicle_charger['id']] = vehicle_charger
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(vehicle_charger['id'], 'vehicle-charger', stop_event))
            thread = threading.Thread(target=vehicle_charger_simulation,
                                      args=(VEHICLE_CHARGERS[vehicle_charger['id']], stop_event))
            VEHICLE_CHARGERS_THREADS[vehicle_charger['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            vehicle_charger_client.subscribe(f'{base_topic}/{vehicle_charger["id"]}')
        return jsonify({'message': 'Vehicle Chargers posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@vehicle_charger_controller.route('/add-vehicle-charger', methods=['POST'])
def add_vehicle_charger():
    try:
        vehicle_charger = request.get_json()
        VEHICLE_CHARGERS[vehicle_charger['id']] = vehicle_charger
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(vehicle_charger['id'], 'vehicle_charger', stop_event))
        thread = threading.Thread(target=vehicle_charger_simulation,
                                  args=(VEHICLE_CHARGERS[vehicle_charger['id']], stop_event))
        VEHICLE_CHARGERS_THREADS[vehicle_charger['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()

        vehicle_charger_client.subscribe(f'{base_topic}/{vehicle_charger["id"]}')
        return jsonify({'message': 'Vehicle Charger posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
