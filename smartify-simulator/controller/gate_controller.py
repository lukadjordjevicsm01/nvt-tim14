import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.outside.gate import gate_simulation, process_mqtt_message
from globals import GATES, GATES_THREADS

from mqtt_client.mqtt_client import create_mqtt_client

gate_controller = Blueprint('gate-controller', __name__)

gate_client = create_mqtt_client(process_mqtt_message)
base_topic = "gate-simulator"


@gate_controller.route('/set-gates', methods=['POST'])
def set_gates():
    try:
        gates = request.get_json()
        for gate in gates:
            GATES[gate['id']] = gate
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(gate['id'], 'gate', stop_event))
            thread = threading.Thread(target=gate_simulation,
                                      args=(GATES[gate['id']], stop_event))
            GATES_THREADS[gate['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            gate_client.subscribe(f'{base_topic}/{gate["id"]}')

        return jsonify({'message': 'Gates posted.'}), 200
    except Exception as e:
        print(e.with_traceback())
        return jsonify({'error': str(e)}), 500


@gate_controller.route('/add-gate', methods=['POST'])
def add_gate():
    try:
        gate = request.get_json()

        GATES[gate['id']] = gate
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(gate['id'], 'gate', stop_event))
        thread = threading.Thread(target=gate_simulation,
                                  args=(GATES[gate['id']], stop_event))
        GATES_THREADS[gate['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()

        gate_client.subscribe(f'{base_topic}/{gate["id"]}')

        return jsonify({'message': 'Gates posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
