import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.outside.lamp import lamp_simulation, process_mqtt_message
from globals import LAMPS, LAMPS_THREADS
from mqtt_client.mqtt_client import create_mqtt_client

lamp_controller = Blueprint('lamp-controller', __name__)

lamp_client = create_mqtt_client(process_mqtt_message)
base_topic = "lamp-simulator"

@lamp_controller.route('/set-lamps', methods=['POST'])
def set_lamps():
    try:
        lamps = request.get_json()
        for lamp in lamps:
            LAMPS[lamp['id']] = lamp
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(lamp['id'], 'lamp', stop_event))
            thread = threading.Thread(target=lamp_simulation,
                                      args=(LAMPS[lamp['id']], stop_event))
            LAMPS_THREADS[lamp['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            lamp_client.subscribe(f'{base_topic}/{lamp["id"]}')

        return jsonify({'message': 'Lamps posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@lamp_controller.route('/add-lamp', methods=['POST'])
def add_lamp():
    try:
        lamp = request.get_json()
        LAMPS[lamp['id']] = lamp
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(lamp['id'], 'lamp', stop_event))
        thread = threading.Thread(target=lamp_simulation,
                                  args=(LAMPS[lamp['id']], stop_event))
        LAMPS_THREADS[lamp['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()

        lamp_client.subscribe(f'{base_topic}/{lamp["id"]}')

        return jsonify({'message': 'Lamp added.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
