import threading

from flask import Blueprint, request, jsonify

from devices.device import heartbeat
from devices.electro_energetic.solar_panel_system import solar_panel_system_simulation, proccess_mqtt_message
from globals import SOLAR_PANEL_SYSTEMS, SOLAR_PANEL_SYSTEMS_THREADS
from mqtt_client.mqtt_client import create_mqtt_client

solar_panel_system_controller = Blueprint('solar-panel-system-controller', __name__)

solar_panel_system_client = create_mqtt_client(proccess_mqtt_message)
base_topic = "solar-panel-system-simulator"


@solar_panel_system_controller.route('/set-solar-panel-systems', methods=['POST'])
def set_solar_panel_systems():
    try:
        solar_panel_systems = request.get_json()
        for solar_panel_system in solar_panel_systems:
            SOLAR_PANEL_SYSTEMS[solar_panel_system['id']] = solar_panel_system
            stop_event = threading.Event()
            heartbeat_thread = threading.Thread(target=heartbeat,
                                                args=(solar_panel_system['id'], 'solar-panel-system', stop_event))
            thread = threading.Thread(target=solar_panel_system_simulation,
                                      args=(SOLAR_PANEL_SYSTEMS[solar_panel_system['id']], stop_event))
            SOLAR_PANEL_SYSTEMS_THREADS[solar_panel_system['id']] = (thread, heartbeat_thread, stop_event)
            heartbeat_thread.start()
            thread.start()

            solar_panel_system_client.subscribe(f'{base_topic}/{solar_panel_system["id"]}')

        return jsonify({'message': 'Solar Panel Systems posted.'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@solar_panel_system_controller.route('/add-solar-panel-system', methods=['POST'])
def add_solar_panel_system():
    try:
        solar_panel_system = request.get_json()
        SOLAR_PANEL_SYSTEMS[solar_panel_system['id']] = solar_panel_system
        stop_event = threading.Event()
        heartbeat_thread = threading.Thread(target=heartbeat,
                                            args=(solar_panel_system['id'], 'solar_panel_system', stop_event))
        thread = threading.Thread(target=solar_panel_system_simulation,
                                  args=(SOLAR_PANEL_SYSTEMS[solar_panel_system['id']], stop_event))
        SOLAR_PANEL_SYSTEMS_THREADS[solar_panel_system['id']] = (thread, heartbeat_thread, stop_event)
        heartbeat_thread.start()
        thread.start()

        solar_panel_system_client.subscribe(f'{base_topic}/{solar_panel_system["id"]}')
        return jsonify({'message': 'Solar Panel System posted.'}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 500
