import random
import time

from influxdb_client import Point
from datetime import datetime, timedelta

from globals import INFLUX_CLIENT, WRITE_API, INFLUX_ORG, INFLUX_BUCKET


def fill_humidity():
    measurement = "humidity"
    current_time = datetime.utcnow()

    for i in range(1, 3 * 30 * 24 * 60 + 1):  # 3 months * 30 days/month * 24 hours/day * 60 minutes/hour
        # Calculate timestamp for each minute in the past
        timestamp = int((current_time - timedelta(minutes=i)).timestamp())

        date_for_value = datetime.utcfromtimestamp(timestamp)
        _, humidity = calculate_temperature_humidity(date_for_value)
        humidity += random.uniform(-3, 3)
        # Create a Point object with the calculated timestamp
        data_point = (Point(measurement)
                      .tag("id", 1)
                      .field("humidity", humidity)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)

        humidity += random.uniform(-3, 3)
        data_point = (Point(measurement)
                      .tag("id", 13)
                      .field("humidity", humidity)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)


def fill_temperature():
    measurement = "temperature"
    current_time = datetime.utcnow()

    for i in range(1, 3 * 30 * 24 * 60 + 1):  # 3 months * 30 days/month * 24 hours/day * 60 minutes/hour
        # Calculate timestamp for each minute in the past
        timestamp = int((current_time - timedelta(minutes=i)).timestamp())

        date_for_value = datetime.utcfromtimestamp(timestamp)
        temperature, _ = calculate_temperature_humidity(date_for_value)
        temperature += random.uniform(-3, 3)
        # Create a Point object with the calculated timestamp
        data_point = (Point(measurement)
                      .tag("id", 1)
                      .field("temperature", temperature)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)

        temperature += random.uniform(-3, 3)
        data_point = (Point(measurement)
                      .tag("id", 13)
                      .field("temperature", temperature)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)


def fill_device_status(device_id):
    measurement = "DeviceStatus"
    current_time = datetime.utcnow()

    for i in range(1, 3 * 30 * 24 * 60 * 2 + 1):
        timestamp = int((current_time - timedelta(seconds=i * 30)).timestamp())
        # Create a Point object with the calculated timestamp

        states = ["online", "offline"]
        probabilities = [0.7, 0.3]
        status = random.choices(states, probabilities)[0]

        data_point = (Point(measurement)
                      .tag("deviceId", device_id)
                      .field("status", status)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)


def fill_brightness():
    measurement = "Brightness"
    current_time = datetime.utcnow()

    for i in range(1, 3 * 30 * 24 * 60 + 1):  # 3 months * 30 days/month * 24 hours/day * 60 minutes/hour
        # Calculate timestamp for each minute in the past
        timestamp = int((current_time - timedelta(minutes=i)).timestamp())

        date_for_value = datetime.utcfromtimestamp(timestamp)
        brightness = get_brightness(date_for_value)
        # Create a Point object with the calculated timestamp
        data_point = (Point(measurement)
                      .tag("lampId", "2")
                      .field("brightness", brightness)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)

        brightness = get_brightness(date_for_value)
        data_point = (Point(measurement)
                      .tag("lampId", "14")
                      .field("brightness", brightness)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)

    pass


def fill_real_estate_distribution_energy():
    measurement = "Real Estate Distribution Energy"
    current_time = datetime.utcnow()

    for i in range(1, 3 * 30 * 24 * 6 + 1):  # 3 months * 30 days/month * 24 hours/day * 60 minutes/hour
        # Calculate timestamp for each minute in the past
        timestamp = int((current_time - timedelta(minutes=i * 10)).timestamp())

        value = 0.5 + random.uniform(-0.1, 0.1)
        data_point = (Point(measurement)
                      .tag("id", "9")
                      .tag("message", "Energy given to electro distribution.")
                      .tag("realEstateId", "1")
                      .tag("cityId", "1")
                      .field("distributionEnergy", value)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)

        value = 0.5 + random.uniform(-0.1, 0.1)
        data_point = (Point(measurement)
                      .tag("id", "21")
                      .tag("message", "Energy given to electro distribution.")
                      .tag("realEstateId", "2")
                      .tag("cityId", "2")
                      .field("distributionEnergy", value)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)


def fill_real_estate_last_consumption():
    measurement = "Real Estate Last Consumption"
    current_time = datetime.utcnow()

    for i in range(1, 3 * 30 * 24 * 60 + 1):  # 3 months * 30 days/month * 24 hours/day * 60 minutes/hour
        # Calculate timestamp for each minute in the past
        timestamp = int((current_time - timedelta(minutes=i)).timestamp())

        value = 0.2 + random.uniform(-0.05, 0.05)
        data_point = (Point(measurement)
                      .tag("homeBatteryId", "9")
                      .tag("realEstateId", "1")
                      .tag("cityId", "1")
                      .field("lastMinuteConsumption", value)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)

        value = 0.2 + random.uniform(-0.05, 0.05)
        data_point = (Point(measurement)
                      .tag("homeBatteryId", "21")
                      .tag("realEstateId", "2")
                      .tag("cityId", "2")
                      .field("lastMinuteConsumption", value)
                      .time(timestamp, 's'))
        # Write the data point to InfluxDB
        WRITE_API.write(bucket=INFLUX_BUCKET, org=INFLUX_ORG, record=data_point)


def calculate_temperature_humidity(current_date):
    month = current_date.month
    hour = current_date.hour
    if month == 1:
        humidity = 79
        if 7 <= hour <= 16:
            temperature = 4.5
        else:
            temperature = -2.6
    elif month == 2:
        humidity = 70
        if 6 <= hour <= 17:
            temperature = 7.4
        else:
            temperature = -1.4
    elif month == 3:
        humidity = 63
        if 5 <= hour <= 18:
            temperature = 12.8
        else:
            temperature = 2.2
    elif month == 4:
        humidity = 60
        if 6 <= hour <= 19:
            temperature = 18.4
        else:
            temperature = 6.9
    elif month == 5:
        humidity = 60
        if 5 <= hour <= 20:
            temperature = 23
        else:
            temperature = 11.5
    elif month == 6:
        humidity = 64
        if 5 <= hour <= 20:
            temperature = 15.2
        else:
            temperature = 26.8
    elif month == 7:
        humidity = 60
        if 5 <= hour <= 20:
            temperature = 29
        else:
            temperature = 16.6
    elif month == 8:
        humidity = 60
        if 5 <= hour <= 19:
            temperature = 29.3
        else:
            temperature = 16.7
    elif month == 9:
        humidity = 65
        if 6 <= hour <= 18:
            temperature = 24
        else:
            temperature = 12.3
    elif month == 10:
        humidity = 70
        if 7 <= hour <= 17:
            temperature = 18.4
        else:
            temperature = 7.5
    elif month == 11:
        humidity = 78
        if 6 <= hour <= 16:
            temperature = 11.9
        else:
            temperature = 3.2
    else:
        humidity = 80
        if 7 <= hour <= 16:
            temperature = 5.5
        else:
            temperature = -1.2

    return temperature, humidity


def get_brightness(current_date):
    current_month = current_date.month
    current_hour = current_date.hour

    if current_month == 1:
        if 7 <= current_hour <= 10 or 14 <= current_hour <= 16:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 2:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 17:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 3:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 18:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 4:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 19:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 5:
        if 5 <= current_hour <= 10 or 14 <= current_hour <= 20:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 6:
        if 5 <= current_hour <= 10 or 14 <= current_hour <= 20:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 7:
        if 5 <= current_hour <= 10 or 14 <= current_hour <= 20:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 8:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 20:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 9:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 19:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 10:
        if 7 <= current_hour <= 10 or 14 <= current_hour <= 18:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    elif current_month == 11:
        if 6 <= current_hour <= 10 or 14 <= current_hour <= 16:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)
    else:
        if 7 <= current_hour <= 10 or 14 <= current_hour <= 16:
            return random.uniform(400, 5000)
        elif 11 <= current_hour <= 13:
            return random.uniform(6000, 10000)
        else:
            return random.uniform(1, 350)


if __name__ == '__main__':
    # fill_humidity()
    # fill_temperature()
    # fill_brightness()

    # device_ids = ["1", "2", "3", "4", "8", "9", "10", "11", "12",
    #               "13", "14", "15", "16", "20", "21", "22", "23", "24"]
    #
    # for device_id in device_ids:
    #     print(f"Started filling for device {device_id}")
    #     fill_device_status(device_id)
    #     time.sleep(5)

    # fill_real_estate_distribution_energy()
    # fill_real_estate_last_consumption()
    pass

